import seg_mator as sm
import numpy as np
import pims
import os


# A short script to trigger the OOM error. 
## change thes to binned image paths. 
model_path = os.path.join('models','unet_yeast_seg_new_v2.hdf5')
data_path = os.path.join('test_data','pretty','preprocessed','img_crop')
mask_path = os.path.join('test_data','pretty','seg_output_crop')
output_path = './'

print(data_path)
print(mask_path)
# load the images.
images = pims.ImageSequence(data_path)

# get the segmentation object
seg_track = sm.SegMator(data_path,mask_path,model_path,output_path, input_size=(1024,1024,1))
#seg_track = sm.SegMator(data_path,mask_path,model_path,output_path, input_size=(512,512,1))

for i,image in enumerate(images):
    original,masks,contours = seg_track.segment_single_frame(images[i])
