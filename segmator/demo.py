from importlib import reload
import seg_mator as sm
reload(sm)
import vis
reload(vis)
import os
import pims
import time
import pandas as pd
import matplotlib.pyplot as plt
import trackpy as tp

model_path = os.path.join('models','unet_yeast_seg_new_v2.hdf5')
#data_path = os.path.join('C:\\Users','lifeware','Desktop','test','test_imgs')
data_path = os.path.join('C:\\Users\\lifeware\\Desktop\\test\\test_imgs\\binned_img_*_BF.tif')
mask_path = os.path.join('C:\\Users\\lifeware\\Desktop\\test\\mask_path')
#data_path = os.path.join('..','..','data\\binned_trim')
#mask_path = os.path.join('..','..','data','mask_path_trim')
visual_path = os.path.join('C:\\Users\\lifeware\\Desktop\\test\\')

print(data_path)
print(mask_path)
# load the images.
images = pims.ImageSequence(data_path)

# get the segmentation object
seg_track = sm.SegMator(data_path,mask_path,model_path,os.path.join('C:\\Users\\lifeware\\Desktop\\test\\'),input_size=(1024,1024,1))

# segment a single frame
# im,masks = seg_track.segment_single_frame(images[0],return_individual_masks=True)
# f,ax = vis.plot_segmented_image(im,masks)
# f.show()

# do some tracking
#traq = seg_track.track_frames()
#
# # make an animation of tracked cells.
#vis.make_animation(images,traq,fname='difficult_tracking.gif')


# Demo the one frame at a time with tracking.
# make an empty data frame
tracking_data = pd.DataFrame()
#for i in range(len(images)):
all_masks = []
tracking_fname = 'C:\\Users\\lifeware\\Desktop\\test\\track_data.h5'
for i in range(3):
    f,ax = plt.subplots()
    start = time.time()
    # segment a new image and track it
    original,masks,contours = seg_track.segment_single_frame(images[i])
    all_masks.append(masks)
    print('Segmentation time: {0}'.format(time.time()-start))
    start = time.time()
    test_fluo_img = os.path.join('C:\\Users\\lifeware\\Desktop\\test\\test_imgs\\binned_img_{}_{}.tif')
    seg_track.track_single_frame_v2(masks,i)
    # load all of the tracking data
    with tp.PandasHDFStore('C:\\Users\\lifeware\\Desktop\\test\\track_data.h5') as s:
        tracking_data = s.dump()
    if i:
        csv_data = pd.read_csv('C:\\Users\\lifeware\\Desktop\\test\\track.csv', index_col=[0])
        data_ = pd.DataFrame(tracking_data.loc[tracking_data.frame == i])
    else:
        data_ = tracking_data
    # add the contours to all the frames
    data_ = seg_track.add_contour_to_tracking_single_frame(data_, masks)
    list_channels = ['BF', 'EVENT-CFP-DMD', 'CFP-DMD']
    for channel in list_channels:
        data_ = seg_track.add_fluolevel_to_tracking_single_frame(data_, test_fluo_img.format(i, channel), channel)
    #colors = vis.get_cell_colors(data_)
    #vis.plot_image_with_masks(original,data_,ax,colors,text_label=True,frame=i)
    # save a proper image (no tracking just yet.)
    image = vis.save_segmented_image(original,contours,data_[data_['frame']==i],frame=i,save_path=visual_path)
    if i:
        csv_data = csv_data.append(data_, sort=True)
        csv_data.to_csv('C:\\Users\\lifeware\\Desktop\\test\\track.csv')
    else:
        data_.to_csv('C:\\Users\\lifeware\\Desktop\\test\\track.csv')

# get the tracking data
#vis.make_animation(images,tracking_data,fname='C:\\Users\\lifeware\\Desktop\\test\\track_online.gif')
