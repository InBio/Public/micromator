# SegMator
Software that uses U-Net to segment cells, and TrackPy to track them. 
The version of U-Net for segmentation is based off of [Mary Dunlop's group at BU](https://gitlab.com/dunloplab/delta), which was originally designed for mother machine data but is also has been used on yeast. 
[Trackpy](http://soft-matter.github.io/trackpy/v0.4.1/) tracking is a python package which does nice single molecule tracking, but works well for other things (like yeast). 
