import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame, Series
import pims
import skimage
from skimage import morphology, util, filters, measure,transform
import matplotlib.patches as mpatches
from matplotlib import animation
import imageio as io
import random
import os
import time
import re

def crop_images():
    '''
    '''
    images_to_skip = [128,148]
    start = 96
    stop = 153
    count = 0
    for i in range(start,stop):
        if i<10:
            fname_image = 'data/sebas_data/preprocessed/img/BF000{0}_clean.png'.format(i)
            #fname_image = 'data/DeLTA_data/yeast/evaluation/preprocessed/img/Position0{0}_Chamber0{1}_Frame00{2}.png'.format(6,1,frame+1)
        elif i<100:
            fname_image = 'data/sebas_data/preprocessed/img/BF00{0}_clean.png'.format(i)
            #fname_image = 'data/DeLTA_data/yeast/evaluation/preprocessed/img/Position0{0}_Chamber0{1}_Frame0{2}.png'.format(6,1,frame+1)
        else:
            fname_image = 'data/sebas_data/preprocessed/img/BF0{0}_clean.png'.format(i)
        if i not in images_to_skip:
            image = io.imread(fname_image)
            A1 = image[:512,:512]
            A2 = image[512:,:512]
            A3 = image[512:,512:]
            A4 = image[:512,512:]
            io.imwrite('data/sebas_data/preprocessed/img_crop_2/img_{0}.png'.format(count),A4)
            count+=1
        # io.imwrite('data/sebas_data/preprocessed/img_crop/img_{0}.png'.format(i*4+1),A2)
        # io.imwrite('data/sebas_data/preprocessed/img_crop/img_{0}.png'.format(i*4+2),A3)
        # io.imwrite('data/sebas_data/preprocessed/img_crop/img_{0}.png'.format(i*4+3),A4)

def crop_images_new():
    '''
    '''
    main = 'data/sebas_data/train'
    paths = [os.path.join(main,'img'),os.path.join(main,'seg'),os.path.join(main,'wei')]
    for path in paths:
        print(os.listdir(path))
        for i,fname in enumerate(os.listdir(path)):
            fname_image = os.path.join(path,fname)
            image = io.imread(fname_image)
            A1 = image[:512,:512]
            A2 = image[512:,:512]
            A3 = image[512:,512:]
            A4 = image[:512,512:]
            io.imwrite(os.path.join(path+'_crop','img_{0}.png'.format(4*i)),A1)
            io.imwrite(os.path.join(path+'_crop','img_{0}.png'.format(4*i+1)),A2)
            io.imwrite(os.path.join(path+'_crop','img_{0}.png'.format(4*i+2)),A3)
            io.imwrite(os.path.join(path+'_crop','img_{0}.png'.format(4*i+3)),A4)

def add_new_cropped_images(source_paths, output_paths):
    ''' crop images and masks for editing and save to output. 
    '''
    # output_contacts = os.listdir(output_paths[0])
    max_im_number = 0
    for i,fname in enumerate(os.listdir(output_paths[0])):
        print(fname)
        im_number = int(fname.split('_')[1].split('.png')[0])
        max_im_number = np.max([im_number,max_im_number])
        print(max_im_number)
    
    max_im_number += 1
    print('Already found images up to {0}'.format(max_im_number))
    for ii, path in enumerate(source_paths):
        print(sorted(os.listdir(path)))
        for i, fname in enumerate(sorted(os.listdir(path))):
            fname_image = os.path.join(path,fname)
            image = io.imread(fname_image)
            A1 = image[:512,:512]
            A2 = image[512:,:512]
            A3 = image[512:,512:]
            A4 = image[:512,512:]
            io.imwrite(os.path.join(output_paths[ii],'img_{0}.png'.format(4*i+max_im_number)),   A1)
            io.imwrite(os.path.join(output_paths[ii],'img_{0}.png'.format(4*i+1+max_im_number)), A2)
            io.imwrite(os.path.join(output_paths[ii],'img_{0}.png'.format(4*i+2+max_im_number)), A3)
            io.imwrite(os.path.join(output_paths[ii],'img_{0}.png'.format(4*i+3+max_im_number)), A4)


