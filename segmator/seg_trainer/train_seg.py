import sys
sys.path.append('../')
from model import unet_seg
from data import trainGenerator_seg
from tensorflow.keras.callbacks import ModelCheckpoint

# Files:
# DeLTA_data = 'C:/DeepLearning/DeLTA_data/'
# training_set = DeLTA_data + 'yeast/training/segmentation_set/train/'
models_path = '../models/'
data_path = 'train/'
training_set = data_path 
model_file_start = models_path+'unet_yeast_seg_new_v2.hdf5'
model_file_finish = models_path + 'unet_yeast_seg_new_v3.hdf5'

# Parameters:
target_size = (512, 512)
#target_size = (1024, 1024)
input_size = target_size + (1,)
#batch_size = 3
batch_size = 1
epochs = 5
#steps_per_epoch = 250
steps_per_epoch = 100

#Data generator:
data_gen_args = dict(
                    rotation = 2,
                    shiftX=.05,
                    shiftY=.05,
                    zoom=.15,
                    horizontal_flip=True,
                    vertical_flip=True,
                    rotations_90d=True,
                    elastic_deformation=dict(
                            sigma=5,
                            points=5
                            )
                    )

myGene = trainGenerator_seg(batch_size,
                           training_set + 'img_crop/',
                           training_set + 'seg_crop/',
                           training_set + 'wei_crop/',
                           augment_params = data_gen_args,
                           target_size = target_size)


# Define model:
model = unet_seg(input_size = input_size)
# Load the weights to make training easier.
model.load_weights(model_file_start)
model.summary()
model_checkpoint = ModelCheckpoint(model_file_finish, monitor='loss',verbose=1, save_best_only=True)

# Train it:
model.fit_generator(myGene,steps_per_epoch=steps_per_epoch,epochs=epochs,callbacks=[model_checkpoint])
