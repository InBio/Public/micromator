import numpy as np
import imageio as io
from skimage import measure,morphology
import os
from scipy.ndimage.morphology import distance_transform_edt
# from preprocessing import weightmap

import matplotlib.pyplot as plt

'''
Load the segmentation images and use them to create a weight map for training
purposes.  adapted from mary dunlop publication.
'''
base_data_dir = os.path.join('data','DeLTA_data','yeast','training','segmentation_set','train')
base_data_dir = os.path.join('train')
img_directory = os.path.join(base_data_dir,'img_crop')
# seg_directory = os.path.join(base_data_dir,'seg_crop_test')
# wei_directory = os.path.join(base_data_dir,'wei_crop_test')
seg_directory = os.path.join(base_data_dir,'seg_crop')
wei_directory = os.path.join(base_data_dir,'wei_crop')


sigma = 1
#w0 = 12
w0 = 52

PNGmultfact = 18


class0 = 0;
class1 = 0;

for n,segfile in enumerate(os.listdir(seg_directory)):
    seg = io.imread(os.path.join(seg_directory,segfile))
    # get the cell part
    seg = seg>125
    class0 += np.sum(np.invert(seg))
    class1 += np.sum(seg)


# give the most represented class a value of 1.
# if class0>class1:
#     weight_c0 = 1
#     weight_c1 = class0/np.float(class1)
# else:
#     weight_c0 = class1/np.float(class0)
#     weight_c1 = 1
weight_c0 = 1
weight_c1 = 1.61
# print(weight_c0)
# print(weight_c1)
# loop through the segment files, compute distances to the segs,
# create a weightmap.
for n,segfile in enumerate(os.listdir(seg_directory)):
    seg = io.imread(os.path.join(seg_directory,segfile))
    #weight_map = weightmap(seg,weight_c0,weight_c1,sigma=2,w0=20,rescale=18,zeroContour=1)
    seg = seg>125

    # compute the distance mask.
    labels,num = measure.label(seg,return_num=1)
    distance_array = np.infty*np.ones(seg.shape+(np.max([num,2]),))
    skeleton_array = np.infty*np.ones(seg.shape+(np.max([num,2]),))
    for i in range(num):
        single_cell = labels==i
        distance_array[:,:,i] = distance_transform_edt(np.logical_not(single_cell))
        # skeleton_array[:,:,i],distance_array[:,:,i] = morphology.medial_axis(single_cell,return_distance=True)
    # sort the distance array.
    distance_array = np.sort(distance_array,axis=2)
    weighted_distance = w0*np.exp((-(distance_array[:,:,2]+distance_array[:,:,1])**2)/(2*sigma**2))
    # get the weighted image.
    weighted_distance = weighted_distance*np.array(np.invert(seg),dtype=np.float32)

    # sum up weights
    weight_map = np.zeros(seg.shape)
    weight_map[seg] = weight_c1
    weight_map[np.invert(seg)] = weight_c0
    weight_map = weight_map + weighted_distance
    # assign 0 weight to contours
    selem = morphology.disk(1)
    weight_map[np.logical_xor(seg,morphology.binary_erosion(seg,selem))] = 0


    # turn into an 8-bit PNG
    weight_map = np.array(weight_map*PNGmultfact,np.uint8)
    print(np.max(weight_map))
    print(np.min(weight_map))
    weight_map = weight_map-np.min(weight_map)
    #io.imwrite(os.path.join(wei_directory,segfile[:-4]+'_python_zf.png'),weight_map)
    io.imwrite(os.path.join(wei_directory,segfile),weight_map)
    # plt.imshow(weighted_distance)
    # plt.colorbar()
