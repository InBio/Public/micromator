import os
import numpy as np
import napari
import pims
import imageio as io
import pandas as pd
from skimage import measure
from skimage.draw import polygon

def make_labeled_masks(images,tracking_data):
    ''' make masks where each mask is colored according to its cell ID
    to verify tracking. 
    '''
    labeled_masks = np.empty(images.shape) 
    for i,image in enumerate(images):
        frame_data = tracking_data[tracking_data['frame']==i]
        labeled_masks[i,:,:] = make_single_image_masks(image,frame_data)
    return labeled_masks

def make_single_image_masks(image,tracking_data):
    '''
    data should be the trackpy data corresponding to a single "frame"
    Plot a single image with masks on top

    image: the microscopy image
    tracking_data: the tracking data (pandas dataframe) for a single frame 
    '''
    labeled_mask = np.zeros(image.shape)
    for i,row in tracking_data.iterrows():
        pid = row['particle']
        rr,cc = polygon(row['contours'][:,0],row['contours'][:,1])
        labeled_mask[rr,cc] = pid
    return labeled_mask 

def run_viewer(images,masks,tracking_data):
    labeled_masks = make_labeled_masks(images,tracking_data)
    with napari.gui_qt():
        # initiate viewer
        viewer = napari.view_image(images,name='brightfield',rgb=False)
        viewer.add_labels(labeled_masks, name='masks')

#if __name__=='__main__':
#    data_path = os.path.join('test_data','preprocessed','img_crop')
#    mask_path = os.path.join('test_data','seg_output_crop')
#    new_mask_path = os.path.join('test_data','seg_output_crop_upd')
#    images = np.array(pims.ImageSequence(data_path)[:10])
#    masks = np.array(pims.ImageSequence(mask_path)[:10])
#    # process masks
#    masks = masks/np.max(masks[0,:,:])
#    test_image = images[0]
#    #masks = preprocess_masks(masks)
#    with napari.gui_qt():
#        # initiate viewer
#        viewer = napari.view_image(images,name='brightfield')
#        viewer.add_labels(masks, name='masks')
#        #tmp_masks = viewer.layers['masks'].data 
#    
#    tmp_masks = viewer.layers['masks'].data
#    # save the new masks
#    for i in range(tmp_masks.shape[0]):
#        io.imwrite(os.path.join(new_mask_path,'new_mask_{0}.png'.format(i)),np.array(255*tmp_masks[i,:,:],dtype=np.uint8))
