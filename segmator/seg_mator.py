import os
import time
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow
import tempfile
import itertools as IT
from PIL import Image
import numpy as np
import pandas as pd
import gc
from tensorflow.keras import backend as K
from scipy.stats import variation
import matplotlib.pyplot as plt
import pims
import trackpy as tp
from skimage import measure
from skimage.draw import polygon
try:
    from segmator.model import unet_seg
    import segmator.data as data
except:
    from model import unet_seg
    import data

#from model import unet_seg
#import data

#from model import unet_seg
#import data
plt.rcParams.update({'figure.max_open_warning': 0})

# supress trackpy printing.
tp.quiet()

class SegMator:
    '''Software that uses U-Net to segment cells, and TrackPy to track them.
    The version of U-Net for segmentation is based off of Mary Dunlop's group at BU, which was originally designed for mother machine data but is also has been used on yeast.
    Trackpy tracking is a python package which does nice single molecule tracking, but works well for other things (like yeast).
    '''
    def __init__(self, image_path, mask_path, model_file, output_path, input_size=(512, 512, 1)):
        '''Initialize
        image_path: path to the images
        mask_path: path to the masks
        model_file: path to the unet model file
        output_path: path where the segmented images and h5 file is stored
        '''
        # load the unet for segmentation
        self.model = unet_seg(input_size=input_size)
        self.model.load_weights(model_file)
        self.image_path = image_path
        self.mask_path = mask_path
        self.output_path = output_path
        self.tracking_h5name = 'tracking_data.h5'
        self.input_size = input_size
        #self.tracking_h5name = self.uniquify(os.path.join(self.output_path, 'track_data.h5'))

    def uniquify(self, path, sep='_'):
        def name_sequence():
            count = IT.count()
            yield ''
            while True:
                yield '{s}{n:d}'.format(s=sep, n=next(count))
        orig = tempfile._name_sequence
        with tempfile._once_lock:
            tempfile._name_sequence = name_sequence()
            path = os.path.normpath(path)
            dirname, basename = os.path.split(path)
            filename, ext = os.path.splitext(basename)
            fd, filename = tempfile.mkstemp(dir=dirname, prefix=filename, suffix=ext)
            tempfile._name_sequence = orig
        return filename

    def segment_single_frame(self, image, target_size=(512, 512), process_size=4096, save_mask=True, filename='mask.png'):
        '''Segment an image. Returns the image and masks, and can save the
        files.
        '''
        # get the input size
        input_size = list(target_size) + [1,]
        # reshape the image for Tensorflow
        image = np.reshape(image, (1,)+image.shape+(1,))
        # predict the gene
        #mask = self.model.predict(image, verbose=0)[:,:,:,0]
        # try using predict on batch to help with OOM memory error.
        mask = self.model.predict_on_batch(image)[:,:,:,0]
        # post process results
        mask = data.postprocess(mask)
        mask = mask[0,:,:]
        # save the mase
        if save_mask:
            fname = os.path.join(self.mask_path, filename)
            data.saveImg(mask, extra_watershed=False, filename=fname)
        contours, positions = self.get_mask_edges(mask*255)
        return image[0,:,:,0], mask, contours

    def segment_batch(self, target_size=(512, 512), process_size=4096, save_mask=True):
        '''
        Segment images in batch, save to mask directory
        '''
        unprocessed = os.listdir(self.image_path)
        while unprocessed:
            # Pop out filenames
            ps = min(process_size, len(unprocessed))
            to_process = unprocessed[0:ps]
            del unprocessed[0:ps]
            # Predict:
            predGene = data.predictGenerator_seg(self.image_path, files_list=to_process, target_size=target_size, scale=4)
            results = self.model.predict_generator(predGene, len(to_process), verbose=1)
            # Post process results:
            results[:,:,:,0] = data.postprocess(results[:,:,:,0])
            # save the mask
            if save_mask:
                data.saveResult_seg(self.mask_path, results, files_list = to_process, extra_watershed=False)

    def preprocess_frame(self, image):
        '''Preprocess the image the same way that cell star does,
        to correct for background, etc.
        '''
        return image

    def track_frames(self, add_contours=True):
        '''Use trackpy to track the cells.

        Uses whatever self.data_dir is to load and track both masks and data
        images.

        Some trackpy details:
        search_range is how many pixels cells can move betweeen frames.
        memory tells you how many frames to look in the past for linking.
        Because our cells currently don't disappear and reappear, it's set to 1.
        '''
        # load the images
        tp.linking.Linker.MAX_SUB_NET_SIZE = 50
        frames = pims.ImageSequence(self.mask_path)
        start = time.time()
        features = self.recognize_cells_for_tracking(frames)
        search_range = 10
        t = tp.link_df(features, search_range, memory=0)
        if add_contours:
            t = self.add_contour_to_tracking(t, frames)
        print('Tracking time: {0}'.format(time.time()-start))
        return t

    def track_single_frame_v2(self, new_image, frame):
        '''new tracking method
        '''
        tp.linking.Linker.MAX_SUB_NET_SIZE = 50
        single_frame_features = self.recognize_cells_single_image(new_image, features=[], num=frame)
        with tp.PandasHDFStore(os.path.join(self.output_path, self.tracking_h5name)) as h5track:
            h5track.put(single_frame_features)
            for linked in tp.link_df_iter(h5track, 10, memory=2):
                h5track.put(linked)

    def get_tracking_data(self,data_fname):
        ''' Get the tracking data loaded from the file.
        '''
        with tp.PandasHDFStore(data_fname) as s:
            return s.dump()

    def recognize_cells_for_tracking(self, frames,plot=False):
        '''Find the cells for tracking. based on trackpy tutorial for
        finding bubbles.
        '''
        features = pd.DataFrame()
        for num, img in enumerate(frames):
            label_image = measure.label(img)
            for region in measure.regionprops(label_image, intensity_image=img):
                # Everywhere, skip small and large areas
                if region.area < 25 or region.area > 2500:
                    continue
                # Only white areas
                if region.mean_intensity < 255:
                    continue
                # On the top, skip small area with a second threshold
                if region.centroid[0] < 260 and region.area < 80:
                    continue
                # Store features which survived to the criterions
                features = features.append([{'y': region.centroid[0],
                                             'x': region.centroid[1],
                                             'frame': num,
                                             },])
        return features

    def recognize_cells_single_image(self,image,features = [], num=0,plot=False):
        '''Find the cells for tracking. based on trackpy tutorial for
        finding bubbles. I am pretty sure this could be made more simple,
        but for the moment it works well.
        '''
        features = pd.DataFrame()
        label_image = measure.label(image)
#        print(label_image)
        for region in measure.regionprops(label_image, intensity_image=image):
           # Everywhere, skip small and large areas
#            if region.area < 25 or region.area > 2500:
#                continue
#            # Only white areas
#            if region.mean_intensity < 255:
#                continue
#            # On the top, skip small area with a second threshold
#            if region.centroid[0] < 260 and region.area < 80:
#                continue
#            minr, minc, maxr, maxc = region.bbox
            # Store features which survived to the criterions
            features = features.append([{'y': region.centroid[0],
                              'x': region.centroid[1],
                              'frame': num,
                              },])
        return features

    def get_mask_edges(self, mask_img):
        '''For a given segmentation image (i.e. a binary image of masks),
        this function gets the edges of the mask (a bunch of x,y coordinates),
        and their center, defined as the mean of x and y coordinates.
        '''
        zs = []
        contours = measure.find_contours(mask_img, np.max(mask_img)/2)
        for n, contour in enumerate(contours):
            zs.append(np.mean(contour, axis=0))
        return contours, np.array(zs)

    def add_contour_to_tracking(self, tracking_data, masks):
        '''Adds the outline of each cell to the current tracking dataframe.
        '''
        max_n_particles = np.max(tracking_data['particle'])
        n_frames = np.max(tracking_data['frame'])+1
        # loop over the frames
        contour_list = []
        for i in range(n_frames):
            data_ = tracking_data[tracking_data['frame'] == i]
            contours, positions = self.get_mask_edges(masks[i])
            xs = data_['x'].tolist()
            ys = data_['y'].tolist()
            particles = data_['particle'].tolist()
            # loop over the tracking data
            for j in range(len(xs)):
                cell_pos = np.array([ys[j],xs[j]])
                ind = np.argmin(np.sqrt(np.sum((cell_pos-positions)**2, axis=1)))
                contour_list.append(contours[ind])
        tracking_data['contours'] = contour_list
        return tracking_data

    def add_contour_to_tracking_single_frame(self, tracking_data, mask, image):
        '''Adds the outline of each cell to the current tracking dataframe.
        '''
        # loop over the frames
        contour_list = []
        area_list = []
        contours, positions = self.get_mask_edges(mask)
        xs = tracking_data['x'].tolist()
        ys = tracking_data['y'].tolist()
        #particles = data_['particle'].tolist()
        # loop over the tracking data
        for j in range(len(xs)):
            cell_pos = np.array([ys[j],xs[j]])
            ind = np.argmin(np.sqrt(np.sum((cell_pos-positions)**2, axis=1)))
            contour_list.append(contours[ind])
            row, col = polygon(contours[ind][:, 0], contours[ind][:, 1])
            list_pixels = image[(row), (col)]
            area = len(list_pixels)
            area_list.append(area)
        tracking_data['contours'] = contour_list
        tracking_data['area'] = area_list
        return tracking_data

    # def add_fluolevel_to_tracking(self, tracking_data, image, channel, micromanager=False):
    #     '''
    #     '''
    #     stat_list = []
    #     n_frames = np.max(tracking_data['frame'])+1
    #     for i in range(n_frames):
    #         if not micromanager:
    #             nimage = image.format(i)
    #         else:
    #             nimage = image
    #         if os.path.isfile(nimage):
    #             nimage = Image.open(nimage)
    #             npimage = np.asarray(nimage)
    #         else:
    #             npimage = np.zeros((self.input_size[0:2]))
    #         data_ = tracking_data[tracking_data['frame'] == i]
    #         for _, cell in enumerate(data_['contours']):
    #             stat_list.append(go_through_pixels(npimage, cell))
    #     name = '{} mean, min, max, median, cvar'.format(channel)
    #     tracking_data[name] = stat_list
    #     return tracking_data

    def add_fluolevel_to_tracking_single_frame(self, tracking_data, nimage, channel):
        ''' add statistics
        '''
        stat_list = []
        channel_mean = []
        channel_min = []
        channel_max = []
        channel_median = []
        channel_var = []
        channel_sum = []
        channel_background = []
        channel_area = []
        if os.path.isfile(nimage):
            nimage = Image.open(nimage)
            npimage = np.asarray(nimage)
        else:
            npimage = np.zeros((self.input_size[0:2]))
        for _, cell in enumerate(tracking_data['contours']):
            mean_, min_, max_, median_, var_, sum_, background_, area_ = go_through_pixels(npimage, cell)
            channel_mean.append(mean_)
            channel_min.append(min_)
            channel_max.append(max_)
            channel_median.append(median_)
            channel_var.append(var_)
            channel_sum.append(sum_)
            channel_background.append(background_)
            channel_area.append(area_)
        names = '{0} mean,{0} min,{0} max,{0} median,{0} var,{0} sum,{0} background,{0} area'.format(channel).split(',')
        stat_list = [channel_mean, channel_min, channel_max, channel_median, channel_var, channel_sum, channel_background, channel_area]
        for i, name in enumerate(names):
            tracking_data[name] = stat_list[i]
        return tracking_data

    def add_fluolevel_per_frame(self, tracking_data, frame, channel, frame_data):
        '''method adds stats per frame
        '''
        names = 'mean,min,max,median,var,sum,background,area'.split(',')
        tracking_data = tracking_data[tracking_data['frame'] == frame]
        stat_list = []
        for col in tracking_data.columns:
            if channel in col:
                stat_list.append(np.mean(tracking_data[col]))
        #frame_data[channel].append(stat_list)
        for i, name in enumerate(names):
            frame_data[channel][name].append(stat_list[i])
        return frame_data

def reset_keras(self):
    ''' reset the tensorflows.
    '''
    sess = K.get_session()
    K.clear_session()
    sess.close()
    sess = K.get_session()
    try:
        del self.model # this is from global space - change this as you need
    except:
        pass
    #print(gc.collect()) # if it's done something you should see a number being outputted
    # use the same config as you used to create the session
    config = tensorflow.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 1
    config.gpu_options.visible_device_list = "0"
    K.set_session(tensorflow.Session(config=config))

def go_through_pixels(image, contour):
    '''function goes through pixels in mask and returns info (mean, median, etc...)
    '''
    row, col = polygon(contour[:, 0], contour[:, 1])

    max_row = np.max(row)
    im_crop = image[:max_row+1,:]
    blank_crop = np.ones(im_crop.shape)
    try:
        list_pixels = image[(row), (col)]
        blank_crop[row,col] = 0
    except:
        print('unable to crop')
        list_pixels = np.zeros(image.shape)
        blank_crop = np.zeros(im_crop.shape)
    mean = np.mean(list_pixels)
    min_ = np.min(list_pixels)
    max_ = np.max(list_pixels)
    median = np.median(list_pixels)
    if mean:
        cvar = np.std(list_pixels)/mean
    else:
        cvar = float('nan')
    sum_ = np.sum(list_pixels)
    background_ = np.mean(blank_crop*im_crop)
    area_ = row.shape[0]
    return mean, min_, max_, median, cvar, sum_, background_, area_
