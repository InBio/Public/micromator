from importlib import reload
import seg_mator as sm
reload(sm)
import vis
reload(vis)
import os
import pims
import time
import pandas as pd
import matplotlib.pyplot as plt
import trackpy as tp
import numpy as np
import interactive_viewers as iv

model_path = os.path.join('models','unet_yeast_seg_new_v2.hdf5')
#data_path = os.path.join('C:\\Users','lifeware','Desktop','test','test_imgs')
#data_path = os.path.join('C:\\Users\\lifeware\\Desktop\\test\\test_imgs')
#mask_path = os.path.join('C:\\Users\\lifeware\\Desktop\\test\\mask_path')
#data_path = os.path.join('..','..','data\\binned_trim')
#mask_path = os.path.join('..','..','data','mask_path_trim')
#visual_path = os.path.join('C:\\Users\\lifeware\\Desktop\\test\\')

data_path = os.path.join('test_data','preprocessed','img_crop')
mask_path = os.path.join('test_data','seg_output_crop')
output_path = './'

print(data_path)
print(mask_path)
# load the images.
images = pims.ImageSequence(data_path)

# get the segmentation object
#seg_track = sm.SegMator(data_path,mask_path,model_path,output_path, input_size=(1024,1024,1))
seg_track = sm.SegMator(data_path,mask_path,model_path,output_path, input_size=(512,512,1))

# segment a single frame
# im,masks = seg_track.segment_single_frame(images[0],return_individual_masks=True)
# f,ax = vis.plot_segmented_image(im,masks)
# f.show()

# do some tracking
#traq = seg_track.track_frames()
#
# # make an animation of tracked cells.
#vis.make_animation(images,traq,fname='difficult_tracking.gif')


# Demo the one frame at a time with tracking.
# make an empty data frame
tracking_data = pd.DataFrame()
#for i in range(len(images)):
all_masks = []
tracking_fname = 'tracking_data.h5'
N = 30
for i in range(N):
    f,ax = plt.subplots()
    start = time.time()
    # segment a new image and track it
    original,masks,contours = seg_track.segment_single_frame(images[i])
    all_masks.append(masks)
    print('Segmentation time: {0}'.format(time.time()-start))
    start = time.time()
    # update tracking data
    seg_track.track_single_frame_v2(masks,i)
    # load all of the tracking data
    with tp.PandasHDFStore('tracking_data.h5') as s:
        tracking_data = s.dump()
    # add the contours to all the frames
    print('len of masks: ',len(all_masks))
    print(tracking_data)
    tracking_data = seg_track.add_contour_to_tracking(tracking_data,all_masks)
    print(tracking_data)
    image = vis.save_segmented_image(original,contours,tracking_data[tracking_data['frame']==i],frame=i,save_path='test_data/pretty')
#tracking_data.to_csv('C:\\Users\\lifeware\\Desktop\\test\\track.csv')

# get the tracking data
#vis.make_animation(images,tracking_data,fname='C:\\Users\\lifeware\\Desktop\\test\\tracking_online.gif')

# Visualize with napari
#images_short = [images[i] for i in range(N)]
#iv.run_tracking_viewer(images,tracking_data)
