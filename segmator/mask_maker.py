import os
import numpy as np
import napari
import pims
import imageio as io
from skimage import measure
from skimage.draw import polygon
from skimage.morphology import binary_dilation, binary_erosion



def preprocess_masks_untracked(masks):
    new_masks = np.zeros(masks.shape)
    # Take binary mask images and process into images for correction
    for i,mask in enumerate(masks): 
        contours = measure.find_contours(mask, np.max(mask)/2)
        for n,contour in enumerate(contours):
            rr,cc = polygon(contours[n][:,0],contours[n][:,1])
            new_masks[i,rr,cc] = n+1 
    return new_masks

def preprocess_masks_tracked(masks,tracking_data):
    new_masks = np.zeros(masks.shape)
    # Take binary mask images and process into images for correction
    for i,mask in enumerate(masks): 
        contours = measure.find_contours(mask, np.max(mask)/2)
        for n,contour in enumerate(contours):
            rr,cc = polygon(contours[n][:,0],contours[n][:,1])
            new_masks[i,rr,cc] = n+1 
    return new_masks

if __name__=='__main__':
    data_path = os.path.join('test_data','preprocessed','img_crop')
    mask_path = os.path.join('test_data','seg_output_crop')
    new_mask_path = os.path.join('test_data','seg_output_crop_upd')
    images = np.array(pims.ImageSequence(data_path)[:1])
    masks = np.array(pims.ImageSequence(mask_path)[:1])
    # process masks
    masks = preprocess_masks_untracked(masks)
    test_image = images[0]
    #masks = preprocess_masks(masks)
    with napari.gui_qt():
        # initiate viewer
        viewer = napari.Viewer()
        image_layer = viewer.add_image(images,name='brightfield')
        new_mask_layer = viewer.add_labels(np.zeros(images.shape),name='new_masks',num_colors=2)
        mask_layer = viewer.add_labels(masks, name='masks')
        fluorescence_layer = image_layer
        selected_masks = []
        #tmp_masks = viewer.layers['masks'].data 
        #lambda layer,event: get_cell_shape(layer,event,new_mask_layer)

        @mask_layer.mouse_drag_callbacks.append
        def get_cell_shape(layer, event):
            cords = np.round(layer.coordinates).astype(int)
            val = layer.get_value()
            if val is None:
                return
            if val != 0:
                data = layer.data
                new_data = new_mask_layer.data
                binary = data == val
                size = np.sum(binary)
                if np.sum(viewer.layers['new_masks'].data[binary]) > 0:
                    print('removing')
                    new_data[binary]=0
                    data[binary] = 0
                    #layer._data_view[binary[0,:,:]]=0
                else:
                    print('adding')
                    new_data[binary]=100
                    data[binary] = 100
                # read from fluorescence layers
                #layer.data = data
                new_mask_layer.data = new_data
                if fluorescence_layer:
                    # Note: this could be optimized to do a bunch of other stuff.
                    fluorescence = np.sum(fluorescence_layer.data[data==val])
                msg = f'clicked at {cords} on blob {val} which is now {size} pixels, and has fluorescence value {fluorescence}, added to new mask'
            else:
                msg = f'clicked at {cords} on background which is ignored'
            layer.status = msg
            print(msg)

    tmp_masks = viewer.layers['new_masks'].data 
    # save the new masks
    for i in range(tmp_masks.shape[0]):
        io.imwrite(os.path.join(new_mask_path,'generated_masks_{0}.png'.format(i)),np.array(255*tmp_masks[i,:,:]/100,dtype=np.uint8))
