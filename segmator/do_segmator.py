import os
import errno
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from copy import deepcopy
import pims
import glob
import segmator.seg_mator as sm
import segmator.vis as vis
import pandas as pd
import trackpy as tp
import interactive_viewers as tv
import time

def makedirs_(path):
    try:
        os.makedirs(path)
    except OSError as err:
        if err.errno != errno.EEXIST:#check custom experiment folder exists
            raise

def downscale(image_folder, dest_folder, image):
    '''downscale the image if needed
    Args:
        image(str): the name of the image
        path(str): the full path to the image
    '''
    image_name = image
    image_path = image_folder
    #open and downscale by 2
    img = Image.open(os.path.join(image_path), 'r')
    width, height = img.size
    rimg = img.resize((int(width/2), int(height/2)), resample=Image.NEAREST)
    #save with new name binned_{}
    rimage_name = 'binned_{}'.format(image_name)
    rimage = os.path.join(dest_folder, rimage_name)
    rimg.save(rimage)
    return rimage

def bin_(image_folder, dest_folder):
    '''bins all the images in the pos folders
    '''
    print('downscaling images from {} ... '.format(image_folder))
    makedirs_(os.path.join(dest_folder))
    for image in os.listdir(image_folder):
        if os.path.isfile(os.path.join(image_folder, image)) and image.endswith('.tif'):
            downscale(os.path.join(image_folder, image), dest_folder, image)


class DoSegmator():
    def __init__(self, acq_software, model, exp, data, mask, output_path, seg_track, pos, threshold):
        self.exp = exp
        self.output_path = output_path
        self.acq_software = acq_software
        self.track_file_name = os.path.join(output_path, 'track.csv')
        self.frame_file_name = os.path.join(output_path, 'frame_stats_{}.csv')
        self.tracking_data = pd.DataFrame()
        self.frame_data = {}
        self.images = pims.ImageSequence(data)
        self.pos = pos
        self.threshold = threshold
        self.seg_track = seg_track
        self.list_channels = []
        self.col_names = 'mean,min,max,median,var,sum,background,area'.split(',')
        self.list_files = glob.glob(os.path.join(self.exp, 'Analysis\\Binned_images\\pos{}\\binned_img_{}_*.tif').format(self.pos, 0))
        for channel in self.list_files:
            real_channel = channel.split('.')[0].split('_')[-1]
            if real_channel.isdigit():
                real_channel = channel.split('.')[0].split('_')[-2]+'_'+channel.split('.')[0].split('_')[-1]
            if real_channel not in self.list_channels:
                self.list_channels.append(real_channel)
        for channel in self.list_channels:
            self.frame_data[channel] = dict((ele, []) for ele in self.col_names)
        print(self.list_channels, self.frame_data)
        self.list_files = []
        makedirs_(output_path)

    def go_through_channels(self, index, data_):
        if self.acq_software == 'micromanager':
            self.list_files = os.listdir(self.exp)
            for channel in self.list_files:
                channel = channel.split('_')[2]
                if channel not in self.list_channels:
                    self.list_channels.append(channel)
                    self.frame_data[channel] = pd.DataFrame(columns=['mean', 'min', 'max', 'median', 'cv'])
        elif self.acq_software == 'micromator':
            self.list_files = glob.glob(os.path.join(self.exp, 'Analysis\\Binned_images\\pos{}\\binned_img_{}_*.tif').format(self.pos, index))
            for channel in self.list_files:
                real_channel = channel.split('.')[0].split('_')[-1]
                if real_channel.isdigit():
                    real_channel = channel.split('.')[0].split('_')[-2]+'_'+channel.split('.')[0].split('_')[-1]
                if real_channel not in self.list_channels:
                    self.list_channels.append(real_channel)
                    self.frame_data[real_channel] = pd.DataFrame(columns=['mean', 'min', 'max', 'median', 'cv'])
        for channel in self.list_channels:
            if self.acq_software == 'micromanager':
                chimage_path = os.path.join(os.path.join(self.exp, 'binned_img_{}_position000_time000000000_z000.tif'.format(channel)))
            elif self.acq_software == 'micromator':
                chimage_path = os.path.join(self.exp, 'Analysis\\Binned_images\\pos{}\\binned_img_{}_{}.tif'.format(self.pos, index, channel))
            print('calculating stats of fluo channel {}'.format(channel))
            data_ = self.seg_track.add_fluolevel_to_tracking_single_frame(data_, chimage_path, channel)
            print('calculating stats of frame channel {}'.format(channel))
            self.frame_data = self.seg_track.add_fluolevel_per_frame(data_, index, channel, self.frame_data)
            self.frame_data[channel].to_csv(self.frame_file_name.format(channel))
        return data_

    def go_through_channels_bis(self, current_frame, data_):
        '''method goes through the additional channels and call functions to calculate stats
        '''
        #self.update_current_channels(pos, current_frame)
        for channel in self.list_channels:
            chimage_path = os.path.join(self.exp, 'Analysis\\Binned_images\\pos{}\\binned_img_{}_{}.tif'.format(self.pos, current_frame, channel))
            start = time.time()
            data_ = self.seg_track.add_fluolevel_to_tracking_single_frame(data_, chimage_path, channel)
            print('fluo', time.time()-start)
            self.frame_data = self.seg_track.add_fluolevel_per_frame(data_, current_frame, channel, self.frame_data)
            print('calculating fluo channel stats {}'.format(channel))
            frame_data = pd.DataFrame(self.frame_data[channel])
            frame_data.to_csv(self.frame_file_name.format(channel))
        return data_

    def online_segmator_sim(self):
        '''does an online segmentation 'simulation'
        '''
        for index, image in enumerate(self.images):
            if index > self.threshold:
                break
            print('pos{}: segmenting imgs frame {}...'.format(self.pos, index))
            img, masks, countour = self.seg_track.segment_single_frame(image, (1024, 1024), filename='mask{}.tif'.format(index))
            print('pos{}: tracking imgs ...'.format(self.pos))
            self.seg_track.track_single_frame_v2(masks, index)
            with tp.PandasHDFStore(os.path.join(self.output_path, self.seg_track.tracking_h5name)) as s:
                self.tracking_data = s.dump()
            if index:
                start = time.time()
                csv_data = pd.read_csv(self.track_file_name, index_col=[0])
                print('csvload', time.time()-start)
                data_ = pd.DataFrame(self.tracking_data.loc[self.tracking_data.frame == index])
            else:
                data_ = self.tracking_data
            data_ = self.seg_track.add_contour_to_tracking_single_frame(data_, masks, image)
            data_ = self.go_through_channels_bis(index, data_)
            image = vis.save_segmented_image(img, countour, frame=index, save_path=self.output_path)
            vis.save_mask(img, countour, frame=index, save_path=self.output_path, tracking_data=data_)
            if index:
                csv_data = csv_data.append(data_, sort=True)
                csv_data.to_csv(self.track_file_name)
            else:
                data_.to_csv(self.track_file_name)

def visualize_data(exp, track_file_name, pos, output_name):
    image_path = os.path.join(exp, 'Analysis\\Binned_images\\pos{}\\'.format(pos))
    tracking_data = pd.read_csv(os.path.join(exp, 'Analysis\\{}\\pos{}\\{}'.format(output_name, pos, track_file_name)))
    tv.run_tracking_viewer(image_path, tracking_data, label_key='BF')

def batch_segmator(model, exp, data, mask, pos):
    '''does batch segmator
    '''
    images = pims.ImageSequence(data)
    output_path = os.path.join(exp, 'Analysis', 'SegMator', 'pos{}'.format(pos))
    seg_track = sm.SegMator(data, mask, model, output_path, input_size=(1024, 1024, 1))
    try:
        os.makedirs(output_path)
    except OSError as err:
        if err.errno != errno.EEXIST:#check custom experiment folder exists
            raise
    seg_track.segment_batch(target_size=(1024, 1024))
    traq = seg_track.track_frames()
    print('writing to csv...')
    traq.to_csv(os.path.join(exp, 'Analysis\\SegMator\\pos{}\\track.csv'.format(pos)))
    print('...done')


if __name__ == '__main__':
    import threading
    #MODIFY the line below
    list_pos = [0,1,2,3,4]
    def do_segmator_Micromator(pos):
        binning = True
        exp_path = os.path.join('\experiment_path')
        output_name = 'SegMator'
        threshold = 200
        model_path = os.path.join('C:\\Users\\lifeware\\workspace\\segmator\\models\\unet_yeast_seg_new_v3.hdf5')
        ######
        print('launching segmator for pos{}'.format(pos))
        acq_software = 'micromator'
        #acq_software = 'micromanager'
        if acq_software == 'micromator':
            pos = pos
            data_path = os.path.join(exp_path, 'Analysis\\Binned_images\\pos{}\\binned_img_*_BF.tif'.format(pos))
            mask_path = os.path.join(exp_path, 'Analysis\\Bitmaps\\pos{}\\'.format(pos))
            binned_path = os.path.join(exp_path, 'Analysis\\Binned_images\\pos{}\\'.format(pos))
            output_path = os.path.join(exp_path, 'Analysis', output_name, 'pos{}'.format(pos))
            if binning:
                bin_(os.path.join(exp_path, 'Acquisitions', 'pos{}'.format(pos)), binned_path)
            seg_track = sm.SegMator(data_path, mask_path, model_path, output_path, input_size=(1024, 1024, 1))
            segm = DoSegmator(acq_software, model_path, exp_path, data_path, mask_path, output_path, seg_track, pos, threshold)
            segm.online_segmator_sim()
        else:
            exp_path = os.path.join('\experiment_path')
            binning = False
            list_dirs = os.listdir(exp_path)
            for file_ in list_dirs:
                posfolder = os.path.join(exp_path, file_)
                if os.path.isdir(posfolder):
                    print('segmenting {}'.format(posfolder))
                    subposfolder = os.path.join(posfolder, 'Default')
                    dest_folder = os.path.join(exp_path, file_, 'binned')
                    if binning:
                        bin_(subposfolder, dest_folder)
                    data_path = os.path.join(dest_folder, 'binned_img_channel000_position000_time000000000_z000.tif')
                    output_path = os.path.join(exp_path, file_, 'SegMator_bis')
                    mask_path = os.path.join(exp_path, file_, 'Masks')
                    makedirs_(mask_path)
                    seg_track = sm.SegMator(data_path, mask_path, model_path, output_path, input_size=(1024, 1024, 1))
                    segm = DoSegmator(acq_software, model_path, dest_folder, data_path, mask_path, output_path, seg_track, pos, threshold)
                    segm.online_segmator_sim()
for pos in list_pos:
    do_segmator_Micromator(pos)

