import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib import animation, cm
from skimage.draw import polygon
from skimage.color import grey2rgb
from skimage.exposure import rescale_intensity
import imageio as io
import random
import os

def plot_image_with_masks(image,tracking_data,ax,colors,text_label=False,frame=0):
    '''
    data should be the trackpy data corresponding to a single "frame"
    Plot a single image with masks on top

    image: the microscopy image
    tracking_data: the tracking data for a single frame
    '''
    ax.imshow(image,cmap='Greys')
    for i,row in tracking_data.iterrows():
        pid = row['particle']
        patch = mpatches.Polygon(np.fliplr(row['contours']),fill=1,alpha=.3,color=colors[int(pid)],animated=False)
        ax.add_patch(patch)
        if text_label:
            ax.text(row['x'],row['y'],'{0}'.format(int(pid)),color=colors[int(pid)],horizontalalignment='center',verticalalignment='center')

        ax.set_title('frame {0}'.format(frame))
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    return ax

def plot_segmented_image(image,masks,ax=None,cmap ='RdYlBu' ):
    '''
    Plot a single image with masks on top

    image: the microscopy image
    masks: the segmented image
    '''
    if not ax:
        f,ax = plt.subplots()
    ax.imshow(image,cmap='Greys')
    cmap = cm.get_cmap(cmap)
    ids = np.linspace(0,1,len(masks))
    colors = [cmap(i) for i in ids]
    random.shuffle(colors)
    for i in range(len(masks)):
        patch = mpatches.Polygon(np.fliplr(masks[i]),fill=1,alpha=.3,color=colors[i],animated=False)
        ax.add_patch(patch)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    return f,ax

def save_segmented_image(image,masks,tracking_data=None,ax=None,cmap ='RdYlBu',save_path='',frame=0,alpha=0.4,text_labels=True):
    ''' similar to plot segmented image, but instead created a proper .png image instead of a
    matplotlib thing.
    '''
    random.seed(1)
    cmap = cm.get_cmap(cmap)
    ids = np.linspace(0,1,len(masks))
    colors = [cmap(i) for i in ids]
    random.shuffle(colors)
    # convert the image to RGB
    image = grey2rgb(image)
    image = rescale_intensity(image,out_range=(0,255))
    blank = np.zeros(image.shape)
    
    if tracking_data is not None:
        for i,row in tracking_data.iterrows():
            pid = row['particle']
            rr, cc = polygon(row['contours'][:,0], row['contours'][:,1])
            blank_tmp = np.zeros(image.shape)
            blank_tmp[rr,cc,:] = 255
            image[rr,cc,:] = np.ubyte((1-alpha)*image[rr,cc,:] + alpha*np.array(colors[pid])[:-1]*blank_tmp[rr,cc,:])
    else:
        for i in range(len(masks)):
            pid = i
            rr,cc = polygon(masks[i][:,0],masks[i][:,1])
            blank_tmp = np.zeros(image.shape)
            blank_tmp[rr,cc,:] = 255
            image[rr,cc,:] = np.ubyte((1-alpha)*image[rr,cc,:] + alpha*np.array(colors[pid])[:-1]*blank_tmp[rr,cc,:])
            
    fname = os.path.join(save_path,'segment_img_{0}.png'.format(frame))
    io.imwrite(fname,image.astype(np.uint8))
    return image

def save_mask(image,masks,tracking_data=None,ax=None,cmap ='RdYlBu',save_path='',frame=0,alpha=0.4,text_labels=True):
    ''' similar to plot segmented image, but instead created a proper .png image instead of a
    matplotlib thing.
    '''
    blank = np.zeros(image.shape)
    for i,row in tracking_data.iterrows():
        pid = row['particle']
        rr, cc = polygon(row['contours'][:,0], row['contours'][:,1])
        blank_tmp = np.zeros(image.shape)
        blank_tmp[rr,cc] = pid
        blank += blank_tmp
    fname = os.path.join(save_path,'track_pattern_{0}.tif'.format(frame))
    io.imwrite(fname,blank.astype(np.uint16))

def get_cell_colors(tracking_data,cmap_name='RdYlBu'):
    '''
    associate each cell with a color
    colors[particle_id] returns the color for that particle.
    '''
    random.seed(1)
    cmap = cm.get_cmap(cmap_name)
    ncells = np.max(tracking_data['particle'])
    # generate all the colors.
    ids = np.linspace(0,1,int(ncells)+1)
    colors = [cmap(i) for i in ids]
    random.shuffle(colors)
    return colors

def plot_frame(frame,images,tracking_data,ax,colors,text_label=False):
    '''
    make a movie.
    frame: frame of the movie
    tracking_data: positions and labels of the tracked cells, must also have the
    mask edges for each object.
    masks: mask images from segmentation
    '''
    ax.clear()
    data = tracking_data[tracking_data['frame'] == frame]
    image = images[frame]
    plot_image_with_masks(image,data,ax,colors,text_label,frame=frame)
    return ax

def make_animation(images,tracking_data,frames_per_second=2,fname='segmentation_animation.gif',save=True,text_label=False):
    '''
    Make the animation
    '''
    f,ax = plt.subplots(figsize=(12,8))
    colors = get_cell_colors(tracking_data,cmap_name='RdYlBu')
    aniwrap = lambda i: plot_frame(i,images,tracking_data,ax,colors,text_label=text_label)
    anim = animation.FuncAnimation(f,aniwrap,frames=len(images),interval=1000*(1/frames_per_second))
    if save:
        anim.save(fname,writer='imagemagick',fps=frames_per_second)
