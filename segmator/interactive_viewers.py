import os
import numpy as np
import napari
import pims
import imageio as io
import pandas as pd
import glob
from skimage import measure
from skimage.exposure import rescale_intensity
from skimage.draw import polygon

def make_labeled_masks(images,tracking_data):
    ''' make masks where each mask is colored according to its cell ID
    to verify tracking.
    '''
    labeled_masks = np.empty(images.shape)
    for i,image in enumerate(images):
        frame_data = tracking_data[tracking_data['frame']==i]
        labeled_masks[i,:,:] = make_single_image_masks(image, frame_data)
    return labeled_masks

def make_single_image_masks(image,tracking_data):
    '''
    data should be the trackpy data corresponding to a single "frame"
    Plot a single image with masks on top

    image: the microscopy image
    tracking_data: the tracking data (pandas dataframe) for a single frame
    '''
    labeled_mask = np.zeros(image.shape)
    for i,row in tracking_data.iterrows():
        pid = row['particle']
        contours = np.fromstring(row['contours'].replace('\n','').replace('[','').replace(']','').replace('  ',' '),sep = ' ')
        contours = contours.reshape(int(len(contours)/2),2)
        rr,cc = polygon(contours[:,0],contours[:,1])
        labeled_mask[rr,cc] = pid+1
    return labeled_mask

def run_tracking_viewer(image_path, tracking_data, label_key='BF'):
    '''Run the napari based viewer with the tracked cells. Cells will be given random colors but their
    should stay the same throughout.

    images should be a n x N x N array, where n is the number of frames.
    tracking_data is the pandas object which must include the contours column corresponding to each type of file.
    '''
    all_channel_dict = load_multichannel_images(image_path)
    labeled_masks = make_labeled_masks(np.asarray(all_channel_dict[label_key]),tracking_data)
    with napari.gui_qt():
        # initiate viewer
        viewer = napari.view_labels(labeled_masks, name='masks')
        for channel,images in all_channel_dict.items():
            print(channel)
            viewer.add_image(np.asarray(images),name=channel,rgb=False)

def run_mask_adjuster(images,masks,new_mask_path, contrast_boost = False):
    ''' A widget to adjust labeled masks for creating new
    training data and updating the Unet.

    writes the new masks to disk.
    '''
    # boost image contrast
    if contrast_boost:
        images = boost_contrast(images)
    # process masks
    masks = masks/np.max(masks[0,:,:])
    test_image = images[0]
    #masks = preprocess_masks(masks)
    with napari.gui_qt():
        # initiate viewer
        viewer = napari.view_image(images,name='brightfield')
        viewer.add_labels(masks, name='masks')
        #tmp_masks = viewer.layers['masks'].data

    tmp_masks = viewer.layers['masks'].data
    # save the new masks
    for i in range(tmp_masks.shape[0]):
        io.imwrite(os.path.join(new_mask_path,'new_mask_{0}.png'.format(i)),np.array(255*tmp_masks[i,:,:],dtype=np.uint8))

def run_curation_gui(image_path, tracking_data, label_key='BF',savename='tracking_curated.csv'):
    '''Run the napari based viewer with the tracked cells. Cells will be given random colors but their
    should stay the same throughout.

    if a cell is clicked 

    images should be a n x N x N array, where n is the number of frames.
    tracking_data is the pandas object which must include the contours column corresponding to each type of file.
    '''
    all_channel_dict = load_multichannel_images(image_path)
    labeled_masks = make_labeled_masks(np.asarray(all_channel_dict[label_key]),tracking_data)
    marked_cell = {}
    with napari.gui_qt():
        # initiate viewer
        viewer = napari.Viewer()
        for channel,images in all_channel_dict.items():
            print(channel)
            viewer.add_image(np.asarray(images),name=channel,rgb=False)
        labels_layer = viewer.add_labels(labeled_masks, name='masks')
        # add a function that mouse clicks remove the cell that was clicked.   
        @labels_layer.mouse_drag_callbacks.append
        def toggle_cell(layer, event):
            val = layer.get_value()
            if val is None: 
                return
            if np.abs(val) > 0:
                data = layer.data
                if np.abs(val) in list(marked_cell.keys()):
                    data[marked_cell[np.abs(val)]] = np.abs(val)
                    del marked_cell[np.abs(val)]
                else:
                    binary = data == val
                    marked_cell[val] = binary
                    data[binary] = -val 
                layer.data = data
                print(list(marked_cell.keys()))
    cell_list = list(marked_cell.keys())
    print('removing cells with id: {0}'.format(cell_list))
    for cell in cell_list:
        tracking_data = tracking_data[tracking_data['particle']!=int(cell)]            
    tracking_data.reset_index(drop=True)
    tracking_data.to_csv(savename)  
    
    
def boost_contrast(images):
    ''' take input image array and boost the contrast.
    '''
    for i, image in enumerate(images):
        images[i,:,:] = rescale_intensity(image, out_range = (0, 255) )
    return images

def preprocess_masks_untracked(masks):
    new_masks = np.zeros(masks.shape)
    # Take binary mask images and process into images for correction
    for i,mask in enumerate(masks):
        contours = measure.find_contours(mask, np.max(mask)/2)
        for n,contour in enumerate(contours):
            rr,cc = polygon(contours[n][:,0],contours[n][:,1])
            new_masks[i,rr,cc] = n+1
    return new_masks

def preprocess_masks_tracked(masks,tracking_data):
    new_masks = np.zeros(masks.shape)
    # Take binary mask images and process into images for correction
    for i,mask in enumerate(masks):
        contours = measure.find_contours(mask, np.max(mask)/2)
        for n,contour in enumerate(contours):
            rr,cc = polygon(contours[n][:,0],contours[n][:,1])
            new_masks[i,rr,cc] = n+1
    return new_masks

def run_mask_generator(images,masks):
    '''Generate masks by clicking on the cells you want.

    Works on each frame independently, but in the future it would
    potentially be nice to have a mode where you click a tracked cell
    and it is collected across all of the frames.
    '''
    # process masks
    masks = preprocess_masks_untracked(masks)
    test_image = images[0]
    #masks = preprocess_masks(masks)
    with napari.gui_qt():
        # initiate viewer
        viewer = napari.Viewer()
        image_layer = viewer.add_image(images,name='brightfield')
        new_mask_layer = viewer.add_labels(np.zeros(images.shape),name='new_masks',num_colors=2)
        mask_layer = viewer.add_labels(masks, name='masks')
        fluorescence_layer = image_layer
        selected_masks = []
        #tmp_masks = viewer.layers['masks'].data
        #lambda layer,event: get_cell_shape(layer,event,new_mask_layer)

        @mask_layer.mouse_drag_callbacks.append
        def get_cell_shape(layer, event):
            cords = np.round(layer.coordinates).astype(int)
            val = layer.get_value()
            if val is None:
                return
            if val != 0:
                data = layer.data
                new_data = new_mask_layer.data
                binary = data == val
                size = np.sum(binary)
                if np.sum(viewer.layers['new_masks'].data[binary]) > 0:
                    print('removing')
                    new_data[binary]=0
                    data[binary] = 0
                    #layer._data_view[binary[0,:,:]]=0
                else:
                    print('adding')
                    new_data[binary]=100
                    data[binary] = 100
                # read from fluorescence layers
                #layer.data = data
                new_mask_layer.data = new_data
                if fluorescence_layer:
                    # Note: this could be optimized to do a bunch of other stuff.
                    fluorescence = np.sum(fluorescence_layer.data[data==val])
                msg = f'clicked at {cords} on blob {val} which is now {size} pixels, and has fluorescence value {fluorescence}, added to new mask'
            else:
                msg = f'clicked at {cords} on background which is ignored'
            layer.status = msg
            print(msg)

    tmp_masks = viewer.layers['new_masks'].data
    # save the new masks
    for i in range(tmp_masks.shape[0]):
        io.imwrite(os.path.join(new_mask_path,'generated_masks_{0}.png'.format(i)),np.array(255*tmp_masks[i,:,:]/100,dtype=np.uint8))

def load_multichannel_images(path_to_images):
    '''from the imaging folder, get the images.
    '''
    list_files = glob.glob(os.path.join(path_to_images,'*.tif'))
    list_channels = extract_channels_names_from_directory(list_files)
    all_image_dict = sort_images_into_channels(list_channels, path_to_images)
    return all_image_dict

def extract_channels_names_from_directory(list_files, image_filename_base = 'img_{}_{}.tif'):
    ''' based on the filenames, extract the images.
    return a list of strings corresponding to each channel name.
    '''
    list_channels = []
    for channel in list_files:
        channel = channel.split('.')[0].split('_')[-1]
        if channel not in list_channels:
            list_channels.append(channel)
    return list_channels

def sort_images_into_channels(list_channels, path_to_images):
    ''' sort the images into different channels
    '''
    all_image_dict = {}
    for channel in list_channels:
        all_image_dict[channel] = pims.ImageSequence(os.path.join(path_to_images,'*_'+channel+'.tif'))
    return all_image_dict

if __name__ == '__main__':
    # load tracking data
    image_path = 'test_data/napari_issue/raw/pos0/'
    track_file_name = 'test_data/napari_issue/segmator/pos0/track.csv'
    tracking_data = pd.read_csv(track_file_name, index_col=[0])
    run_tracking_viewer(image_path, tracking_data, label_key='BF')
