import os
import numpy as np
import napari
import pims
import imageio as io
from skimage import measure
from skimage.draw import polygon



def preprocess_masks(masks):
    new_masks = np.zeros(masks.shape)
    # Take binary mask images and process into images for correction
    for i,mask in enumerate(masks):
        contours = measure.find_contours(mask, np.max(mask)/2)
        for n,contour in enumerate(contours):
            #print(n)
            #print(len(contour))
            rr,cc = polygon(contours[i][:,0],contours[i][:,1])
            new_masks[i,rr,cc] = n+1
    return new_masks
