# -*- coding: utf-8 -*-
'''
Created on Jul 19, 2017

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import time
import re
import sys
import threading
import subprocess
import datetime
import libonix
import libonix.cellasic_run_data as casic_protocol
from MicroMator.FileManagement import logger

WARNING = threading.Event()
MODULE = 'CellASIC'

def initializer(parameters, folder_manager_obj, globalonix, guithread):
    '''calls all the necessary functions to have microfluidics module work
    Args:
        parameters(dict): a dict of parameters
            step_script(str): path to the callasic script that will be used for the experiment
            expfile(str): path to the dummy ONIX2 format expfile
            onix2location(str): location of the ONIX2.exe
            autoonix2(bool): if True will open ONIX2.exe on it's own
        folder_manager_obj(Filer object): the Filer object instance wher all the relevent paths are
        globalonix(dict): the dict with all the global signals for the GUI
        guithread(Thread object)(optional): the GUI Thread
    '''
    if parameters['autoonix2']: #auto open ONIX2
        logger('Opening ONIX2', module=MODULE)
        subprocess.Popen(parameters['onix2location'])
    step_script = os.path.join(folder_manager_obj.protocols_path, parameters['step_script'])
    if guithread:
        guithread.start()
    else:
        globalonix['INIT'].value = True
    microfluidics_run = Manualrun(parameters['expfile'], folder_manager_obj.microfluidics_log_path, step_script, globalonix)
    cathread = threading.Thread(target=microfluidics_run.run, args=())
    cathread.start()
    logger('Started casic thread', module=MODULE)


class Manualrun(object):
    '''class regroups all the methods to do a manual run using CellASIC ONIX2
    '''
    def __init__(self, experimentfile, logfile, stepfile, globalonix):
        '''
        Args:
            experimentfile(str): PATH and file name of template experiment.
            logfile(str): PATH and file name of logger file (list of statuses).
            stepfile(str): PATH and file name of protocol experiment.
            globalonix(dict): the dict with all the global signals for the GUI
        '''
        self.globalonix = globalonix
        self.exptime = 0
        self.stepfile = stepfile
        self.experimentfile = experimentfile
        self.logfile = logfile
        self.current_step = {}
        self.current_step_number = 0
        self.current_time = 0
        self.cellasic_data_obj = casic_protocol.CellasicData(self.stepfile)
        self.cellasicprotocol = []

    def run(self):
        '''this method is the main loop which makes the experiment run
        '''
        while True:
            try:
                self.check_seal()
                break
            except Exception as err:
                print(err)
                logger("Manifold isn't seal, please make sure it's sealed", print_=False, module=MODULE)
                _ = input("Manifold isn't seal, please make sure it's sealed then and press Enter when ready...")
        while True:
            time.sleep(0.2)
            if self.globalonix['ABORT'].value:
                return
            if self.globalonix['INIT'].value:
                with open(self.logfile, 'a') as logfile:
                    libonix.open_experiment(self.experimentfile)  # needs open experiment to run commands
                    self.cellasic_data_obj.get_protocol_steps() # calls function to get the steps from the cellasic protocol
                    self.cellasicprotocol = self.cellasic_data_obj.cellasicprotocol
                    for step in self.cellasicprotocol: #to log the steps
                        logger(step, module=MODULE)
                    self.exptime = step.steptime
                    while self.current_time < self.exptime and not self.globalonix['ABORT'].value:  # main loop
                        self.cellasic_logger(logfile, self.current_time, self.current_step_number)
                        time.sleep(1)
                        step = self.cellasicprotocol[self.current_step_number]
                        if self.current_time == step.steptime:
                            steptype = step.type
                            previous_step = self.cellasicprotocol[self.current_step_number]
                            self.current_step = self.current_time, self.cellasicprotocol[self.current_step_number]
                            self.step_selector(steptype, step)
                            self.current_step_number += 1
                            if self.current_step_number == len(self.cellasicprotocol):
                                break
                        if self.globalonix['PAUSE'].value:
                            if not self.globalonix['KEEPFLOW'].value: # if keepflow is not pressed will stop flow
                                logger('Stopping flow', module=MODULE)
                                self.step_selector('P', casic_protocol.Step(-1, 'P', 0, ['X:0.01', 'Y:0', '00000000']))
                            while True: #pause loop
                                time.sleep(1)
                                self.cellasic_logger(logfile, self.current_time, self.current_step_number, pause='pause')
                                if self.globalonix['UNPAUSE'].value or self.globalonix['ABORT'].value:  # wait for unpause or Abort button press
                                    self.step_selector(steptype, previous_step)
                                    break
                        self.current_time += 1
                break
            elif self.globalonix['ABORT'].value:
                break
        if self.globalonix['ABORT'].value:
            self.step_selector('P', casic_protocol.Step(-1, 'P', 0, ['X:0', 'Y:0', '00000000']))
        time.sleep(2)
        libonix.close_experiment()
        self.globalonix['RUNEND'].value = True
        logger('CellASIC manual run has ended normally', module=MODULE)

    def step_selector(self, steptype, step):
        '''this method chooses which step activator will be called.
        Args:
            steptype(str): the type of the step.
            step(object): the Step object
        '''
        if steptype.upper() == 'P':
            xvalue = step.xpressure
            yvalue = step.ypressure
            wellsvalue = step.wells
            logger('X:{}  Y:{}  {}'.format(xvalue, yvalue, wellsvalue), module=MODULE)
            if xvalue == '-0.0': #if Value is -0 don't change Pressure X
                xvalue = None
            if yvalue == '-0.0': #if Value is -0 don't change Pressure Y
                yvalue = None
            if wellsvalue == '-0':
                wellsvalue = None
            if '-' in wellsvalue:
                wellsvalue = self.wells_step_constructor(step)
            self.P_step_activator(xvalue, yvalue, wellsvalue)

        elif steptype.upper() == 'G':
            flow = step.flow
            if flow == 'on':
                nozzle = step.nozzle
                flowspeed = step.flowspeed
            else:
                nozzle = 'a'
                flowspeed = 'f'
            self.G_step_activator(flow, nozzle, flowspeed)

        elif steptype.upper() == 'T':
            temp = step.temparature
            self.T_step_activator(temp)
        else:
            raise Exception('Unexpected steptype : steptypes can be G,P or T (case insensitive).')

    def P_step_activator(self, X=None, Y=None, wells=None):
        '''this method calls upon the various libonix commands according to X, Y and wells.
        Args:
            X(float, optional): value to set the X pressure at.
            Y(float, optional): value to set the Y pressure at.
            wells(string, optional): of length 8 char X, Y or 0, each position being a well #.
        '''
        if X:
            libonix.set_pressure_x(X)
        if Y:
            libonix.set_pressure_y(Y)
        if wells:
            libonix.open_well_groups(wells)

    def G_step_activator(self, on, source, flow):
        '''this method calls upon the libonix set_gas command
        Args:
            on(str): turn on or off gas flow.
            source(str): value to set the gas source at (A or B).
            flow(str): value to set the flow at (fast or slow).
        '''
        libonix.set_gas(source, flow, on)

    def T_step_activator(self, T):
        '''this method calls upon the libonix set_temperature command
        Args:
            T(int): value to set the temperature at (in C).
        '''
        libonix.set_temperature(int(T))

    def cellasic_logger(self, logfile, sec, step, pause=None):
        '''this method uses the command status of libonix to log the status of the running experiment at different timestamps.
        Args:
            logile(str): PATH and file name of logger file (list of statuses).
            sec(int): the timestep
            step(object): the step object
            pause(bool): if True will print differently to show it's paused
        '''
        date = datetime.datetime.now()
        fulldate = '{}/{}/{} {}:{}:{} : '.format(date.month, date.day, date.year, date.hour, date.minute, date.second)
        stat = libonix.status()
        if pause:
            stat['RunState'] = '2'  # sets RunState as paused
            stat['TimeStamp'] = str(sec)  # sets Timestamp at current time of the experiment
            stat['Step'] = str(step)  # sets Step at current step of the protocol
            #sys.stdout.write("Run progress: %d/%d - Paused\r" % (sec+1, self.exptime))
            sys.stdout.flush()
        else:
            stat['RunState'] = '3'  # sets RunState as m_run (code 4) (manual run)
            stat['TimeStamp'] = str(sec)  # sets Timestamp at current time of the experiment
            stat['Step'] = str(step)  # sets Step at current step of the protocol
            #sys.stdout.write("Run progress: %d/%d             \r" % (sec+1, self.exptime))
            #sys.stdout.flush()
        status = libonix.log_status(stat)
        logfile.write(fulldate+str(status)+'\n')

    def wells_step_constructor(self, step):
        '''this method will build the well step if there are '-' wells needs to access previous well step
        Args:
            step(object): the Step object
        '''
        previous_wells = self.cellasicprotocol[step.index-1].wells
        new_wells = re.findall('.', step.wells)
        for index, well in enumerate(step.wells):
            if well == '-':
                new_wells[index] = previous_wells[index]
        step.wells = ''.join(new_wells)
        return step.wells

    def check_seal(self):
        '''function that checks if the manifiold is sealed
        '''
        if libonix.status()['Flags0'][-3] == '0':
            raise Exception('Manifold not sealed')
