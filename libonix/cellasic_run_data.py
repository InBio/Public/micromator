# -*- coding: utf-8 -*-
'''
Created on Jul 25, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import warnings
import re
import threading
from MicroMator.FileManagement import logger

WARNING = threading.Event()
MODULE = 'CellASIC'

def calculate_step_time(index, steptime_list):
    '''this function calculates the time at which the respective step takes place
    Args:
        index(int): the index we currently are in the steps
        steptime_list(list): the list of step times
    '''
    if index == 0:
        return steptime_list[index]
    return steptime_list[index-1] + steptime_list[index]

#TODO: change exptime to self calculate
class CellasicData(object):
    '''this is the class that regroups all the steps for a protocol
    '''
    def __init__(self, stepfile):
        '''Args:
            stepfile(str): PATH and filename of file containing protocol.
                           stepfile format must be :
                                1    T    37.5  0/1
                                1    G    0/1    A/B    F(ast)/S(low)
                                10    P    X:1    Y:0    XX000000
                                50    P    X:2    Y:1    XX000Y00
                                150    P    X:0    Y:2    0000XYY0
                                250    P    X:1    Y:3    XX000YYX
        '''
        self.stepfile = stepfile
        self.rx = re.compile(r'[X-x]:[0-9]*\.?[0-9]+$') # regex used for the format checker function
        self.ry = re.compile(r'[Y-y]:[0-9]*\.?[0-9]+$')
        self.rw = re.compile(r'[0,X,x,Y,y,-]{8}')
        self.cellasicprotocol = []

    def get_protocol_steps(self):
        '''this method reads and returns the step file
        '''
        while True:
            try:
                file_ = open(self.stepfile, 'r')
                break
            except FileNotFoundError:
                logger("No CellASIC step script found, please make sure one is in the correct folder and press Enter when ready...", print_=False, module=MODULE)
                _ = input("No CellASIC step script found, please make sure one is in the correct folder and press Enter when ready...")
        with open(self.stepfile, 'r') as file_:
            index = 0
            lines = file_.readlines()
            steplength = [0]
            for line in lines:
                nline = line.split()
                if nline:
                    self.format_checker(nline)  #calls file_checker
                    steplength.append(int(nline[0]))
                    steptype = nline[1].upper()
                    steptime = calculate_step_time(index, steplength)
                    steplength[index] = steptime
                    self.cellasicprotocol.append(Step(index, steptype, steptime, nline[2:]))
                    index += 1
        steptime = calculate_step_time(index, steplength)
        self.cellasicprotocol.append(Step(index, 'P', steptime, ['X:0', 'Y:0', '00000000']))

    def format_checker(self, nline):
        '''this method makes sure the given line is correctly written.
        Args:
            nline(list of str): a list of slipt from the protocol file
        '''
        steptime = nline[0]
        warnings.simplefilter('always')
        if nline[1].upper() not in ('T', 'G', 'P'):
            self.warning_caller(steptime, 'steptype', parameter=nline[1])
        elif nline[1].upper() == 'T':
            if len(nline) != 4:
                self.warning_caller(steptime, 'generic2')
        elif nline[1].upper() == 'G':
            if nline[2] not in ('off', 'on'):
                self.warning_caller(steptime, 'generic1', parameter=nline[2])
            if nline[3].upper() not in ('A', 'B'):
                self.warning_caller(steptime, 'generic1', parameter=nline[3])
            if nline[4].lower() not in ('f', 's', 'slow', 'fast'):
                self.warning_caller(steptime, 'generic1', parameter=nline[4])
            if len(nline) != 5:
                self.warning_caller(steptime, 'generic2')
        elif nline[1].upper() == 'P':
            if len(nline) != 5:
                self.warning_caller(steptime, 'generic2')
            if not self.rx.match(nline[2]):
                self.warning_caller(steptime, 'generic1', parameter=nline[2])
            if not self.ry.match(nline[3]):
                self.warning_caller(steptime, 'generic1', parameter=nline[3])
            if not self.rw.match(nline[4]):
                self.warning_caller(steptime, 'generic1', parameter=nline[4])

    def warning_caller(self, steptime, warningtype, parameter=None):
        '''this method calls the correct warning to display
        Args:
            steptime(int): the time at which the step takes place
            warningtype(str): a str to choose the correct message
            parameter(str): the wrong parameter
        '''
        WARNING.set()
        if warningtype == 'generic1':
            warnings.warn('Step {} parameter "{}" not accepted.'.format(steptime, parameter),
                          stacklevel=6, category=SyntaxWarning)
        elif warningtype == 'generic2':
            warnings.warn('Step {} # of parameters wrong.'.format(steptime),
                          stacklevel=6, category=SyntaxWarning)
        elif warningtype == 'steptype':
            warnings.warn('Step {} type "{}" not recognized.'.format(steptime, parameter),
                          stacklevel=6, category=SyntaxWarning)


class Step(object):
    '''this class is one step in the overall cellasic protocol
    '''
    def __init__(self, index, type_, steptime, parameters):
        '''Args:
             index(int): the index (order) the step takes place, if index = -1 then it's an event
             type_(str): the type of step it is ('P', 'G' or 'T')
             steptime(int): the time at which the step takes place
             parameters(list): the parameters that are tied to the steps
        '''
        self.index = index
        if self.index == -1:
            self.index = 'e'
            cellasicdata = CellasicData(None)
            checkformat = [steptime, type_] + parameters
            cellasicdata.format_checker(checkformat)
        self.type = type_.upper()
        self.steptime = steptime
        if self.type == 'G':
            self.flow = parameters[0]
            self.nozzle = parameters[1]
            self.flowspeed = parameters[2]
        elif self.type == 'P':
            self.xpressure = float(parameters[0][2:])
            self.ypressure = float(parameters[1][2:])
            self.wells = parameters[2]
        elif self.type == 'T':
            self.temparature = float(parameters[0])
            self.on = int(parameters[1])
        else:
            logger('something went wrong when creating the step {} {}'.format(index, parameters), 'error', module=MODULE)

    def __str__(self):
        '''prints the object according to a specified format
        '''
        if self.type == 'G':
            return 'index: {} | steptime:{} | flow: {} | nozzle: {} | flowspeed: {}'.format(self.index, self.steptime, self.flow, self.nozzle, self.flowspeed)
        elif self.type == 'P':
            return 'index: {} | steptime:{} | X:{} | Y:{} | wells: {} '.format(self.index, self.steptime, self.xpressure, self.ypressure, self.wells)
        elif self.type == 'T':
            return 'index: {} | steptime:{} | T°C: {}'.format(self.index, self.steptime, self.temparature)


if __name__ == '__main__':
    step_file = 'D:\\lifeware\\Experiments\\StevenFletcher\\MicroMator\\2018-17-10_shutter_test\\Protocols\\cellasic_protocol.txt'
    C=CellasicData(step_file)
    C.get_protocol_steps()
    for step in C.cellasicprotocol:
        print(step)
    print('last step {} time'.format(step.steptime))
    step = Step(-1, 'P', 10, ['X:1', 'Y:0', '---X----'])
