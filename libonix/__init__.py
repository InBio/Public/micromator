# -*- coding: utf-8 -*-

"""Wrapper around the Web Services API for the CellASIC ONIX2.

@author: Virgile Andreani - Steven Fletcher - INRIA - Institut Pasteur - InBio
"""
import logging
import requests


logging.getLogger("requests").setLevel(logging.WARNING) #sets the log level of requests library to print only if warnings appear
logging.getLogger("urllib3").setLevel(logging.WARNING)

BASE_URL = 'http://localhost:8888/onixserver/'

FLAGS0 = {0: 'System is ready',
          1: 'Manifold XT is connected',
          2: 'Manifold is sealed',
          7: 'A warning condition is true',
          8: 'An error condition is true',
          15: 'Pump error'}

FLAGS1 = {0: 'Gas A is empty or not connected',
          1: 'Gas B is empty or not connected',
          2: 'Gas A is low',
          3: 'Gas B is low',
          4: 'Seal lost',
          11: 'Pressure X leakage detected',
          15: 'Pressure Y leakage detected'}

FLAGS2 = {1: 'Manifold overtemperature',
          4: 'Manifold lower blower timed out',
          5: 'Manifold upper blower timed out',
          10: 'Incorrect gas flow',
          11: 'Manifold unable to reach target temperature',
          13: 'Incompatible manifold connected'}


def _request(command, params=None):
    answer = requests.get(BASE_URL + command, params=params)
    answer.raise_for_status()
    answer_json = answer.json()
    if 'success' not in answer_json:
        return answer_json
    if not answer_json['success']:
        raise Exception('Command failed (server side).')


def _writefile(filename, datalist):
    with open(filename, 'w') as file:
        for item in datalist:
            file.write('%s\n' % item)


def is_experiment_open():
    """Checks if an experiment is open.

    Returns:
        A tuple with the experiment filename and a boolean describing
        if a run is in progress, or None if no experiment is open.
    """
    answer = _request('IsExperimentOpen')
    if not answer["experimentOpen"]:
        return None
    return (answer["experimentFile"], answer["containsRunData"])


def close_experiment(save=True):
    """Closes the current experiment. Defaults to save.

    Args:
        save: Whether or not to save before closing.
    """
    _request('CloseExperiment', {'save': save})


def save_experiment(filename=None):
    """Saves the current experiment.

    Args:
        filename: Filename for save (optional).
    """
    _request('SaveExperiment', {'filename': filename})


def open_experiment(filename):
    """Opens an existing experiment.

    Args:
        filename: Filename of the experiment to open.
    """
    _request('OpenExperiment', {'filename': filename})


def create_experiment(filename, templatename):
    """Creates a new experiment.

    Args:
        filename: Filename of the experiment to create.
        templatename: Template file for the experiment i.e. an already existing
            experiment file
    """
    params = {'filename': filename, 'templatename': templatename}
    _request('CreateExperiment', params)


def start_run():
    """Starts run of the current experiment.
    """
    _request('StartRun')


def pause(keepflow):
    """Pauses the experiment.

    Args:
        keepflow: Whether or not to keep the flow.
    """
    _request('Pause', {'keepflow': keepflow})


def resume():
    """Resumes paused experiment.
    """
    _request('Resume')


def abort():
    """Aborts current run.
    """
    _request('Abort')


def status():
    """Returns status of device.

    Returns:
        a dictionary containing all the raw output of the status command
    """
    return _request('Status')

def log_status(stat=None):
    '''puts the info in a nice string for the log
    '''
    if not stat:
        stat = status()
    status_ = 'TimeStamp : {}    RunState : {}    Step : {}    Gas : {}    Temp.(C): {}    X (kPa) : {}    Y (kPa) : {}    V1 : {}    V2 : {}    V3 : {}    V4 : {}    V5 : {}    V6 : {}   V7 : {}    V8 : {}'.format(stat['TimeStamp'],
    ['idle', 'run', 'pause', 'mrun'][int(stat['RunState'])], '-' if not stat['Step'] else stat['Step'], stat['Gas'], stat['Temperature'], stat['X'],stat['Y'], '-' if not stat['V1'] else stat['V1'],'-' if not stat['V2'] else stat['V2'], '-' if not stat['V3'] else stat['V3'],'-' if not stat['V4'] else stat['V4'],'-' if not stat['V5'] else stat['V5'], '-' if not stat['V6'] else stat['V6'], '-' if not stat['V7'] else stat['V7'], '-' if not stat['V8'] else stat['V8'])
    return status_

def pstatus(stat=None):
    """Prints status of device:

    RunState: pause Timestamp : 0
    Step    : 0     Repetition: 0
    Gas     : off   Temp. (°C): 37.5
    X (kPa) : 123   Y (kPa)   : 142
    V1: X   V2: X   V3: Y   V4: Y
    V5: X   V6: -   V7: -   V8: -

    Flags0:
        System is ready
        Manifold is sealed
    etc...
    """

    if not stat:
        stat = status()
    if stat['RunState']:
        print('RunState: {:6}'.format(
            ['idle', 'run', 'pause', 'mrun'][int(stat['RunState'])]), end='')
        print('TimeStamp : {}'.format(stat['TimeStamp']))

        print('Step    : {:<6}'.format(
            '-' if not stat['Step'] else stat['Step']), end='')
        print('Repetition: {}'.format(
            '-' if not stat['Repetition'] else stat['Repetition']))
    else:
        print('ONIX2 Idle (no experiment running)')

    print('Gas     : {:6}'.format(stat['Gas']), end='')
    print('Temp. (°C): {}'.format(stat['Temperature']))

    print('X (kPa) : {:<6}'.format(stat['X']), end='')
    print('Y (kPa)   : {:<6}'.format(stat['Y']))

    for i in range(8):
        vi = 'V{}'.format(i+1)
        print('{}: {:4}'.format(vi, '-' if not stat[vi] else stat[vi]),
              end=' \n'[i % 4 == 3])

    pflags(stat)


def pflags(stat=None):
    """Prints flags of device.
    """
    if not stat:
        stat = status()

    if stat['Flags0'] != '0'*16:
        print('\nFlags 0:')
        for i, flag in enumerate(stat['Flags0'][::-1]):
            if flag == '1' and i in FLAGS0:
                print('    '+FLAGS0[i])

    if stat['Flags1'] != '11'+'0'*14 and stat['Gas'] != 'Off':
        print('\nFlags 1:')
        for i, flag in enumerate(stat['Flags1'][::-1]):
            if i <= 1 and flag == '0':
                print('    '+FLAGS1[i])
            elif flag == '1' and i in FLAGS1:
                print('    '+FLAGS1[i])

    if stat['Flags2'] != '0'*16:
        print('\nFlags 2:')
        for i, flag in enumerate(stat['Flags2'][::-1]):
            if flag == '1' and i in FLAGS2:
                print('    '+FLAGS2[i])


def run_data(outfile=None):
    """Returns full run of data.

    Returns:
        a list of the status of the experiment run at different timesteps
        (every 10 seconds) if an outfile is given it will save the run_data
        to this file.
    """
    run = _request('RunData')
    if outfile:
        _writefile(outfile, run)
    else:
        return run


def open_well_groups(wells):
    """Opens wells.

    Args:
        wells: A string of 8 characters either being X, Y, or 0.
            The first character is well 1 and the last well 8.
            e.g.: 'X0Y00000' opens well 1 at pressure X and well 3
            at pressure Y, all other wells stay closed.
    """
    if len(wells) != 8 or any(well not in 'XY0-' for well in wells):
        raise ValueError('8 characters in "XY0-" expected.')
    _request('OpenWellGroups', {'wells': wells})


def set_temperature(temperature):
    """Sets temperature.

    Args:
        temperature: Temperature in degree Celsius.
    """
    _request('SetTemperature', {'temperature': temperature})


def set_pressure_x(pressure):
    """Sets X pressure.

    Args:
        pressure: Pressure in kPa.
    """
    _request('SetPressureX', {'pressure': pressure})


def set_pressure_y(pressure):
    """Sets Y pressure.

    Args:
        pressure: Pressure in kPa.
    """
    _request('SetPressureY', {'pressure': pressure})


def set_gas(gas, flow, on='on'):
    """Sets gas flow.

    Args:
        on(optional): On (defalut) / Off.
        gas: A or B (case insensitive).
        flow: F(ast) or s(low) (case insensitive).

    """
    if on.lower() == 'on':
        on = True
    elif on.lower() == 'off':
        on = False
    else:
        raise Exception('incorrect on value : on or off (case insensitive).')

    if gas.lower() == 'a':
        gas = False
    elif gas.lower() == 'b':
        gas = True
    else:
        raise Exception('incorrect gas value : A or B (case insensitive).')

    if flow.lower() in ('fast', 'f'):
        flow = True
    elif flow.lower() in ('slow', 's'):
        flow = False
    else:
        raise Exception('incorrect flow value : f(ast) or s(low) (case insensitive).')

    _request('SetGas', {'on': on, 'b': gas, 'fast': flow})
