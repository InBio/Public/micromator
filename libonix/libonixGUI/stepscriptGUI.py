'''
Created on Dec 7, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''

import os
import tkinter as Tk
import datetime
from MicroMator.FileManagement import logger

MODULE = 'CellASIC-stepscriptGUI'

class Application(Tk.Frame):
    '''this class is the GUI for the libonix manual run class
    '''
    def __init__(self, stepscript, folder_manager_obj_protocol, master):
        Tk.Frame.__init__(self, master)
        self.folder_manager_obj_protocol = folder_manager_obj_protocol
        self.master = master
        self.pack()
        self.createWidgets()
        self.stepfile = stepscript

    def write_script(self):
        '''opens file and writes text
        Args:
            text(str): step text
        '''
        path = os.path.join(self.folder_manager_obj_protocol, self.stepfile)
        file_ = open(path, 'w')
        text = self.textframe.get(1.0, Tk.END)
        file_.write(text)
        logger('Wrote step script to {}'.format(path), module=MODULE)
        file_.close()

    def add_load_step(self):
        '''adds load cells step for cellASIC Y04C-02 plate
        '''
        self.textframe.insert(Tk.END, '5\tP\tX:55\tY:0\t0000000X\n')
        logger('added load step', module=MODULE)

    def add_wash_step(self):
        '''adds wash step for for cellASIC Y04C-02 plate
        '''
        self.textframe.insert(Tk.END, '300\tP\tX:35\tY:0\tX0000000\n')
        logger('added wash step', module=MODULE)

    def reset(self):
        '''resets stepfile contents
        '''
        self.textframe.delete(1.0, Tk.END)
        logger('reset', module=MODULE)

    def formatgasstep(self):
        '''returns formatted and logs value of gas step given from user in GUI
        '''
        gasinfo = self.readgasvalues()
        step = '{}\tG\t{}\t{}\t{}\n'.format(gasinfo['timestep'],
                                            gasinfo['on'],
                                            gasinfo['source'],
                                            gasinfo['flow']
                                            )
        logger(step, module=MODULE)
        self.textframe.insert(Tk.END, step)
        return step

    def formattempstep(self):
        '''returns formatted and logs value of gas step given from user in GUI
        '''
        tempinfo = self.readtempvalues()
        step = '{}\tT\t{}\t{}\n'.format(tempinfo['timestep'],
                                        tempinfo['tempvalue'],
                                        tempinfo['on']
                                        )
        logger(step)
        self.textframe.insert(Tk.END, step)
        return step

    def formatperfstep(self):
        '''returns formatted and logs value of perf step given from user in GUI
        '''
        perfinfo = self.readperfvalues()
        step = '{}\tP\tX:{}\tY:{}\t{}\n'.format(perfinfo['timestep'],
                                                perfinfo['X'],
                                                perfinfo['Y'],
                                                perfinfo['wells']
                                                )
        logger(step, module=MODULE)
        self.textframe.insert(Tk.END, step)
        return step

    def readperfvalues(self):
        '''reads values from GUI and stores in dict
        '''
        valuedict = {}
        day = int(self.timestep[0].get())
        hour = int(self.timestep[1].get())
        min_ = int(self.timestep[2].get())
        sec = int(self.timestep[3].get())
        date = datetime.timedelta(day, sec, 0, 0, min_, hour, 0)

        valuedict['timestep'] = int(date.total_seconds())
        valuedict['X'] = self.perfstep[0].get()
        valuedict['Y'] = self.perfstep[1].get()
        valuedict['wells'] = self.readwells()
        return valuedict

    def readtempvalues(self):
        '''reads values from GUI and stores in dict
        '''
        valuedict = {}
        day = int(self.timestep[0].get())
        hour = int(self.timestep[1].get())
        min_ = int(self.timestep[2].get())
        sec = int(self.timestep[3].get())
        date = datetime.timedelta(day, sec, 0, 0, min_, hour, 0)

        valuedict['timestep'] = int(date.total_seconds())
        valuedict['tempvalue'] = self.tempspinbox.get()
        valuedict['on'] = self.tempscaleonoff.get()
        return valuedict

    def readgasvalues(self):
        '''reads values from GUI and stores in dict
        '''
        valuedict = {}
        day = int(self.timestep[0].get())
        hour = int(self.timestep[1].get())
        min_ = int(self.timestep[2].get())
        sec = int(self.timestep[3].get())
        date = datetime.timedelta(day, sec, 0, 0, min_, hour, 0)

        valuedict['timestep'] = int(date.total_seconds())
        valuedict['on'] = self.gasradioonoff.get()
        if self.gasradioab.get() == 0:
            valuedict['source'] = 'A'
        else:
            valuedict['source'] = 'B'
        if self.gasradiofastslow.get() == 0:
            valuedict['flow'] = 'S'
        else:
            valuedict['flow'] = 'F'
        return valuedict

    def readwells(self):
        '''read the wells and returns formatted well string
        '''
        wells = ''
        for well in range(2, 10):
            if self.perfstep[well].get() == 0:
                wells += '0'
            elif self.perfstep[well].get() == 1:
                wells += 'X'
            elif self.perfstep[well].get() == 2:
                wells += 'Y'
            else:
                wells += '-'
        return wells

    def createWidgets(self):
        '''this method is where all the widgets and buttons are created
        '''
        #Frames
        self.labelframe = Tk.LabelFrame(self, text="cellASIC step scripter module")
        self.labelframe.pack(side="bottom", fill="both", expand="yes")
        self.timeframe = Tk.LabelFrame(master=self.labelframe, text="Time Step")
        self.timeframe.grid(row=0, column=1, sticky=Tk.N)
        self.temperatureframe = Tk.LabelFrame(master=self.labelframe, text="Temperature", height=2)
        self.temperatureframe.grid(row=0, column=2, sticky=Tk.N)
        self.perfusionframe = Tk.LabelFrame(master=self.labelframe, text="Perfusion")
        self.perfusionframe.grid(row=0, column=3, sticky=Tk.N, rowspan=14)
        self.gasframe = Tk.LabelFrame(master=self.labelframe, text="Gas")
        self.gasframe.grid(row=0, column=4, sticky=Tk.N)
        self.stepframe = Tk.LabelFrame(master=self.labelframe, text="Protocol Steps")
        self.stepframe.grid(row=0, column=5, sticky=Tk.N)
        self.textframe = Tk.Text(master=self.stepframe)
        self.textframe.pack()

        #Menu variables
        self.menubar = Tk.Menu(self.master)
        self.loadstepmenu = Tk.Menu(self.master)
        self.steptypemenu = Tk.Menu(self.master)

        self.loadstepmenu.add_command(label="Y04C-02", command=self.add_load_step)
        self.menubar.add_cascade(label="Load step", menu=self.loadstepmenu)
        self.menubar.add_command(label="Wash step", command=self.add_wash_step)
        self.menubar.add_command(label="Write to file", command=self.write_script)
        self.menubar.add_command(label="Reset", command=self.reset)

        #timemodule
        timevalue = [('Day', 365),
                     ('Hour', 24),
                     ('Minute', 60),
                     ('Second', 60)
                     ]

        row = 0
        self.timestep = []
        for title, value in timevalue:
            self.spinbox = Tk.Spinbox(master=self.timeframe, from_=0, to_=value, justify=Tk.RIGHT, width=10, wrap=True)
            self.timestep.append(self.spinbox)
            self.label = Tk.Label(master=self.timeframe, text=title)
            self.label.grid(row=row, column=0)
            self.spinbox.grid(row=row, column=1)
            row += 1

        #perfmodule
        pressures = [('X (kPa)', 100), ('Y (kPa)', 100)]
        self.kpapsi = Tk.Label(master=self.perfusionframe, text='1Psi = 6.89475729 kPa', bg='green', fg='white')
        self.kpapsi.grid(row = 0, column=0, columnspan=2)
        row = 1
        self.perfstep = []
        for title, value in pressures:
            self.pressurebox = Tk.Spinbox(master=self.perfusionframe, from_=0, to_=value, justify=Tk.RIGHT, width=10, wrap=True, increment=0.1)
            self.pressurelabel = Tk.Label(master=self.perfusionframe, text=title)
            self.pressurelabel.grid(row=row, column=0)
            self.pressurebox.grid(row=row, column=1)
            self.perfstep.append(self.pressurebox)
            row += 1
        self.label = Tk.Label(master=self.perfusionframe, text='0        X        Y        -')
        self.label.grid(row=row, column=1)
        row += 1
        for well in range(8):
            self.wellscale = Tk.Scale(master=self.perfusionframe, from_=0, to=3, orient=Tk.HORIZONTAL, length=120, showvalue=0)
            self.wellscale.grid(row=row, column=1)
            self.welllabel = Tk.Label(master=self.perfusionframe, text='Well {}'.format(well))
            self.welllabel.grid(row=row, column=0)
            self.perfstep.append(self.wellscale)
            row += 1
        self.addperfstep = Tk.Button(master=self.perfusionframe, text='Add step', command=self.formatperfstep, foreground='red')
        self.addperfstep.grid(row=15, columnspan=2)

        #gasmodule
        self.gasradioonoff = Tk.Scale(master=self.gasframe, from_=0, to=1, orient=Tk.HORIZONTAL, length=50, showvalue=0)
        self.gasradioonoff.grid(row=0, column=0)
        self.gasradiolabel = Tk.Label(master=self.gasframe, text='ON/OFF')
        self.gasradiolabel.grid(row=0, column=1)
        self.gasradioab = Tk.Scale(master=self.gasframe, from_=0, to=1, orient=Tk.HORIZONTAL, length=50, showvalue=0)
        self.gasradioab.grid(row=1, column=0)
        self.gasradiofastslow = Tk.Scale(master=self.gasframe, from_=0, to=1, orient=Tk.HORIZONTAL, length=50, showvalue=0)
        self.gasradiofastslow.grid(row=2, column=0)
        self.gasradiolabel = Tk.Label(master=self.gasframe, text='A/B')
        self.gasradiolabel.grid(row=1, column=1)
        self.gasradiolabel = Tk.Label(master=self.gasframe, text='SLOW/FAST')
        self.gasradiolabel.grid(row=2, column=1)
        self.addgasfstep = Tk.Button(master=self.gasframe, text='Add step', foreground='red', command=self.formatgasstep)
        self.addgasfstep.grid(row=3, columnspan=2)

        #tempmodule
        self.tempscaleonoff = Tk.Scale(master=self.temperatureframe, from_=0, to=1, orient=Tk.HORIZONTAL, length=50, showvalue=0)
        self.tempscaleonoff.grid(row=0, column=0)
        self.tempradiolabel = Tk.Label(master=self.temperatureframe, text='ON/OFF')
        self.tempradiolabel.grid(row=0, column=1)
        self.tempspinbox = Tk.Spinbox(master=self.temperatureframe, from_=0, to_=50, justify=Tk.RIGHT, width=10, wrap=True, increment=0.1)
        self.tempspinbox.grid(row=1, column=0)
        self.templabel = Tk.Label(master=self.temperatureframe, text='°C')
        self.templabel.grid(row=1, column=1)
        self.addtempfstep = Tk.Button(master=self.temperatureframe, text='Add step', foreground='red', command=self.formattempstep)
        self.addtempfstep.grid(row=2, columnspan=2)


if __name__ == '__main__':
    root = Tk.Tk()
    root.geometry('1300x500') #window size
    root.iconbitmap('C:\\Users\\lifeware\\workspace\\MicroMator\\gui\\m.ico') #icon location (must be .ico)
    stepfile_ = 'cellasic_protocol.txt'
    app = Application(stepfile_, 'D:\\lifeware\\Experiments\\StevenFletcher\\MicroMator\\cellstar_test\\Protocols', master=root)
    root.config(menu=app.menubar)
    app.mainloop()
    root.quit()
