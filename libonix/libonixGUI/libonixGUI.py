'''
Created on Jan 25, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''

import tkinter as Tk
import threading
from MicroMator.FileManagement import logger

MODULE = 'CellASIC-GUI'

class Application(Tk.Frame):
    '''this class is the GUI for the libonix manual run class
    '''
    def __init__(self, globalonix, master=None):
        Tk.Frame.__init__(self, master)
        self.globalonix = globalonix
        self.pack()
        self.createWidgets()

    def combine_funcs(self, *funcs):
        '''this method allows for the combination of 2 functions
        so that tkinter commands can have multiple effects
        '''
        def combined_func(*args, **kwargs):
            for f in funcs:
                f(*args, **kwargs)
        return combined_func

    def abort_event(self):
        '''this method sets the threading event so the current run can stop
        '''
        self.globalonix['ABORT'].value = True
        self.labelframe.pack_forget()
        logger('aborting cellASIC run', message_type='normal', module=MODULE)
        self.destroy()

    def pause_event(self):
        '''this method acts as the toggle button to pause the current run
        '''
        if self.pause.config('text')[-1] == 'Pause':
            self.globalonix['UNPAUSE'].value = False
            self.globalonix['PAUSE'].value = True
            self.pause.config(text='Resume')
            logger('pausing cellASIC run', message_type='normal', module=MODULE)
        else:
            self.pause.config(text='Pause')
            self.globalonix['UNPAUSE'].value = True
            self.globalonix['PAUSE'].value = False
            logger('resuming cellASIC run', message_type='normal', module=MODULE)

    def keepflow_event(self):
        '''the tickable allows the user to control if the flow continues when he pauses
        '''
        if not self.globalonix['KEEPFLOW'].value:
            self.globalonix['KEEPFLOW'].value = True
        else:
            self.globalonix['KEEPFLOW'].value = False
        print("keepflow is", self.globalonix['KEEPFLOW'].value)

    def init_event(self):
        self.globalonix['INIT'].value = True
        self.start.pack_forget()
        logger("Starting cellASIC Run", message_type='normal', module=MODULE)

    def createWidgets(self):
        '''this method is where all the widgets and buttons are created
        '''
        self.labelframe = Tk.LabelFrame(self, text="cellASIC module")
        self.labelframe.pack(side="bottom", fill="both", expand="yes")
        #Start run
        self.start = Tk.Button(self.labelframe)
        self.start["text"] = 'Start'
        self.start["command"] = lambda:self.init_event() #not sure why but lambda is needed
        self.start.pack({"side": "left"})

        #Quit button
        self.quit_ = Tk.Button(self.labelframe)
        self.quit_["text"] = 'Abort'
        self.quit_["fg"] = "red"
        self.quit_["command"] = self.abort_event
        self.quit_.pack({"side": "left"})

        #Pause toggle button
        self.pause = Tk.Button(self.labelframe)
        self.pause["text"] = "Pause",
        self.pause["command"] = self.pause_event
        self.pause.pack({"side": "left"})

        #Keepflow tickable
        self.keepflow = Tk.Checkbutton(self.labelframe)
        self.keepflow["text"] = 'Keepflow'
        self.keepflow['variable'] = self.globalonix['KEEPFLOW'].value
        self.keepflow["command"] = self.keepflow_event
        #self.keepflow({"side": "left"})
        self.keepflow.pack()
