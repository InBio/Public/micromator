# libonix

- **\__init__.py** : contains the python library to communicate with the cellasic ONIX2
- **cellasic\_run_data.py** : contains all the classes and functions that extract and store the protocol for a cellasic run
- **manual_run.py** : the file contains the main class used to call and control all the relevant packages for a cellasic run
