# **MicroMator Project** 

#### In this repository you will find:
- MicroMator, an open and flexible software for reactive microscopy. This main folder notably contains creators for diverse generally-useful events. Information on the software can be found in the README [**here**](../MicroMator/README.md) and a simple tutorial experiment is described [here](../MicroMator/docs/tutorial_experiment.md).
- AnalysisTools, a set of functions we created to help analysing data generated with MicroMator, as well as jupyter notebooks used for analysing experiments presented in the MicroMator paper.
- ExampleExperiments contains various Event creators (see Event module of MicroMator) used for some experiments we did. These event creators use custom events that can be found in inbio_events.
- libonix contains a microfluidic module for MicroMator that is set to control a cellasic ONIX2 microfluidic machine. 
- Segmator contains an image analysis software for segmentation and tracking of yeast. This software can be used independently or with the Analysis module of MicroMator for real-time segmentation and tracking.
- Stoched contains the Model module for MicroMator we used to make model predictive control of yeast fluoresent protein expression in the MicroMator paper.
