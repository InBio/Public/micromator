'''
Created on Jun 30, 2020

@author: Steven Fletcher - Institut Pasteur - INRIA - InBio
'''
from MicroMator.DataManagement.protocol_structure import Channel
import inbio_events as cpe

def create_events(mmc, globaldict):
    '''this is the functions that users will use to define their events
    Args:
        mmc(object): the MMCorePy object
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
    '''
    #parameters
    busy_signal = []
    events_list = []
    channels = []
    channels.append(Channel('EVENT', name="CFP-DMD-EL222", exposure=1000, fluo=True, picture=True, event=True))
    area = .33
    sorter = 'external_sorter'
    sortargs = []
    trigger_ = 'Current_timeleft_over_threshold'
    triggerargs = [0, 10]
    #generating event
    for pos in mmc.positions:
        posi = pos.index
        busy_signal.append(False)
        event = cpe.generic_singlecell_event(mmc, globaldict, [posi], channels, area, sorter, sortargs, trigger_, triggerargs, busy_signal=(busy_signal, len(events_list)))
        events_list.append(event)
    return events_list
