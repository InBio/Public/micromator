'''
Created on Apr 19, 2020

@author: Steven Fletcher - Institut Pasteur - INRIA - InBio

Those are complex event creating functions that we used for our own experiments and setup
'''
import os
import threading
import numpy as np
import time
import csv
from PIL import Image
import MicroMator.Events.manager as manager
import MicroMator.Events.trigger as trigger
import MicroMator.Events.complexe_events.multi_mask.effect as mme
import MicroMator.Events.complexe_events.multi_mask.mask_manager as mm
import MicroMator.ImageAnalysis.SegMator.single_cell as sc
from MicroMator.FileManagement import logger
import inbio_events as ie



MODULE = 'generic_complexe_events'

def generic_singlecell_event(mmc, globaldict, list_pos, list_channels, area, sorter, sortargs, trigger_, triggerargs, subsort=None, subsortargs=None, busy_signal=None):
    '''This is a generic event instance for single cell based events
    Args:
        mmc(object): Instance of mmwrapper.Scripter
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        list_pos(list of ints): list of positions used by the event [0,1]
        list_channels(list of Channel): list of channels used by the event [Channel('EVENT', name="CFP-DMD-EL222-REC", exposure=1000, fluo=True, picture=True, event=True)]
        area(float): value b/w (1-0) ratio of cell kept in mask
        sorter(str): name of sorter function - (random_sorter, all_cells, clostest_sorter, all_fluo, threshold_sorter)
        sortargs(list): list of sorter arguments values
        trigger_(str): name of trigger class
        tiggerargs(list): list of trigger_ arguments values
        busy_signal(tuple or None)(default as None): the list of booleato avoid tiggering when other events are triggering, and the index of the event in the list
    '''
    # globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = False
    frame = globaldict['CURRENT_FRAME'].value
    logger('entering event pos {}'.format(list_pos), module=MODULE, frame=frame, pos=list_pos)
    all_dmd_channel = {}
    for pos in list_pos:
        for channel in list_channels:
            all_dmd_channel[pos] = []
            all_dmd_channel[pos] = list_channels
    exposure_sequence = [list_channels[0].exposure]
    masks_paths, pattern_to_mask = ie.get_pattern_to_mask_npy_dict(mmc, list_pos, all_dmd_channel)
    if globaldict['EVENT'].value and globaldict['INIT'].value:
        threads = []
        for pos in list_pos:
            dataframe_path = os.path.join(mmc.folder_manager_obj.analysis_path, 'SegMator', 'pos{}'.format(pos), 'track.csv')
            dwc = sc.CellSorter(mmc.folder_manager_obj, os.path.join(mmc.folder_manager_obj.analysis_path, 'cell_ids_{}.npy'.format(pos)), dataframe_path, frame, globaldict, pos, subsort, subsortargs)
            if sorter:
                sorter_function = dwc.__getattribute__(sorter)
                sorter_function(*sortargs)
            for index, channel in enumerate(all_dmd_channel[pos]):
                thread = threading.Thread(target=ie.do_pattern_to_mask,
                                          args=(pattern_to_mask[pos], index, dataframe_path, frame, dwc, mmc, masks_paths, pos, mmc, channel, area))
                thread.start()
                threads.append(thread)
        for thread in threads:
            thread.join()
        for pos in list_pos:
            if len(list_channels) > 1:
                masks_paths = ie.mask_merging(masks_paths, mmc, pos, all_dmd_channel, frame)
                for channel in list_channels[1:]:
                    exposure_sequence.append(int(channel.exposure - list_channels[list_channels.index(channel)-1].exposure))
                all_dmd_channel[pos] = [list_channels[-1]]
        ie.wait_for_masks(masks_paths)
    #effect && trigger
    trigger_func = trigger.__getattribute__(trigger_)
    trigger_instance = trigger_func(*triggerargs)
    effect_take_dmd_pics = mme.Load_dmd_mask_pos_dif_channels(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, save=True)
    if len(list_channels) > 1:
        effect_take_dmd_pics = mme.Load_dmd_mask_sequence(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, exposure_sequence, save=False)
    #Event Creation
    event = manager.Event(trigger_instance, effect_take_dmd_pics, globaldict, name='single cell pos{}'.format(list_pos), all_events_busy_signal=busy_signal)
    if globaldict['CURRENT_FRAME'].value == 0:
        logger('event {} loaded'.format(event.name), module=MODULE)
    # if busy_signal:
        # globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = True
    return event

def generic_singlecell_event_nosequence(mmc, globaldict, list_pos, list_channels, area, sorter, sortargs, trigger_, triggerargs, subsort=None, subsortargs=None, busy_signal=None):
    '''This is a generic event instance for single cell based events
    Args:
        mmc(object): Instance of mmwrapper.Scripter
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        list_pos(list of ints): list of positions used by the event [0,1]
        list_channels(list of Channel): list of channels used by the event [Channel('EVENT', name="CFP-DMD-EL222-REC", exposure=1000, fluo=True, picture=True, event=True)]
        area(float): value b/w (1-0) ratio of cell kept in mask
        sorter(str): name of sorter function - (random_sorter, all_cells, clostest_sorter, all_fluo, threshold_sorter)
        sortargs(list): list of sorter arguments values
        trigger_(str): name of trigger class
        tiggerargs(list): list of trigger_ arguments values
        busy_signal(tuple or None)(default as None): the list of booleato avoid tiggering when other events are triggering, and the index of the event in the list
    '''
    # globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = False
    frame = globaldict['CURRENT_FRAME'].value
    logger('entering event pos {}'.format(list_pos), module=MODULE, frame=frame, pos=list_pos)
    all_dmd_channel = {}
    for pos in list_pos:
        all_dmd_channel[pos] = list_channels
    masks_paths, pattern_to_mask = ie.get_pattern_to_mask_npy_dict(mmc, list_pos, all_dmd_channel)
    if globaldict['EVENT'].value and globaldict['INIT'].value:
        threads = []
        for pos in list_pos:
            dataframe_path = os.path.join(mmc.folder_manager_obj.analysis_path, 'SegMator', 'pos{}'.format(pos), 'track.csv')
            dwc = sc.CellSorter(mmc.folder_manager_obj, os.path.join(mmc.folder_manager_obj.analysis_path, 'cell_ids_{}.npy'.format(pos)), dataframe_path, frame, globaldict, pos, subsort, subsortargs)
            if sorter:
                sorter_function = dwc.__getattribute__(sorter)
                sorter_function(*sortargs)
            for index, channel in enumerate(all_dmd_channel[pos]):
                thread = threading.Thread(target=ie.do_pattern_to_mask,
                                          args=(pattern_to_mask[pos], index, dataframe_path, frame, dwc, mmc, masks_paths, pos, mmc, channel, area))
                thread.start()
                threads.append(thread)
        for thread in threads:
            thread.join()
        ie.wait_for_masks(masks_paths)
    #effect && trigger
    trigger_func = trigger.__getattribute__(trigger_)
    trigger_instance = trigger_func(*triggerargs)
    effect_take_dmd_pics = mme.Load_dmd_mask_pos_dif_channels(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, save=True)
    event = manager.Event(trigger_instance, effect_take_dmd_pics, globaldict, name='single cell pos{}'.format(list_pos), all_events_busy_signal=busy_signal)
    if globaldict['CURRENT_FRAME'].value == 0:
        logger('event {} loaded'.format(event.name), module=MODULE)
    return event

def dynamic_pattern_event(mmc, globaldict, list_pos, list_channels, trigger_, triggerargs, busy_signal=None):
    '''This is a generic event instance for pattern based events
    Args:
        mmc(object): Instance of mmwrapper.Scripter
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        list_pos(list of ints): list of positions used by the event [0,1]
        list_channels(list of Channel): list of channels used by the event [Channel('EVENT', name="CFP-DMD-EL222-REC", exposure=1000, fluo=True, picture=True, event=True)]
        trigger_(str): name of trigger class
        tiggerargs(list): list of trigger_ arguments values
        busy_signal(tuple or None)(default as None): the list of booleato avoid tiggering when other events are triggering, and the index of the event in the list
    '''
    # globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = False
    frame = globaldict['CURRENT_FRAME'].value
    logger('entering event pos {}'.format(list_pos), module=MODULE, frame=frame, pos=list_pos)
    all_dmd_channel = {}
    masks_paths = {}
    exposure_sequence = [list_channels[0].exposure]
    for pos in list_pos:
        all_dmd_channel[pos] = []
        all_dmd_channel[pos] = list_channels
    #Mask generation & management code
        if globaldict['EVENT'].value and globaldict['INIT'].value:
            pattern_to_mask = os.path.join(mmc.folder_manager_obj.protocols_path, 'pattern_to_mask_{}.npy'.format(list_channels[0].name))
            print(pattern_to_mask)
            list_patterns = mm.get_patterns(os.path.join(mmc.folder_manager_obj.bitmaps_path), True)
            logger('loaded patterns...', module=MODULE, frame=frame, pos=list_pos)
            list_masks = mm.do_list_mask(mmc.dmd, list_patterns, np.load(pattern_to_mask))
            logger('...transformed to masks', module=MODULE, frame=frame, pos=list_pos)
            masks_paths[pos] = mm.write_mask(mmc, list_masks, 'masks', pos=pos)
            ie.wait_for_masks(masks_paths)
        else:
            path = os.path.join(mmc.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos), 'masks')
            masks = os.listdir(path)
            if masks:
                masks = []
            for mask in masks:
                masks_paths[pos].append(os.path.join(path, mask))
    for pos in list_pos:
        if len(list_channels) > 1:
            masks_paths = ie.mask_merging(masks_paths, mmc, pos, all_dmd_channel, frame)
            for channel in list_channels[1:]:
                exposure_sequence.append(int(channel.exposure - list_channels[list_channels.index(channel)-1].exposure))
            logger('merged masks', module=MODULE, frame=frame, pos=list_pos)
            all_dmd_channel[pos] = [list_channels[-1]]
    #effect && trigger
    trigger_func = trigger.__getattribute__(trigger_)
    trigger_instance = trigger_func(*triggerargs)
    if len(list_channels) > 1:
        effect_take_dmd_pics = mme.Load_dmd_mask_sequence(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, exposure_sequence, save=False)
    else:
        effect_take_dmd_pics = mme.Load_dmd_mask_pos_dif_channels(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, save=True)
    #Event Creation
    event = manager.Event(trigger_instance, effect_take_dmd_pics, globaldict, name='pattern pos{}'.format(list_pos), all_events_busy_signal=busy_signal)
    if globaldict['CURRENT_FRAME'].value == 0:
        logger('event {} loaded'.format(event.name), module=MODULE)
    # if busy_signal:
    #     globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = True
    return event


def static_pattern_event(mmc, globaldict, list_pos, list_channels, trigger_, triggerargs, pos_specific=False, busy_signal=None):
    '''This is an event instance for static pattern based events
    Args:
        mmc(object): Instance of mmwrapper.Scripter
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        list_pos(list of ints): list of positions used by the event [0,1]
        list_channels(list of Channel): list of channels used by the event [Channel('EVENT', name="CFP-DMD-EL222-REC", exposure=1000, fluo=True, picture=True, event=True)]
        trigger_(str): name of trigger class
        tiggerargs(list): list of trigger_ arguments values
        busy_signal(tuple or None)(default as None): the list of booleato avoid tiggering when other events are triggering, and the index of the event in the list
    '''
    # globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = False
    frame = globaldict['CURRENT_FRAME'].value
    logger('entering event pos {}'.format(list_pos), module=MODULE, frame=frame, pos=list_pos)
    all_dmd_channel = {}
    masks_paths = {}
    threads = []
    exposure_sequence = [list_channels[0].exposure]
    for pos in list_pos:
        all_dmd_channel[pos] = []
        all_dmd_channel[pos] = list_channels
    #Mask generation & management code
        if not globaldict['INIT'].value:
            threads.append(threading.Thread(target=ie.do_pattern_to_mask_pattern, args=(masks_paths, pos, pos_specific, mmc, list_channels)))
            threads[-1].start()
            threads[-1].join()
        else:
            pattern_to_mask = os.path.join(mmc.folder_manager_obj.protocols_path, 'pattern_to_mask_{}.npy'.format(list_channels[0].name))
            if not pos_specific:
                list_patterns = mm.get_patterns(os.path.join(mmc.folder_manager_obj.bitmaps_path), True)
            else:
                list_patterns = mm.get_patterns(os.path.join(mmc.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos)), True)
            masks_paths[pos] = mm.get_masks_path(mmc, list_patterns, 'masks', pos=pos)
            logger('loading masks', module=MODULE)
    for pos in list_pos:
        if len(list_channels) > 1:
            masks_paths = ie.mask_merging(masks_paths, mmc, pos, all_dmd_channel, frame)
            for channel in list_channels[1:]:
                exposure_sequence.append(int(channel.exposure - list_channels[list_channels.index(channel)-1].exposure))
            logger('merged masks', module=MODULE)
            all_dmd_channel[pos] = [list_channels[-1]]
    ie.wait_for_masks(masks_paths)
    #effect && trigger
    trigger_func = trigger.__getattribute__(trigger_)
    trigger_instance = trigger_func(*triggerargs)
    if len(list_channels) > 1:
        effect_take_dmd_pics = mme.Load_dmd_mask_sequence(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, exposure_sequence, save=False)
    else:
        effect_take_dmd_pics = mme.Load_dmd_mask_pos_dif_channels(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, save=True)
    #Event Creation
    event = manager.Event(trigger_instance, effect_take_dmd_pics, globaldict, name='pattern pos{}'.format(list_pos), all_events_busy_signal=busy_signal)
    if globaldict['CURRENT_FRAME'].value == 0:
        logger('event {} loaded'.format(event.name), module=MODULE)
    # if busy_signal:
        # globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = True
        # print(globaldict['SINGLE_EVENT_TRIGGER'])
    return event

def static_pattern_event_nopic(mmc, globaldict, list_pos, list_channels, trigger_, triggerargs, pos_specific=False, busy_signal=None):
    '''This is an event instance for static pattern based events
    Args:
        mmc(object): Instance of mmwrapper.Scripter
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        list_pos(list of ints): list of positions used by the event [0,1]
        list_channels(list of Channel): list of channels used by the event [Channel('EVENT', name="CFP-DMD-EL222-REC", exposure=1000, fluo=True, picture=True, event=True)]
        trigger_(str): name of trigger class
        tiggerargs(list): list of trigger_ arguments values
        busy_signal(tuple or None)(default as None): the list of booleato avoid tiggering when other events are triggering, and the index of the event in the list
    '''
    # globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = False
    frame = globaldict['CURRENT_FRAME'].value
    logger('entering event pos {}'.format(list_pos), module=MODULE, frame=frame, pos=list_pos)
    all_dmd_channel = {}
    masks_paths = {}
    threads = []
    exposure_sequence = [list_channels[0].exposure]
    for pos in list_pos:
        all_dmd_channel[pos] = []
        all_dmd_channel[pos] = list_channels
    #Mask generation & management code
        if not globaldict['INIT'].value:
            threads.append(threading.Thread(target=ie.do_pattern_to_mask_pattern, args=(masks_paths, pos, pos_specific, mmc, list_channels)))
            threads[-1].start()
            threads[-1].join()
        else:
            pattern_to_mask = os.path.join(mmc.folder_manager_obj.protocols_path, 'pattern_to_mask_{}.npy'.format(list_channels[0].name))
            if not pos_specific:
                list_patterns = mm.get_patterns(os.path.join(mmc.folder_manager_obj.bitmaps_path), True)
            else:
                list_patterns = mm.get_patterns(os.path.join(mmc.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos)), True)
            masks_paths[pos] = mm.get_masks_path(mmc, list_patterns, 'masks', pos=pos)
            logger('loading masks', module=MODULE)
    for pos in list_pos:
        if len(list_channels) > 1:
            masks_paths = ie.mask_merging(masks_paths, mmc, pos, all_dmd_channel, frame)
            for channel in list_channels[1:]:
                exposure_sequence.append(int(channel.exposure - list_channels[list_channels.index(channel)-1].exposure))
            logger('merged masks', module=MODULE)
            all_dmd_channel[pos] = [list_channels[-1]]
    ie.wait_for_masks(masks_paths)
    #effect && trigger
    trigger_func = trigger.__getattribute__(trigger_)
    trigger_instance = trigger_func(*triggerargs)
    if len(list_channels) > 1:
        effect_take_dmd_pics = mme.Load_dmd_mask_sequence(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, exposure_sequence, save=False)
    else:
        effect_take_dmd_pics = mme.Load_dmd_mask_pos_dif_channels_nopic(mmc, masks_paths, mmc.dmd, all_dmd_channel, list_pos, save=True)
    #Event Creation
    event = manager.Event(trigger_instance, effect_take_dmd_pics, globaldict, name='pattern pos{}'.format(list_pos), all_events_busy_signal=busy_signal)
    if globaldict['CURRENT_FRAME'].value == 0:
        logger('event {} loaded'.format(event.name), module=MODULE)
    # if busy_signal:
        # globaldict['SINGLE_EVENT_TRIGGER'][busy_signal[-1]].value = True
        # print(globaldict['SINGLE_EVENT_TRIGGER'])
    return event


    
def mask_merging(list_masks_paths, mmc, pos, all_dmd_channel, frame):
    '''if more than 1 channel per position, this function will merge masks to avoid
    to much light being shined
    '''
    import MicroMator.Microscope.dmd as dmd
    list_masks = []
    for index, mask in enumerate(list_masks_paths[pos]):
        list_masks.append(dmd.load_mask(list_masks_paths[pos][index]))
    for index, mask in enumerate(list_masks[:-1]):
        list_masks[index] = sum(list_masks[index:])
    for index, channel in enumerate(all_dmd_channel[pos]):
        list_masks_paths[pos][index] = mm.write_mask(mmc, [list_masks[index]], channel.path+'_'+str(channel.exposure)+'ms', pos, frame)[0]
    return list_masks_paths

def wait_for_masks(masks_paths):
    '''custom wait because .join() not working
    '''
    for paths in masks_paths:
        while not any(masks_paths[paths]):
            pass

def enable_event(bool_=False):
    '''small function that retruns bool ainly for visuals
    '''
    return bool_

def read_timesteps(filepath):
    '''this function will read the timesteps tiggers
    Args:
        filepath(str): file and it's path
    '''
    timesteps = []
    with open(filepath, 'r') as file_:
        for line in file_.readlines():
            if line != '\n':
                timesteps.append(int(line))
    return timesteps

def read_timesteps_v2(filepath):
    '''this function will read the timesteps tiggers
    Args:
        filepath(str): file and it's path
    '''
    with open(filepath, 'r') as file_:
        reader = csv.reader(file_)
        list_ = list(reader)
        list_ = list(map(int, list_[0][0].split()))
        return list_

def add_channel_to_protocol(protocol_channels, channels_to_add):
    '''adds channels to the protocol.channel_list object
    '''
    pchannel_names = []
    for channel in protocol_channels:
        pchannel_names.append(channel.name)
    for channel in channels_to_add:
        if channel.name not in pchannel_names:
            protocol_channels.append(channel)
    return protocol_channels

def do_pattern_to_mask(pattern_to_mask_list, index, dataframe_path, frame, dwc, acq_engine, masks_paths, pos, mmc, channel, area=0.25):
    '''transforms the patterns in the list in masks
    '''
    pattern_to_mask = pattern_to_mask_list[index]
    pattern = sc.make_multi_cells_mask(dataframe_path, frame, pos, (1024, 1024), dwc.df_cell_ids[index], area)
    logger('loaded patterns {} - {}ms ...'.format(channel.path, channel.exposure), module=MODULE, frame=frame, pos=pos)
    pattern = resizer(pattern, (2048, 2048))
    mm.write_pattern(mmc, [pattern], channel.path+'_'+str(channel.exposure)+'ms', pos, frame)
    logger('saved patterns {} - {}ms ...'.format(channel.path, channel.exposure), module=MODULE, frame=frame, pos=pos)
    list_masks = mm.do_list_mask(acq_engine.dmd, pattern, np.load(pattern_to_mask))
    masks_paths[pos][index] = mm.write_mask(mmc, list_masks, channel.path+'_'+str(channel.exposure)+'ms', pos, frame)[0]
    logger('...transformed to masks channel {} - {}ms'.format(channel.path, channel.exposure), module=MODULE, frame=frame, pos=pos)

def do_pattern_to_mask_pattern(masks_paths, pos, pos_specific, mmc, list_channels):
    '''transforms the patterns in the list in masks
    '''
    pattern_to_mask = os.path.join(mmc.folder_manager_obj.protocols_path, 'pattern_to_mask_{}.npy'.format(list_channels[0].name))
    if not pos_specific:
        list_patterns = mm.get_patterns(os.path.join(mmc.folder_manager_obj.bitmaps_path), True)
    else:
        list_patterns = mm.get_patterns(os.path.join(mmc.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos)), True)
    logger('loaded patterns...', module=MODULE)
    list_masks = mm.do_list_mask(mmc.dmd, list_patterns, np.load(pattern_to_mask))
    logger('...transformed to masks', module=MODULE)
    masks_paths[pos] = mm.write_mask(mmc, list_masks, 'masks', pos=pos)

def resizer(pattern, size):
    '''resizes the pattern
    '''
    pattern = Image.fromarray(pattern)
    pattern = pattern.resize(size)
    pattern = np.asarray(pattern)
    return pattern

def get_all_positions(mmc):
    '''gets all positions
    '''
    allpos = mmc.positions
    allposindex = []
    for index, _ in enumerate(allpos):
        allposindex.append(index)
    return allposindex

def get_pattern_to_mask_npy(mmc, list_pos, all_dmd_channel):
    '''gets the matrixs need for pattern to mask transform
    '''
    masks_paths = {}
    pattern_to_mask_list = []
    for index in list_pos:
        masks_paths[index] = [None]*len(all_dmd_channel[index])
        pattern_to_mask_list.append([])
        for channel in all_dmd_channel[index]:
            pattern_to_mask_list[index].append(os.path.join(mmc.folder_manager_obj.protocols_path, 'pattern_to_mask_{}.npy'.format(channel.name)))
    return masks_paths, pattern_to_mask_list

def get_pattern_to_mask_npy_dict(mmc, list_pos, all_dmd_channel):
    '''gets the matrixs need for pattern to mask transform
    '''
    masks_paths = {}
    pattern_to_mask = {}
    for pos in list_pos:
        masks_paths[pos] = [None]*len(all_dmd_channel[pos])
        pattern_to_mask[pos] = []
        for channel in all_dmd_channel[pos]:
            pattern_to_mask[pos].append(os.path.join(mmc.folder_manager_obj.protocols_path, 'pattern_to_mask_{}.npy'.format(channel.name)))
    return masks_paths, pattern_to_mask


def sleep_(time_, globaldict):
    '''custom sleep function
    Args:
        time(float)
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
      example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
    '''
    for _ in range(int(time_)):
        time.sleep(1)
        if globaldict['ABORT'].value:
            return