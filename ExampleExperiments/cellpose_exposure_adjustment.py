import os
import numpy as np
import MicroMator.Events.manager as manager
import MicroMator.Events.effect as effect
from MicroMator.DataManagement.protocol_structure import Channel
from skimage import io
from MicroMator.FileManagement import logger

class trigger_true:
    '''triggers
    '''
    def check(self, globaldict):
        return True

class effect_expadjust:
    def __init__(self, mmc, target_fluo, increment, channel_name, position, list_pos):
        '''Args:
            mmc: MMCore object
            value(float): how much we raise the exposure
            max_exposure
        '''
        self.mmc = mmc
        self.target_fluo = target_fluo
        self.increment = increment
        self.channel_name = channel_name
        self.position = position
        self.posi = position.index
        self.max_exposure = 4000
        self.list_pos = list_pos

    def wait_for_masks(self, masks_paths):
        '''custom wait because .join() not working
        '''
        for paths in masks_paths.values():
            while not os.path.exists(paths):
                pass

    def act(self, globaldict):
        exposure_path = os.path.join(self.mmc.folder_manager_obj.analysis_path, f"exposure_{self.posi}.npy")
        if os.path.exists(exposure_path):
            exposure_list = list(np.load(os.path.join(self.mmc.folder_manager_obj.analysis_path, f"exposure_{self.posi}.npy")))
            previous_image_path = os.path.join(self.mmc.folder_manager_obj.acquisitions_path, f"pos{self.posi}", f"img_{int(exposure_list[-1][0])}_EVENT-{self.channel_name}.tif")
            masks_paths = {}
            for p in self.list_pos:
                p = p.index
                masks_paths[p] = os.path.join(self.mmc.folder_manager_obj.analysis_path, "Bitmaps", f"pos{p}", f"mask_{globaldict['CURRENT_FRAME'].value}_BF.tif")
            if os.path.exists(previous_image_path):
                self.wait_for_masks(masks_paths)
                image = np.array(io.imread(previous_image_path), dtype=np.float64)
                mask = np.array(io.imread(masks_paths[self.posi]), dtype=np.float64)
                mean = 0
                try:
                    mean = np.average(image, weights=(mask > 0))
                except: ZeroDivisionError
                new_exposure = exposure_list[-1][1]
                print(f"Event expadjust frame {globaldict['CURRENT_FRAME'].value} position {self.posi} measured mean {mean} old exposure: {new_exposure} target fluo: {self.target_fluo}")
                if (new_exposure + self.increment < self.max_exposure) and (mean < self.target_fluo):
                    new_exposure += self.increment
                if (new_exposure - self.increment > 0) and (mean > self.target_fluo):
                    new_exposure -= self.increment
                print(f"Event expadjust frame {globaldict['CURRENT_FRAME'].value} position {self.posi} measured mean {mean} new exposure: {new_exposure}")
                exposure_list.append((globaldict['CURRENT_FRAME'].value, new_exposure, mean))
                np.save(os.path.join(self.mmc.folder_manager_obj.analysis_path, f"exposure_{self.posi}"), exposure_list, allow_pickle=True)
            else:
                print(f"path {previous_image_path} not found")
        else:
            print(f"Event expadjust frame {globaldict['CURRENT_FRAME'].value} position {self.posi} initializing")
            exposure_list = []
            exposure_list.append((globaldict['CURRENT_FRAME'].value, self.mmc.protocol.channel_list[1].exposure, 0))
            np.save(os.path.join(self.mmc.folder_manager_obj.analysis_path, f"exposure_{self.posi}"), exposure_list, allow_pickle=True)
        logger(f"[event expadjust] pos {self.posi} frame {globaldict['CURRENT_FRAME'].value} expo {exposure_list[-1][1]}", module='EVENT')
        adapted_channel = Channel('EVENT', name=self.channel_name, exposure=exposure_list[-1][1], fluo=True, picture=True, event=True)
        dmd = self.mmc.dmd
        
        self.mmc.mmc.waitForSystem()
        self.mmc.set_position(self.position)
        self.mmc.channel_manager(adapted_channel, self.position.index, globaldict['CURRENT_FRAME'].value, save=True, event=True)
        # dmd.displaymask(exposure_list[-1][1], dmd.allpixel)
        self.mmc.mmc.setConfig(self.mmc.config, 'IDLE')

        # effect.Open_channel_allpos(adapted_channel, self.mmc, [self.position]).act(globaldict)



def create_events(mmc, globaldict):
    events_list = []
    busy_signal = []

    for pos in mmc.positions:
        posi = pos.index
        busy_signal.append(False)
        if posi >= 0:
            mytrigger = trigger_true()
            myeffect = effect_expadjust(mmc, 3000, 50, "RHOD-DMD", pos, mmc.positions)
            event = manager.Event(mytrigger, myeffect, globaldict, name=f'exposure adjustment pos {posi}', all_events_busy_signal=(busy_signal, len(events_list)))
            events_list.append(event)
    return events_list


