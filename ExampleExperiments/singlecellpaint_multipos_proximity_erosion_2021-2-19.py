'''
Created on 27/11/2020

@author: Achile Fraisse - Institut Pasteur - INRIA - InBio
'''

from MicroMator.DataManagement.protocol_structure import Channel
import inbio_events as cpe

def create_events(mmc, globaldict):
    '''this is the functions that users will use to define their events
    Args:
        mmc(object): the MMCorePy object
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
    '''
    #parameters
    busy_signal = []
    events_list = []
    channels = []
    channels.append(Channel('EVENT', name="RHOD-DMD", exposure=1000, fluo=True, picture=True, event=True))
    channels.append(Channel('EVENT', name="CFP-DMD", exposure=750, fluo=True, picture=True, event=True))
    channels.append(Channel('EVENT', name="YFP-DMD", exposure=450, fluo=True, picture=True, event=True))
    area = [.75]
    sorter = 'random_sorter'
    sortargs = [3]
    trigger_ = 'Current_timeleft_over_threshold'
    triggerargs = [0, 35]
    #generating event
    for pos in mmc.positions:
        pos = pos.index
        busy_signal.append(False)
        event = cpe.generic_singlecell_event_nosequence(mmc, globaldict, [pos], channels, area[pos], sorter, sortargs, trigger_, triggerargs, busy_signal=(busy_signal, len(events_list)))
        events_list.append(event)
    return events_list
