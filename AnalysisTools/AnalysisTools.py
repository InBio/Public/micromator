import os
import re
import numpy as np
import matplotlib.pyplot as plt
from ipywidgets import *
import pandas as pd
from PIL import Image
from sklearn.neighbors import KernelDensity

def get_cell_ids(cids_path):
    '''this functions gets the cell id buckets for MicroMator expriements
    Args:
        cids_path(str): path the the .csv (old format) or .npy
    '''
    _, file_extension = os.path.splitext(cids_path)
    if 'csv' in file_extension:
        cell_ids = pd.read_csv(cids_path, index_col=[0])
        list_ids = []
        for row in cell_ids:
            list_ids.append(list(cell_ids[row].dropna()))
    elif 'npy' in file_extension:
        list_ids = np.load(cids_path, allow_pickle=True)
    else:
        print('wrong data format')
    return list_ids

def load_csvs(pos_list, pos_ids_list, exp_path, channel=True, SegMatorName='SegMator'):
    '''
    Args:
        pos_list(list of int): list of all the positions ine exp
        pos_ids_list(list of int): list of positions with single cell sorting
        exp_path(str): path to exp (the main folder)
    '''
    track_csvs = []
    channel_perframe_infos = []
    list_ids_allpos = []
    save_paths = []
    if pos_ids_list:
        for pos in pos_ids_list:
            #cids_path = os.path.join(exp_path, 'Analysis\\cell_ids_{}.npy'.format(pos))
            cids_path = os.path.join(exp_path, 'Analysis','cell_ids_{}.npy'.format(pos))
            list_ids_allpos.append(get_cell_ids(cids_path))
    for pos in pos_list:
        save_path = os.path.join(exp_path, 'Analysis',SegMatorName,'Figures','pos{}'.format(pos))
        save_paths.append(save_path)
        # makedirs_(save_path)
        track_path = os.path.join(exp_path, 'Analysis',SegMatorName,'pos{}'.format(pos),'track.csv')
        track_csvs.append(pd.read_csv(track_path, index_col=[0], low_memory=False))
        if channel:
            channel_perframe_path = os.path.join(exp_path, 'Analysis',SegMatorName,'pos{}'.format(pos),'frame_stats_RHOD-DIRECT.csv')
            channel_perframe_infos.append(pd.read_csv(channel_perframe_path, index_col=[0], low_memory=False))
    return track_csvs, channel_perframe_infos, save_paths, list_ids_allpos

def get_n_cells_per_frame(df):
    '''function calculates number of cells per frame
    Args:
        df(dataframe): the track.csv dataframe
    '''
    num_cells = []
    maxframe = df['frame'].max()
    for i in range(maxframe):
        num_cells.append(len(df.loc[df.frame == i]))
    return num_cells

def generate_storing_obj(pos_list, list_channels, legend_buckets):
    '''generates generic object to put parsed data. data has that strcuture.
    [pos0, pos..., posn], pos being an empty dict with channels as keys
    '''
    list_obj = []
    for pos in pos_list:
        list_obj.append({})
        for channel in list_channels:
            list_obj[pos][channel] = []
            for _ in legend_buckets[pos]:
                list_obj[pos][channel].append([])
    return list_obj

def sort_filter_data(track_csvs, pos_list, list_channels, legend_buckets, list_ids_allpos, yfilter=150, all_cells=False):
    '''will sort & filter the raw data in an obj:
    {pos0:{channel1:[bucket1, ..., bucketn],
           ...,
           channeln:[bucket1, ...,bucketn]},
     ...,
     posn:{channel1:[bucket1, ..., bucketn],
           ...,
           channeln:[bucket1, ...,bucketn]}}
    track_csvs(list): list of dataframes
    pos_list(list): list of pos indexs
    list_channels(list): list of channels ('RHOD-DIRECT'...)
    legend_buckets(list): list of legends per bucket
    list_ids_allpos(list): list of cells per bucket
    yfilter(int): the y coordiantes bellow which cells are used (for DMD use 150)
    '''
    filtered_obj = generate_storing_obj(pos_list, list_channels, legend_buckets)
    for pos in pos_list:
        for channel in list_channels:
            for index, _ in enumerate(legend_buckets[pos]):
                if not all_cells:
                    for cell in list_ids_allpos[pos][index]:
                        cell_info = track_csvs[pos].loc[track_csvs[pos].particle == cell]
                        if not any(cell_info['y'] > yfilter):
                            continue
                        filtered_obj[pos][channel][index].append(cell_info)
                else:
                    for cell in track_csvs[pos].particle.unique():
                        cell_info = track_csvs[pos].loc[track_csvs[pos].particle == cell]
                        if not any(cell_info['y'] > yfilter):
                            continue
                        filtered_obj[pos][channel][index].append(cell_info)
    return filtered_obj

def get_subset_data(pos_list, list_channels, legend_buckets, framecutoff, filtered_obj, tail=True, col='mean'):
    sub_obj = generate_storing_obj(pos_list, list_channels, legend_buckets)
    for index_, pos in enumerate(filtered_obj):
        for channel in pos:
            for index, bucket in enumerate(pos[channel]):
                for cell in bucket:
                    if any(cell['frame'] < framecutoff):
                        if tail:
                            cell_info = cell.tail(1)
                            cell_info = float(cell_info['{} {}'.format(channel, col)])
                            sub_obj[index_][channel][index].append(cell_info)
                        else:
                            cell_info = cell[['{} {}'.format(channel, col), 'frame']]
                            sub_obj[index_][channel][index].append(cell_info[:framecutoff])
    return sub_obj

def get_subset_data_bis(pos_list, list_channels, legend_buckets, framecutoff, filtered_obj, tail=True, channel=False):
    sub_obj = generate_storing_obj(pos_list, list_channels, legend_buckets)
    for index_, pos in enumerate(filtered_obj):
        for channel in pos:
            for index, bucket in enumerate(pos[channel]):
                for cell in bucket:
                    if any(cell['frame'] < framecutoff):
                        if tail:
                            cell_info = cell.tail(1)
                            if not channel:
                                cell_info = float(cell_info['{} mean'.format(channel)])
                            else:
                                cell_info = float(cell_info['{} mean, min, max, median, cvar'.format(channel)])
                        else:
                            if not channel:
                                cell_info = cell[['{} mean'.format(channel), 'frame']]
                            else:
                                cell_info = cell[['{} mean, min, max, median, cvar'.format(channel), 'frame']]
                                #frame = cell_info['frame']
                                #print(float(re.findall("\d+\.\d+", list(cell_info['{} mean, min, max, median, cvar'.format(channel)])[0].split(',')[0])[0]), cell_info['frame'])
                                #cell_info = float(re.findall("\d+\.\d+", list(cell_info['{} mean, min, max, median, cvar'.format(channel)])[0].split(',')[0])[0]), frame
                        sub_obj[index_][channel][index].append(cell_info)
    return sub_obj

def get_subset_data_from_raw(track_csvs, list_ids_allpos, pos_list, list_channels, legend_buckets, col='mean'):
    sub_obj = generate_storing_obj(pos_list, list_channels, legend_buckets)
    for index, raw in enumerate(track_csvs):
        for bucket, _ in enumerate(legend_buckets[index]):
            cells = raw.loc[raw.particle.isin(list_ids_allpos[index][bucket])]
            cells = cells.loc[cells.y > 150]
            for frame in range(150):
                cells_in_frame = cells.loc[cells['frame'] == frame]
                sub_obj[index]['RHOD-DIRECT'][bucket].append(np.mean(cells_in_frame['RHOD-DIRECT {}'.format(col)]))
    return sub_obj

def get_center(pos_list, list_channels, legend_buckets, filtered_obj, frames):
    sub_obj_frame = generate_storing_obj(pos_list, list_channels, legend_buckets)
    for index_, pos in enumerate(filtered_obj):
        for channel in pos:
            for index, bucket in enumerate(pos[channel]):
                for rframe, frame in enumerate(frames):
                    sub_obj_frame[index_][channel][index].append([])
                    for cell in bucket:
                        if any(cell['frame'] < 50):
                            cell_info = cell[['particle', 'x', 'y', 'frame']]
                            #cell_info = cell_info.loc[cell_info.frame.isin(frames)]
                            cell_info = cell_info.loc[cell_info.frame == frame]
                            if cell_info.empty:
                                continue
                            # path = os.path.join('C:\\Users\\Steven\\Documents\\Work\\StevenFletcher\\characterization\\2020-3-3_EL222_multipos_difmaxexp_proximity_sameerosion\\Analysis\\Binned_images\\pos{}\\binned_img_{}_RHOD-DIRECT.tif'.format(index_, frame))
                            # MWUAHAHAHAHHAHAH MUAHHAHAHHAHAH MWAHAHAHAHHA
                            path = os.path.join('/Volumes/Inbioscope/MicroMator_Article/characterization/2020-3-3_EL222_multipos_difmaxexp_proximity_sameerosion/Analysis/Binned_images/pos{}/binned_img_{}_RHOD-DIRECT.tif'.format(index_, frame))
                            img = Image.open(path)
                            arrimg = np.asarray(img)
                            x = float(cell_info.loc[cell_info.frame == frame]['x'])
                            y = float(cell_info.loc[cell_info.frame == frame]['y'])
                            center = arrimg[int(y)][int(x)]
                            sub_obj_frame[index_][channel][index][rframe].append(center)
    return sub_obj_frame

def do_mean_per_frame(individual_cell_trajectories, pos_list, list_channels, legend_buckets, total_frames, col='mean'):
    means_singlecells_perframe = generate_storing_obj(pos_list, list_channels, legend_buckets)
    means_allcells_perframe = generate_storing_obj(pos_list, list_channels, legend_buckets)
    for index, pos in enumerate(individual_cell_trajectories):
        for channel in pos:
            for i, bucket in enumerate(pos[channel]):
                for frame in range(total_frames):
                    mean = count = 0
                    means_singlecells_perframe[index][channel][i].append([])
                    for cell in bucket:
                        cell_info = cell.loc[cell.frame == frame]['{} {}'.format(channel, col)]
                        if cell_info.any():
                            cell_info = int(list(cell_info)[0])
                            count += 1
                            mean += cell_info
                            means_singlecells_perframe[index][channel][i][frame].append(cell_info)
                    mean /= count
                    means_allcells_perframe[index][channel][i].append(mean)
    return means_allcells_perframe, means_singlecells_perframe

def get_means(sobj, maxframe, pos, bucket):
    means = []
    listmeans = []
    for frame in range(maxframe):
        mean_current_frame = []
        for cell in sobj[pos]['RHOD-DIRECT'][bucket]:
            cell_info_frame = cell.loc[cell['frame'] == frame]
            if not cell_info_frame.empty:
                mean_current_frame.append(float(cell_info_frame['RHOD-DIRECT mean']))
        means.append(np.mean(mean_current_frame))
        listmeans.append(mean_current_frame)
    return means, listmeans

def multiline_plot(data, ax, title, colormap, legend, ylab, xlab):
    for index, cell in enumerate(data):
        if not index:
            ax.plot(cell, c=colormap, label=legend)
        else:
            ax.plot(cell, c=colormap)
    ax.legend()
    ax.set_ylabel(ylab)
    ax.set_xlabel(xlab)
    ax.set_title(title)

def multiline_cell_traj_plot(data, col, ax, title, colormap, legend, ylab, xlab, xaxis=None, linewidth=0.5, multiplier=3):
    for index, cell in enumerate(data):
        if not index:
            ax.plot(cell['frame']*multiplier, cell[col], c=colormap, label=legend, linewidth=linewidth)
        else:
            ax.plot(cell['frame']*multiplier, cell[col], c=colormap, linewidth=linewidth)
    ax.legend()
    ax.set_ylabel(ylab)
    ax.set_xlabel(xlab)
    ax.set_title(title)

def multiline_cell_traj_plot_pd(data, ax, title, colormap, ylab, xlab, xaxis=None, multiplier=3):
    xaxis = pd.Series(range(len(data.columns)))*multiplier
    ax.set_xticks(np.arange(0, xaxis[len(xaxis)-1], step=90))
    for cellnum in range(len(data)):
        ax.plot(xaxis, data.iloc[cellnum], c=colormap, linewidth=0.5)
    ax.set_ylabel(ylab)
    ax.set_xlabel(xlab)
    ax.set_title(title)

def simpleline_plot(data, ax, title, colormap, legend, ylab, xlab, xaxis=None, dashes=False, multiplier=3):
    xaxis = pd.Series(range(len(data)))*multiplier
    ax.set_xticks(np.arange(0, xaxis[len(xaxis)-1], step=90))
    if dashes:
        ax.plot(xaxis, data, c=colormap, label=legend, dashes=dashes, linewidth=2)
    else:
        ax.plot(xaxis, data, c=colormap, label=legend, linewidth=2)
    ax.legend()
    ax.set_ylabel(ylab)
    ax.set_xlabel(xlab)
    ax.set_title(title)

def boxplot(data, ax, colors, legend):
    bp = ax.boxplot(data, patch_artist=True)
    for index, box in enumerate(bp['boxes']):
        box.set(color='black', linewidth=1)
        box.set(facecolor=colors[index])
    ax.set_xticklabels(legend)

def histplot(ax, means_singlecells_perframe, colors, bins, legend_buckets, pos_list, start, maxframe, step, count=False):
    col = 0
    c = 0
    for frame in range(start, maxframe, step):
        rframe = frame
        for pos in pos_list:
            ax_ = ax[pos][col]
            for bucket, legend in enumerate(legend_buckets[pos]):
                if count:
                    frame = c
                ax_.hist(means_singlecells_perframe[pos]['RHOD-DIRECT'][bucket][frame], bins, alpha=0.5, color=colors[pos][bucket], label=legend)
                if not frame:
                    ax_.set_ylabel('pos0')
                    ax_.legend()
                ax_.set_xlabel('frame {}'.format(rframe))
        col += 1
        c += 1

def histplot_interactive( pos, nframes, means_singlecells_perframe, colors, bins, legend_buckets,):
    ''' Make an interactive version of the plots.
    '''
    f,ax = plt.subplots()
    frame=0
    all_data = []
    for bucket, legend in enumerate(legend_buckets[pos]):
        ax.hist(means_singlecells_perframe[pos]['RHOD-DIRECT'][bucket][frame], bins, alpha=0.5, color=colors[pos][bucket], label=legend)

    def update(frame):
        ax.clear()
        for bucket, legend in enumerate(legend_buckets[pos]):
            ax.hist(means_singlecells_perframe[pos]['RHOD-DIRECT'][bucket][frame], bins, alpha=0.5, color=colors[pos][bucket], label=legend)
        f.canvas.draw()
    # update_wrap = lambda t: update(t,data1,data2)
    interact(update, frame=widgets.IntSlider(min=0, max=nframes, step=1, value=0))

def parse_contour(cell):
    '''returns parsed contour & coordiantes inside it
    '''
    contour = [float(index) for index in re.findall(r"[-+]?\d*\.\d+|\d+", cell.contours.iloc[0])]
    return contour

def concat_replicates(df1, df2):
    df1['particle'] = df1['particle'].astype(int) + 100000
    newdf = pd.concat([df1, df2], keys=['1', '2'])
    return newdf


def clean_maxframe(df, maxframe):
    '''removes later frames where data quality is often iffy
    '''
    newdf = df.loc[df.frame < maxframe]
    return newdf

def clean_blips(df, framenum=10):
    '''removes cells that are present under a certain amount of frames
    '''
    freq = df.sort_values('particle').groupby('particle').count()
    blipcells = freq.loc[freq.frame > framenum].index
    sorteddf = df[df['particle'].isin(blipcells)]
    return sorteddf

def clean_DMDborder(df, DMDborder=150):
    '''removes cells that are under y=150 (in the DMD black bar)
    '''
    dmddf = df.loc[df.y > DMDborder]
    return dmddf

def clean_borders(df, imageborders, borderlimit=5):
    '''removes cells that are too close to the borders
    '''
    xlow = df.loc[df.x > imageborders[0]+borderlimit]
    xhigh = xlow.loc[xlow.x < imageborders[1]-borderlimit]
    ylow = xhigh.loc[xhigh.y > imageborders[0]+borderlimit]
    yhigh = ylow.loc[ylow.y < imageborders[1]-borderlimit]
    return yhigh

def get_cellfreq(df):
    '''gets number of cells per frame
    '''
    freq = df.sort_values('frame').groupby('frame').count()
    return freq.particle

def get_cellsize(df):
    '''gets the average cell size per time
    '''
    means = []
    for i in range(np.max(np.array(df['frame']))):
        means.append(np.mean(np.array(df[df['frame'] == i]['area'])))
    return means

def clean_cellswitch(df, col, threshold):
    '''removes cells that have a diff lower or upper from threshold
    '''
    removed_cells = []
    for cell in df.particle.unique():
        if any(abs(np.diff(df.loc[df.particle == cell][col])) > threshold):
            removed_cells.append(cell)
            df = df[df.particle != cell]
    return df

def get_multipos_all_cell_light_signals(path_to_log):
    '''Gets the log file, parses into a boolean matrix for each position.
    '''
    import distutils
    light_switching = []
    with open(path_to_log , 'r') as f:
        for line in f:
            light_switching.append( line.split('[')[-1].split(']')[0].split(','))
    for frame in light_switching:
        for i,pos in enumerate(frame):
            frame[i] = distutils.util.strtobool(pos.strip())
    return np.array(light_switching)

def plot_light_sequence(light_sequence):
    '''makes a plot of the light input sequence
    '''
    f,ax = plt.subplots(figsize = (10,1))
    ax.imshow(light_sequence.T,cmap='Blues',aspect='auto')
    ax.set_yticks([])
    ax.set_yticklabels([])
    f.tight_layout()
    return ax

def plot_kde(ax, list_csv, col, col_range, frame, color, label='', ylab='', xlab='', title='', concat=True, bandwidth=10):
    '''Args:
    list_csv(dataframe or list of dataframes): if concat is True, then list_csv is list, otherwise dataframe.
    '''
    if concat:
        data = []
        for csv in list_csv:
            data.append(np.array(csv.loc[csv.frame == frame][col]))
        data = np.concatenate(data)
    else:
        data = list_csv.loc[list_csv.frame == frame][col]
    x_plot = np.linspace(col_range[0], col_range[1], col_range[2])[:, np.newaxis]
    kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(data[:, np.newaxis])
    log_dens = kde.score_samples(x_plot)
    ax.fill_between(x_plot[:, 0], np.zeros(len(x_plot[:, 0])), np.exp(log_dens), fc=color, alpha=0.5, label=label)
    ax.legend()
    ax.set_xlabel(xlab)
    ax.set_ylabel(ylab)
    ax.set_title(title)
