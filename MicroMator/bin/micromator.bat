:: This wrapper script is necessary to make the "micromator_run_experiment" command on windows work

@SET "PYTHON_EXE=%~dp0\..\python.exe"
call "%PYTHON_EXE%" "%~dp0\micromator_run_experiment.py" %*
