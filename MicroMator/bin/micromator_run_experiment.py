#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Jul 10, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import sys
import site
import os
import datetime
import argparse
import errno
from shutil import copy
import tkinter as Tk
import json
import multiprocessing as mp
import MicroMator.Core.core_functions as core
import MicroMator.GUI.environmentGUI as envgui
import MicroMator.Microscope.mmwrapper as mmw


def get_micromanager_version(micromanager_location):
    '''returns the version of micromanger which should include should include in name either gamma or beta. If none a present assumes it's 1.4
    Args:
        micromanager_location(path): the path to the micromanger folder (should include in name either gamma or beta)
    '''
    if 'gamma' in micromanager_location:
        version = 'gamma'
    elif 'beta' in micromanager_location:
        version = 'beta'
    elif '1.422' in micromanager_location or '1.423' in micromanager_location:
        version = '1.4'
    else:
        version = 'unkown'
    print('µmanager version: {}'.format(version))
    return version

def str2bool(value):
    '''checks if value is a bool
    '''
    if str(value).lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    if str(value).lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    raise argparse.ArgumentTypeError('Boolean value expected.')

def str2boolanalysis(value):
    '''argument choices for analysis module
    '''
    if str(value).lower() in ('cellstar', 'cell', 'c'):
        return 'cellstar'
    if str(value).lower() in ('numpy', 'np', 'nu', 'n'):
        return 'numpy'
    if str(value).lower() in ('false', 'f', '0'):
        return False
    raise argparse.ArgumentTypeError('(c)ellstar or (n)umpy expected')

def dir_path(string):
    '''checks if valid path to file
    '''
    if os.path.exists(string):
        return string
    raise FileNotFoundError(string)

def create_documents_folder():
    '''calls this function to make sure the options folder is created (install can fail)
    '''
    for sitepackage in site.getsitepackages():
        if 'site-packages' in sitepackage:
            break
    jsonfile_ = os.path.join(os.path.join(sitepackage, 'MicroMator\\default_options.json'))
    with open(jsonfile_) as jfile:
        default_options_ = json.load(jfile)
    default_options_['ANALYSIS_SOFTWARE']['cellstar']['module'] = os.path.join(sitepackage, 'MicroMator\\ImageAnalysis\\cellstar\\analysis_caller.py')
    default_options_['ANALYSIS_SOFTWARE']['numpy']['module'] = os.path.join(sitepackage, 'MicroMator\\ImageAnalysis\\numpy\\analysis_caller.py')
    default_options_['EVENTS_PARAMETERS']['creator'] = os.path.join(sitepackage, 'MicroMator\\Events\\example_event_creator.py')
    userdocpath = os.path.expanduser('~/Documents/MicroMator/')
    try:
        os.makedirs(userdocpath)
    except OSError as err:
        if err.errno != errno.EEXIST:#check custom experiment folder exists
            raise
    if not os.path.isfile(os.path.join(userdocpath, 'default_options.json')):
        with open(os.path.join(userdocpath, 'default_options.json'), 'w') as jfile:
            json.dump(default_options_, jfile, indent=4, separators=(',', ': '))
        copy(jsonfile_, userdocpath)


parser = argparse.ArgumentParser(description='Start a MicroMator experiment. Arguments default value are False. If No arguments are given GUI opens instead.')
parser.add_argument('-m', type=str2bool, nargs='?', const=True, default=0, help='MICROFLUIDICS: if enabled, will use the microfluidics module')
parser.add_argument('-a', type=str2boolanalysis, nargs='?', const=True, default=0, help='ANALYSIS: c(ellstar), n(umpy) or false enables the module')
parser.add_argument('-e', type=str2bool, nargs='?', const=True, default=0, help='EVENTS: if enabled, uses events module')
parser.add_argument('-gui', type=str2bool, nargs='?', const=True, default=0, help='NO GUI: if enabled, disables all GUI')
parser.add_argument('-path', type=dir_path, help='if enabled will read given options.json instead of default one')
args = parser.parse_args()

if __name__ == '__main__':
    #Global signals to communicate in between != processes and modules
    create_documents_folder()
    date = datetime.datetime.now()
    GLOBALVARS = mp.Manager()
    GLOBADICT = GLOBALVARS.dict({'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
                                 'PAUSE': GLOBALVARS.Value('i', False), #Bool that contains the Pause signal for the GUI & the Acqu loop
                                 'UNPAUSE': GLOBALVARS.Value('i', False), #Bool that contains the Unpause for the GUI & the Acqu loop
                                 'END_ACQ': GLOBALVARS.Value('i', False), #Bool that contains the signal End acquisition signal for the GUI & the Acqu loop
                                 'ALREADY_QUITTING': GLOBALVARS.Value('i', False), #Bool that contains the signal to tell that it's already quitting
                                 'EXPTIMER': GLOBALVARS.Value('i', 0), #Int that contains the global experiment timer in seconds
                                 'MMLOADED': GLOBALVARS.Value('i', False), #Bool that contains the signal to update the GUI with the Microscope Module
                                 'PLOADED': GLOBALVARS.Value('i', False), #Bool that contains the signal that the protocol file has been loaded
                                 'TIME_TO_NEXT_FRAME': GLOBALVARS.Value('i', 0), #Int that contains the countdown to next frame
                                 'ABSOLUTE_TIME_TO_NEXT_FRAME': GLOBALVARS.Value('i', 0), #Int that contains the absolute time to next frame
                                 'REAL_TIME_NEXT_FRAME': GLOBALVARS.Value('i', 0),
                                 'FRAME': GLOBALVARS.Value('i', False), #Bool that contains the signal for the segmentation (When true start)
                                 'ACQU': GLOBALVARS.Value('i', False), #Bool that contains the signal to make sure no interference with acquisition loop
                                 'RUN': GLOBALVARS.Value('i', False), #Bool that contains the signal to show when MMwrapper controller is launched
                                 'EVENT': GLOBALVARS.Value('i', False), #Bool that contains the signal for the Event module
                                 'START_TIME': GLOBALVARS.Value('i', '{}-{}-{} {}:{}:{}'.format(date.year, date.month, date.day, date.hour, date.minute, date.second)),
                                 'ABORT': GLOBALVARS.Value('i', False), #bool abort signal
                                 'ACQ_INTERVAL': GLOBALVARS.Value('i', 0), #time between 2 acqu loops
                                 'LIST_CHANNELS': GLOBALVARS.list([]), #List of channels test
                                 #String used for GUI that contains the time at which the user clicked the start acq button
                                 'COMPLEXE_GUI_DONE': GLOBALVARS.Value('i', False),
                                 'CURRENT_POS': GLOBALVARS.Value('i', 0), #Int that contains at which position the stage is
                                 'CURRENT_CHANNEL': GLOBALVARS.Value('i', '-'), #str that contains the name of current channel
                                 'ANALYZED': GLOBALVARS.Value('i', False), #Analyzer signal
                                 'CURRENT_FRAME': GLOBALVARS.Value('i', 0), #Int that contains the current frame
                                 'TOTAL_FRAMES': GLOBALVARS.Value('i', 0), #Int that contains total number of frames
                                 'ANALYSIS_PER_POS': GLOBALVARS.list([]), #list for per pos analysis signaling
                                 'SINGLE_EVENT_TRIGGER': GLOBALVARS.list([]), #list for per EVENT signaling
                                 'MPC':GLOBALVARS.Value('i', False)})

    GLOBALFLUIDICS = GLOBALVARS.dict({'RUNEND': GLOBALVARS.Value('i', False), #Bool that contains the Stop signal to kill GUI
                                      'ABORT': GLOBALVARS.Value('i', False), #Bool that contains the Abort signal for the GUI & the microfluidics loop
                                      'UNPAUSE': GLOBALVARS.Value('i', False), #Bool that contains the Unpause signal for the GUI & the microfluidics loop
                                      'PAUSE': GLOBALVARS.Value('i', False), #Bool that contains the Pause signal for the GUI & the microfluidics loop
                                      'KEEPFLOW': GLOBALVARS.Value('i', False), #Bool that contains the Keepflow signal for the GUI & the microfluidics loop
                                      'INIT': GLOBALVARS.Value('i', False)}) #Bool that contains the Start signal for the GUI & the microfluidics loop

    if not args.path:
        for sitepackage in site.getsitepackages():
            if 'site-packages' in sitepackage:
                break
        options_json_file = os.path.join(sitepackage, 'MicroMator', 'default_options.json')
    else:
        options_json_file = os.path.join(args.path)
    with open(options_json_file) as jsonfile:
        default_options = json.load(jsonfile)

    #If no arguments are given, the GUI laucher is chosen
    if not args.m and not args.e and not args.a and not args.gui and not args.path:
        root = Tk.Tk()
        options = envgui.launch_gui(default_options, root)
    elif args.path:
        options = default_options
    else:
        #Initializes the modules from the arguments given in the function call
        options = core.load_micromator_options(args.m, args.a, args.e, options_json_file)

    if options == 'Abort':
        sys.exit()

    #gets the version of micromanger
    options['MICROMANAGER_PARAMETERS']['mmversion'] = get_micromanager_version(options['MICROMANAGER_PARAMETERS']['micromanagerlocation'])
    #goes the micromanger folder
    os.chdir(options['MICROMANAGER_PARAMETERS']['micromanagerlocation'])

    #Calls the MicroMator class that will initialze and store the rest of the modules
    micromator = core.MicroMator(options, GLOBALVARS, GLOBADICT, GLOBALFLUIDICS, args.gui)

    if GLOBADICT['ABORT'].value:
        sys.exit()
    #Initializing the Micromanager Custom Acquisition Engine
    #Unpacking the variables is needed because some connot be pickled...
    if options['MICROMANAGER_PARAMETERS']['complexeprotocol']:
        while not GLOBADICT['COMPLEXE_GUI_DONE'].value:
            pass
    micromanager_acquisition_engine = mp.Process(target=mmw.Scripter, args=(micromator.protocol,
                                                                            micromator.folder_manager_obj,
                                                                            micromator.positions,
                                                                            micromator.frame_object_list,
                                                                            options,
                                                                            GLOBADICT))

    micromanager_acquisition_engine.start()
    micromanager_acquisition_engine.join() #needed not as to not stop the execution of the process early and kill the process Manager
