'''Setup for MicroMator software
'''
import os
import setuptools
from setuptools.command.install import install
import versioneer

cmdclass = versioneer.get_cmdclass()
_sdist = cmdclass['sdist']

class sdist(_sdist):
    def run(self):
        _sdist.run(self)

cmdclass['sdist'] = sdist

# set basic metadata
PACKAGENAME = 'MicroMator'
DISTNAME = 'MicroMator'
AUTHOR = 'Steven Fletcher'
AUTHOR_EMAIL = 'steven.fletcher@inria.fr'

#build
setup_requires = ['setuptools', 'versioneer']

#run
install_requires = []
with open(os.path.join(os.path.normpath(os.path.dirname(os.path.realpath(__file__))), 'requirements.txt'), 'r') as file_:
    for line in file_.readlines():
        install_requires.append(line.split('\n')[0])

# read description
with open("README.md", "r") as fh:
    long_description = fh.read()

class PostInstallCommand(install):
    """Post-installation for installation mode."""
    def run(self):
        import site
        from shutil import copy
        import errno
        import json
        jsonfile = os.path.join('./MicroMator/default_options.json')
        with open(jsonfile) as jfile:
            default_options = json.load(jfile)
            print(default_options.keys())
        for sitepackage in site.getsitepackages():
            if 'site-packages' in sitepackage:
                break
        default_options['ANALYSIS_SOFTWARE']['cellstar']['module'] = os.path.join(sitepackage, 'MicroMator\\ImageAnalysis\\cellstar\\analysis_caller.py')
        default_options['ANALYSIS_SOFTWARE']['numpy']['module'] = os.path.join(sitepackage, 'MicroMator\\ImageAnalysis\\numpy\\analysis_caller.py')
        default_options['EVENTS_PARAMETERS']['creator'] = os.path.join(sitepackage, 'MicroMator\\Events\\example_event_creator.py')
        default_options['DISCORD'] = False
        userdocpath = os.path.expanduser('~/Documents/MicroMator/')
        try:
            os.makedirs(userdocpath)
        except OSError as err:
            if err.errno != errno.EEXIST:#check custom experiment folder exists
                raise
        print(os.path.join(userdocpath, 'default_options.json'))
        if not os.path.isfile(os.path.join(userdocpath, 'default_options.json')):
            with open(os.path.join(userdocpath, 'default_options.json'), 'w') as jfile:
                json.dump(default_options, jfile, indent=4, separators=(',', ': '))
            copy(jsonfile, userdocpath)
        install.run(self)

setuptools.setup(
    name="MicroMator_temp",#DISTNAME,
    author=AUTHOR,
    version='2021.4.2.3',#versioneer.get_version(),
    author_email=AUTHOR_EMAIL,
    description="A python software for real time control of a microscope platform",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.inria.fr/InBio/Public/micromator/-/tree/master/MicroMator",
    setup_requires=setup_requires,
    install_requires=install_requires,
    include_package_data=True,
    packages=setuptools.find_packages(),
    cmdclass={'install': PostInstallCommand},
    scripts=['bin/micromator_run_experiment.py', 'bin/micromator.bat', 'bin/micromator_install.bat'],
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3 :: Only",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: Microsoft :: Windows :: Windows 10"
    ],
)
