'''
Created on Jun 24, 2020

@author: Steven Fletcher - Institut Pasteur - INRIA - InBio
'''
import os
import pandas as pd
import MicroMator.Events.manager as manager
from MicroMator.FileManagement import logger

class effect_augment_Exposure:
    '''this class augments the exposure of channel 1 (channel 0 is bright field)
    '''
    def __init__(self, mmc, value):
        '''Args:
            mmc(object): the MMCore object
            value(float): how much we raise the exposure
        '''
        self.mmc = mmc
        self.value = value

    def act(self, globaldict):
        '''this effector changes the exposure'''
        logger('changed the Exposure({}) to new value {} '.format(self.mmc.protocol.channel_list[1].exposure, self.mmc.protocol.channel_list[1].exposure + self.value), message_type='normal', module="event")
        self.mmc.protocol.channel_list[1].exposure = self.mmc.protocol.channel_list[1].exposure + self.value

class trigger_fluorescence_below_treshold:
    '''triggers if the fluorescence value measured by numpy is below the treshold
    '''
    def __init__(self, mmc, treshold):
        '''Args:
            mmc(object): the MMCore object
            threshold: a number (int or float)
        '''
        self.mmc = mmc
        self.treshold = treshold

    def check(self, globaldict):
        '''This check checks if the fluorescence value measured by numpy is below the treshold
        '''
        numpy_data_path = os.path.join(self.mmc.folder_manager_obj.analysis_path, 'data_0.csv')
        data = pd.read_csv(numpy_data_path)
        v = float(data['YFP-DIRECT'])
        print("TRIGGER: fluo value with current exposure: ", v, " wanted value: ",self.treshold)
        if v < self.treshold:
            return True
        else:
            return False

def create_events(mmc, globaldict):
    '''this is the functions that users will use to define their events
    Args:
        mmc(object): the MMCorePy object
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
    '''
    #list where all the created events are put
    events_list = []
    
    mytrigger = trigger_fluorescence_below_treshold(mmc, 500)
    myeffect = effect_augment_Exposure(mmc, 100)

    event = manager.Event(mytrigger, myeffect, globaldict, name='exposure adjustment')
    events_list.append(event)
    return events_list


