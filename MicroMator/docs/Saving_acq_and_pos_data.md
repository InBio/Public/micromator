
*Micromanager window to generate & save the .acq file*

![multidwindow](multidwindow.png "Micromanager window to generate & save the .acq file")


*Micromanager window to generate & save the .pos file*

![Stage Pos](Stage_Pos_list.png "Micromanager Window to generate & save the .pos file")
