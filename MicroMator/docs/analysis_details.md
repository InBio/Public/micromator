## Implementing a new image analysis module

This short document is to explain how to incorporate "any" image analysis software in MicroMator.

##### Requirements:
- be familiar with python coding
- Python (**3.6 only** as micromanager's newer releases aren't fully python 3.7 compatible yet!)

##### Advanced Guide:

check the MicroMator source code for simple examples (```MicroMator\\MicroMator\\ImageAnalysis\\numpy\\analysis_caller.py``` & ```MicroMator\\MicroMator\\ImageAnalysis\\SegMator\\analysis_caller.py```)

1. create a folder that is in PYTHONPATH
2. create an empty file called ```__init__.py```
3. create a file called ```analysis_caller.py```
  1. create a python class ```Analysis()```
  2. the constructor must have one and only one parameter : ```micromator``` (this parameter is the instance of the ```MicroMator``` and contains all the needed parameters for the analysis module)
    - ```micromator.globaldict``` (contains all the important signals and counters like current frame, current pos, experiment timer etc...)
    - ```micromator.options``` (contains all the options pertaining to the current micromator experiment such as analysis parameters for instance, or some microscope parameters)
    - ```micromator.folder_manager_obj``` (this object contains most paths for the current micromator experiment)
  3. write the  ```looper(self)``` attribute. This attribute is called in a thread in the main code and is there to see if the analysis software needs to be called  or not (when a new set of images is saved on disk)
  an example of a looper function :
  ```python
  def looper(self):
        '''this method loops over all the positions & frames
        '''
        frame = 0
        # while it's not last frame and experiment has not ended
        while frame <= self.protocol.acqNumframes-1 and not self.globaldict['END_ACQ'].value:
            # while its not a new frame, i.e. when there are no new images
            while not self.globaldict['FRAME'].value:
                if self.globaldict['END_ACQ'].value:
                    return
            # go through all the positions
            for position in self.protocol.positions_list:
                # call the do_analysis() attribute with parameters frame and position
                analysis_thread = threading.Thread(target=self.do_analysis, args=(frame, position))
                analysis_thread.start()
            frame += 1
            # set the global signal that you've started the analysis
            self.globaldict['FRAME'].value = False
    ```
  4. open the ```user\\Documents\\MicroMator\\default_options.json``` file and add an item to the ```ANALYSIS_SOFTWARE``` dictionnary :
  ```json
  "MyCustomModule":{"module": "my/path/to/module/analysis_caller.py",
                    "parameters": {"option1": "value1",
                                   "option2": 1,
                                   "optionfile": "Path\\to\\option.file"}
                    }
  ```
  5. if all the steps have been followed then when you start MicroMator your analysis module should appear in the list
