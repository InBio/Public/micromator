## Tutorial experiment

In this experiment we want to image fluorescent objects without having a too strong exposure while seeing enough fluorescence. We want the object fluorescence to be at 500 (pixel value). We will set up an experiment with a 10ms fluorescent channel exposure, and add an event that will raise this exposure until we reach this fluorescence value. The Analysis module is enabled using numpy as in [this MicroMator_parameters.json](docs/demo_experiment/Protocols/MicroMator_parameters.json) file, in ```MicroMator\docs\demo_experiment\```. The acquisition file will look like [this](docs/demo_experiment/Protocols/acqusettings.acq) with only one fluorescent channel (CFP-DIRECT in the example) enabled with an initial exposure set to 10ms. However, remember that the ```.pos``` and ```.acq``` file must be generated using your micromanager linked to your microscopy setup. For this example you need to save one position with fluorescent objects. You will also need to edit the Paths in the ```.json``` file.

Now we will present the event that will change the acquisition exposure during the experiment (more information on the event module and API in the *Software description* section above). This event will be created inside the event creator python file, that you can find in  ```MicroMator\docs\demo_experiment\```. The path to this file has to be entered when you launch the experiment with the event module as well (either in the GUI or in the json file).

```python
import os
import pandas as pd
import MicroMator.Events.manager as manager
from MicroMator.FileManagement import logger
```
Fist we begin with the event's trigger:

*Trigger:*

```python
class trigger_fluorescence_below_treshold:
    '''triggers if the fluorescence value measured by numpy is below the treshold
    '''
    def __init__(self, mmc, treshold):
        '''Args:
            mmc(object): the MMCore object
            threshold: a number (int or float)
        '''
        self.mmc = mmc 
        self.treshold = treshold    #we will give this threshold as an argument to the trigger when creating the event, in our example we will give 500

    def check(self, globaldict):    #Your check() function must take (self, globaldict) as args, globaldict holds information for modules to communicate between each other
       '''This check checks if the fluorescence value measured by numpy is below the treshold
        '''
        numpy_data_path = os.path.join(self.mmc.folder_manager_obj.analysis_path, 'data_0.csv')     
        data = pd.read_csv(numpy_data_path) #retrieving the numpy analysis results in demo_experiment\Analysis\data_0.csv
        v = float(data['YFP-DIRECT'])
        print("TRIGGER: fluo value with current exposure: ", v, " wanted value: ",self.treshold)
        if v < self.treshold: #comparing last frame fluo value to our treshold to see if we need to raise the exposure
            return True
        else:
            return False
```
This trigger works with the Analysis module, that stores its data in a .csv file. It checks the fluorescence value and returns true if it is below the chosen threshold.

*Effect:*

```python
class effect_augment_Exposure:
    '''this class augments the exposure of channel 1 (channel 0 is bright field, channel 1 is the first channel you set after Bright field in micromanager)
    '''
    def __init__(self, mmc, value):
        '''Args:
            mmc(object): the MMCore object
            value(float): how much we raise the exposure
        '''
        self.mmc = mmc
        self.value = value

    def act(self, globaldict):
        '''this effector changes the exposure'''
        logger('changed the Exposure({}) to new value {} '.format(self.mmc.protocol.channel_list[1].exposure, self.mmc.protocol.channel_list[1].exposure + self.value), message_type='normal', module="event") #logging what we change
        self.mmc.protocol.channel_list[1].exposure = self.mmc.protocol.channel_list[1].exposure + self.value    #editing the mmc.protocol object to change the acquisition parameters
```
This effect changes the exposure in the protocol object of the micromator python instance (mmc). In this example ```mmc.protocol.channel_list``` has 2 elements, the first is the Bright field channel (see in the [.acq file](docs/demo_experiment/Protocols/acqusettings.acq)), and the second is the CFP-DIRECT channel. We edit ```mmc.protocol.channel_list[1].exposure``` to change the exposure of CFP acquisition in the main acquisition loop. We also use the logger micromator module to keep track of what the event does (this will be logged in ```micromator.log``` during the experiment)

*Event creator function*

```python
def create_events(mmc, globaldict):
    '''this is the functions that users will use to define their events
    Args:
        mmc(object): the MMCorePy object
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
    '''
    #list where all the created events are put
    events_list = []
    mytrigger = trigger_fluorescence_below_treshold(mmc, 1600)
    myeffect = effect_augment_Exposure(mmc, 50)

    event = manager.Event(mytrigger, myeffect, globaldict, name='exposure adjustment')
    events_list.append(event)
    return events_list
```

This function will be launched at each time frame, and will create the event.
