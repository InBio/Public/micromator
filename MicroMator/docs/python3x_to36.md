# How to switch from python Anaconda 3.x distribution to 3.6 :

[doc page on how to create anaconda environments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)

1. To create an environment:

``conda create --name myenv``

2. To create an environment with a specific version of Python:

``conda create -n myenv python=3.6``

3. To activate an environment:

``conda activate myenv``
___

If the user does not need for anything else then 3.6 they can simply change the used python version using the following command :

``conda install python=3.6``
