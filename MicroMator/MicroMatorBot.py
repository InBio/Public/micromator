import discord
from discord.ext import commands
import os
import time

bot = commands.Bot(command_prefix = "!", description = "MicroMator Interactive Bot")

@bot.event
async def on_ready():
    activity = discord.Activity(name='Yeast getting cooked 👀', type=discord.ActivityType.watching)
    await bot.change_presence(activity=activity)
    print("Ready")

@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    if message.content[:7] == "!update":
        await message.delete()
    if message.content[:17] == "!auto_error_check":
        await message.delete()
    if message.content[:14] == "disconnect bot":
        await message.delete()
        await message.channel.send("Bot Disconnected")
        exit()
    if message.content[:4] == "path":
        global path, expname, master_message
        path = message.content[5:]
        expname = path.split('\\')[-1]
        embed = discord.Embed(title=expname, description='The bot will edit this message, use the !update [position] [frame ou last] command, example: `!update 0 last` The command will then disappear for clarity.', colour=discord.Colour.blue())
        master_message = await message.channel.send(embed = embed)
    await bot.process_commands(message)

@bot.command()
async def hi(ctx):
    await ctx.send("hi")

@bot.command()
async def die(ctx):
    await ctx.send("Bot Disconnected")
    exit()

@bot.command()
async def exp_path(ctx):
    await ctx.send(path)

@bot.command()
async def getlogs(ctx):
    if path:
        await ctx.send(file=discord.File(os.path.join(path,'Logs','micromator.log')))
        await ctx.send(file=discord.File(os.path.join(path,'Logs','powershell_log.txt')))

@bot.command()
async def auto_error_check(ctx):
    if path:
        logfile = open(os.path.join(path,'Logs','powershell_log.txt'))
        text = str(logfile.read())
        logfile.close
        list = text.split('\n')
        for n in list:
            if 'Traceback' in n or 'traceback' in n:
                await ctx.send(f'Exception detected in the log file line {list.index(n)}:')
                await ctx.send(file=discord.File(os.path.join(path,'Logs','powershell_log.txt')))

@bot.command()
async def update(ctx, pos, frame):
    if path and master_message:
        if frame == 'last':
            logfile = open(os.path.join(path,'Logs','micromator.log'))
            text = str(logfile.read())
            logfile.close
            list = text.split('\n')
            for n in list:
                if '-- FRAME: ' in n:
                    frame = n.split(' ')[8]
        logfile = open(os.path.join(path,'Logs','micromator.log'))
        text = str(logfile.read())
        logfile.close
        list = text.split('\n')
        num = '[cell number not found]'
        for n in list:
            if f'p{pos}' in n and 'cells in frame found' in n and f'F{frame} ' in n:
                num = n.split(' ')[10]
        p = os.path.join(path,'Analysis','SegMator',f'pos{pos}',f'segment_img_{frame}.png')
        if os.path.exists(p):
            embed = discord.Embed(title=expname, description='The bot will edit this message, use the !update [position] [frame ou last] command, example: `!update 0 last` The command will then disappear for clarity.', colour=discord.Colour.blue())
            embed.add_field(name=f'Currently displaying position {pos} at frame {frame}', value = f'{num} cells found in this frame')
            image_message = await ctx.send(file=discord.File(p))
            url = image_message.attachments[0].url
            embed.set_image(url = url)
            await master_message.edit(embed = embed)
            time.sleep(1)
            await image_message.delete()
        else:
            await ctx.send(f'file {p} not found')        

@bot.command()
async def lastframe(ctx):
    if path:
        logfile = open(os.path.join(path,'Logs','micromator.log'))
        text = str(logfile.read())
        logfile.close
        list = text.split('\n')
        for n in list:
            if '-- FRAME: ' in n:
                frame = n.split(' ')[8]
        await ctx.send(f'[{expname}] currently at frame {frame}')

@bot.command()
async def view_pos_frame(ctx, pos, frame):
    if path:
        if frame == 'last':
            logfile = open(os.path.join(path,'Logs','micromator.log'))
            text = str(logfile.read())
            logfile.close
            list = text.split('\n')
            for n in list:
                if '-- FRAME: ' in n:
                    frame = n.split(' ')[8]
        p = os.path.join(path,'Analysis','SegMator',f'pos{pos}',f'segment_img_{frame}.png')
        if os.path.exists(p):
            await ctx.send(file=discord.File(p))
        else:
            await ctx.send(f'file {p} not found')

@bot.command()
async def ncell(ctx, pos):
    if path:
        logfile = open(os.path.join(path,'Logs','micromator.log'))
        text = str(logfile.read())
        logfile.close
        list = text.split('\n')
        for n in list:
            if f'p{pos}' in n and 'cells in frame found' in n:
                num = n.split(' ')[10]
        await ctx.send(f'[{expname}] currently {num} cells in position {pos}')

bot.run('your bot token')