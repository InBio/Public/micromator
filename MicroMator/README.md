# **MicroMator**

##### **An open and flexible software for reactive microscopy**  

<img src=docs/micromator-v2-CS5.jpg width="200"> 

#### MicroMator's main aim:
Software for microscopy automation are essential to support reproducible high-throughput microscopy experiments. Samples can now be routinely imaged using complex spatial and temporal patterns. Yet, in the overwhelming majority of the cases, executions of experiments are still cast in stone at the beginning, with little to no possibility for human or computer-driven interventions. 
**Software empowering microscopy with real-time adaptation capabilities is needed to exploit the full potential of automated microscopes**. The aim of MicroMator is to help filling this gap.


#### MicroMator's key features:
- MicroMator uses **events** to add reactiveness to a main acquisition loop. Examples of possible events are
  - if there is more than 100 cells in the field of view, then open a microfluidic valve,
  - if mean cell fluorescence exceeds 1000 au, then shine light on cells at the border of microcolonies using a micromirror device,
  - if the focus is lost, then warn the user with a message on Discord,
- MicroMator is written in **Python** and uses [Micromanager](https://micro-manager.org/), a reference cross-platform software for controlling microscopes, and its Python library [MMCorePy](https://micro-manager.org/wiki/Using_the_Micro-Manager_python_library),
- MicroMator has a **modular design** and can be easily extended,
- MicroMator's current capabilities are the support for **real-time image analysis** for brightfield yeast segmentation and tracking using [SegMator](../segmator/), support for the CellAsic Onix **microfluidics platform** via a web application framework, support for the Andor Mosaic **DMD** via the MicroManager interface, and support for **Discord communication**. MicroMator can also use the Matlab tool [CellStar](https://www.cellstar-algorithm.org/).
- MicroMator also has a **comprehensive data & metadata organization** and an **extensive logging** system.
___

## How does MicroMator work?

The proper functioning of MicroMator necessitates that MicroManager is properly configured. This has to be done only once.

For each experiment, 
1) the user launches the MicroManager GUI to define and save reference imaging positions and the main acquisition loop,
2) the user launches the MicroMator GUI to specify experiment-specific data, such as the path to the experiment folder or to the modules that are used. This includes the path to the event creator file defining the reactiveness. Alternatively, a .json file can be used to avoid manual interactions,
3) The Microscope Controller Module of MicroMator then effectively starts the experiment. 

![pipeline](docs/MM_diagram_git2.png "The main workflow for MicroMator-driven experiments.")

**Events** are defined by combining a **Trigger** and an **Effect**. Events are by default instantiated after image acquisition. The Trigger function is then executed to test whether the Effect should be applied. Effects can have very generic software or hardware actions. Generally useful triggers and effects are provided with MicroMator (Events module), and more can be found in ExampleExperiments. 

The software consists of a few different modules:
- The **Microscope module**, that is in charge of acquiring the raw images, according to the acquisition protocol, the different channels and positions the user has pre-set at the start of the experiment, and the past sequence of events.
- The **ImageAnalysis module**, that can use different tools to analyze raw images. 
- The **Event module**, that instantiate the relevant events, test their trigger function and potentially implements their effects.
- The **File Management module**, that create a folder structure for each experiment and stores all the relevant information,
- The **Data Management modules**, that manages images, data analysis results and experiment metadata.

___

## How to install MicroMator?

#### Requirements:
- Python **3.6** 
  [How to switch python versions using anaconda](docs/python3x_to36.md)
- A recent Micromanager 2.0 beta version. The nightly from 20210505 will work (link to the [.exe](https://valelab4.ucsf.edu/~MM/nightlyBuilds/2.0.0-gamma/Windows/MMSetup_64bit_2.0.0-gamma1_20210505.exe))
- the pymmcore (python wrapper for Micromanager) library ```python -m pip install --user pymmcore```. If you are using an older version of Micromanager, information on how to get the right pymmcore version can be found [on the pymmcore github](https://github.com/micro-manager/pymmcore/)
- The software is in Python 3.6, is cross-platform and has been extensively tested with Windows 10.

#### Installing with pip:
```
pip install -i https://test.pypi.org/simple/ MicroMator-temp
```

#### Installing after cloning the repository:
In the ```MicroMator\bin\``` folder you should find a file named ```micromator_install.bat```; Edit this file changing the path on the first line with the path to your own MicroMator folder, then run ```micromator_install.bat``` in a command shell.


**Please make sure micromanager's python library is in the python path.** In order to do so add a custom.pth file in the site-packages folder. In this file add micromanager like so:
```
C:\Program Files\Micro-Manager
C:\My\Library
D:\Some\Other\Path
```

After installing micromator, the user is has to edit the *default_options.json* file in a text editor. This file will be in your python installation folder for example with Miniconda3 it will be in  ```\Miniconda3\Lib\site-packages\MicroMator```. The user needs to input the correct paths and device names to the various modules (this should only be done once, those are the default path for each of the modules, you can set the dictionary of each module to false if you dont plan to use them)

Installation should take around 20-30min for MicroMator, the required packages can be found in [requirements.txt](requirements.txt). If you want to use segmator it may take more time because you will need to install tensorflow.
A good understanding of Micromanager will help greatly. The Micromanager user guide is [here](https://micro-manager.org/wiki/Micro-Manager_User%27s_Guide)

___

## How to use MicroMator?

[This video goes trough all the steps to launch a reactive experiment](docs/Launching_a_Reactive_Experiment_with_Micromator_720p.mp4), you can also find it in better quality at this youtube link: https://www.youtube.com/watch?v=1Y-yMsxwRcs

We assume that MicroManager is installed and properly configured for the microscopy setup of the user.

Then for each experiment, the user should use the GUI of MicroManager to define (i) a main image acquisition loop and (ii) relevant imaging positions. Note that both the acquisition loop and the imaging positions might be modified during the experiment by event effects. However, we found it convenient to define an initial backbone for the experiments in this way. 
This information should then saved in MicroManager as .acq and .pos files.  
- **.acq** file is a json file that is generated automatically when saving the acquisition parameters of the experiment from Micromanager’s multidimensional acquisition window.
Please note that this file could be taken from a previous experiment if the main loop is unchanged.
- **.pos** file is also a json file that is generated when saving the saved positions from Micromanager’s position window.

We describe [here](docs/Saving_acq_and_pos_data.md) how this can be done in MicroManager.

Then **MicroManager should be closed**. Closing MicroManager is necessary because the python API cannot load the MicroManager configuration if it's already loaded by the MicroManager GUI.

### Launching MicroMator from the MicroMator GUI
The MicroMator configuration GUI is started by calling ```micromator.bat``` in a command shell. This will open a window that will allow the user to select the different modules and options needed for his experiment.

![multidwindow](docs/MM_GUI_git.png "MicroMator configuration GUI")

The various modules are described below in the software description section.

Once the customization GUI is closed and the options saved, MicroMator will generate a set of folders that will act as storage for all the data relevant to the current experiment:

![folder structure](docs/folder_structure.png "The folders created by MicroMator for each experiment")

The .acq and .pos files should be placed in Protocols.
Readying up will initiate the loading of the configuration file by the Microscope Controller Module.
When the user is ready, pressing the start button will launch the experiment.

### Launching MicroMator from the MicroMator_parameters.json path
MicroMator can also be started without the configuration GUI. MicroMator is then launched with the ``-path`` option to provide a MicroMator configuration file.
Below are all the available options:
```
Options:
   -h           help shows all the available options.
   -m           if enabled, will use the microfluidics module.
   -a <module>  c(ellstar), n(umpy) or false will enable the corresponding image analysis module.
   -e           if enabled, uses events module.
   -gui         if enabled, disables all GUI.
   -path        if enabled, reads subsequent path to .json file
```
The MicroMator_parameters.json file looks like [this](docs/parameters_json_file.png), it is created in the protocols folder when you launch MicroMator using the GUI. You can get the MicroMator_parameters.json file from an experiment and use it for subsequent similar experiment editing the path in the ```"FOLDER_PARAMETERS"``` part, without using the GUI. When the parameters of a given module are set to ```false``` this module will be disabled (like the microfluidic module in the example image)

To launch MicroMator using your json file you need to:
1. create the experiment folder, and inside it the ```Protocols``` folder
2. put in the protocols folder the .acq, .pos, and .json files [example](docs/protocols_folder.png)
3. run this command:
```
micromator.bat -path "Path_to_your_experiments_folder\Your_UserName\year-month-day_Current_Experiment_name\Protocols\MicroMator_parameters.json"
```
You can just drag and drop the json file in your command window to get the path

#### Additional option to save the command window log
MicroMator has its own logging system you can checking the ```Logs\micromator.log``` file after an experiment. However if you are designing custom events and modules you may be interested in recording what happens in the command shell (Python errors, Python print()). To do this you have to:
1. create the ```\Logs``` folder as well in your experiment folder
2. create an empty text file named in this example ```powershell_log.tx``` in the Logs folder
3. run MicroMator with this command:
```
micromator.bat -path "Path_to_your_experiments_folder\Your_UserName\year-month-day_Current_Experiment_name\Protocols\MicroMator_parameters.json" *> Path_to_your_experiments_folder\Your_UserName\year-month-day_Current_Experiment_name\Logs\powershell_log.txt
```
This will disable all command shell prints and put them in the text file instead, you will need to open this text file with a text editor that refresh in real time (like Notepad++ Monitor option) in order to know when MicroMator is ready.

___

## Software description

Since MicroMator was built in a modular way, the user can choose to write his/her own modules.

#### Microscopy module
MicroMator uses Micromanager as a base for controlling the microscope. Micromanager comes with several ways to manipulate your microscope but here mainly 2 are interesting for us:
- Micromanager’s GUI (ImageJ Plugin)
- python Library (MMCorePy)

Here is a link to the [Micromanager wiki](https://micro-manager.org/wiki/Micro-Manager%20Project%20Overview)

As explained previously, micromanager's GUI is used to generate the experiments initial conditions, such as the number of time steps, the different channels and the interesting positions for the user.
Once the metadata for the experiment is set MicroMator's Microscopy module will read, save this data and then execute the acquisition.

#### Image Analysis module

Once an experiment has started, MicroMator will initialize the images generated.
MicroMator comes with 2 default image analysis modules, CellStar & an average pixel value calculator that uses numpy.
To use the numpy, cellstar or segmator (after installing segmator) image analyzer you need to give to MicroMator as Analysis module path the path of the corresponding ```analysis_caller.py``` file in ```MicroMator\MicroMator\ImageAnalysis``` like in [the .json parameters example](docs/parameters_json_file.png).

However, new modules can be incorporated. An analysis module must contain:
- An ```Analysis``` class with a ```looper()``` method.
the *looper* method is called after every new frame and should go through all the newly acquired images after every new timestep.

Here is a link to a detailed guide on how to implement a new [Image Analysis module](docs/analysis_details.md)
You can take example on the numpy module to get started (```MicroMator\MicroMator\ImageAnalysis\numpy\analysis_caller.py```)

When the analysis is done, the data is stored in relevant structures explained below.

#### Data management objects

##### Images & Analyzed data management

*Diagram of the Data Structure*

```
├── Frame0
│   ├── Image Position 0
│   │   ├── Cell 0
│   │   ├── Cell 1
│   │   └── ...
│   ├── Image Position 1
│   │   ├── Cell 0
│   │   └── ...
│   └── ...
├── ...
│
└── Frame n
    ├── Image Position 0
    │   ├── Cell 0
    │   ├── Cell 1
    │   └── ...
    ├── Image Position 1
    │   ├── Cell 0
    │   └── ...
    └── ...
```
- *Frame* instance is generated per time step. The main attribute of this class is a list that contains all the individual *Image* classes.
- *Image* instance is generated per position. The *Image* class contains all the relevant attributes of an *Image* taken from the microscope, notably a list of *Cell* instances.
- *Cell* instance is generated per individual cell found. The Cell class contains all the relevant attributes needed to store the information of single cell (i.e. fluorescence levels, coordinates, etc...)

##### Metadata management
MicroMator stores all the parameters in a few separate objects:

- *Protocol* class stores all the information regarding the microscope acquisition loop.
- *Channel* class stores all the channels the user will need for an experiment.
- *Position* class stores all the different positions the user saves for the experiment.

It’s from these objects that the various modules get their parameters.
Analyzed Data gets stored in Frame, Image and Cell objects.

When the experiment starts, MicroMator creates various folders & subfolders to store all the relevant information that is generated during the experiment, i.e. raw images, analyzed images, protocols, positions, logs etc…

*MicroMator Folder Architecture*

```
├── LabExperiments
    ├── user1
    │   ├── project1
    │   │   ├── date_experiment1
    │   │   │   ├── Acquisitions
    │   │   │   ├── Analysis
    │   │   │   ├── Logs
    │   │   │   ├── Models
    │   │   │   └── Protocols
    │   │   └── date_experiment2
    │   │       ├── Acquisitions
    │   │       ├── Analysis
    │   │       ├── Logs
    │   │       ├── Models
    │   │       └── Protocols
    │   └── project2
    │       └── ...
    ├── user2
    │   ├── project1
    │   │  └── ...  
    │   └── ...
    └── ...
```


#### Event module
To run events during your experiment you will need to enable the Event module, and to give the path to your event creator, which is a .py file specific to your experiment, we take the example of ```ExampleExperiments\reach_fluorescence_value_event_creator.py```. The event creator file must contain a create_events function that returns a list of event objects as shown in the examples below [and in this file](docs/tutorial_experiment.md). 

Steps to create an event:
1. Create a *Trigger* object, that must contain a ```check(*args, globadict)``` function, that should return ``True`` when the event needs to be triggered (i.e. – number of cells > 200, it’s 15:30, etc…) or False., you can also import a generic pre-written trigger from ```MicroMator\MicroMator\Events\trigger.py```.
2. Create an *Effect* object, that must contain an ```act(*args, globaldict)``` function. This function doesn’t need to return anything as it’s calling is only when the *Trigger* object is True. In this function the user will write what the effect of the Event is when it’s triggered. Generic pre-written effects can be imported from ```MicroMator\MicroMator\Events\effect.py```.
3. create the *Event* object:
```python
import MicroMator.Events.manager as manager
my_event = manager.Event(mytrigger, myeffect)
```

#### Model module
When enabling this module you will provide the path to a .py file, MicroMator will launch the ```looper()``` function inside at the beginning of the experiment in a thread. From then you can do what you want in this looper function, wait for frames, run simulations etc.. Check out ```MicroMator\MiroMator\Core\core_function.py, lines 166-186``` to see how parameters are imported from the ```MicroMator_parameters.json``` file and how the module is launched. You can take example on the model we used for model-predictive control of yeast gene expression in ```stoched\get_model.py```.

### Microfluidics module
When enabling this module you will provide the path to a .py file, MicroMator will import it and launch the ```initializer()``` function inside at the beginning of the experiment. You can check the libonix repository that holds the microfluidics module we use in ```manual_run.py``. Libonix is meant to work with a CellASIC ONIX2 microfluidic machine.

### Discord module
This module allows you to inspect your experiment from anywhere using a Discord Bot in your discord server. The bot will give you information about when the experiment starts and ends, and through commands you can ask him to show you images from the experiment.

Steps to make the discord module work:

1. Have a discord server or create one
https://www.howtogeek.com/364075/how-to-create-set-up-and-manage-your-discord-server/

2. Create a channel in your discord dedicated to your bot

3. Create a discord webhook linked to this channel (in server settings -> integrations -> webhooks) *The webhook URL is what you give to the DISCORD Module when you launch MicroMator from the json file, [example](docs/discord_options.png)* 

4. Create a Discord Bot user 
https://realpython.com/how-to-make-a-discord-bot-python/

5. When your Bot is created, added to your server, and given admin rights, get the bot Token and put it at the end of the ```MicroMatorBot.py``` file at the space specified on the last line.


