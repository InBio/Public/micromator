'''
Created on Sep 5, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import time
import numpy as np
from math import ceil
from PIL import Image
import MicroMator.Microscope.dmd as dmdlib
import MicroMator.Events.complexe_events.multi_mask.mask_manager as mm
from MicroMator.FileManagement import logger

MODULE = 'Event_complexe_trigger'

def sleep_(time_, globaldict):
    '''custom sleep function
    Args:
        time(float)
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
      example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
    '''
    for _ in range(int(time_)):
        time.sleep(1)
        if globaldict['ABORT'].value:
            return


class Load_dmd_mask():
    '''will load the mask given
    '''
    def __init__(self, acq_engine, image, DMDdevice, channel, pos, save=False):
        '''Args:
            acq_engine(object): the microscope scripting object
            image(str): full name & path to image that will be a dmd mask
            DMDdevice(object): the DMD device object
            channel(object or list of object): the channel wished to use for pic
            save(bool): if true will save image as event
        '''
        self.image = image
        self.DMD = DMDdevice
        self.channel = channel
        self.acq_engine = acq_engine
        self.save = save
        self.list_pos = pos

    def act(self, globaldict):
        '''Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
            example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        import MicroMator.Microscope.dmd as dmd
        if globaldict['TOTAL_FRAMES'].value == globaldict['CURRENT_FRAME'].value+1:
            return
        if not isinstance(self.channel, list):
            self.channel = [self.channel]
        for pos in self.list_pos:
            logger('--------------------------------------------- Event Pos: {} ---------------------------------------------'.format(pos), module=MODULE)
            self.acq_engine.set_position(self.acq_engine.positions[pos])
            for index, channel in enumerate(self.channel):
                if not channel.exposure:
                    channel.exposure = (globaldict['TIME_TO_NEXT_FRAME'].value-globaldict['EXPTIMER'].value)*1000
                logger(channel, module=MODULE)
                self.acq_engine.mmc.setConfig(self.acq_engine.config, channel.name)
                self.acq_engine.mmc.waitForConfig(self.acq_engine.config, channel.name)
                self.acq_engine.mmc.setShutterOpen(True)
                mask = dmd.Mask(self.DMD, dmd.load_mask(self.image[pos][index]))
                if channel.exposure > 200*1000:
                    for _ in range(ceil(channel.exposure/(200*1000))):
                        if globaldict['ABORT'].value:
                            return
                        self.DMD.displaymask(200*1000, mask)
                        sleep_(200, globaldict)
                else:
                    self.DMD.displaymask(channel.exposure, mask)
                    if channel.picture:
                        self.acq_engine.mmc.setExposure(channel.exposure)
                        self.acq_engine.mmc.snapImage()
                        img = self.acq_engine.mmc.getImage()
                        self.acq_engine.save_(pos, globaldict['CURRENT_FRAME'].value, img, channel, event=True, zslicenum=None)
        if not globaldict['ACQU'].value:
            self.acq_engine.mmc.setShutterOpen(False)
            self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')

class Load_dmd_mask_pos_dif_channels():
    '''will load the mask given
    '''
    def __init__(self, acq_engine, image, DMDdevice, channel_list, pos, save=False):
        '''Args:
            acq_engine(object): the microscope scripting object
            image(str): full name & path to image that will be a dmd mask
            DMDdevice(object): the DMD device object
            channel(object or list of object): the channel wished to use for pic
            save(bool): if true will save image as event
        '''
        self.image = image
        self.DMD = DMDdevice
        self.channel = channel_list
        self.acq_engine = acq_engine
        self.save = save
        self.list_pos = pos

    def act(self, globaldict):
        '''Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
            example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        import MicroMator.Microscope.dmd as dmd
        if globaldict['TOTAL_FRAMES'].value == globaldict['CURRENT_FRAME'].value+1:
            return
        for pos in self.list_pos:
            logger('--------------------------------------------- Event Pos: {} ---------------------------------------------'.format(pos), module=MODULE)
            self.acq_engine.set_position(self.acq_engine.positions[pos])
            for index, channel in enumerate(self.channel[pos]):
                if not channel.exposure:
                    channel.exposure = (globaldict['TIME_TO_NEXT_FRAME'].value-globaldict['EXPTIMER'].value)*1000
                logger(channel, module=MODULE)
                self.acq_engine.mmc.setConfig(self.acq_engine.config, channel.name)
                self.acq_engine.mmc.waitForConfig(self.acq_engine.config, channel.name)
                self.acq_engine.mmc.setShutterOpen(True)
                mask = dmd.Mask(self.DMD, dmd.load_mask(self.image[pos][index]))
                if channel.exposure > 200*1000:
                    for _ in range(ceil(channel.exposure/(200*1000))):
                        if globaldict['ABORT'].value:
                            return
                        self.DMD.displaymask(200*1000, mask)
                        sleep_(200, globaldict)
                else:
                    self.DMD.displaymask(channel.exposure, mask)
                    if channel.picture:
                        self.acq_engine.mmc.setExposure(channel.exposure)
                        self.acq_engine.mmc.snapImage()
                        img = self.acq_engine.mmc.getImage()
                        self.acq_engine.save_(pos, globaldict['CURRENT_FRAME'].value, img, channel, event=True, zslicenum=None)
        if not globaldict['ACQU'].value:
            self.acq_engine.mmc.setShutterOpen(False)
            self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')

class Load_dmd_mask_pos_dif_channels_nopic():
    '''will load the mask given but wont open shutter nor take picture (i thought it would be bad for the camera to receive 30s of UV light in cell-killing experiments)
    '''
    def __init__(self, acq_engine, image, DMDdevice, channel_list, pos, save=False):
        '''Args:
            acq_engine(object): the microscope scripting object
            image(str): full name & path to image that will be a dmd mask
            DMDdevice(object): the DMD device object
            channel(object or list of object): the channel wished to use for pic
            save(bool): if true will save image as event
        '''
        self.image = image
        self.DMD = DMDdevice
        self.channel = channel_list
        self.acq_engine = acq_engine
        self.save = save
        self.list_pos = pos

    def act(self, globaldict):
        '''Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
            example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        import MicroMator.Microscope.dmd as dmd
        if globaldict['TOTAL_FRAMES'].value == globaldict['CURRENT_FRAME'].value+1:
            return
        for pos in self.list_pos:
            logger('--------------------------------------------- Event Pos: {} ---------------------------------------------'.format(pos), module=MODULE)
            self.acq_engine.set_position(self.acq_engine.positions[pos])
            for index, channel in enumerate(self.channel[pos]):
                if not channel.exposure:
                    channel.exposure = (globaldict['TIME_TO_NEXT_FRAME'].value-globaldict['EXPTIMER'].value)*1000
                logger(channel, module=MODULE)
                self.acq_engine.mmc.setConfig(self.acq_engine.config, channel.name)
                self.acq_engine.mmc.waitForConfig(self.acq_engine.config, channel.name)
                #self.acq_engine.mmc.setShutterOpen(True)
                mask = dmd.Mask(self.DMD, dmd.load_mask(self.image[pos][index]))
                if channel.exposure > 200*1000:
                    for _ in range(ceil(channel.exposure/(200*1000))):
                        if globaldict['ABORT'].value:
                            return
                        self.DMD.displaymask(200*1000, mask)
                        sleep_(200, globaldict)
                else:
                    self.DMD.displaymask(channel.exposure, mask)
                    sleep_(int(channel.exposure/1000), globaldict)
                    # if channel.picture:
                    #     self.acq_engine.mmc.setExposure(channel.exposure)
                    #     self.acq_engine.mmc.snapImage()
                    #     img = self.acq_engine.mmc.getImage()
                    #     self.acq_engine.save_(pos, globaldict['CURRENT_FRAME'].value, img, channel, event=True, zslicenum=None)
        if not globaldict['ACQU'].value:
            # self.acq_engine.mmc.setShutterOpen(False)
            self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')

class Load_dmd_mask_sequence():
    '''will load the mask given
    '''
    def __init__(self, acq_engine, image, DMDdevice, channel, pos, exposure_sequence, save=False):
        '''Args:
            acq_engine(object): the microscope scripting object
            image(str): full name & path to image that will be a dmd mask
            DMDdevice(object): the DMD device object
            channel(object or list of object): the channel wished to use for pic
            save(bool): if true will save image as event
        '''
        self.image = image
        self.DMD = DMDdevice
        self.channel = channel
        self.acq_engine = acq_engine
        self.save = save
        self.exposure_sequence = exposure_sequence
        self.list_pos = pos

    def act(self, globaldict):
        '''Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
            example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        import MicroMator.Microscope.dmd as dmd
        if globaldict['TOTAL_FRAMES'].value == globaldict['CURRENT_FRAME'].value+1:
            return
        for pos in self.list_pos:
            logger('--------------------------------------------- Event Pos: {} ---------------------------------------------'.format(pos), module=MODULE)
            self.acq_engine.set_position(self.acq_engine.positions[pos])
            for index, channel in enumerate(self.channel[pos]):
                logger(channel, module=MODULE)
                self.acq_engine.mmc.setConfig(self.acq_engine.config, channel.name)
                self.acq_engine.mmc.waitForConfig(self.acq_engine.config, channel.name)
                self.acq_engine.mmc.setShutterOpen(True)
                #sequence = dmdlib.load_mask_sequence(self.image[pos])
                masks_sequence = []
                for image in self.image[pos]:
                    masks_sequence.append(dmd.Mask(self.DMD, dmd.load_mask(image)))
                self.DMD.displaymask_sequence(self.exposure_sequence, masks_sequence)
                # if channel.picture:
                #     self.acq_engine.mmc.setExposure(channel.exposure)
                #     self.acq_engine.mmc.snapImage()
                #     img = self.acq_engine.mmc.getImage()
                #     self.acq_engine.save_(pos, globaldict['CURRENT_FRAME'].value, img, channel, event=True, zslicenum=None)
        if not globaldict['ACQU'].value:
            self.acq_engine.mmc.setShutterOpen(False)
            self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')


if __name__ == '__main__':
    # import MMCorePy3 as MMCorePy

    # mmcp = MMCorePy.CMMCore()
    import pymmcore
    mmcp = pymmcore.CMMCore()
    mmcp.unloadAllDevices()
    mmcp.loadSystemConfiguration('D:\\lifeware\\Documents\\InBio_gamma.cfg')
    slm = dmdlib.DMD(mmcp)
    slm.list_channels = "RHOD-DIRECT"
    mmcp.unloadAllDevices()

    channels = ['C:\\Users\\lifeware\\Desktop\\affines_Acquisition_RHOD-DMD.txt']
    for channel in channels:
        name = channel.split('_')[-1].split('.')[0]
        slm.get_affines(channel)
        print('got affine {}'.format(channel))
        pattern = np.zeros((2048, 2048), dtype=(int, 2))
        P = dmdlib.Pattern(slm, name, pattern)
        #threading.Thread(target=P.pattern_to_mask).start()
        P.do_distance_rectangle()
    protocols_files = os.listdir('D:\\lifeware\\Experiments\\StevenFletcher\\test\\2019-13-9_test\\Protocols\\')
    pattern_to_mask = os.path.join('C:\\Users\\lifeware\\Desktop\\', 'array.npy')
    #pattern_to_mask = os.path.join('C:\\Users\\lifeware\\Desktop\\', 'old_test_pattern_to_mask.npy')
    list_patterns = mm.get_patterns('C:\\Users\\lifeware\\Desktop\\patterns\\')
    list_masks = mm.do_list_mask(slm, list_patterns, np.load(pattern_to_mask))
    for count, mask in enumerate(list_masks):
        size = mask.shape[::-1]
        data = np.packbits(mask, axis=1)
        img = Image.frombytes(mode='1', size=size, data=data)
        img.save(os.path.join('C:\\Users\\lifeware\\Desktop\\', 'masks', 'mask_{}.bmp'.format(count+1)))
