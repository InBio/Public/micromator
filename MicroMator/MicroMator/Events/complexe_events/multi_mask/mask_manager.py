'''
Created on Oct 10, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import time
import numpy as np
import scipy.io as sio
from PIL import Image
import MicroMator.Microscope.dmd as dmdlib
from MicroMator.FileManagement import logger, makedirs_

MODULE = 'Mask Manager'

def add_extension_bis(fullname, image_exist_count):
    '''adds _# if file already there
    '''
    original_filename = fullname
    image_exist_count = 0
    while os.path.exists(fullname):
        path_, ext = os.path.splitext(original_filename)
        fullname = path_+'_{}'.format(image_exist_count)+ext
        image_exist_count += 1
    return fullname, image_exist_count

def write_mask(mmc, list_masks, channel_name, pos=None, frame=None):
    '''writes mask to disk
    '''
    masks_path = []
    image_exist_count = 0
    for count, mask in enumerate(list_masks):
        size = mask.shape[::-1]
        data = np.packbits(mask, axis=1)
        img = Image.frombytes(mode='1', size=size, data=data)
        if pos is not None:
            bitpath = os.path.join(mmc.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos), channel_name)
            makedirs_(os.path.join(bitpath))
            if frame is not None:
                fullname = os.path.join(bitpath, 'mask{}_f{}.tif'.format(count, frame))
                fullname, image_exist_count = add_extension_bis(fullname, image_exist_count)
                img.save(fullname)
                masks_path.append(fullname)
            else:
                fullname = os.path.join(bitpath, 'mask{}.tif'.format(count))
                fullname, image_exist_count = add_extension_bis(fullname, image_exist_count)
                img.save(fullname)
                masks_path.append(fullname)
        else:
            bitpath = os.path.join(mmc.folder_manager_obj.bitmaps_path, channel_name)
            makedirs_(os.path.join(bitpath))
            if frame is not None:
                fullname = os.path.join(bitpath, 'mask{}_f{}.tif'.format(count, frame))
                fullname, image_exist_ = add_extension_bis(fullname, image_exist_)
                img.save(fullname)
                masks_path.append(fullname)
            else:
                fullname = os.path.join(bitpath, 'mask{}.tif'.format(count+1))
                fullname, image_exist_ = add_extension_bis(fullname, image_exist_)
                img.save(fullname)
                masks_path.append(fullname)
    return masks_path

def get_masks_path(mmc, list_masks, channel_name, pos=None, frame=None):
    masks_path = []
    for count, mask in enumerate(list_masks):
        bitpath = os.path.join(mmc.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos), channel_name)
        makedirs_(os.path.join(bitpath))
        fullname = os.path.join(bitpath, 'mask{}.tif'.format(count))
        masks_path.append(fullname)
    return masks_path

def upload_mask_to_dmd(acq_engine, DMDdevice, channel, mask, exposure):
    '''function loads and displays mask
    Args:
        acq_engine(object): the microscope scripting object
        image_mask(str): dmdlib.load_mask(image_mask) output
        DMDdevice(object): the DMD device object
        channel(object): the channel wished to use for pic
        exposure(float): exposure time for channel in ms
    '''
    acq_engine.mmc.setShutterDevice('pE4000-DMD')
    acq_engine.mmc.setAutoShutter(True)
    acq_engine.mmc.setConfig(acq_engine.config, channel.name)
    acq_engine.mmc.waitForConfig(acq_engine.config, channel.name)
    mask = dmdlib.Mask(DMDdevice, mask)
    DMDdevice.displaymask(exposure, mask)

def check_merge_masks(list_occurences, list_masks, list_exposures):
    '''calls merge_masks() when needed i.e. exposures are the same
    Args:
        list_occurences(list of list): a list of list containing indexes
        list_masks(list of dmdlib.load_mask()): a list of arrays that can be concatenated
        list_exposures(list of float): a list of the exposures of all the masks
    '''
    list_merged_masks = []
    for list_indexes in list_occurences:
        if len(list_indexes) > 1:
            list_merged_masks.append([merge_masks(list_indexes, list_masks), list_exposures[list_indexes[0]]])
        else: #if list of single occurence
            list_merged_masks.append([list_masks[list_indexes[0]], list_exposures[list_indexes[0]]])
    logger('merged masks that needed', module=MODULE)
    return list_merged_masks

def dmd_sequence(acq_engine, dmddevice, channel, list_merge_masks):
    '''function loops through list of (mask, exposure)
    Args:

        acq_engine(object): the microscope scripting object
        image_mask(str): dmdlib.load_mask(image_mask) output
        dmddevice(object): the DMD device object
        channel(object): the channel wished to use for pic
        list_merge_masks(list of list): mask, exposure
    '''
    for mask, exposure in list_merge_masks:
        if exposure:
            upload_mask_to_dmd(acq_engine, dmddevice, channel, mask, exposure)
        else:
            pass
        time_ = exposure/1000
        while time_ < exposure:
            time.sleep(1)
            time_ += 1

def merge_masks(list_mask_indexes, list_masks):
    '''Args:
        list_mask_indexes(list of list): lis of indexes of masks that need merging
        list_masks(list of dmdlib.load_mask()): a list of arrays that can be concatenated
    '''
    conc_mask = np.zeros(list_masks[0].shape, dtype=bool)
    for index in list_mask_indexes:
        logger('added mask {} to merged mask'.format(index), module=MODULE)
        conc_mask += list_masks[index]
    return conc_mask

def get_occurences_in_list(list_):
    '''func returns duplicate indexes per item in list
    Args:
        list_(list): any list
    '''
    list_occurences = []
    for item in list_:
        list_occurences.append([i for i, x in enumerate(list_) if x == item])
    return list_occurences

def remove_duplicates(list_):
    '''func removes duplicates in list
    Args:
        list_(list): any list
    '''
    newlist = []
    for i in list_:
        if i not in newlist:
            newlist.append(i)
    logger('removed duplicates', module=MODULE)
    return newlist

def average_fluo(pattern, matpath, channel, binning_factor=None):
    '''func calculates average fluo of cell in pattern using center of snakes
    Args:
        pattern(bool array): boolean array of camera size
        matpath(str): the path to the segementaion.mat file
        channel(int): the index of the channel
    '''
    mat = sio.loadmat(matpath)
    fluo = 0
    cell_list = []
    for index, _ in enumerate(mat['snakes'][0]):
        y_cell_center = int(mat['snakes'][0][index][0][0][10][0][0][7])
        x_cell_center = int(mat['snakes'][0][index][0][0][10][0][0][8])
        if binning_factor:
            x_cell_center *= binning_factor
            y_cell_center *= binning_factor
        if pattern[int(x_cell_center)][int(y_cell_center)]:
            cell_list.append({'cell#': index+1, 'x': x_cell_center, 'y': y_cell_center, 'fluo': mat['fluorescence'][0][0][0][channel][index]})
            fluo += mat['fluorescence'][0][0][0][channel][index]
    fluo /= len(cell_list)
    fluo *= 65536
    return fluo, cell_list

def do_list_mask(dmddevice, list_patterns, pattern_to_mask_array):
    '''this func goes through the patterns and call the func that creates the masks from them
    Args:
        dmddevice(object): the DMD device object
        list_patterns(list of bitmap np arrays): the list of individual patterns
    '''
    list_masks = []
    if not isinstance(list_patterns, list):
        list_patterns = [list_patterns]
    for pattern in list_patterns:
        list_masks.append(do_mask(dmddevice, pattern, pattern_to_mask_array))
    return list_masks

def do_mask(dmddevice, pattern, pattern_obj):
    '''this func transforms a pattern into a mask
    Args:
        dmddevice(object): the DMD device object
        pattern(bitmap np array): a bitmap numpy array of size camera
        pattern_obj(object): the pattern object that has the affine transforms to go from pattern to mask
    '''
    mask = np.zeros((dmddevice.height, dmddevice.width), dtype=bool)
    for row in range(pattern.shape[0]):
        for col in range(pattern.shape[1]):
            if pattern[row][col]:
                mask_x = pattern_obj[row][col][0]
                mask_y = pattern_obj[row][col][1]
                if mask_x >= dmddevice.width:
                    mask_x = dmddevice.width-1
                if mask_y >= dmddevice.height:
                    mask_y = dmddevice.height-1
                mask[mask_y][mask_x] = True
    #mask = np.rot90(mask, 2)
    return mask

def get_patterns(folder, resize=False):
    '''func gets all the patterns puts them in a list after making them numpy arrays
    Args:
        folder(str): absolute path to folder
    '''
    list_patterns = []
    for file_ in os.listdir(folder):
        if 'pattern' in file_ and 'mask' not in file_:
            img_pattern = Image.open(os.path.join(folder, file_))
            if resize:
                img_pattern = img_pattern.resize((2048, 2048))
            list_patterns.append(np.asarray(img_pattern, dtype=bool))
    return list_patterns

def mean_threshold_subarray(pattern, image, threshold):
    '''mean with custom threshold to eliminate background
    Args:
        pattern(bitmap np array): a bitmap numpy array of size camera
        image(array): the array containing the pixel values
        threshold(int): the value under wich pixels will not be counted for the mean
    '''
    subarray = pattern*image
    tavg = np.ma.average(subarray, weights=(subarray > threshold))
    if np.isnan(tavg):
        return 0
    return tavg

def write_pattern(mmc, list_patterns, channel_name, pos=None, frame=None):
    '''writes pattern to disk
    '''
    image_exist_count = 0
    for count, pattern in enumerate(list_patterns):
        size = pattern.shape[::-1]
        data = np.packbits(pattern, axis=1)
        img = Image.frombytes(mode='1', size=size, data=data)
        if pos is not None:
            bitpath = os.path.join(mmc.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos), channel_name)
            makedirs_(os.path.join(bitpath))
            if frame is not None:
                fullname = os.path.join(bitpath, 'pattern{}_f{}.tif'.format(count, frame))
                fullname, image_exist_count = add_extension_bis(fullname, image_exist_count)
                img.save(fullname)
            else:
                fullname = os.path.join(bitpath, 'pattern{}.tif'.format(count))
                fullname, image_exist_count = add_extension_bis(fullname, image_exist_count)
                img.save(fullname)
        else:
            bitpath = os.path.join(mmc.folder_manager_obj.bitmaps_path, channel_name)
            makedirs_(os.path.join(bitpath))
            if frame is not None:
                fullname = os.path.join(bitpath, 'pattern{}_f{}.tif'.format(count, frame))
                fullname, image_exist_ = add_extension_bis(fullname, image_exist_)
                img.save(fullname)
            else:
                fullname = os.path.join(bitpath, 'pattern{}.tif'.format(count+1))
                fullname, image_exist_ = add_extension_bis(fullname, image_exist_)
                img.save(fullname)
