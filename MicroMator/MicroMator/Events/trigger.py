'''
Created on Dec 13, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
from threading import Event
from datetime import datetime, time, timedelta
from MicroMator.FileManagement import logger
#custom events triggers should be a class of there own. each trigger needs to have a check function. this function is called by the manager.py file
BOOL = Event()
BOOL.set()
MODULE = 'Event_trigger'

class Generic:
    '''generic trigger class where user gives the field, the math operator & what it's compared to
    '''
    def __init__(self, arguments):
        '''Args:
            arguments(dict):
                Keys : data_field, math, comparison
                Values :
                    data_field(a field from the data structure): cell#, fluorescence, #positions, time, etc...
                    math(str): a math symbol that is among this list ('+' '-' '>' '<' '=' '!')
                    comparison(float): a float that acts as the compared value
        '''
        import operator
        self._ops = {'+': operator.add, '-': operator.sub, '>' : operator.gt, '<' : operator.lt, '=': operator.eq, '!': operator.is_not}
        self.data_field = arguments['data_field']
        self.math = arguments['math']
        self.comparison = arguments['comparison']

    def check(self, globaldict):
        '''True self.ops[self.math](float(self.data_field), float(self.comparison)) is True
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        trigger = False
        if self.math not in self._ops:
            logger("the operation is not it the possible list : '+' '-' '>' '<' '=' '!' ", message_type='error', print_=False, module=MODULE)
        if self._ops[self.math](float(self.data_field), float(self.comparison)):
            logger('digit event triggered : {}{}{}'.format(self.data_field, self.math, self.comparison), module=MODULE)
            trigger = True
        return trigger


class Total_cellpop_equal_onepos:
    '''trigger when the total cell population of one image equal given value
    '''
    def __init__(self, arguments):
        '''Args:
            data(object): the data object where total_cell_pop is stored
            value(float): what the data is compared to
        '''
        self.data = arguments['data']
        self.value = arguments['value']

    def check(self, position, globaldict):
        '''will return True if data.total_cell_pop == value
        Args:
            position(object): the Position object
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        trigger = self.data[position.index].total_cell_pop == self.value
        if trigger:
            logger('event triggered cellpop({}) == {} '.format(self.data.total_cell_pop, self.value), module=MODULE)
        return trigger


class Total_cellpop_sup_onepos:
    '''trigger when the total cell population of one image superior to given value
    '''
    def __init__(self, arguments):
        '''Args:
            data(object): the data object where the wanted field is stored
            value(float): what the data is compared to
        '''
        self.data = arguments['data']
        self.value = arguments['value']

    def check(self, position, globaldict):
        '''this method will return True if data.total_cell_pop > value
        Args:
            position(object): the Position object
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        trigger = self.data[position.index].total_cell_pop > self.value
        if trigger:
            logger('event triggered cellpop({}) > {} '.format(self.data[position.index].total_cell_pop, self.value), module=MODULE)
        return trigger


class Total_cellpop_sup_allpos:
    '''trigger when the total cell population of one image superior to given value
    '''
    def __init__(self, arguments):
        '''Args:
            data_list(object): the data object where the wanted field is stored
            value(float): what the data is compared to
        '''
        self.data_list = arguments['data']
        self.value = arguments['value']

    def check(self, globaldict):
        '''will return True when the sum of all the total_cell_pop is superior to value
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        sum_ = sum(data.total_cell_pop for data in self.data_list)
        trigger = sum_ > self.value
        if trigger:
            logger('event triggered total_cell_pop of all pos ({}) > {}'.format(sum_, self.value), module=MODULE)
        return trigger


class Avg_fluo_BF:
    '''trigger when the average fluo is MATH value of BF channel
    '''
    def __init__(self, arguments):
        '''Args:

        '''
        import operator
        self._ops = {'+': operator.add, '-': operator.sub, '>' : operator.gt, '<' : operator.lt, '=': operator.eq, '!': operator.is_not}
        self.data = arguments['data']
        self.math = arguments['math']
        self.value = arguments['value']

    def check(self, globaldict):
        '''True self.ops[self.math](float(self.data_field), float(self.value)) is True
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        trigger = False
        list_ = [data.average_fluo['BF'] for data in self.data]
        for item in list_:
            if not item:
                return trigger
        avgfluo = sum(list_)/len(list_)
        if self._ops[self.math](float(avgfluo), float(self.value)):
            logger('digit event triggered : {}{}{}'.format(avgfluo, self.math, self.value), module=MODULE)
            trigger = True
        return trigger


class Exptime:
    '''trigger when given argument equals global timer
    '''
    def __init__(self, arguments, globaldict):
        '''Args:
            arguments(int): an int representing time in seconds
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        self._value = arguments
        self.globaldict = globaldict

    def check(self, globaldict):
        '''will return True if self.globaldict['EXPTIMER'].value == value
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        trigger = self._value == globaldict['EXPTIMER'].value
        if trigger:
            logger('timed event at {} triggered'.format(globaldict['EXPTIMER'].value), module=MODULE)
        return trigger


class Exptime_list:
    '''trigger when given argument is in global timer
    '''
    def __init__(self, arguments):
        '''Args:
            arguments(int): an int representing time in seconds
        '''
        self._value = arguments

    def check(self, globaldict):
        '''will return True if self.globaldict['EXPTIMER'].value == value
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        trigger = bool(globaldict['EXPTIMER'].value in self._value)
        if trigger:
            logger('timed event at {} triggered'.format(globaldict['EXPTIMER'].value), module=MODULE)
        return trigger


class True_if_not_acqu:
    '''trigger always except in acqloop
    '''
    def check(self, globaldict):
        '''will return true allways except when in acquisition loop
        '''
        if globaldict['END_ACQ'].value:
            return False
        if not globaldict['ACQU'].value:
            return True
        return False


class True_if_not_acqu_not_frame0:
    '''trigger always except in acqloop and not frame 0
    '''
    def check(self, globaldict):
        '''will return true allways except when in acquisition loop
        '''
        if not globaldict['ACQU'].value and globaldict['CURRENT_FRAME'].value != 0:
            return True
        return False


class True_once:
    '''trigger's once and then not anymore
    '''
    def check(self, globaldict):
        boolean = BOOL.isSet()
        BOOL.clear()
        return boolean


class Channel_time_lower:
    '''trigger always except in acqloop and current time over given time
    '''
    def __init__(self, time_):
        '''Args:
            time_(datetime.datetime): a datetime object
        '''
        self.time = time_

    def check(self, globaldict):
        '''will return true when not in acqloop and current datetime over given datetime
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        if not globaldict['ACQU'].value:
            if self.time < datetime.now():
                return True
        return False


class Channel_time_interval:
    '''trigger always except in acqloop and in time interval
    '''
    def __init__(self, time_lower, time_upper):
        '''Args:
            time_(datetime.datetime): a datetime object
        '''
        self.time_lower = time_lower
        self.time_upper = time_upper

    def check(self, globaldict):
        '''will return true when not in acqloop and current datetime over given datetime
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        if not globaldict['ACQU'].value:
            if self.time_lower < datetime.now() < self.time_upper:
                return True
        return False


class Channel_time_BANG_BANG:
    '''trigger always except in acqloop and current time over given time
    '''
    def __init__(self, time_, data, channel_name, value):
        '''Args:
            time_(datetime.datetime): a datetime object
        '''
        self.time_ = time_
        self.data = data
        self.channel_name = channel_name
        self.value = value

    def check(self, globaldict):
        '''will return true when not in acqloop and current datetime over given datetime
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        if not globaldict['ACQU'].value:
            if self.time_ < datetime.now():
                if 'RHOD-DIRECT' in self.data[0].average_fluo.keys():
                    if self.data[0].average_fluo[self.channel_name] < self.value:
                        return True
        return False


class Current_frame_in_list:
    '''triggers if current frame is in list of frames
    '''
    def __init__(self, list_frame):
        '''Args:
            list_frame(list): a list of int
        '''
        self.list_frame = list_frame

    def check(self, globaldict):
        '''True if globaldict['CURRENT_FRAME'].value in list
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        if not globaldict['ACQU'].value:
            if int(globaldict['CURRENT_FRAME'].value) in self.list_frame:
                return True
        return False


class Current_frame_over:
    '''triggers if current frame is in list of frames
    '''
    def __init__(self, frame):
        '''Args:
            list_frame(list): a list of int
        '''
        self.frame = frame

    def check(self, globaldict):
        '''True if globaldict['CURRENT_FRAME'].value in list
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        if not globaldict['ACQU'].value:
            if int(globaldict['CURRENT_FRAME'].value) >= self.frame:
                return True
        return False


class Current_timeleft_over_threshold:
    '''triggers if current frame is in list of frames
    '''
    def __init__(self, frame, threshold):
        '''Args:
            list_frame(list): a list of int
        '''
        self.frame = frame
        self.threshold = threshold

    def check(self, globaldict):
        '''True if globaldict['CURRENT_FRAME'].value in list
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        fmt = "%Y-%m-%d %H:%M:%S"
        if not globaldict['ACQU'].value:
            if int(globaldict['CURRENT_FRAME'].value) >= self.frame:
                if datetime.strptime(datetime.now().strftime(fmt), fmt) < (globaldict['REAL_TIME_NEXT_FRAME'].value - timedelta(0, self.threshold)):
                    return True
        return False


class Current_timeleft_under_threshold:
    '''triggers if current frame is in list of frames
    '''
    def __init__(self, frame, threshold):
        '''Args:
            list_frame(list): a list of int
        '''
        self.frame = frame
        self.threshold = threshold

    def check(self, globaldict):
        '''True if globaldict['CURRENT_FRAME'].value in list
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        fmt = "%Y-%m-%d %H:%M:%S"
        if not globaldict['ACQU'].value:
            if int(globaldict['CURRENT_FRAME'].value) < self.frame:
                if datetime.strptime(datetime.now().strftime(fmt), fmt) < (globaldict['REAL_TIME_NEXT_FRAME'].value - timedelta(0, self.threshold)):
                    return True
        return False
