'''
Created on Dec 13, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import time
from math import ceil
import threading
import serial
from MicroMator.FileManagement import logger, getmdp
#custom events effect should be a class of there own. each effect needs to have a act function. this function is called by the manager.py file

MODULE = 'Event_effect'

def sleep_(time_, globaldict):
    '''custom sleep function
    Args:
        time(float)
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
      example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
    '''
    for _ in range(int(time_)):
        time.sleep(1)
        if globaldict['ABORT'].value:
            return


class Disable_channel():
    '''Event Disables use of a channel
    '''
    def __init__(self, channel_list, index):
        '''Args:
            channel_list(list): the list of channels
            index(int): the index of the channel in the channel_list
        '''
        self.channel = channel_list
        self.index = index

    def act(self, globaldict):
        '''method changes .use field to False
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        self.channel[self.index].use = False
        logger('disabling channel {}'.format(self.channel[self.index].name), module=MODULE)


class Use_DMD_InBio:
    def __init__(self, protocol):
        self.protocol = protocol

    def act(self, globaldict):
        logger('will use DMD all pixels instead of direct light', module=MODULE)
        for channel in self.protocol.channel_list:
            if 'DIRECT' in channel.name:
                channel.name = channel.name.replace('DIRECT', 'DMD')


class Change_acqInterval:
    '''will change the acqInterval field with given value
    '''
    def __init__(self, field, value):
        '''Args:
            data(object): the location of the stored data data field
            value(any): the new value wished to replace
        '''
        self.field = field
        self.value = value

    def act(self, globaldict):
        '''this simple method will change the the acqInterval field to the new value
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        logger('changed the field {} with value {} '.format(self.field, self.value), message_type='warning', module=MODULE)
        globaldict[self.field].value = self.value


class Generic_change_protocol:
    '''this class changes a field in the protocol object
    '''
    def __init__(self, arguments):
        '''Args:
            data(object): the location of the stored data data field
            value(any): the new value wished to replace
        '''
        self.data = arguments['data']
        self.value = arguments['value']

    def act(self, globaldict):
        '''this effector method changes a field in the protocol object
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        logger('warning you are changing the main acquisition loop', message_type='warning', module=MODULE)
        self.data = self.value
        logger('changed the field {} with value {} '.format(self.data, self.value), message_type='normal', module=MODULE)


class Change_Exposure_BF:
    '''this class changes the exposure
    '''
    def __init__(self, micromator, value):
        '''Args:
            micromator(instance): micromator instance
            value(float): the new exposure value
        '''
        self.micromator = micromator
        self.value = value

    def act(self, globaldict):
        '''this effector changes the exposure
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        logger('warning you are changing the main acquisition loop', message_type='warning', module=MODULE)
        logger('changed the Exposure({}) to new value {} '.format(self.micromator.protocol.brightfield[0].exposure, self.value), message_type='normal', module=MODULE)
        self.micromator.protocol.brightfield[0].exposure = self.value


class Remove_position:
    '''this effector method removes a position from the Acquisition loop
    '''
    def act(self, position, globaldict):
        '''this effector method removes a position from the Acquisition loop
        Args:
            position(object): the Position object
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        position.use = False
        position.cellstar = False
        logger('stopped acquisition for pos {} '.format(position), module=MODULE)


class Take_picture:
    '''this effector method takes a picture
    '''
    def __init__(self, channel, acq_engine):
        '''Args:
            channel(object) : the channel wished to use for pic
            acq_engine(object): the microscope scripting object
        '''
        self.channel = channel
        self.acq_engine = acq_engine

    def act(self, position, globaldict):
        '''this effector method takes a picture
        Args:
            position(object): the Position object
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        logger('taking EVENT picture for pos{} '.format(position.index), module=MODULE)
        self.acq_engine.mmc.waitForSystem()
        self.acq_engine.set_position(position)
        self.acq_engine.channel_changer(self.channel, position.index, self.acq_engine.current_frame, event=True)
        self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')
        self.acq_engine.mmc.waitForSystem()


class Open_channel:
    '''this effector method opens channel
    '''
    def __init__(self, channel, acq_engine):
        '''Args:
            channel(object) : the channel wished to use for pic
            acq_engine(object): the microscope scripting object
        '''
        self.channel = channel
        self.acq_engine = acq_engine

    def act(self, globaldict):
        '''this effector method opens channel
        Args:
            position(object): the Position object
            globaldict(dict): the dict with all the global signals
        '''
        logger('Exposed light from channel {} at exposure {} '.format(self.channel.name, self.channel.exposure), module=MODULE)
        logger(self.channel, module=MODULE)
        self.acq_engine.mmc.enableContinuousFocus(True)
        self.acq_engine.mmc.setConfig(self.acq_engine.config, self.channel.name)
        self.acq_engine.mmc.waitForConfig(self.acq_engine.config, self.channel.name)
        self.acq_engine.mmc.setExposure(self.channel.exposure)
        threading.Thread(target=self.acq_engine.mmc.snapImage, args=()).start()
        self.acq_engine.mmc.waitForSystem()


class Open_channel_dynamic:
    '''this effector method opens channel
    '''
    def __init__(self, channel, acq_engine):
        '''Args:
            channel(object) : the channel wished to use for pic
            acq_engine(object): the microscope scripting object
        '''
        self.channel = channel
        self.acq_engine = acq_engine

    def act(self, globaldict):
        '''this effector method opens channel
        Args:
            position(object): the Position object
            globaldict(dict): the dict with all the global signals
        '''
        self.channel.exposure = (globaldict['TIME_TO_NEXT_FRAME'].value-globaldict['EXPTIMER'].value)*1000
        logger('EVENT: Exposed light from channel {} at exposure {} '.format(self.channel.name, self.channel.exposure), module=MODULE)
        self.acq_engine.mmc.enableContinuousFocus(True)
        self.acq_engine.mmc.setConfig(self.acq_engine.config, self.channel.name)
        self.acq_engine.mmc.waitForConfig(self.acq_engine.config, self.channel.name)
        self.acq_engine.mmc.setShutterOpen(True)
        sleep_(self.channel.exposure/1000-5000, globaldict)
        self.acq_engine.mmc.setShutterOpen(False)

class Open_channel_allpos:
    '''opens channel in all pos
    '''
    def __init__(self, channel, acq_engine, all_positions):
        '''Args:
            all_positions(list of Positions): list of all Position
            channel(object) : the channel wished to use for pic
            acq_engine(object): the microscope scripting object
        '''
        self.all_positions = all_positions
        self.channel = channel
        self.acq_engine = acq_engine

    def act(self, globaldict):
        '''opens channel in all pos
        Args:
            globaldict(dict): the dict with all the global signals
        '''
        #logger('EVENT: Exposed light from channel {} on all pos at exposure {} '.format(self.channel.name, self.channel.exposure))
        for position in self.all_positions:
            self.acq_engine.mmc.waitForSystem()
            self.acq_engine.set_position(position)
            self.acq_engine.channel_manager(self.channel, position.index, globaldict['CURRENT_FRAME'].value, save=True, event=True)
            self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')


class Open_channel_allpos_dynamic_exposure:
    '''opens channel in all pos
    '''
    def __init__(self, channel, acq_engine, all_positions, globaldict):
        '''Args:
            all_positions(list of Positions): list of all Position
            channel(object) : the channel wished to use for pic
            acq_engine(object): the microscope scripting object
        '''
        self.all_positions = all_positions
        self.channel = channel
        self.acq_engine = acq_engine

    def act(self, globaldict):
        '''opens channel in all pos
        Args:
            globaldict(dict): the dict with all the global signals
        '''
        self.channel.exposure = globaldict['ABSOLUTE_TIME_TO_NEXT_FRAME'].value*1000
        logger('EVENT: Exposed light from channel {} on all pos at exposure {} '.format(self.channel.name, self.channel.exposure), module=MODULE)
        for position in self.all_positions:
            self.acq_engine.mmc.waitForSystem()
            self.acq_engine.set_position(position)
            self.acq_engine.channel_manager(self.channel, position.index, globaldict['FRAME'].value, save=True, event=True)
            self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')

class Arduino_blue_light():
    '''will shine blue light for the given amount of time
    '''
    def __init__(self, arguments):
        '''Args:
            intensity(int): between 0 (off) and 255 (full light)
            time(any): the amount of light per 1min : 0 (always off) - 127(30sec on then 30sec off) - 255(always on)
        '''
        self.intensity = arguments['intensity']
        self.time = arguments['time']

    def act(self, globaldict):
        '''this simple method will change the the acqInterval field to the new value
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        logger('Shining blue from arduino at intensity {}% for {}% of 1min'.format(round(100*self.intensity/255, 2), 100*self.time/255), module=MODULE)
        ard = serial.Serial('COM13', 9600, timeout=10)
        time.sleep(2)
        ard.write(bytearray([self.intensity, self.time]))
        while not globaldict['END_ACQ'].value:
            if globaldict['ACQU'].value:
                logger('turning off Arduino', module=MODULE)
                ard.write(bytearray([0, 0]))
                ard.close()
                return
        logger('turning off Arduino', module=MODULE)
        ard.write(bytearray([0, 0]))
        ard.close()


class Load_dmd_mask():
    '''will load the mask given
    '''
    def __init__(self, acq_engine, image, DMDdevice, channel, save=False):
        '''Args:
            acq_engine(object): the microscope scripting object
            image(str): full name & path to image that will be a dmd mask
            DMDdevice(object): the DMD device object
            channel(object or list of object): the channel wished to use for pic
            save(bool): if true will save image as event
        '''
        self.image = image
        self.DMD = DMDdevice
        self.channel = channel
        self.acq_engine = acq_engine
        self.save = save

    def act(self, globaldict):
        '''Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
            example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        import MicroMator.Microscope.dmd as dmd
        if not isinstance(self.channel, list):
            self.channel = [self.channel]
        for channel in self.channel:
            if not channel.exposure:
                channel.exposure = (globaldict['TIME_TO_NEXT_FRAME'].value-globaldict['EXPTIMER'].value)*1000
            self.acq_engine.mmc.setConfig(self.acq_engine.config, channel.name)
            self.acq_engine.mmc.waitForConfig(self.acq_engine.config, channel.name)
            self.acq_engine.mmc.setShutterOpen(True)
            mask = dmd.Mask(self.DMD, dmd.load_mask(self.image))
            if channel.exposure > 200*1000:
                for _ in range(ceil(channel.exposure/(200*1000))):
                    if globaldict['ABORT'].value:
                        return
                    self.DMD.displaymask(200*1000, mask)
                    sleep_(200, globaldict)
            else:
                self.DMD.displaymask(channel.exposure, mask)
                if self.save:
                    self.acq_engine.mmc.snapImage()
                    img = self.acq_engine.mmc.getImage()
                    self.acq_engine.save_(0, globaldict['CURRENT_FRAME'].value, img, channel, event=True, zslicenum=None)


class Load_dmd_mask_duty_cycle():
    '''will load the mask given
    '''
    def __init__(self, acq_engine, image, DMDdevice, channel, duty_cycle):
        '''Args:
            acq_engine(object): the microscope scripting object
            image(str): full name & path to image that will be a dmd mask
            DMDdevice(object): the DMD device object
            channel(object): the channel wished to use for pic
            duty_cycle(int): the invteral between each dmd mask in sec
        '''
        self.image = image
        self.DMD = DMDdevice
        self.channel = channel
        self.acq_engine = acq_engine
        self.duty_cycle = duty_cycle

    def act(self, globaldict):
        '''Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
            example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        import MicroMator.Microscope.dmd as dmd
        self.acq_engine.mmc.setShutterDevice('pE4000-DMD')
        self.acq_engine.mmc.setAutoShutter(True)
        if not self.channel.exposure:
            self.channel.exposure = (globaldict['TIME_TO_NEXT_FRAME'].value-globaldict['EXPTIMER'].value)
        self.acq_engine.mmc.setConfig(self.acq_engine.config, self.channel.name)
        self.acq_engine.mmc.waitForConfig(self.acq_engine.config, self.channel.name)
        mask = dmd.Mask(self.DMD, dmd.load_mask(self.image))
        self.DMD.displaymask(self.channel.exposure, mask)
        otime_ = 0
        time_ = 0
        while otime_ < globaldict['TIME_TO_NEXT_FRAME'].value:
            if globaldict['END_ACQ'].value:
                return
            if time_ == self.duty_cycle:
                self.acq_engine.mmc.setConfig(self.acq_engine.config, self.channel.name)
                self.acq_engine.mmc.waitForConfig(self.acq_engine.config, self.channel.name)
                self.DMD.displaymask(self.channel.exposure, mask)
                time_ = 0
            if time_ == self.channel.exposure/1000:
                self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')
            otime_ += 1
            time_ += 1
            time.sleep(1)
        if not globaldict['ACQU'].value:
            self.acq_engine.mmc.setShutterOpen(False)
            self.acq_engine.mmc.setConfig(self.acq_engine.config, 'IDLE')


class Send_email:
    '''will send an email
    '''
    def __init__(self, email, path, folder_manager_obj):
        '''Args:
            email(str): the email to be sent at
        '''
        self.email = email
        self.path = path
        self.folder_manager_obj = folder_manager_obj

    def act(self, globaldict):
        '''this effector changes the exposure
        Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        logger('sent report email to {}'.format(self.email), module=MODULE)
        import smtplib, ssl
        from email.mime.text import MIMEText
        from email.mime.multipart import MIMEMultipart
        port = 465  # For SSL
        smtp_server = "smtp.gmail.com"
        sender_email = "micromator.test@gmail.com"
        receiver_email = self.email
        mdp = getmdp(self.path)
        subject = "MicroMator frame {} report".format(globaldict["CURRENT_FRAME"].value)
        body = "Here is MicroMator's frame #{} report".format(globaldict["CURRENT_FRAME"].value)
        message = MIMEMultipart()
        message["From"] = sender_email
        message["To"] = receiver_email
        message["Subject"] = subject
        message["Bcc"] = receiver_email
        message.attach(MIMEText(body, "plain"))
        filename = os.path.join(self.folder_manager_obj.micromator_log_path)
        with open(filename, "r") as attachment:
            attachment = MIMEText(attachment.read())
            attachment.add_header('Content-Disposition', 'attachment', filename=filename)
            message.attach(attachment)
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
            server.login(sender_email, mdp)
            server.sendmail(sender_email, receiver_email, text)