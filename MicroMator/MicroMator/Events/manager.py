'''
Created on Jul 5, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import time
import threading

LOCK = threading.Lock()


class Event:
    '''this class regroups all the methods tied to the event system
    '''
    def __init__(self, trigger, effector, globaldict, trigger_type='1', name=None, all_events_busy_signal=None):
        '''Args:
            trigger(class): this is the trigger function the user wishes to tie to the event
                the only requirement a trigger has is it needs a check(*args, globaldict) function that must return True/False.
            effector(class): this is the effector function the user wishes to tie to the event
                the only requirement an effect has is an act(*args, globaldict) function.
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
                example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
            trigger_type(str): one of 2 trigger types :
                - 1: corresponding to 'a Trigger can only trigger once per frame'
                - C: corresponding to 'a Trigger can trigger infnite amount of times per frame'
            name(str)(optional): the name of the event
        '''
        self.effector = effector
        self.trigger = trigger
        self.trigger_type = trigger_type
        self.globaldict = globaldict
        self.name = name
        if all_events_busy_signal:
            self.all_events_busy_signal = all_events_busy_signal[0]
            self.event_index = all_events_busy_signal[1]
        else:
            self.all_events_busy_signal = all_events_busy_signal

    def call(self, position=None):
        '''this is the method that checks for the trigger countinously regardless of frame or
        whether it has been previously triggered or not
        '''
        trigger = False
        current_frame = self.globaldict['CURRENT_FRAME'].value
        while not self.globaldict['END_ACQ'].value and current_frame == self.globaldict['CURRENT_FRAME'].value:
            #time.sleep(0.1)
            #check if trigger
            if position:
                trigger = self.trigger.check(position, self.globaldict)
            else:
                trigger = self.trigger.check(self.globaldict)
            print(self.name, self.globaldict['CURRENT_FRAME'].value, trigger)
            if trigger:
                if self.all_events_busy_signal:
                    while any(self.all_events_busy_signal):
                        pass
                    self.all_events_busy_signal[self.event_index] = True
                #do effect
                if position:
                    self.effector.act(position, self.globaldict)
                else:
                    self.effector.act(self.globaldict)
            if self.all_events_busy_signal:
                self.all_events_busy_signal[self.event_index] = False
            if self.trigger_type == '1':
                break
            trigger = False

    def looper(self, list_positions):
        '''this method creates and returns a list of trigger threads with index
        Args:
            list_positions(list of Positions object): this list of all the positions saved at the start of the acqu
        '''
        trigger_threads = []
        if 'position' in self.trigger.check.__code__.co_varnames:
            for position in list_positions:
                if position.use:
                    trigger_threads.append(threading.Thread(target=self.call, args=(position, )))
        else:
            trigger_threads.append(threading.Thread(target=self.call))
        return trigger_threads
