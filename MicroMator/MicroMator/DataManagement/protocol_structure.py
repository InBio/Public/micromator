'''
Created on 14 Aug 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''

class Channel():
    '''this Class regroups all the information concerning 1 Channel
    '''
    def __init__(self, index, name, exposure, fluo=False, skipFactorFrame=-1, picture=True, dmd=False, event=False, path=False):
        '''Args:
        index(int): the index/order in which the channel is in the acquisition loop
        name(str): the name of the channel
        exposure(float): the Exposure time of the channel
        fluo(Bool)(optional): a tag to know if the channel is linked to fluorescence observation - defalut False
        skipFactorFrame(int)(optional): uses this channel only every skipFactorFrame frames
        picture(Bool)(optional): a tag that can be used to associate a channel with a compulsory picture
        dmd(Bool)(optional): a tag that lets micromator know if a channel is tied to the use of an SLM device
        '''
        if not path:
            self.path = name
        else:
            self.path = path
        self.name = name
        self.index = index
        self.exposure = exposure
        self.fluo = fluo
        self.skip_factor_frame = skipFactorFrame
        self.count_to_skip = {}
        self.picture = picture
        self.dmd = dmd
        self.use = True
        self.event = event

    def check_channel_dmd(self, name_list_dmd):
        '''this method will check if a channel needs the dmd tag or not
        Args:
            name_list_dmd(list): list of substrings in name that can make so this method can identify whether it's dmd or not
        '''
        if self.name.lower() in name_list_dmd:
            self.dmd = True

    def __str__(self):
        '''prints the object according to a specified format
        '''
        if self.skip_factor_frame != -1:
            return 'channel: {}. {} | exposure: {} | fluo: {} | SkipFactorFrame: {}'.format(self.index, self.name, self.exposure, self.fluo, self.skip_factor_frame)
        return 'channel: {}. {} | exposure: {} | fluo: {}'.format(self.index, self.path, self.exposure, self.fluo)


class Protocol():
    '''the class stores all the data related to the acquition protocol
    '''
    def __init__(self, timestamp, acquorder, acqNumframes, acqInterval, channel_list, positions_list, dmd, track):
        '''Args:
            timestamp(int): the current time
            acquorder(str): the acuqisition order (TPC, PTC or DEBUG)
            acqNumframes(int): the total # of frames
            acqInterval(int): the interval in s between each frame
            channel_list(list of Channel objects): the list of channels used during the acquisition
            positions_list(Positions object): the list of positions used during the acquisition
            dmd(bool): True if DMD is enabled
            track(bool): True if Tracking is enabled
        '''
        self.timestamp = timestamp
        self.acquorder = acquorder
        self.acqNumframes = acqNumframes
        self.acqInterval = acqInterval
        self.channel_list = channel_list
        self.slices = []
        self.relativeZSlice = False
        self.skipAutofocusCount = 0
        self.brightfield = []
        self.fluochannels = []
        self.channelGroup = None
        if channel_list:
            self.sort_channels()
        self.positions_list = positions_list
        self.dmd = dmd
        self.track = track

    def sort_channels(self):
        '''separates fluo channels from "brightfield" ones
        '''
        for channel in self.channel_list:
            if channel.fluo:
                self.fluochannels.append(channel)
            else:
                self.brightfield.append(channel)

    def __str__(self):
        '''prints the object according to a specified format
        '''
        return 'timestamp: {} | acquorder: {} | acqNumframes: {} | acqInterval: {} sec | positions_list: {} | Channels:{}'.format(self.timestamp,
            self.acquorder, self.acqNumframes, self.acqInterval, [pos.label for pos in self.positions_list], [str(channel) for channel in self.channel_list])
