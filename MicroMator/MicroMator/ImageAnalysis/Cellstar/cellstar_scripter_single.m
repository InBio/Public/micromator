function a = cellstar(PosAcqupath, backgroundpath, analysispath, current_frame, analysis_func, brightfield, fluochannel, binning)
    %% Args:
    %     PosAcqupath(str): the path to the Images
    %     analysispath(str): the path to the segements folder
    %     current_frame(int): the current frame img_0_BF.tif so 0 for example
    %     analysis_func(string): the users custom DefaultParameters function
    %     brightfield(str): since my images are names img_0_BF.tif need to give 'BF'
    %     fluochannel(str): fluochannel name img_0_RHOD-DIRECT.tif so 'RHOD-DIRECT' for example
    a=1;
    tic
    debuglevel = 1;
    %% logging file setup
    loggingfile = strcat(analysispath,'cellstar.log');
    fileID = fopen(loggingfile, 'a');
    setGloballogging(fileID);
    %% giving location of images
    destDirSeg = fullfile(analysispath);
    PrintMsg(debuglevel,1,'Init done!');
    %% parameter setup
    defaultparameters = str2func(char(analysis_func));
    parameters = defaultparameters(); %Users modify the file or uses parameter.foo = bar to change what they want
    parameters.debugLevel = debuglevel; %goes up to 6
    parameters.interfaceMode= 'batch';
    parameters.files.destinationDirectory = destDirSeg;
    parameters.files.background.imageFile = fullfile(backgroundpath, 'background.png');
    if ~ isfile(fullfile(backgroundpath, 'background.png'));
      parameters.files.background.imageFile = fullfile(analysispath, 'background.png');
    end
    %% filepaths
    if binning
      imgfile = strcat('binned_img_',char(string(current_frame)),'_', brightfield,'.tif');
    else
      imgfile = strcat('img_',char(string(current_frame)),'_', brightfield,'.tif');
    end
    imgfilepath = fullfile(PosAcqupath, imgfile);
    segfile = {imgfilepath};
    segfiles = {};

    if current_frame ~0;
        if binning
          imgfile_1 = strcat('binned_img_',char(string(current_frame-1)),'_', brightfield,'.tif');
          imgfilepath_1 = fullfile(PosAcqupath, imgfile_1);
          segfiles(1) = {imgfilepath};
          imgfile_0 = strcat('binned_img_',char(string(current_frame)),'_', brightfield,'.tif');
          imgfilepath_0 = fullfile(PosAcqupath, imgfile_0);
          segfiles(2) = {imgfilepath_0};
        else
          imgfile_1 = strcat('img_',char(string(current_frame-1)),'_', brightfield,'.tif');
          imgfilepath_1 = fullfile(PosAcqupath, imgfile_1);
          segfiles(1) = {imgfilepath};
          imgfile_0 = strcat('img_',char(string(current_frame)),'_', brightfield,'.tif');
          imgfilepath_0 = fullfile(PosAcqupath, imgfile_0);
          segfiles(2) = {imgfilepath_0};
        end
    end

    parameters.files.imagesFiles = segfile;
    pause(2); %to make sure fluo image is taken
    %% Find the fluochannel
    if fluochannel{1} ~false;
        for i = 1:length(fluochannel)
            parameters.files.additionalChannels{i}.fileMap = {'regexp', '_BF', fluochannel{i}};
        end
    end
    parameters = CompleteParameters(parameters); % This is to be run to be sure that nothing is missing, and to process regular expressions in file names
    PrintMsg(debuglevel,1,'config done!');
    %% Segment pictures
    results = RunSegmentation(parameters);
    PrintMsg(debuglevel,1,'Segmentation done!');
    %% Run the tracking
    if current_frame ~0;
        disp(imgfilepath_1);
        disp(imgfilepath_0);
        parameters.files.imagesFiles = [{imgfilepath_0}, {imgfilepath_1}];
    end
    disp('Tracking strating...');
        %tracking = ComputeFullTracking(parameters);
    disp('Tracking done!');

    delete(gcp('nocreate'));
    time = toc;
    PrintMsg(debuglevel,1,['elapsed time ',num2str(time)]);
    PosAcqupath = PosAcqupath(1:end-2);
    [filepath,name] = fileparts(PosAcqupath);
    PrintMsg(debuglevel,1,['CellStar analysis done with ', name])
end
