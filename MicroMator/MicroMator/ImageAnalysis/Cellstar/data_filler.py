'''
Created on 11 june 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import errno
import numpy as np
import scipy.io as sio
import MicroMator.ImageAnalysis.Cellstar.meta_tracker as meta_tracker
import  MicroMator.ImageAnalysis.Cellstar.cellstar_data_extractor as cdt

def loadmat(matfile):
    '''loads the tracking .mat generated from cellStar and extracts the traces
    Args:
        mat(.mat file): the .mat file that stores the tracking matrix
    '''
    trackmat = sio.loadmat(matfile)
    trackingtraces = trackmat['tracking']['traces']
    return trackingtraces[0][0]

def test_track(trackingmatpath, frame):
    trackingmat = os.path.join(trackingmatpath, 'tracking.mat')
    trackingtraces = loadmat(trackingmat)
    metatracker = os.path.join(trackingmatpath, 'traces{}.txt'.format(frame))
    np.savetxt(metatracker, trackingtraces, fmt='%d')

class Datafiller(object):
    '''the class will fill the data after extraction of the segmentation & tracking of the image
    '''
    def __init__(self, folder_manager_obj, frame_object_list, position, frame, protocol, brightfield):
        '''Args:
        folder_manager_obj(object): the instance of the Folder_manager object
        frame_object_list(list of Frame objects): the list of Frame objects
        frame(int): the current frame
        position(int): the index of the current position
        protocol(Protocol object): the protocol object that contains the information related to the experiement Acquisition loop when Image was taken
        '''
        self.analysis_path = folder_manager_obj.analysis_path
        self.bitmaps_path = folder_manager_obj.bitmaps_path
        self.frame_object_list = frame_object_list
        self.position = position
        self.positions_object = protocol.positions_list
        self.frame = frame
        self.protocol = protocol
        test_track(os.path.join(self.analysis_path, 'segments'+str(position), 'tracking'), frame)
        if protocol.track:
            self.protocol.track = meta_tracker.TrackerExtractor(self.analysis_path, position, self.protocol.acqNumframes)
        self.data_extractor = cdt.CellstarDataExtractor(self.analysis_path, self.positions_object, brightfield)
        self.segmentmat = self.data_extractor.get_cellstar_segment(self.frame, self.position)
        #self.sep = separator.Separator(self.segmentmat)

    def bitmap_folder_creater(self):
        '''function creates the custom bitmap folder if already existing, does nothing
        '''

        try:
            os.makedirs(self.bitmaps_path)
        except OSError as err:
            if err.errno != errno.EEXIST:
                raise

    def extract_track_data(self):
        '''this method calls the tracker functions that create the meta tracker table
        '''
        trackmat = self.data_extractor.get_cellstar_tracking(self.position)
        self.protocol.track.create_meta_tracker(self.protocol.acqNumframes, trackmat, self.frame)
        meta_trackmat = self.protocol.track.allframesmat
        return meta_trackmat

    def create_masks(self):
        '''this method calls all the relevant methods of the cell_separator object to create the individual cell masks
        '''
        self.bitmap_folder_creater()
        bitpath = os.path.join(self.bitmaps_path, 'pos{}'.format(self.position), 'frame{}'.format(self.frame))

    def analyse(self):
        '''this method call all the relevant function for a full extraction of the various wanted analysed data
        '''
        meta_trackmat = None
        if self.protocol.track:
            meta_trackmat = self.extract_track_data()
        if self.protocol.dmd:
            self.create_masks()
        self.data_extractor.fill_frame_structure(self.positions_object[self.position], self.frame_object_list[self.frame],
                                                 self.segmentmat, meta_trackmat, self.protocol.fluochannels, self.protocol)
