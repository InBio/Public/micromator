# -*- coding: utf-8 -*-
'''
Created on Sep 20, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import numpy as np
from PIL import Image
import scipy.io as sio


def get_segmat(matpath):
    '''loads the matlab file with the segments
    Args:
        matpath(str): the path to the the segmentation matlab .mat
    '''
    mat = sio.loadmat(matpath)
    segmat = mat['segments']
    return segmat

def extract_single_segments(segmat, bitmaps_path):
    '''extracts all the cells and saves them in seperate bitmap images
    Args:
        segmat(array): a numpy array (get_segmat output)
        bitmaps_path(string): full path to where bitmaps will be saved
    '''
    shape = segmat.shape
    number_cells = len(np.unique(segmat))
    for cell in range(number_cells):
        bit_image = segmat == cell
        bit_image = np.ascontiguousarray(bit_image, dtype=bool)
        bit_image = np.packbits(bit_image, axis=1)
        if cell == 0:
            bit_image = np.invert(bit_image)
            img = Image.frombytes(mode='1', size=shape, data=bit_image)
            img.save(os.path.join(bitmaps_path, 'all_cells.bmp'))
        else:
            img = Image.frombytes(mode='1', size=shape, data=bit_image)
            img.save(os.path.join(bitmaps_path, 'cell_{}.bmp'.format(cell)))


if __name__ == '__main__':
    segmat = get_segmat('D:\\lifeware\\Experiments\\StevenFletcher\\DMD_test\\2019-20-9_DMD_multi_mask_2\\Analysis\\segments0\\binned_img_0_BF_segmentation.mat')
    extract_single_segments(segmat, 'C:\\Users\\lifeware\\Desktop\\test\\pos0')
