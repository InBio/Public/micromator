function b = cellstar_parallel_looper(listcellstarpos, acqupath, analysispath, current_frame, parameters, brightfield, fluochannel, binning)
% this function will make a loop that call the cellstar_scripter for each position in the list
% Args:
%     listcellstarpos(list): a list of the positions that are cellstared
%     analysispath(str): the path to the Acquistition folder
%     parameters(function): a function
%     totalframes(int): the total number of frames no know when to end while loop
%     fluochannel(str): '_' + the name of the fluorescence channel if there is one
%     binning(bool): if true will use binned images to analyse
b=1;
parfor i=listcellstarpos;
    pos = strcat('\pos',string(i));
    if binning
      PosAcqupath = strcat(analysispath,'\Binned_images',pos,'\\');
      disp(PosAcqupath);
    else
      PosAcqupath = strcat(acqupath,pos,'\\');
      disp(PosAcqupath);
    end
    segment = strcat('\segments',string(i));
    Analysispath = strcat(analysispath,segment,'\\');
    backgroundpath = strcat(analysispath,'\Background','\\');
    cellstar_scripter_single(char(PosAcqupath), char(backgroundpath), char(Analysispath), current_frame, char(parameters), brightfield, fluochannel, binning);
end
