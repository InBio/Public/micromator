'''
Created on Apr 6, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import numpy as np
import scipy.io as sio
from MicroMator.FileManagement import logger

def column(matrix, i):
    '''extracts column from matrix
    '''
    return [row[i] for row in matrix]

def loadmat(matfile):
    '''loads the tracking .mat generated from cellStar and extracts the traces
    Args:
        mat(.mat file): the .mat file that stores the tracking matrix
    '''
    trackmat = sio.loadmat(matfile)
    trackingtraces = trackmat['tracking']['traces']
    return trackingtraces[0][0]

def extend_matrix(totalframes, frame):
    '''Extends the first frame and creates the fillable matrix
    Args:
        totalframes(int): number of frames in total
        frame(array): the current array
    '''
    allframes = np.zeros((len(frame), totalframes), dtype=int)
    ext_size = totalframes-1
    for i in range(len(frame)):
        a = np.append(frame[i], [0]*ext_size)
        allframes[i] = a
    return allframes


class TrackerExtractor(object):
    '''this class regroups the methods that are used to link the tracking data from one frame to another
    '''
    def __init__(self, acqufolder, position, totalframes):
        '''Args:
            acqufolder(str): the location of the images
            position(int): the current position
            totalframes(int): the total number of frames in the experiment
        '''
        self.position = position
        self.trackingmatpath = os.path.join(acqufolder, 'segments'+str(position), 'tracking')
        self.trackingmat = os.path.join(self.trackingmatpath, 'tracking.mat')
        trackingtraces = loadmat(self.trackingmat)
        self.allframesmat = extend_matrix(totalframes, trackingtraces).tolist()

    def savemat(self):
        '''the method will save the meta tracker matrix
        '''
        logger('saving tracker matrix for position {}'.format(self.position), message_type='normal', print_=False)
        metatracker = os.path.join(self.trackingmatpath, 'meta_tracker.txt')
        np.savetxt(metatracker, self.allframesmat, fmt='%d') #save as unsigned int

    def create_meta_tracker(self, totalframes, currentframe, currentframenum):
        '''This method adds the tracking mat to the global tracker matrix (2 frames vs all the frames together)
        Args:
            totalframes(int): total number of frames set by the user
            currentframe(numpyarray): the current output from the tracking of cellstar
            allframes(list) = the list that has all the traces
            currentframenum(int) = the current frame number (starts at 0)
        '''
        col = column(self.allframesmat, currentframenum-1) #extracts the column from the global matrix
        for row in currentframe.tolist(): #loops the current frame array (transformed into a list)
            if row[0] == 0:
                self.allframesmat.append([0]*totalframes)
                self.allframesmat[-1][currentframenum] = row[1]
            else:
                if row[0] not in col:
                    self.allframesmat.append([0]*totalframes)
                    self.allframesmat[-1][currentframenum] = row[0]
                else:
                    index = col.index(row[0])
                    self.allframesmat[index][currentframenum] = row[1]
        self.savemat()


if __name__ == '__main__':
    T = TrackerExtractor('D:\\lifeware\\Experiments\\StevenFletcher\\CellstarTests\\tracking matrixes\\0.07', 0, 3)
    mat = loadmat('D:\\lifeware\\Experiments\\StevenFletcher\\CellstarTests\\tracking matrixes\\0.07\\tracking1.mat')
    T.create_meta_tracker(3, mat, 1)
    print('\n\n')
    mat = loadmat('D:\\lifeware\\Experiments\\StevenFletcher\\CellstarTests\\tracking matrixes\\0.07\\tracking2.mat')
    T.create_meta_tracker(3, mat, 2)
