# -*- coding: utf-8 -*-
'''
Created on Aug 10, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import sys
import time
import threading
import errno
import ntpath
import logging
import matlab.engine
from MicroMator.FileManagement import logger, makedirs_
import MicroMator.ImageAnalysis.Cellstar.data_filler as df

MODULE = 'CellStar_Analysis'

class Analysis():
    '''this class regroups the methods that are tied to cellstar in Micromator
    '''
    def __init__(self, micromator):
        '''Args:
            micromator(MicroMator object): the instance of the MicroMator for the experiment
        '''
        self.brightfield = micromator.protocol.brightfield[0].name
        self.folder_manager = micromator.folder_manager_obj
        logging.basicConfig(filename=self.folder_manager.micromator_log_path, level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')
        self.protocol = micromator.protocol
        self.binning = micromator.options["MICROMANAGER_PARAMETERS"]['binning']
        # userpath = os.path.join(os.path.expanduser('~/Documents/MicroMator/'), micromator.user)
        # parameters = os.path.join(userpath, 'DefaultParameters.m')
        # self.analysis_parameters = parameters
        # self.analysis_parameters = micromator.options["ANALYSIS_SOFTWARE"]["parameters"]["file"]
        # self.analysis_parameters = ntpath.basename(self.analysis_parameters).split('.')[0]
        self.analysis_parameters = 'DefaultParametersMicromator'
        self.frame_object_list = micromator.frame_object_list
        self.globaldict = micromator.globaldict
        self.file_creator()
        logger('MATLAB engine loading...', module=MODULE)
        self.engine = matlab.engine.start_matlab()
        self.engine.parpool()
        logger('MATLAB engine loaded', module=MODULE)
        self.fluochannels = self.fluorescence(self.protocol.fluochannels)
        logger('fluochannels are {}'.format(self.fluochannels), module=MODULE)
        if self.protocol.acquorder == 'TPC':
            self.looper = self.looper_tpc
        elif self.protocol.acquorder == 'PTC':
            self.looper = self.looper_ptc

    def file_creator(self):
        '''this method creats subfolders & it's logs for cellstar
        '''
        for position in self.protocol.positions_list:
            path = os.path.join(self.folder_manager.analysis_path, 'segments{}'.format(position.index))
            logfile = os.path.join(path, 'cellstar.log')
            makedirs_(path)
            try:
                open(logfile, 'a').close()
            except OSError as err:
                if err.errno != errno.EEXIST: #check Experiments folder exist
                    raise

    def looper_tpc(self):
        '''this method loops over all the positions & frames
        '''
        frame = 0
        while frame <= self.protocol.acqNumframes-1 and not self.globaldict['END_ACQ'].value:
            while not self.globaldict['FRAME'].value and not self.globaldict['END_ACQ'].value: #wait loop
                time.sleep(0.2)
            if self.globaldict['END_ACQ'].value:
                self.engine.quit()
                sys.exit()
                return
            list_analyzed_positions = []
            for position in self.protocol.positions_list:
                list_analyzed_positions.append(position.index)
            analysis_thread = threading.Thread(target=self.do_analysis, args=(frame, matlab.double(list_analyzed_positions)), daemon=True)
            analysis_thread.start()
            while not self.globaldict['ANALYZED'].value:
                time.sleep(0.2)
            for position in self.protocol.positions_list:
                list_analyzed_positions.append(position.index)
                if position.analyse:
                    fill_data_thread = threading.Thread(target=self.fill_data, args=(frame, position))
                    fill_data_thread.start()
            frame += 1
            self.globaldict['FRAME'].value = False
            self.globaldict['ANALYZED'].value = False

    def looper_ptc(self):
        '''calls the analysis software when needed (IE .mat is written on disk)
        '''
        for position in self.protocol.positions_list:
            list_analyzed_positions = []
            frame = 0
            list_analyzed_positions.append(position.index)
            while frame <= self.protocol.acqNumframes-1 and not self.globaldict['END_ACQ'].value:
                while not self.globaldict['FRAME'].value and not self.globaldict['END_ACQ'].value:
                    time.sleep(0.2)
                if self.globaldict['END_ACQ'].value:
                    self.engine.quit()
                    sys.exit()
                    return
                analysis_thread = threading.Thread(target=self.do_analysis, args=(frame, matlab.double(list_analyzed_positions)), daemon=True)
                analysis_thread.start()
                while not self.globaldict['ANALYZED'].value:
                    time.sleep(0.2)
                if position.analyse:
                    fill_data_thread = threading.Thread(target=self.fill_data, args=(frame, position))
                    fill_data_thread.start()
                frame += 1
                self.globaldict['FRAME'].value = False
                self.globaldict['ANALYZED'].value = False

    def do_analysis(self, current_frame, list_analyzed_positions):
        '''this method will construct the image path & name and proceed to do the analysis
        Args:
            current_frame(int): the current frame the acquisition is at
            list_analyzed_positions(object): the position wished to be analysed
        '''
        acqpath = self.folder_manager.acquisitions_path
        analysis_path = self.folder_manager.analysis_path
        self.engine.cellstar_parallel_looper(list_analyzed_positions,
                                             acqpath,
                                             analysis_path,
                                             current_frame,
                                             self.analysis_parameters,
                                             self.brightfield,
                                             self.fluochannels,
                                             self.binning)
        self.globaldict['ANALYZED'].value = True

    def fluorescence(self, fluochannels):
        '''this method will check if there are fluorescent channels
        Args:
            fluochannels(list of Channels): lis of fluo Channels for analysis
        '''
        fluorescence = []
        for channel in fluochannels:
            fluorescence.append('_{}'.format(channel.name))
        if not fluorescence:
            fluorescence = [False]
        return fluorescence

    def fill_data(self, current_frame, position):
        '''this method will call the data_filler functions to fill the frame_structure
        Args:
            current_frame(int): the current frame the acquisition is at
            position(object): the position wished to be analysed
        '''
        filename = 'img_{}_{}_segmentation.mat'.format(current_frame, self.brightfield)
        filepath = os.path.join(self.folder_manager.analysis_path, 'segments{}'.format(position.index), filename)
        if os.path.isfile(filepath):
            df.Datafiller(self.folder_manager, self.frame_object_list, position.index, current_frame, self.protocol, self.brightfield).analyse()
