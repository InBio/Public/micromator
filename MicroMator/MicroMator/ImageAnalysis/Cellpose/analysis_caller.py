import os
import numpy as np
import logging
from MicroMator.FileManagement import logger, makedirs_
import pims
import imageio as io
from cellpose import models
import pandas as pd
from PIL import Image
from skimage.draw import polygon
from skimage import measure

MODULE = 'Cellpose_analysis'

class Analysis():
    '''this class regroups the different methods to do call the image analysis with Cellpose
    '''
    def __init__(self, micromator):
        '''Args:
            micromator(MicroMator object): the instance of the MicroMator for the experiment
        '''
        self.micromator = micromator
        logging.basicConfig(filename=self.micromator.folder_manager_obj.micromator_log_path, level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')
        self.globaldict = micromator.globaldict
        self.binned = micromator.options['MICROMANAGER_PARAMETERS']['binning']
        if self.binned:
            self.image_path = micromator.folder_manager_obj.binned_images
        else:
            self.image_path = micromator.folder_manager_obj.acquisitions_path
        self.protocol = micromator.protocol
        self.analysis_parameters = micromator.options['ANALYSIS_SOFTWARE']['parameters']
        self.bf = self.micromator.options['MICROMANAGER_PARAMETERS']['brightfield']
        self.model_path = self.analysis_parameters['model']
        self.input_size = (self.analysis_parameters['input_size_x'], self.analysis_parameters['input_size_y'], self.analysis_parameters['depth'])
        self.fluo_channel = self.analysis_parameters['fluo_channel']
        self.newchannels = self.analysis_parameters['new_channels']
        if not self.fluo_channel:
            logger('will not analyse fluo channels', module=MODULE)
        else:
            logger('will analyse fluo channels', module=MODULE)
        if self.newchannels:
            logger('will analyse new channels', module=MODULE)
        else:
            logger('will not analyse new channels', module=MODULE)
        self.list_all_masks = []
        self.list_channels = []
        self.replace_frame_data = []
        self.col_names = 'mean,min,max,median,var,sum,background,area'.split(',')
        for pos, _ in enumerate(self.micromator.positions):
            self.replace_frame_data.append({})
            for channel in self.micromator.protocol.channel_list:
                if channel.path != self.bf:
                    self.replace_frame_data[pos][channel.path] = dict((ele, []) for ele in self.col_names)
        for _ in self.protocol.positions_list:
            self.list_all_masks.append([])
        for channel in self.micromator.protocol.channel_list:
            self.list_channels.append(channel.path)
            
        self.model = models.Cellpose(gpu=models.use_gpu(), model_type='cyto')

    def do_analysis(self, current_frame, position, seg_track, fluo_channel=True):
        '''this method will construct the image path & name and proceed to do the analysis
        Args:
            current_frame(int): the current frame the acquisition is at
            position(object): the position wished to be analysed
        '''
        self.globaldict['ANALYSIS_PER_POS'][position.index].value = False
        self.globaldict['ANALYZED'].value = False
        pos = position.index
        logger('--------------------------------------------- Cellpose Pos: {} ---------------------------------------------'.format(pos), print_=True, module=MODULE)
        if self.binned:
            images_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'binned_img_*_{}.tif'.format(self.bf))
        else:
            images_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'img_*_{}.tif'.format(self.bf))
        mask_path = os.path.join(self.micromator.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos))

        if self.newchannels:
            print("newchannels = True, taking fluo images for segmentation")
            if self.binned:
                images_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'binned_img_*_{}.tif'.format("YFP-DIRECT"))
            else:
                images_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'img_*_{}.tif'.format("YFP-DIRECT"))
        # images_path = r"E:\Experiments\AchilleFraisse\Images for Zach\BF_*.tif"

        images = pims.ImageSequence(images_path)
        cellpose_path = os.path.join(self.micromator.folder_manager_obj.analysis_path, 'Cellpose', 'pos{}'.format(pos))
        track_file_name = os.path.join(cellpose_path, 'track.csv')
        makedirs_(cellpose_path)

        logger('segmenting img frame {}...'.format(current_frame), module=MODULE, frame=current_frame, pos=pos)

        masks, flows, styles, diams = self.model.eval(images[current_frame], diameter=15, flow_threshold=None, channels=[0,0])

        fname = os.path.join(mask_path, 'mask_{}_{}.tif'.format(current_frame, self.bf))
        io.imwrite(fname, masks.astype(np.uint16))
        logger('...finished segmenting imgs frame {}'.format(current_frame), module=MODULE, frame=current_frame, pos=pos)

        contours, positions = self.get_mask_edges(masks)
        logger('{} particles in frame found'.format(len(contours)), module=MODULE, frame=current_frame, pos=pos)

        data_ = pd.DataFrame()
        data_['contours'] = contours
        data_['frame'] = [current_frame for i in range(len(contours))]
        data_['positions'] = [np.array(i) for i in positions]
        if fluo_channel:
            data_ = self.go_through_channels(data_,1,pos,current_frame)
        if current_frame:
            csv_data = pd.read_csv(track_file_name, index_col=[0])
            csv_data = csv_data.append(data_, sort=True)
            csv_data.to_csv(track_file_name)
        else:
            data_.to_csv(track_file_name)       
    
        logger('...finished Cellpose', module=MODULE, frame=current_frame, pos=pos)
        self.globaldict['ANALYZED'].value = True
        self.globaldict['ANALYSIS_PER_POS'][position.index].value = True

    def looper(self):
        '''this method loops over all the positions & frames
        '''
        frame = 0
        while frame <= self.protocol.acqNumframes-1 and not self.globaldict['END_ACQ'].value:
            while not self.globaldict['FRAME'].value: #wait loop
                if self.globaldict['END_ACQ'].value:
                    return
            for position in self.protocol.positions_list:
                if not position.analyse:
                    pass
                else:
                    self.do_analysis(frame, position, None, fluo_channel=self.fluo_channel)
                    self.globaldict['ANALYSIS_PER_POS'][position.index].value = True
            frame += 1
            self.globaldict['FRAME'].value = False

    def go_through_channels(self, data_, seg_track, pos, current_frame):
        '''method goes through the additional channels and call functions to calculate stats
        '''
        # if self.newchannels:
        #     self.update_current_channels(pos, current_frame)
        for channel in self.list_channels:
            if channel == self.bf:
                continue
            if self.binned:
                chimage_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'binned_img_{}_{}.tif'.format(current_frame, channel))
                if 'EVENT' in channel:
                    chimage_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'binned_img_{}_{}.tif'.format(current_frame-1, channel))
            else:
                chimage_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'img_{}_{}.tif'.format(current_frame, channel))
                if 'EVENT' in channel:
                    chimage_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'img_{}_{}.tif'.format(current_frame-1, channel))
            data_ = self.add_fluolevel_to_tracking_single_frame(data_, chimage_path, channel)
            logger('calculating fluo channel stats {}'.format(channel), module=MODULE, frame=current_frame, pos=pos)
        return data_

    def add_fluolevel_to_tracking_single_frame(self, tracking_data, nimage, channel):
        ''' add statistics
        '''
        stat_list = []
        channel_mean = []
        channel_min = []
        channel_max = []
        channel_median = []
        channel_var = []
        channel_sum = []
        channel_background = []
        channel_area = []
        if os.path.isfile(nimage):
            nimage = Image.open(nimage)
            npimage = np.asarray(nimage)
        else:
            npimage = np.zeros((self.input_size[0:2]))
        for i in range(len(tracking_data['contours'])):
            cell = tracking_data['contours'][i]
            mean_, min_, max_, median_, var_, sum_, background_, area_ = self.go_through_pixels(npimage, cell)
            channel_mean.append(mean_)
            channel_min.append(min_)
            channel_max.append(max_)
            channel_median.append(median_)
            channel_var.append(var_)
            channel_sum.append(sum_)
            channel_background.append(background_)
            channel_area.append(area_)
        names = '{0} mean,{0} min,{0} max,{0} median,{0} var,{0} sum,{0} background,{0} area'.format(channel).split(',')
        stat_list = [channel_mean, channel_min, channel_max, channel_median, channel_var, channel_sum, channel_background, channel_area]
        for i, name in enumerate(names):
            tracking_data[name] = stat_list[i]
        return tracking_data

    def go_through_pixels(self, image, contour):
        '''function goes through pixels in mask and returns info (mean, median, etc...)
        '''
        row, col = polygon(contour[:, 0], contour[:, 1])
        if len(row) > 0:
            max_row = np.max(row)
            im_crop = image[:max_row+1, :]
            blank_crop = np.ones(im_crop.shape)
            try:
                list_pixels = image[(row), (col)]
                blank_crop[row, col] = 0
            except:
                print('unable to crop')
                list_pixels = np.zeros(image.shape)
                blank_crop = np.zeros(im_crop.shape)
            mean = np.mean(list_pixels)
            min_ = np.min(list_pixels)
            max_ = np.max(list_pixels)
            median = np.median(list_pixels)
            if mean:
                cvar = np.std(list_pixels)/mean
            else:
                cvar = float('nan')
            sum_ = np.sum(list_pixels)
            background_ = np.mean(blank_crop*im_crop)
            area_ = row.shape[0]
            return mean, min_, max_, median, cvar, sum_, background_, area_
        return -1, -1, -1, -1, -1, -1, -1, -1
    
    def get_mask_edges(self, mask_img):
        '''For a given segmentation image (i.e. a binary image of masks),
        this function gets the edges of the mask (a bunch of x,y coordinates),
        and their center, defined as the mean of x and y coordinates.
        '''
        zs = []
        contours = measure.find_contours(mask_img, 0)
        for n, contour in enumerate(contours):
            zs.append(np.mean(contour, axis=0))
        return contours, np.array(zs)

