'''
Created on Oct 10, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import logging
import glob
import pandas as pd
import trackpy as tp
import pims
import segmator.seg_mator as sm
import segmator.vis as vis
from MicroMator.FileManagement import logger, makedirs_

MODULE = 'SegMator_analysis'


class Analysis():
    '''this class regroups the different methods to do call the image analysis software SegMator
    '''
    def __init__(self, micromator):
        '''Args:
            micromator(MicroMator object): the instance of the MicroMator for the experiment
        '''
        self.micromator = micromator
        logging.basicConfig(filename=self.micromator.folder_manager_obj.micromator_log_path, level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')
        self.globaldict = micromator.globaldict
        self.binned = micromator.options['MICROMANAGER_PARAMETERS']['binning']
        if self.binned:
            self.image_path = micromator.folder_manager_obj.binned_images
        else:
            self.image_path = micromator.folder_manager_obj.acquisitions_path
        self.protocol = micromator.protocol
        self.analysis_parameters = micromator.options['ANALYSIS_SOFTWARE']['parameters']
        self.bf = self.micromator.options['MICROMANAGER_PARAMETERS']['brightfield']
        self.model_path = self.analysis_parameters['model']
        self.input_size = (self.analysis_parameters['input_size_x'], self.analysis_parameters['input_size_y'], self.analysis_parameters['depth'])
        self.fluo_channel = self.analysis_parameters['fluo_channel']
        self.newchannels = self.analysis_parameters['new_channels']
        if not self.fluo_channel:
            logger('will not analyse fluo channels', module=MODULE)
        else:
            logger('will analyse fluo channels', module=MODULE)
        if self.newchannels:
            logger('will analyse new channels', module=MODULE)
        else:
            logger('will not analyse new channels', module=MODULE)
        self.list_all_masks = []
        self.list_channels = []
        self.replace_frame_data = []
        self.col_names = 'mean,min,max,median,var,sum,background,area'.split(',')
        for pos, _ in enumerate(self.micromator.positions):
            self.replace_frame_data.append({})
            for channel in self.micromator.protocol.channel_list:
                if channel.path != self.bf:
                    self.replace_frame_data[pos][channel.path] = dict((ele, []) for ele in self.col_names)
        for _ in self.protocol.positions_list:
            self.list_all_masks.append([])
        for channel in self.micromator.protocol.channel_list:
            self.list_channels.append(channel.path)

    def update_current_channels(self, pos, current_frame):
        '''goes through channels on disk and adds them to channel_list (Event channels for example)
        '''
        if self.binned:
            list_files = glob.glob(os.path.join(self.image_path, 'pos{}'.format(pos), 'binned_img_{}_*.tif'.format(current_frame-1)))
        else:
            list_files = glob.glob(os.path.join(self.image_path, 'pos{}'.format(pos), 'img_{}_*.tif'.format(current_frame-1)))
        for channel in list_files:
            channel = channel.split('.')[0].split('_')[-1]
            if channel not in self.list_channels:
                if channel != self.bf:
                    logger('found new channel {}, adding to list of analyzed channels'.format(channel), module=MODULE)
                    self.list_channels.append(channel)
            self.replace_frame_data[pos][channel] = dict((ele, []) for ele in self.col_names)

    def go_through_channels(self, data_, seg_track, pos, current_frame):
        '''method goes through the additional channels and call functions to calculate stats
        '''
        if self.newchannels:
            self.update_current_channels(pos, current_frame)
        for channel in self.list_channels:
            if channel == self.bf:
                continue
            if self.binned:
                chimage_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'binned_img_{}_{}.tif'.format(current_frame, channel))
                if 'EVENT' in channel:
                    chimage_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'binned_img_{}_{}.tif'.format(current_frame-1, channel))
            else:
                chimage_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'img_{}_{}.tif'.format(current_frame, channel))
                if 'EVENT' in channel:
                    chimage_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'img_{}_{}.tif'.format(current_frame-1, channel))
            data_ = seg_track.add_fluolevel_to_tracking_single_frame(data_, chimage_path, channel)
            self.replace_frame_data[pos] = seg_track.add_fluolevel_per_frame(data_, current_frame, channel, self.replace_frame_data[pos])
            logger('calculating fluo channel stats {}'.format(channel), module=MODULE, frame=current_frame, pos=pos)
            frame_data = pd.DataFrame(self.replace_frame_data[pos][channel])
            frame_data.to_csv(self.frame_file_name.format(channel))
        return data_

    def do_analysis(self, current_frame, position, seg_track, fluo_channel=True):
        '''this method will construct the image path & name and proceed to do the analysis
        Args:
            current_frame(int): the current frame the acquisition is at
            position(object): the position wished to be analysed
        '''
        self.globaldict['ANALYSIS_PER_POS'][position.index].value = False
        self.globaldict['ANALYZED'].value = False
        pos = position.index
        logger('--------------------------------------------- SegMator Pos: {} ---------------------------------------------'.format(pos), print_=True, module=MODULE)
        if self.binned:
            images_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'binned_img_*_{}.tif'.format(self.bf))
        else:
            images_path = os.path.join(self.image_path, 'pos{}'.format(pos), 'img_*_{}.tif'.format(self.bf))
        mask_path = os.path.join(self.micromator.folder_manager_obj.bitmaps_path, 'pos{}'.format(pos))
        images = pims.ImageSequence(images_path)
        segmator_path = os.path.join(self.micromator.folder_manager_obj.analysis_path, 'SegMator', 'pos{}'.format(pos))
        track_file_name = os.path.join(segmator_path, 'track.csv')
        self.frame_file_name = os.path.join(segmator_path, 'frame_stats_{}.csv')
        makedirs_(segmator_path)
        seg_track = sm.SegMator(images_path, mask_path, self.model_path, segmator_path, input_size=self.input_size)
        logger('segmenting img frame {}...'.format(current_frame), module=MODULE, frame=current_frame, pos=pos)
        img, masks, countour = seg_track.segment_single_frame(images[current_frame], list(self.input_size[:2]), filename='mask_{}_{}.tif'.format(current_frame, self.bf))
        self.list_all_masks[pos].append(masks)
        logger('...finished segmenting imgs frame {}'.format(current_frame), module=MODULE, frame=current_frame, pos=pos)
        logger('{} cells in frame found'.format(len(countour)), module=MODULE, frame=current_frame, pos=pos)
        seg_track.track_single_frame_v2(masks, current_frame)
        logger('tracked imgs ...', module=MODULE, frame=current_frame, pos=pos)
        with tp.PandasHDFStore(os.path.join(segmator_path, seg_track.tracking_h5name)) as data:
            tracking_data = data.dump()
        if current_frame:
            csv_data = pd.read_csv(track_file_name, index_col=[0])
            data_ = pd.DataFrame(tracking_data.loc[tracking_data.frame == current_frame])
        else:
            data_ = tracking_data
        data_ = seg_track.add_contour_to_tracking_single_frame(data_, masks, images[current_frame])
        if fluo_channel:
            data_ = self.go_through_channels(data_, seg_track, pos, current_frame)
        vis.save_segmented_image(img, countour, frame=current_frame, save_path=segmator_path)
        vis.save_mask(img, countour, frame=current_frame, save_path=segmator_path, tracking_data=data_)
        if current_frame:
            csv_data = csv_data.append(data_, sort=True)
            csv_data.to_csv(track_file_name)
        else:
            data_.to_csv(track_file_name)
        logger('...finished SegMator', module=MODULE, frame=current_frame, pos=pos)
        self.globaldict['ANALYZED'].value = True
        self.globaldict['ANALYSIS_PER_POS'][position.index].value = True

    def looper(self):
        '''this method loops over all the positions & frames
        '''
        frame = 0
        while frame <= self.protocol.acqNumframes-1 and not self.globaldict['END_ACQ'].value:
            while not self.globaldict['FRAME'].value: #wait loop
                if self.globaldict['END_ACQ'].value:
                    return
            sm.reset_keras(self)
            for position in self.protocol.positions_list:
                if not position.analyse:
                    pass
                else:
                    self.do_analysis(frame, position, None, fluo_channel=self.fluo_channel)
                    self.globaldict['ANALYSIS_PER_POS'][position.index].value = True
            frame += 1
            self.globaldict['FRAME'].value = False
