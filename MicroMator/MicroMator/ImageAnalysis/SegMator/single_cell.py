
# -*- coding: utf-8 -*-
'''
Created on Oct 17, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import re
import os
import time
from PIL import Image
from datetime import datetime, timedelta
import random
from collections import defaultdict, Counter
from operator import add, sub
import numpy as np
import pandas as pd
from skimage.draw import polygon
from scipy.ndimage import morphology
import MicroMator.FileManagement as fm
from numpy.linalg import norm #added for the lineage sorter

MODULE = 'single cell'

class CellSorter:
    '''class deals with assigning cells to buckets
    '''
    def __init__(self, folder_manager_obj, path_df_cells, data_path, frame, globaldict, pos=None, subsort=None, subsortargs=None):
        '''Args:
            folder_manager_obj(obj): the folder_manager instance
            path_df_cells(str): path to sorted cells file
            data_path(str): the path to the track.csv
            frame(int): the current frame number
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
                                example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        self.frame = frame
        self.data, self.list_cells = read_track_data(data_path, self.frame, globaldict, pos)
        self.particle_counter = Counter(self.data.particle.sort_values())
        self.num_cells = len(self.list_cells)
        self.path_df_cells = path_df_cells
        self.folder_manager_obj = folder_manager_obj
        self.df_cell_ids = []
        self.pos = pos
        self.globaldict = globaldict
        if subsort:
            self.subsort = getattr(self, subsort)
            self.subsortargs = subsortargs
        else:
            self.subsort = subsort

    def lineage_sorter(self, distance=100, channel=None, treshold=None):
        t0 = time.time()
        fm.logger('[lineage sorter] starting', module=MODULE, frame=self.frame, pos=self.pos)
        print('[lineage sorter] starting frame {0} pos {1}'.format(self.frame, self.pos))
        '''goes through all the cells and deals with the new cells by adding them if they are far from other selected cells or from other over-treshold cells
            Args:
             distance(int): minimal distance to other chosen cells
             channel: to test the treshold
             threshold(int): above this value in channel cells are considered activated
        '''
        def compute_distance(X, n):
            distance = np.zeros((X.shape[0], n))
            for k in range(n):
                row_norm = norm(X - X[k, :], axis=1)
                distance[:, k] = row_norm
            return distance
        X = np.array([[list(self.data.loc[self.data.frame == self.frame].x)[i], list(self.data.loc[self.data.frame == self.frame].y)[i]] for i in range(self.num_cells)])
        actual_list_cells = list(self.list_cells)
        if not os.path.exists(self.path_df_cells):
            distance_matrix = compute_distance(X, self.num_cells)
            self.df_cell_ids = [[], []]
            untested_cells = actual_list_cells.copy()
            done = 0
            while untested_cells:
                cell = untested_cells.pop()
                neighbours = []
                n_unwanted_cells = 0
                k = 0
                while k < len(actual_list_cells):
                    other_cell = actual_list_cells[k]
                    if other_cell != cell and distance_matrix[actual_list_cells.index(cell), k] < distance:
                        neighbours.append(other_cell)
                        if other_cell in self.df_cell_ids[0]:
                            n_unwanted_cells += 1
                        elif channel and treshold and int(self.data.loc[(self.data.frame == self.frame) & (self.data.particle == cell)][channel]) > treshold:
                            n_unwanted_cells += 1
                    k += 1
                if n_unwanted_cells == 0:
                    self.df_cell_ids[0].append(cell)
                    untested_cells = diff(untested_cells, neighbours)
                done += 1
            self.df_cell_ids[1] = diff(self.list_cells, self.df_cell_ids[0])
            np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
            self.save_sorting_per_frame()
            fm.logger('found {} cells'.format(len(self.list_cells)), module=MODULE, frame=self.frame, pos=self.pos)
            print('[lineage sorter] found {0} cells, selected {1} cells'.format(len(self.list_cells), len(self.df_cell_ids[0])))
            if self.subsort:
                self.subsort(*self.subsortargs)
            return
        self.df_cell_ids = np.load(self.path_df_cells, allow_pickle=True)
        self.df_cell_ids = self.df_cell_ids.tolist()
        concat_list = [j for i in self.df_cell_ids for j in i]
        non_sorted_cells = diff(self.list_cells, concat_list)
        if non_sorted_cells:
            fm.logger('found {} new cells among the total {} cells'.format(len(non_sorted_cells), len(self.list_cells)), module=MODULE, frame=self.frame, pos=self.pos)
            print('[lineage sorter] found {} new cells among the total {} cells'.format(len(non_sorted_cells), len(self.list_cells)))
            distance_matrix = compute_distance(X, self.num_cells)
            untested_cells = non_sorted_cells.copy()
            done = 0
            while untested_cells:
                cell = untested_cells.pop()
                neighbours = []
                n_unwanted_cells = 0
                k = 0
                while k<len(actual_list_cells):
                    other_cell = actual_list_cells[k]
                    if other_cell != cell and distance_matrix[actual_list_cells.index(cell), k] < distance:
                        neighbours.append(other_cell)
                        if other_cell in self.df_cell_ids[0]:
                            n_unwanted_cells += 1
                        elif channel and treshold and int(self.data.loc[(self.data.frame == self.frame) & (self.data.particle == cell)][channel]) > treshold:
                            n_unwanted_cells += 1
                    k += 1
                if n_unwanted_cells == 0:
                    self.df_cell_ids[0].append(cell)
                    untested_cells = diff(untested_cells, neighbours)
                done += 1
            np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
        else:
            fm.logger('no new cells found...', module=MODULE, frame=self.frame, pos=self.pos)
            print('Lineage sorter frame {0} pos {1} no new cells found...'.format(self.frame, self.pos))
        fm.logger('[lineage sorter] over, took {0} seconds'.format(time.time() - t0), module=MODULE, frame=self.frame, pos=self.pos)
        print('[lineage sorter] over frame {0} pos {1}, took {2} seconds'.format(self.frame, self.pos, time.time() - t0))
        if self.subsort:
            self.subsort(*self.subsortargs)
        self.save_sorting_per_frame()

    def external_sorter(self):
        '''no sort, just read
        '''
        fmt = "%Y-%m-%d %H:%M:%S"
        path = os.path.join(self.folder_manager_obj.analysis_path, 'Signal_logs', 'pos{}'.format(self.pos), 'cell_ids_{}.npy'.format(self.frame))
        while not os.path.exists(path) and not self.globaldict['END_ACQ'].value:
            pass
        while True and not self.globaldict['END_ACQ'].value:
            try:
                self.df_cell_ids = [np.load(path, allow_pickle=True)]
                fm.logger('loaded the cells for event', module=MODULE, frame=self.frame, pos=self.pos)
                break
            except OSError as err:
                fm.logger(err, module=MODULE, frame=self.frame, pos=self.pos)

    def all_cells(self):
        '''gets all cells
        '''
        self.df_cell_ids = [self.data.loc[(self.data.frame == self.frame) & (self.data.y > 75)].particle]
        if self.subsort:
            self.subsort(*self.subsortargs)
        np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)

    def random_sorter(self, num_buckets, purge=False):
        '''goes through all the cells and deals with the new cells by adding them at random in any bucket
        Args:
            num_buckets(int): number of sorted buckets
            purge(bool): if True will forget past sorted cells and re randomize them
            repeat(int or None): if int will only use cells which have a lower count than repeat
        '''
        if not os.path.exists(self.path_df_cells) or purge:
            size_buckets = self.num_cells//num_buckets
            if not size_buckets:
                size_buckets = 1
            random_dist = random.sample(range(0, self.num_cells), self.num_cells)
            self.df_cell_ids = list(break_list_in_n(random_dist, num_buckets, size_buckets))
            np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
            self.save_sorting_per_frame()
            fm.logger('found {} cells'.format(len(self.list_cells)), module=MODULE, frame=self.frame, pos=self.pos)
            if self.subsort:
                self.subsort(*self.subsortargs)
            return
        self.df_cell_ids = np.load(self.path_df_cells, allow_pickle=True)
        self.df_cell_ids = self.df_cell_ids.tolist()
        concat_list = [j for i in self.df_cell_ids for j in i]
        non_sorted_cells = diff(self.list_cells, concat_list)
        random.shuffle(non_sorted_cells)
        if non_sorted_cells:
            chunks = np.array_split(non_sorted_cells, num_buckets)
            random.shuffle(chunks)
            fm.logger('found {} new cells among the total {} cells'.format(len(non_sorted_cells), len(self.list_cells)), module=MODULE, frame=self.frame, pos=self.pos)
            for index, chunk in enumerate(chunks):
                oldcells = self.df_cell_ids[index]
                newcells = list(np.ndarray.tolist(chunk.astype(np.float)))
                fm.logger((type(oldcells), type(newcells)), print_=False, module=MODULE, frame=self.frame, pos=self.pos)
                allcells = oldcells + newcells
                self.df_cell_ids[index] = allcells
                #self.df_cell_ids[index] += np.ndarray.tolist(chunk.astype(np.int))
                #self.df_cell_ids[index] = self.df_cell_ids[index] + np.ndarray.tolist(chunk.astype(np.float))
                fm.logger('adding new {} cells to event channel {}'.format(len(chunk), index), module=MODULE, frame=self.frame, pos=self.pos)
            np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
        else:
            fm.logger('no new cells found...', module=MODULE, frame=self.frame, pos=self.pos)
        if self.subsort:
            self.subsort(*self.subsortargs)
        self.save_sorting_per_frame()

    def clostest_sorter(self, num_buckets):
        '''goes through all the cells and deals with the new cells by adding them at semi random
        (assignes one of the channels with a degree of variance)
        Args:
            empty_channel(bool): if True adds an off channel
            event_channels(list): list of event channels
            variance(int): degree to which the channel
            num_buckets(int)(optional): number of sorted buckets (4 is default)
        '''
        if not os.path.exists(self.path_df_cells):
            random_dist = random.sample(range(0, self.num_cells), self.num_cells)
            size_buckets = self.num_cells//num_buckets
            if not size_buckets:
                size_buckets = 1
            self.df_cell_ids = list(break_list_in_n(random_dist, num_buckets, size_buckets))
            if len(self.df_cell_ids) != num_buckets:
                self.df_cell_ids = self.df_cell_ids + [[]]*(num_buckets-len(self.df_cell_ids))
                random.shuffle(self.df_cell_ids)
            np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
            #self.save_sorting_per_frame()
            fm.logger('found {} cells'.format(len(self.list_cells)), module=MODULE, frame=self.frame, pos=self.pos)
            if self.subsort:
                self.subsort(*self.subsortargs)
            return
        self.df_cell_ids = np.load(self.path_df_cells, allow_pickle=True)
        self.df_cell_ids = self.df_cell_ids.tolist()
        concat_list = [j for i in self.df_cell_ids for j in i]
        non_sorted_cells = diff(self.list_cells, concat_list)
        fm.logger('list of non sorted cells : {}'.format(non_sorted_cells), module=MODULE, frame=self.frame, pos=self.pos)
        old_cells = self.data.loc[(self.data['frame'] == self.frame)& self.data['particle'].isin(concat_list)]
        old_cells_stacked_xy = np.dstack((old_cells['x'], old_cells['y']))[0]
        new_cells = self.data.loc[(self.data['frame'] == self.frame)& self.data['particle'].isin(non_sorted_cells)]
        new_cells_stacked_xy = np.dstack((new_cells['x'], new_cells['y']))[0]
        if non_sorted_cells:
            for index, new_cell_xy in enumerate(new_cells_stacked_xy):
                group, closest_cell, random_ = closest_node(new_cell_xy, old_cells_stacked_xy, self.df_cell_ids, old_cells)
                new_cell = int(non_sorted_cells[index])
                if not random_:
                    fm.logger('assigning new cell {} to cell {} in group {}'.format(new_cell, closest_cell, group), print_=False, module=MODULE, frame=self.frame, pos=self.pos)
                else:
                    fm.logger('closest cell not assigned... default assigning new cell {} to group {}'.format(new_cell, group),
                              print_=False, module=MODULE, frame=self.frame, pos=self.pos)
                res = group
                self.df_cell_ids[res].append(new_cell)
            np.save(self.path_df_cells, np.asarray(self.df_cell_ids), allow_pickle=True)
            fm.logger('found {} new cells among the total {} cells'.format(len(non_sorted_cells), len(self.list_cells)), module=MODULE, frame=self.frame, pos=self.pos)
        else:
            fm.logger('no new cells found...', module=MODULE, frame=self.frame, pos=self.pos)
        if self.subsort:
            self.subsort(*self.subsortargs)
        self.save_sorting_per_frame()

    def threshold_sorter(self, threshold, col, over_under='under', memory=False):
        '''goes through all the cells and sorts them by fluo_levels (mean fluo)
        Args:
            fluo_threshold(int)(optional): fluo threshold in pixel number (150 is default)
        '''
        if over_under == 'over':
            cellids = list(self.data.loc[(self.data.frame == self.frame) & (self.data[col] >= threshold)].particle)
        else:
            cellids = list(self.data.loc[(self.data.frame == self.frame) & (self.data[col] < threshold)].particle)
        if memory and os.path.exists(self.path_df_cells):
            self.df_cell_ids = np.load(self.path_df_cells, allow_pickle=True)
            self.df_cell_ids = self.df_cell_ids.tolist()
            self.df_cell_ids[0] += cellids
            self.df_cell_ids[0] = list(set(self.df_cell_ids[0]))
        else:
            self.df_cell_ids = [cellids]
        fm.logger('selecting {} cells {} {} threshold {}'.format(len(cellids), over_under, col, threshold),
                  module=MODULE, frame=self.frame, pos=self.pos)
        fm.logger(self.data.loc[(self.data.frame == self.frame) & (self.data.particle.isin(cellids))][['particle', col]],
                  print_=False, module=MODULE, frame=self.frame, pos=self.pos)
        np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
        if self.subsort:
            self.subsort(*self.subsortargs)
        self.save_sorting_per_frame()

    def fluo_sorter(self, num_buckets, fluo_channel, targets=None, purge=False):
        '''this sorter will sort new cells in the closest average bucket
        '''
        if not os.path.exists(self.path_df_cells) or purge:
            size_buckets = self.num_cells//num_buckets
            if not size_buckets:
                size_buckets = 1
            random_dist = random.sample(range(0, self.num_cells), self.num_cells)
            self.df_cell_ids = list(break_list_in_n(random_dist, num_buckets, size_buckets))
            np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
            self.save_sorting_per_frame()
            fm.logger('found {} cells'.format(len(self.list_cells)), module=MODULE, frame=self.frame, pos=self.pos)
            if self.subsort:
                self.subsort(*self.subsortargs)
            return
        self.df_cell_ids = np.load(self.path_df_cells, allow_pickle=True)
        self.df_cell_ids = self.df_cell_ids.tolist()
        if not targets:
            targets = []
            for bucket in self.df_cell_ids:
                mean = np.mean(self.data.loc[(self.data.frame == self.frame) & (self.data.particle.isin(bucket))][fluo_channel])
                targets.append(mean)
        concat_list = [j for i in self.df_cell_ids for j in i]
        non_sorted_cells = diff(self.list_cells, concat_list)
        if non_sorted_cells:
            for cell in non_sorted_cells:
                fluo = float(self.data.loc[(self.data.frame == self.frame) & (self.data.particle == cell)][fluo_channel])
                diff_ = np.abs(np.asarray(targets)-fluo)
                index = list(diff_).index(min(diff_))
                self.df_cell_ids[index].append(cell)
                fm.logger('assigning new cell {} in group {}'.format(cell, index), print_=False, module=MODULE, frame=self.frame, pos=self.pos)
            np.save(self.path_df_cells, np.asarray(self.df_cell_ids), allow_pickle=True)
        else:
            fm.logger('no new cells found...', module=MODULE, frame=self.frame, pos=self.pos)
        if self.subsort:
            self.subsort(*self.subsortargs)
        self.save_sorting_per_frame()

    def pattern_sorter(self, mask_path, memory=False, invert=False):
        '''if cell in pattern then turn it on
        Args:
            fluo_threshold(int)(optional): fluo threshold in pixel number (150 is default)
        '''
        img_array = np.asarray(Image.open(mask_path).convert('L'))
        coords = np.nonzero(img_array)
        data_now = self.data.loc[self.data.frame == self.frame]
        xys = [data_now.x.round(), data_now.y.round()]
        cellids = []
        for particle in data_now.particle:
            if img_array[int(data_now.loc[data_now.particle == particle].y.round()), int(data_now.loc[data_now.particle == particle].x.round())]:
                cellids.append(particle)
        if memory and os.path.exists(self.path_df_cells):
            self.df_cell_ids = np.load(self.path_df_cells, allow_pickle=True)
            self.df_cell_ids = self.df_cell_ids.tolist()
            self.df_cell_ids[0] += cellids
            self.df_cell_ids[0] = list(set(self.df_cell_ids[0]))
        else:
            self.df_cell_ids = [cellids]
        if not invert:
            fm.logger('selecting {} cells inside mask'.format(len(cellids)), module=MODULE, frame=self.frame, pos=self.pos)
        else:
            fm.logger('selecting {} cells outside mask'.format(len(cellids)), module=MODULE, frame=self.frame, pos=self.pos)
        fm.logger(self.data.loc[(self.data.frame == self.frame) & (self.data.particle.isin(cellids))][['particle', 'x', 'y']], print_=False, module=MODULE, frame=self.frame, pos=self.pos)
        np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
        if self.subsort:
            self.subsort(*self.subsortargs)
        self.save_sorting_per_frame()

    def repeats_subsorter(self, repeat):
        '''function removes particles that appear more than repeat
        '''
        for index, bucket in enumerate(self.df_cell_ids):
            data_bucket = self.data.loc[self.data.particle.isin(bucket)]
            particle_counter = Counter(data_bucket.particle.sort_values())
            kept_cells = [key for key, value in particle_counter.items() if value < repeat]
            fm.logger('removing from bucket {} cells {}'.format(index, diff(self.df_cell_ids[index], kept_cells)), print_=True, module=MODULE, frame=self.frame, pos=self.pos)
            self.df_cell_ids[index] = kept_cells
        np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)

    def threshold_subsorter(self, threshold, col, over_under='under', buckets=[0]):
        '''method removes cell that are over or below a certain threshold of a certain column
        '''
        notcellids_path = os.path.join(self.folder_manager_obj.analysis_path, 'pos{}'.format(self.pos), 'not_cell_ids.npy')
        if self.frame:
            notcellids = np.load(notcellids_path, allow_pickle=True)
        else:
            notcellids = None
        for index in buckets:
            data_bucket = self.data.loc[(self.data.particle.isin(self.df_cell_ids[index])) & (self.data.frame == self.frame)]
            if notcellids:
                data_bucket = self.data.loc[(self.data.particle.isin(self.df_cell_ids[index])) & (self.data.frame == self.frame) & (~self.data.particle.isin(notcellids))]
            if over_under == 'over':
                cellids = list(data_bucket.loc[data_bucket[col] >= threshold].particle)
                notcellids = list(data_bucket.loc[data_bucket[col] < threshold].particle)
            else:
                cellids = list(data_bucket.loc[data_bucket[col] < threshold].particle)
                notcellids = list(data_bucket.loc[data_bucket[col] >= threshold].particle)
            fm.logger('removing {} cells for bucket {} (see logs for details)...'.format(len(data_bucket.particle)-len(cellids), index), module=MODULE, frame=self.frame, pos=self.pos)
            fm.logger('keeping cells {} for bucket {}'.format(cellids, index), print_=False, module=MODULE, frame=self.frame, pos=self.pos)
            self.df_cell_ids[index] = cellids
        np.save(self.path_df_cells, self.df_cell_ids, allow_pickle=True)
        np.save(notcellids_path, notcellids, allow_pickle=True)

    def save_sorting_per_frame(self):
        '''debug function to save per frame sorted cells
        '''
        test = os.path.join(self.folder_manager_obj.analysis_path, 'Signal_logs', 'pos{}'.format(self.pos))
        fm.makedirs_(test)
        np.save(os.path.join(test, 'cell_ids_{}.npy'.format(self.frame)), self.df_cell_ids, allow_pickle=True)

def read_track_data(data_path, frame, globaldict, pos):
    '''gets the data from track csv
    Args:
        data_path(str): the path to the track.csv
        frame(int): the current frame number
    '''
    while not os.path.exists(data_path):
        pass
    time.sleep(2)
    while not globaldict['ANALYSIS_PER_POS'][pos].value:
        pass
    data = pd.read_csv(data_path, engine='python')
    list_cells = data.loc[(data['frame'] == frame)]
    list_cells = list_cells['particle']
    return data, list_cells

def break_list_in_n(random_dist, num_buckets, size_buckets):
    '''breaks list in n parts
    '''
    final = [random_dist[i * size_buckets:(i + 1) * size_buckets] for i in range((len(random_dist) + size_buckets - 1) // size_buckets)]
    if len(final) != num_buckets:
        index = np.random.randint(len(final)-1)
        final[index] = final[index] + final[-1]
        final = final[:-1]
    return final

def diff(li1, li2):
    '''get != between 2 lists'''
    return list(set(li1) - set(li2))

def fill_cell(row, col, mask):
    '''function takes contour and returns single cell bitmap
    Args:
        countour(array): coordinates that form the contour of a cell
        mask(array): full sized image mask (np.zeros)
    '''
    mask[row, col] = 255
    return mask

def make_multi_cells_mask(data_path, frame, pos, shape, list_cell_ids, area=1):
    '''function goes through list of cell ids and outputs all cells in one mask
    '''
    while not os.path.exists(data_path):
        pass
    time.sleep(2)
    data = pd.read_csv(data_path, engine='python', error_bad_lines=False)
    list_cells = data.loc[(data['frame'] == frame)]
    while list_cells.empty:
        time.sleep(1)
        list_cells = data.loc[(data['frame'] == frame)]
        try:
            data = pd.read_csv(data_path, engine='python', error_bad_lines=False)
        except (pd.errors.EmptyDataError, pd.errors.ParserError) as err:
            fm.logger(err, print_=False, module=MODULE, frame=frame, pos=pos)
    list_cells = list_cells.loc[(data['particle'].isin(list_cell_ids))]
    masks = np.zeros(shape, dtype=int)
    fm.logger('going through selected cells...', module=MODULE, frame=frame, pos=pos)
    if area != 1:
        fm.logger('eroding selected cells to area is {}% of normal size'.format(area), module=MODULE, frame=frame, pos=pos)
    eroded_contours = []
    for contour in list(list_cells.contours):
        eroded_contours.append(erode_contour(parse_contour(contour), area))
    masks = make_mask(eroded_contours, np.copy(masks))
    fm.logger('...finished selecting cells', module=MODULE, frame=frame, pos=pos)
    return masks

def erode_contour(contour, area):
    '''takes input polygon, and erodes it to a smaller size.
    '''
    mu = np.mean(contour, axis=0)
    centered_contour = contour-mu
    scaled_contour = np.round(centered_contour*area)
    if not all(np.nonzero(scaled_contour)[0]):
        scaled_contour = np.ceil(centered_contour*area)
    return scaled_contour + mu

def parse_contour(cell):
    '''return all the cell mask as on
    '''
    contour = re.findall(r"[-+]?\d*\.\d+|\d+", cell)
    contour = [float(index) for index in contour]
    recontour = np.zeros((int(len(contour)/2), 2))
    recontour[:, 0] = contour[0::2]
    recontour[:, 1] = contour[1::2]
    return recontour

def area_erode_cell(cell_in_mask, area):
    '''erodes cell to a certain fraction of its current area
    '''
    full_area = np.sum(cell_in_mask > 0)
    eroded_cell_in_mask = np.copy(cell_in_mask)
    area_ratio = 1.0
    n_iter = 0
    while area_ratio >= area:
        eroded_cell_in_mask = morphology.binary_erosion(eroded_cell_in_mask, iterations=1).astype(cell_in_mask.dtype)
        area_ratio = np.sum(eroded_cell_in_mask)/full_area
        n_iter += 1
    eroded_cell_in_mask = np.where(eroded_cell_in_mask == 1, 255, eroded_cell_in_mask)
    return eroded_cell_in_mask, n_iter

def erode_cell(full_mask, niter):
    eroded_cell_in_mask = morphology.binary_erosion(full_mask, iterations=niter).astype(full_mask.dtype)
    eroded_cell_in_mask = np.where(eroded_cell_in_mask == 1, 255, eroded_cell_in_mask)
    return eroded_cell_in_mask

def make_mask(contours, mask):
    for contour in contours:
        row, col = polygon(contour[:, 0], contour[:, 1])
        mask = fill_cell(row, col, mask)
    return mask

def get_full_cell(cell, mask, erode='area', iterations=0.25):
    '''return all the cell mask as on
    '''
    contour = re.findall(r"[-+]?\d*\.\d+|\d+", cell)
    contour = [float(index) for index in contour]
    recontour = np.zeros((int(len(contour)/2), 2))
    recontour[:, 0] = contour[0::2]
    recontour[:, 1] = contour[1::2]
    row, col = polygon(recontour[:, 0], recontour[:, 1])
    single_cell_mask = fill_cell(row, col, mask)
    single_cell_mask = area_erode_cell(single_cell_mask, iterations)
    return single_cell_mask

def closest_node(node, nodes, df_cell_ids, old_cells):
    '''gives index of closest point to list of points
    Args:
        node(tuple): an x,y coordinate
        nodes(list): list of points like so [[1,2],[3,4],[5,8]]
        df_cell_ids(dataframe): list of cell ids per bucket
        old_cells(dataframe): the sub DataFrame of only the old cells
    '''
    nodes = np.asarray(nodes)
    dist_2 = np.sum((nodes - node)**2, axis=1)
    closest_particle_index = int(np.argmin(dist_2))
    closest_particle = old_cells.iloc[closest_particle_index].particle
    for group, bucket in enumerate(df_cell_ids):
        if closest_particle in bucket:
            return group, closest_particle, False
    group = 0
    return group, None, True

def get_sublist(list_of_list):
    '''gets a sublist
    '''
    dict_ = defaultdict(list)
    for index, sublist in enumerate(list_of_list):
        for item in sublist:
            dict_[item].append(index)
    return dict_

def choose_random_ops(max_group, group, variance):
    '''gets random group
    '''
    ops = (add, sub)
    op = random.choice(ops)
    variance = random.randint(0, variance)
    result = op(group, variance)
    if result < 0:
        return 0
    elif result >= max_group:
        result = max_group-1
    return result

def get_fluo_all_cells(csv, frame, yfilter=75):
    '''cleans the csv to get only relevant cells
    '''
    csv = csv.loc[(csv.y > yfilter) & (csv.frame == frame)]['RHOD-DIRECT mean']
    mean_all_fluo = np.mean(csv)
    return csv, mean_all_fluo
