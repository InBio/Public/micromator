'''
Created on Aug 13, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import threading
import logging
import numpy as np
import pandas as pd
from skimage import io
from MicroMator.FileManagement import logger

MODULE='np_analysis'

def mean_nobg(image, threshold):
    '''mean with custom threshold to eliminate background
    Args:
        image(array): the array containing the pixel values
        threshold(int): the value under wich pixels will not be counted for the mean
    '''
    image = np.array(image, dtype=np.float64)
    return np.average(image, weights=(image > threshold))

class Analysis():
    '''this class regroups the different methods to do an very rough image analysis (pixel values etc..)
    '''
    def __init__(self, micromator):
        '''Args:
            micromator(MicroMator object): the instance of the MicroMator for the experiment
        '''
        self.micromator = micromator
        self.frame_object_list = micromator.frame_object_list
        self.protocol = micromator.protocol
        self.acqupath = micromator.folder_manager_obj.acquisitions_path
        logging.basicConfig(filename=micromator.folder_manager_obj.micromator_log_path, level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')
        self.analysis_parameters = micromator.options['ANALYSIS_SOFTWARE']['parameters']
        logger('analysis paramters are {}'.format(self.analysis_parameters), module=MODULE)
        self.globaldict = micromator.globaldict

    def do_analysis(self, current_frame, position):
        '''this method will construct the image path & name and proceed to do the analysis
        Args:
            current_frame(int): the current frame the acquisition is at
            position(object): the position wished to be analysed
        '''
        data = {}
        pixelmean = {}
        for channel in self.protocol.channel_list:
            imagename = 'img_{}_{}.tif'.format(current_frame, channel.name)
            posname = 'pos{}'.format(position.index)
            imagepath = os.path.join(self.acqupath, posname, imagename)
            image = None
            while image is None: #use this or a sure way to have current frame
                try:
                    image = io.imread(imagepath)
                except FileNotFoundError:
                    pass
            if channel.name == self.micromator.options['MICROMANAGER_PARAMETERS']["brightfield"]:
                mean = np.mean(image)
                pixelmean[channel.name] = mean
                data.setdefault(channel.name, []).append(mean)
            else:
                #custom mean to eliminate background
                mean = mean_nobg(image, self.analysis_parameters['background'])
                pixelmean[channel.name] = mean
                data.setdefault(channel.name, []).append(mean)
        pd.DataFrame(data).to_csv(os.path.join(self.micromator.folder_manager_obj.analysis_path, 'data_{}.csv'.format(position.index)), header=data.keys(), index=None)
        self.frame_object_list[current_frame].list_images[position.index] = self.frame_object_list[current_frame].update_image(position, self.protocol, None, None, pixelmean)
        logger('numpy analysis of position {} done : {}'.format(position.index, self.frame_object_list[current_frame].list_images[position.index].average_fluo), module=MODULE)

    def looper(self):
        '''this method loops over all the positions & frames
        '''
        frame = 0
        while frame <= self.protocol.acqNumframes-1 and not self.globaldict['END_ACQ'].value:
            while not self.globaldict['FRAME'].value: #wait loop
                if self.globaldict['END_ACQ'].value:
                    return
            for position in self.protocol.positions_list:
                analysis_thread = threading.Thread(target=self.do_analysis, args=(frame, position))
                analysis_thread.start()
            frame += 1
            self.globaldict['FRAME'].value = False
