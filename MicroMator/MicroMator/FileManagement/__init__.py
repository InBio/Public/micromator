'''
Created on Feb 6, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import logging
import errno
import threading
from datetime import datetime
import warnings
import json
from colorama import init, Fore, Back, Style

init(convert=True)
MODULE = 'missing module'

def logger(message, message_type='normal', print_=True, module=MODULE, frame=None, pos=None): # could be bool?
    '''this simple function prints and logs desired message!
    Args:
        message(str): the message needed to be printed and logged
        message_type(str)(optional): if 'warning' then will print & log as warning
        print_(bool)(optional): if False ewill only log and not print
        module(str)(optional): name of module that logs the message
        pos(int)(optional): position number
        frame(int)(optional): frame number
    '''
    message = str(message)
    pre_message_info = ''
    pre_log_message_info = ''
    if module:
        pre_message_info = '{} : {}'.format(Fore.GREEN + Style.BRIGHT + module + Style.RESET_ALL, pre_message_info)
        pre_log_message_info = '{} : {}'.format(module, pre_log_message_info)
    if pos is not None:
        pre_message_info = 'p{} : {}'.format(Fore.RED + Style.BRIGHT + str(pos) + Style.RESET_ALL, pre_message_info)
        pre_log_message_info = 'p{} : {}'.format(str(pos), pre_log_message_info)
    if frame is not None:
        pre_message_info = 'F{} : {}'.format(Fore.CYAN + Style.BRIGHT + str(frame) + Style.RESET_ALL, pre_message_info)
        pre_log_message_info = 'F{} : {}'.format(str(frame), pre_log_message_info)
    if print_:
        print(pre_message_info + Style.RESET_ALL + message)
    message = '{}{}'.format(pre_log_message_info, message)
    if message_type == 'normal':
        logging.info(message)
    elif message_type == 'warning':
        warnings.warn(message)
        logging.warning(message)
    elif message_type == 'error':
        logging.error(message)

def write_json(variables, path):
    '''this function will write a json file with experiment parameters
    Args:
        variables(dict): a dictionary
        path(str): a valid path with name
    '''
    with open(path, 'w') as outfile:
        json.dump(variables, outfile, indent=4, separators=(',', ': '))

def load_json(file):
    '''loads json file
    '''
    with open(file, 'r') as outfile:
        info = json.load(outfile)
    return info

def sleep_(timeout):
    '''simple sleep function
    '''
    exit_flag = threading.Event()
    while exit_flag.wait(timeout=timeout):
        pass

def makedirs_(directory):
    '''makes a directory if not already there
    '''
    try:
        os.makedirs(directory)
    except OSError as err:
        if err.errno != errno.EEXIST:#check custom experiment folder exists
            raise

def getmdp(path):
    '''gets mdp
    '''
    with open(path, 'r') as file_:
        mdp = file_.readline()
    return mdp

def save_filestatus(globaldict, path):
    '''function saves current status of MicroMator experiment
    '''
    now = datetime.now()
    savefile = os.path.join(path, '{}-{}-{}-{}h{}_savestate.json'.format(now.year, now.month, now.day, now.hour, now.minute))
    saved_status = {'START_TIME': globaldict['START_TIME'].value,
                    'TIME_TO_NEXT_FRAME': globaldict['TIME_TO_NEXT_FRAME'].value,
                    'ABSOLUTE_TIME_TO_NEXT_FRAME': globaldict['ABSOLUTE_TIME_TO_NEXT_FRAME'].value,
                    'ACQ_INTERVAL': globaldict['ACQ_INTERVAL'].value,
                    'CURRENT_POS': globaldict['CURRENT_POS'].value,
                    'CURRENT_CHANNEL': globaldict['CURRENT_CHANNEL'].value,
                    'CURRENT_FRAME': globaldict['CURRENT_FRAME'].value,
                    'TOTAL_FRAMES': globaldict['TOTAL_FRAMES'].value}
    write_json(saved_status, savefile)
