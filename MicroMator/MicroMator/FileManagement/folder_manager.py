'''
Created on Oct 23, 2017

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import datetime
import os
import errno
import logging

class Filer(object):
    '''
    this class creates all the folders needed for an experiment
    '''
    def __init__(self, options):
        '''
        Args:
            options(dict): the options tken from the .json file.
        When the folders already exist new ones are not created and the old ones are not deleted. The old ones are then used for the experiment in this rare case.
        '''
        date = datetime.datetime.now()
        self.user = options['FOLDER_PARAMETERS']['user']
        self.platform = options['FOLDER_PARAMETERS']['platform'] #used for custom options for now
        if options['FOLDER_PARAMETERS']['project_name']:
            self.project_name = options['FOLDER_PARAMETERS']['project_name']
            self.user_experience_path = os.path.join(options['FOLDER_PARAMETERS']['exp_folder'], self.user, options['FOLDER_PARAMETERS']['project_name'])
        else:
            self.user_experience_path = os.path.join(options['FOLDER_PARAMETERS']['exp_folder'], self.user)
        if options['FOLDER_PARAMETERS']['usedate']:
            self.expname = '{}-{}-{}_{}'.format(date.year, date.month, date.day, options['FOLDER_PARAMETERS']['exp_name'])
        else:
            self.expname = options['FOLDER_PARAMETERS']['exp_name']
        self.specific_experience_path = os.path.join(self.user_experience_path, self.expname) #name of the custom experiment

        self.protocols_path = os.path.join(self.specific_experience_path, 'Protocols')
        self.acquisitions_path = os.path.join(self.specific_experience_path, 'Acquisitions')
        self.math_path = os.path.join(self.specific_experience_path, 'Models')
        self.logs_path = os.path.join(self.specific_experience_path, 'Logs')
        self.analysis_path = os.path.join(self.specific_experience_path, 'Analysis')
        self.bitmaps_path = os.path.join(self.analysis_path, 'Bitmaps')
        self.background_path = os.path.join(self.analysis_path, 'Background')
        self.binned_images = os.path.join(self.analysis_path, 'Binned_images')
        self.creator()

        self.micromanager_log_path = os.path.join(self.logs_path, 'micromanager.log')
        self.logfile_generator(self.micromanager_log_path)
        self.micromanager_debug_log_path = os.path.join(self.logs_path, 'micromanager_debug.log')
        self.logfile_generator(self.micromanager_debug_log_path)

        self.micromator_log_path = os.path.join(self.logs_path, 'micromator.log')
        logging.basicConfig(filename=self.micromator_log_path, level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')

        if options['MICROFLUIDIC_PARAMETERS']:
            self.microfluidics_log_path = os.path.join(self.logs_path, 'microfluidics.log')
            self.logfile_generator(self.microfluidics_log_path)

    def creator(self):
        '''this method creates the experiment folders in which all the subsequent data
        linked to the specific experiment will be filed in
        '''
        list_paths = [self.user_experience_path,
                      self.specific_experience_path,
                      self.protocols_path,
                      self.acquisitions_path,
                      self.math_path,
                      self.logs_path,
                      self.bitmaps_path,
                      self.background_path,
                      self.analysis_path,
                      self.binned_images]
        for path in list_paths:
            try:
                os.makedirs(path)
            except OSError as err:
                if err.errno != errno.EEXIST:#check custom experiment folder exists
                    raise

    def logfile_generator(self, logfile_path):
        '''creates the logfile for the cellASIC experiments
        Args:
            logfile_path(str): where the logfile is
        '''
        try:
            open(logfile_path, 'a').close()
        except OSError as err:
            if err.errno != errno.EEXIST: #check Experiments folder exist
                raise
