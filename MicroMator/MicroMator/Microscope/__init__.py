'''
Created on Oct 30, 2017

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import threading
from MicroMator.FileManagement import logger


class StoppableThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, run, stop_event):
        '''Args:
            run(function): the function that is the target in the thread
            stop_event(Thread.Event): if Event is True, stops thread
        '''
        self.stop_event = stop_event
        self.run = run
        threading.Thread.__init__(self)

    def stop(self):
        '''sets the stop event to True
        '''
        logger('stopping thread')
        self.stop_event.set()
