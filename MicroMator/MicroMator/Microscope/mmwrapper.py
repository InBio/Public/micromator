# -*- coding: utf-8 -*-
'''
Created on Aug 2, 2017

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
from datetime import datetime, timedelta
import threading
import time
import os
import logging
import importlib.util
import glob
import errno
import fnmatch
import numpy as np
from PIL import Image
import MicroMator.FileManagement as fm
import MicroMator.Microscope.dmd as dmd
import MicroMator.Microscope.image_processor as ip
import MicroMator.Core.core_functions as core
import requests
import json
import pymmcore

MODULE = 'Scope'

# def get_MMCorePy():
#     '''loads either MMCorePy or MMCorePy3 depending on what the func finds in the micromanager location
#     '''
#     for file_ in glob.glob("*.py"):
#         if "MMCorePy" in file_:
#             mmcorepy = importlib.util.spec_from_file_location("MMCorePy", os.path.join(os.getcwd(), file_))
#             mmc = importlib.util.module_from_spec(mmcorepy)
#             mmcorepy.loader.exec_module(mmc)
#             return mmc

# MMCorePy = get_MMCorePy()


class Loader():
    '''Class loads given configuration.
    '''
    def __init__(self, MMDriverPath, configurationPath, folder_manager_obj, globaldict):
        '''
        Args:
            MMDriverPath(str): Path of Micromanager drivers.
            configurationPath(str): Path of configuration file.
            folder_manager_obj(obj): the folder_manager instance
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
                example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        fm.logger('loading Micromanager config {} ...'.format(configurationPath), module=MODULE)
        os.chdir(MMDriverPath)
        # self.mmc = MMCorePy.CMMCore()
        self.mmc = pymmcore.CMMCore()
        count = 0
        while True:
            if globaldict['END_ACQ'].value:
                return
            try:
                self.mmc.unloadAllDevices()
                self.mmc.loadSystemConfiguration(configurationPath)
                break
            except Exception as error:
                #if not count%2:
                fm.logger(error, module=MODULE)
                fm.logger('could not load configuration, please make sure all devices are turned on and no other instances of Micromanager are running...', module=MODULE)
                fm.logger('trying to load again...', module=MODULE)
                print('...')
            #count += 1
        self.configurationPath = configurationPath
        self.acquisitions_path = folder_manager_obj.acquisitions_path
        self.mmc.setPrimaryLogFile(folder_manager_obj.micromanager_log_path)
        self.mmc.startSecondaryLogFile(folder_manager_obj.micromanager_debug_log_path, True)
        self.printinfo()

    def printinfo(self): #make better and in own file?
        '''method prints basic information for the user
        '''
        fm.logger("""
                -----------------------------------------------------------------------
                                    Common information
                -----------------------------------------------------------------------
                """, module=MODULE)
        fm.logger("User {} on host {}".format(self.mmc.getUserId(), self.mmc.getHostName()), module=MODULE)
        fm.logger('getVersionInfo: {}'.format(self.mmc.getVersionInfo()), module=MODULE)
        fm.logger('getAPIVersionInfo: {}'.format(self.mmc.getAPIVersionInfo()), module=MODULE)
        fm.logger("""
                -----------------------------------------------------------------------
                                    Device information
                -----------------------------------------------------------------------
                """, module=MODULE)
        for device in self.mmc.getLoadedDevices():
            try:
                fm.logger('getDeviceDescription: {}'.format(self.mmc.getDeviceDescription(device)), module=MODULE)
                fm.logger('getDeviceLibrary: {}'.format(self.mmc.getDeviceLibrary(device)), module=MODULE)
            except pymmcore.CMMError:
                fm.logger("'{}':\tWon't work".format(device), module=MODULE)
        fm.logger("""
                -----------------------------------------------------------------------
                                    Configuration information
                -----------------------------------------------------------------------
                """, module=MODULE)
        for config in self.mmc.getAvailableConfigGroups():
            fm.logger('Config group : {}'.format(config), module=MODULE)
            fm.logger('Presets: {}'.format(self.mmc.getAvailableConfigs(config)), module=MODULE)
        fm.logger('Successfully loaded Micromanager config {}'.format(self.configurationPath), module=MODULE)


class Scripter():
    '''Class Scripts very basic Acquisitions.
    '''
    def __init__(self, protocol, folder_manager_obj, positions, dataframe, micromator_parameters, globaldict):
        '''
        Args:
            protocol(obj): the Protocol object generated using micromanager GUI
            folder_manager_obj(obj): the folder_manager instance
            position(list of postion objects): a list of position tuples (Z,X,Y)
            dataframe(list of Frame objects): the list of Frame objects that stores all the data
            micromator_parameters(dict): a dict with all the options user chose for experiment
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
                example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        logging.basicConfig(filename=folder_manager_obj.micromator_log_path, level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')
        self.globaldict = globaldict
        self.protocol = protocol
        self.globaldict['TOTAL_FRAMES'].value = self.protocol.acqNumframes
        self.dataframe = dataframe

        #Load Micromanager configuration
        if not micromator_parameters['MICROMANAGER_PARAMETERS']['Simulation']:
            self.mmc = Loader(micromator_parameters['MICROMANAGER_PARAMETERS']['micromanagerlocation'],
                              micromator_parameters['MICROMANAGER_PARAMETERS']['mmconfig'],
                              folder_manager_obj,
                              globaldict
                              ).mmc
        else:
            fm.logger('loading simulator micromanager configuration', module=MODULE)
            self.mmc = Loader(micromator_parameters['MICROMANAGER_PARAMETERS']['micromanagerlocation'],
                              micromator_parameters['MICROMANAGER_PARAMETERS']['mmsimulator'],
                              folder_manager_obj,
                              globaldict
                              ).mmc
        self.folder_manager_obj = folder_manager_obj
        self.back_to_savestate()
        self.acquisitions_path = folder_manager_obj.acquisitions_path
        self.platform = folder_manager_obj.platform
        self.config = self.protocol.channelGroup
        self.simulation = micromator_parameters['MICROMANAGER_PARAMETERS']['Simulation']
        self.xystage = self.mmc.getXYStageDevice()
        self.zdrive = self.mmc.getFocusDevice()
        self.autofocus = self.mmc.getAutoFocusDevice()
        self.image_name = None
        self.useAFC = micromator_parameters['MICROMANAGER_PARAMETERS']['useAFC']
        self.use_z_coord = micromator_parameters['MICROMANAGER_PARAMETERS']['useZforpos']
        self.binning = micromator_parameters['MICROMANAGER_PARAMETERS']['binning']
        self.brightfield = micromator_parameters['MICROMANAGER_PARAMETERS']["brightfield"]
        self.micromator_parameters = micromator_parameters
        self.dmd = self.mmc.getSLMDevice()
        if self.dmd:
            fm.logger('found DMD device {}'.format(self.dmd), module=MODULE)
            self.dmd = dmd.DMD(self.mmc)
        fm.logger('Using Z coordinate: {}'.format(self.use_z_coord), module=MODULE)
        self.positions = positions # same instance position and self.position
        for channel in self.protocol.channel_list:
            self.globaldict['LIST_CHANNELS'].append(channel)
            for pos in self.positions:
                channel.count_to_skip[pos.index] = channel.skip_factor_frame
        self.custom_options(micromator_parameters)
        #Load Events modules
        if micromator_parameters['EVENTS_PARAMETERS']:
            self.event_module = core.load_custom_module(os.path.join(micromator_parameters['EVENTS_PARAMETERS']['creator']))
            self.event_list = []
            fm.logger('events module enabled', module=MODULE)
            self.event_list = self.event_module.create_events(self, self.globaldict) #should call all the time, not sure... need to check
            count = 0
            while not self.event_list and not self.globaldict['END_ACQ'].value:
                if not count%2:
                    fm.logger('No events found even though event module was selected, please make sure the event creator function returns a list of events...', module=MODULE)
                    fm.logger('trying to events load again...', module=MODULE)
                    time.sleep(2)
                    self.event_module = core.load_custom_module(os.path.join(micromator_parameters['EVENTS_PARAMETERS']['creator']))
                    self.event_list = self.event_module.create_events(self, self.globaldict)
                print('...')
                count += 1
            eventthread = threading.Thread(target=self.event_thread_caller, args=())
            eventthread.start()
        threading.Thread(target=self.pause_thread).start()
        self.globaldict['ACQ_INTERVAL'].value = self.protocol.acqInterval
        #Start Micromanager Acquisition
        if not globaldict['END_ACQ'].value:
            self.selector()

    def back_to_savestate(self):
        '''function will find savestate file if present and set globaldict variables accordingly
        '''
        for file in os.listdir(os.path.join(self.folder_manager_obj.protocols_path)):
            if fnmatch.fnmatch(file, '*_savestate.json'):
                savestate = fm.load_json(os.path.join(self.folder_manager_obj.protocols_path, file))
                fm.logger(savestate, module=MODULE)
                for key in savestate:
                    fm.logger(key, self.globaldict[key].value, module=MODULE)
                    self.globaldict[key].value = savestate[key]
                return

    def custom_options(self, micromator_parameters):
        '''this method is used to write custom scripts
        Args:
            micromator_parameters(dict): a dict with all the options user chose for experiment
        '''
        if self.platform == 'Inbio':
            #for pos in self.protocol.positions_list:
                #pos.analyse = False
            if not self.simulation:
                for pos in enumerate(self.positions):
                    path = os.path.join(self.folder_manager_obj.binned_images, 'pos{}'.format(pos[0]))
                    fm.makedirs_(path)
                list_names_dmd = ['dmd', 'slm']
                if micromator_parameters['MICROMANAGER_PARAMETERS']['dmdallpixel']:
                    fm.logger('will use DMD all pixels instead of direct light', module=MODULE)
                    for channel in self.protocol.channel_list:
                        if 'DIRECT' in channel.name:
                            channel.name = channel.name.replace('DIRECT', 'DMD')
                thread_list = []
                for channel in self.protocol.channel_list:
                    self.dmd.list_channels.append(channel.name)
                    affine_name = 'affines_{}_{}.txt'.format(self.config, channel.name)
                    affinepath = os.path.join(self.folder_manager_obj.protocols_path, affine_name)
                    channel.check_channel_dmd(list_names_dmd)
                    ptm_name = os.path.join(self.folder_manager_obj.protocols_path, 'pattern_to_mask_{}.npy'.format(channel.name))
                    if os.path.exists(affinepath) and not os.path.exists(ptm_name):
                        fm.logger('found affine file. Doing the transform from pattern to mask...', module=MODULE)
                        self.dmd.get_affines(affinepath)
                        image = np.zeros((self.mmc.getImageWidth(), self.mmc.getImageHeight()), dtype=(int, 2))
                        pattern = dmd.Pattern(self.dmd, channel.name, image, self.folder_manager_obj)
                        thread = threading.Thread(target=pattern.do_distance_rectangle)
                        thread_list.append(thread)
                        thread.start()
                for thread in thread_list:
                    thread.join()

    def event_threader(self):
        '''this method calls the events trigger threads. Per frame
        '''
        self.event_list = self.event_module.create_events(self, self.globaldict) #should call all the time, not sure... need to check
        for event in self.event_list:
            event_per_pos_threads = event.looper(self.positions)
            for event_thread in event_per_pos_threads:
                event_thread.start()

    def event_thread_caller(self):
        '''this method will call the event threads when there is a new frame
        '''
        while not self.globaldict['END_ACQ'].value:
            #time.sleep(0.2)
            if self.globaldict['EVENT'].value:
                self.event_threader()
                self.globaldict['EVENT'].value = False

    def selector(self):
        '''main acquisition selector
        '''
        self.initialize_acq()
        if self.quit_pause() or self.globaldict['END_ACQ'].value:
            fm.logger('Acquisition Ended', module=MODULE)
            if self.micromator_parameters["DISCORD"]: requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"","embeds" : [{"description":"Ending acquisition", "title":f"Experiment {self.folder_manager_obj.expname}"}]}), headers={"Content-Type": "application/json"}) #[discord bot]
            return
        end_time = self.calculate_total_time()
        fm.logger('the experiment should end on the {}/{}/{} at {}:{}:{}'.format(end_time.month,
                                                                                 end_time.day,
                                                                                 end_time.year,
                                                                                 end_time.hour,
                                                                                 end_time.minute,
                                                                                 end_time.second), module=MODULE)
        if self.micromator_parameters["DISCORD"]: requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"","embeds" : [{"description":"Starting, the experiment should end on the {}/{}/{} at {}:{}:{}".format(end_time.month,end_time.day,end_time.year,end_time.hour,end_time.minute,end_time.second), "title":f"Experiment {self.folder_manager_obj.expname}"}]}), headers={"Content-Type": "application/json"}) #[discord bot]
        if self.micromator_parameters["DISCORD"]: requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":f"path {self.folder_manager_obj.specific_experience_path}"}), headers={"Content-Type": "application/json"}) #[discord bot]        
        if self.simulation:
            fm.logger('SIMULATION ACQUISITION, NO PICTURES TAKEN', module=MODULE)
        if self.protocol.acquorder == 'TPC':
            fm.logger('TPC acquisition starting...', module=MODULE)
            self.globaldict['RUN'].value = True
            self.acquisition_loop(False)
        elif self.protocol.acquorder == 'PTC':
            fm.logger('PTC acquisition starting...', module=MODULE)
            self.globaldict['RUN'].value = True
            self.acquisition_loop(True)
        else:
            fm.logger('Wrong Acquisition Order', message_type='error', print_=False, module=MODULE)

    def set_position(self, pos, zslice=None, nolog=False):
        '''sets the stage controller to the given position
        Args:
            pos(object): the current position object
            zslice(float)(optional): Z axis in µm
            nolog(bool)(optional): if True will not print log
        '''
        self.globaldict['CURRENT_POS'].value = self.positions.index(pos)
        if not self.globaldict['END_ACQ'].value:
            if self.useAFC:
                self.mmc.setXYPosition(self.xystage, pos.X, pos.Y)
            if self.use_z_coord:
                self.mmc.setPosition(self.zdrive, pos.Z)
            elif zslice:
                self.mmc.setPosition(self.zdrive, zslice)
            if not self.simulation:
                self.mmc.waitForDevice(self.zdrive)
                self.mmc.waitForDevice(self.xystage)
                self.mmc.waitForDevice(self.autofocus)
            if not nolog:
                fm.logger(pos, module=MODULE)

    def channel_manager(self, channel, posindex, frame, save=True, event=False, zslicenum=None):
        '''method changes channels in between each taken image
        Args:
            channel(object) : the channel wished to use for pic
            posindex(int): the index of the current position
            frame(int): current frame
            save(bool): if true will change save
            event(bool): if true will change name to EVENT_channelname
            zslicenum(float)(optional): Z axis index in list of slices
        '''
        if self.platform == 'Inbio' and not self.simulation and self.dmd:
            fm.logger(channel, module=MODULE)
            self.mmc.setConfig(self.config, channel.name)
            self.globaldict['CURRENT_CHANNEL'].value = channel.name
            self.mmc.waitForConfig(self.config, channel.name) #maybe change? wait for device?
            if 'DMD' in channel.name:
                self.dmd.displaymask(channel.exposure, self.dmd.allpixel)
                self.mmc.setExposure(channel.exposure)
                self.mmc.snapImage()
            else:
                self.mmc.setExposure(channel.exposure)
                self.mmc.snapImage()
            if save:
                img = self.mmc.getImage()
                self.save_(posindex, frame, img, channel, event, zslicenum)
            self.mmc.clearCircularBuffer()
        else:
            fm.logger(channel, module=MODULE)
            self.mmc.setConfig(self.config, channel.name)
            self.globaldict['CURRENT_CHANNEL'].value = channel.name
            self.mmc.waitForConfig(self.config, channel.name) #maybe change? wait for device?
            self.mmc.setExposure(channel.exposure)
            self.mmc.snapImage()
            if save:
                img = self.mmc.getImage()
                self.save_(posindex, frame, img, channel, event, zslicenum)
            self.mmc.clearCircularBuffer()

    def channels_looper(self, pos, zslicenum=None, lastslice=None):
        '''goes through all the channels
        Args:
            pos(object): the current position object
            zslicenum(float)(optional): Z axis index in list of slices
            lastslice(int)(optional): the last slice
        '''
        for channel in self.protocol.channel_list:
            if channel in pos.channels:
                continue
            if channel.event:
                continue
            if self.quit_pause():
                return
            if channel.count_to_skip[pos.index] < 0:
                channel.count_to_skip[pos.index] = channel.skip_factor_frame
            if channel.count_to_skip[pos.index] == channel.skip_factor_frame:
                self.channel_manager(channel, self.globaldict['CURRENT_POS'].value, self.globaldict['CURRENT_FRAME'].value, zslicenum=zslicenum)
                if zslicenum is None:
                    channel.count_to_skip[pos.index] -= 1
                elif zslicenum == lastslice:
                    channel.count_to_skip[pos.index] -= 1
            else:
                if zslicenum is None:
                    channel.count_to_skip[pos.index] -= 1
                elif zslicenum == lastslice:
                    channel.count_to_skip[pos.index] -= 1

    def take_picture(self, pos):
        '''calls all the necessary commands to take pictures with TPC mode
        Args:
            pos(object): the current position object
        '''
        if not self.globaldict['END_ACQ'].value:
            if self.simulation:
                fm.logger('simulation pos{}'.format(pos.index), module=MODULE)
                for channel in self.protocol.channel_list:
                    if self.quit_pause():
                        return
                    fm.logger('simulation {}'.format(channel), module=MODULE)
            else:
                if self.protocol.slices:
                    for index, zslice in enumerate(self.protocol.slices):
                        if self.protocol.relativeZSlice:
                            relative_zslice = zslice
                            zslice += pos.Z
                        fm.logger('Zslice: {} | Z: {}'.format(relative_zslice, zslice), module=MODULE)
                        self.set_position(pos, zslice, nolog=False)
                        self.channels_looper(pos, zslicenum=index, lastslice=self.protocol.slices.index(self.protocol.slices[-1]))
                    self.set_position(pos, pos.Z, nolog=True)
                else:
                    self.set_position(pos)
                    self.channels_looper(pos)
                if not self.globaldict['END_ACQ'].value:
                    self.mmc.setConfig(self.config, 'IDLE')
                    self.globaldict['CURRENT_CHANNEL'].value = 'IDLE'

    def calculate_total_time(self):
        '''calculates the time at which the acquisition ends
        '''
        total_time_seconds = self.globaldict['TOTAL_FRAMES'].value*self.protocol.acqInterval
        local_time = datetime.now()
        end_time = local_time + timedelta(seconds=total_time_seconds)
        return end_time

    def timeloop(self, time_):
        '''this method is the wait loop. Used in lieu of time.sleep so we can do things during the wait
        Args:
            time_(int): the time between each acquisition in seconds
        '''
        fmt = "%Y-%m-%d %H:%M:%S"
        self.globaldict['ABSOLUTE_TIME_TO_NEXT_FRAME'].value = time_
        self.globaldict['TIME_TO_NEXT_FRAME'].value = self.globaldict['EXPTIMER'].value + time_
        fm.logger('next image acquisition loop in {} seconds'.format(int(self.globaldict['ABSOLUTE_TIME_TO_NEXT_FRAME'].value)), module=MODULE)
        next_image_in = datetime.strptime(datetime.now().strftime(fmt), fmt)
        self.globaldict['REAL_TIME_NEXT_FRAME'].value = next_image_in + timedelta(0, self.globaldict['ABSOLUTE_TIME_TO_NEXT_FRAME'].value)
        while datetime.strptime(datetime.now().strftime(fmt), fmt) < next_image_in + timedelta(0, self.globaldict['ABSOLUTE_TIME_TO_NEXT_FRAME'].value):
            if self.quit_pause():
                return
            time.sleep(0.1)
            if not self.mmc.isContinuousFocusEnabled():
                self.mmc.enableContinuousFocus(True)
        self.globaldict['TIME_TO_NEXT_FRAME'].value = 0

    def save_(self, pos, frame, img, channel, event=False, zslicenum=None):
        '''saves the image
        Args:
            pos(int): number of position
            frame(int): number of frame
            img(array): image
            channel(obj): channel object
            event(bool)(optional): if true will add EVENT_ to name of pic
            zslicenum(float)(optional): Z axis index in list of slices
        '''
        prefix = self.acquisitions_path
        image = Image.fromarray(img, mode='I;16')
        if event:
            image_path = os.path.join(prefix, 'pos{}'.format(pos))
            if zslicenum is not None:
                image_name = 'img_{}_z{}_EVENT-{}.tif'.format(frame, zslicenum, channel.path)
            else:
                image_name = 'img_{}_EVENT-{}.tif'.format(frame, channel.path)
            fullname = os.path.join(image_path, image_name)
            if os.path.exists(fullname):
                image_name = image_name[0:-4]+'_{}'.format(int(channel.exposure))+image_name[-4:]
                #self.image_exist_count += 1
            image.save(os.path.join(image_path, image_name))
            if self.binning:
                ip.downscale(image_path, image_name, save_path=os.path.join(self.folder_manager_obj.binned_images, 'pos{}'.format(pos)), binning_factor=self.binning)
        else:
            image_path = os.path.join(prefix, 'pos{}'.format(pos))
            if zslicenum is not None:
                self.image_name = 'img_{}_z{}_{}.tif'.format('*', 0, self.brightfield)
                image_name = 'img_{}_z{}_{}.tif'.format(frame, zslicenum, channel.path)
            else:
                self.image_name = 'img_{}_{}.tif'.format('*', self.brightfield)
                image_name = 'img_{}_{}.tif'.format(frame, channel.path)
            fullname = os.path.join(image_path, image_name)
            if os.path.exists(fullname):
                image_name = image_name[0:-4]+'-{}'.format(int(channel.exposure))+image_name[-4:]
                #self.image_exist_count += 1
            image.save(os.path.join(image_path, image_name))
            if self.binning:
                ip.downscale(image_path, image_name, save_path=os.path.join(self.folder_manager_obj.binned_images, 'pos{}'.format(pos)), binning_factor=self.binning)

    def doquit(self):
        '''this method will elegantly quit the acquisition_loop
        '''
        if self.globaldict['END_ACQ'].value:
            if not self.globaldict['ALREADY_QUITTING'].value:
                if not self.simulation:
                    self.mmc.enableContinuousFocus(False)
                    self.mmc.setConfig(self.config, 'IDLE')
                    self.mmc.waitForConfig(self.config, 'IDLE')
                self.mmc.waitForSystem()
                while self.mmc.systemBusy():
                    time.sleep(0.5)
                self.mmc.unloadAllDevices()
                self.globaldict['ALREADY_QUITTING'].value = True
                fm.logger('Unloaded all devices, MicroMator can now safely be quit!', module=MODULE)
                return True
        return False

    def quit_pause(self):
        '''will call the doquit or pause acquisition
        '''
        if self.doquit():
            fm.logger('aborting MicroMator acquisition', module=MODULE)
            self.globaldict['END_ACQ'].value = True
            self.globaldict['FRAME'].value = False
            return True
        first = 1
        unpause = 0
        while self.globaldict['PAUSE'].value:
            if first:
                fm.logger('pausing MicroMator acquisition', module=MODULE)
                first = 0
                unpause = 1
            if self.doquit():
                return True
        if unpause:
            fm.logger('resuming MicroMator acquisition', module=MODULE)
        return False

    def pause_thread(self):
        '''pause thread
        '''
        while True:
            if self.globaldict['PAUSE'].value:
                time.sleep(1)
                if not self.globaldict['ACQU'].value:
                    self.globaldict['ABSOLUTE_TIME_TO_NEXT_FRAME'].value += 1
                    self.globaldict['TIME_TO_NEXT_FRAME'].value += 1
            if self.globaldict['END_ACQ'].value:
                break

    def initialize_acq(self):
        '''wait loop for user to press start button
        '''
        fm.logger('waiting for user to start acquisition...', module=MODULE)
        self.set_position(self.positions[0], None, nolog=True)
        self.mmc.enableContinuousFocus(True)
        while not self.globaldict['INIT'].value:
            if self.doquit():
                return True
            time.sleep(0.2)
        self.globaldict['START_TIME'].value = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        return

    def acquisition_loop(self, timefirst):
        '''this method is the main acquisition loop (TPC or PTC)
        Args:
            timefirst(bool): if false TPC, else PTC
        '''
        finished_positions = []
        if not self.simulation:
            self.mmc.enableContinuousFocus(True)
        while self.globaldict['CURRENT_FRAME'].value < self.globaldict['TOTAL_FRAMES'].value and not self.globaldict['END_ACQ'].value:
            self.image_exist_count = 0
            time_ = time.time()
            if self.micromator_parameters["DISCORD"]: requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"!auto_error_check"}), headers={"Content-Type": "application/json"}) #[discord bot]
            fm.logger('--------------------------------------------- FRAME: {} ---------------------------------------------'.format(self.globaldict['CURRENT_FRAME'].value), print_=True, module=MODULE)
            self.globaldict['ACQU'].value = True #event to signal to not send signals to the microscope
            for pos in self.protocol.positions_list:
                fm.logger('--------------------------------------------- Pos: {} ---------------------------------------------'.format(self.positions.index(pos)), print_=True, module=MODULE)
                if self.quit_pause() or self.globaldict['END_ACQ'].value:
                    fm.logger('Acquisition Ended', module=MODULE)
                    if self.micromator_parameters["DISCORD"]: 
                        requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"","embeds" : [{"description":"Ending acquisition", "title":f"Experiment {self.folder_manager_obj.expname}"}]}), headers={"Content-Type": "application/json"}) #[discord bot]
                        requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"disconnect bot"}), headers={"Content-Type": "application/json"}) #[discord bot]
                    return
                if pos.use: #if position is used (not disabled for example)
                    if timefirst: #if PTC and use pos
                        if pos.index not in finished_positions: #if all frame are not taken for this pos
                            self.globaldict['CURRENT_POS'].value = self.positions.index(pos)
                            self.take_picture(pos)
                            self.globaldict['ANALYSIS_PER_POS'][pos.index].value = True
                            break
                        else: #if position is finished
                            pass
                    else: #if TPC
                        self.globaldict['CURRENT_POS'].value = self.positions.index(pos)
                        self.take_picture(pos)
                if self.useAFC and not self.globaldict['END_ACQ'].value:
                    self.mmc.enableContinuousFocus(True)
            if not timefirst and not self.simulation:
                if self.quit_pause() or self.globaldict['END_ACQ'].value:
                    fm.logger('Acquisition Ended', module=MODULE)
                    if self.micromator_parameters["DISCORD"]: 
                        requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"","embeds" : [{"description":"Ending acquisition", "title":f"Experiment {self.folder_manager_obj.expname}"}]}), headers={"Content-Type": "application/json"}) #[discord bot]
                        requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"disconnect bot"}), headers={"Content-Type": "application/json"}) #[discord bot]
                    return
                self.set_position(self.positions[0], None, nolog=True)
            if not self.globaldict['END_ACQ'].value:
                self.globaldict['FRAME'].value = True #event set to signal image analysis to start
            elapsed = time.time() - time_
            self.globaldict['ACQU'].value = False #signal is clear signifying that the microscope can be communicated with
            self.globaldict['EVENT'].value = True
            if not self.globaldict['END_ACQ'].value:
                if self.globaldict['CURRENT_FRAME'].value != self.globaldict['TOTAL_FRAMES'].value-1:
                    self.protocol.acqInterval = self.globaldict['ACQ_INTERVAL'].value
                    if elapsed < self.protocol.acqInterval:
                        self.timeloop(self.protocol.acqInterval - elapsed)
                    else:
                        #self.timeloop(self.protocol.acqInterval)
                        self.timeloop(0)
                    # self.globaldict['ANALYSIS_PER_POS'][pos.index].value = False
                    for posi in self.globaldict['ANALYSIS_PER_POS']: #changed this so tha new frame doesent start with True values in globaldict['ANALYSIS_PER_POS'], otherwise the model module triggers instantly
                        posi.value = False
                    self.globaldict['CURRENT_FRAME'].value += 1
                else: #when at end of loop
                    if timefirst: #if PTC add pos to finished
                        fm.logger('finished position {}'.format(self.globaldict['CURRENT_POS'].value), module=MODULE)
                        finished_positions.append(self.globaldict['CURRENT_POS'].value)
                        self.globaldict['CURRENT_FRAME'].value = 0 #reset frame
                        if len(self.positions) == len(finished_positions):
                            break
                    else:
                        break
            self.globaldict['EVENT'].value = False
        if not self.globaldict['END_ACQ'].value:
            self.globaldict['END_ACQ'].value = True
            self.quit_pause()
        fm.logger('MicroMator Acquisition Ended', module=MODULE)
        if self.micromator_parameters["DISCORD"]: 
            requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"","embeds" : [{"description":"Ending acquisition", "title":f"Experiment {self.folder_manager_obj.expname}"}]}), headers={"Content-Type": "application/json"}) #[discord bot]
            requests.post(self.micromator_parameters["DISCORD"]["webhook_path"], data=json.dumps({"content":"disconnect bot"}), headers={"Content-Type": "application/json"}) #[discord bot]
        return
