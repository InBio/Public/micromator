# -*- coding: utf-8 -*-
'''
Created on Apr 17, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import re
import ntpath
import math
import time
import numpy as np
from PIL import Image
from scipy import ndimage
from copy import deepcopy
from MicroMator.FileManagement import logger

MODULE='DMD'

def do_matmult(affine, pattern_coord):
    '''do the affine transform
    Args:
        affine(list): the current affine transform for this set of corrdinates [x,y,width,height,m00,m10,m01,m11,m02,m12]
        pattern_coord(tuple): X,Y coordinates
    '''
    matrix_affine = np.zeros((3, 3))
    matrix_coords = np.ones((3, 1))
    matrix_coords[0] = pattern_coord[0]
    matrix_coords[1] = pattern_coord[1]
    matrix_affine[0][0] = affine[4]
    matrix_affine[0][1] = affine[5]
    matrix_affine[0][2] = affine[6]
    matrix_affine[1][0] = affine[7]
    matrix_affine[1][1] = affine[8]
    matrix_affine[1][2] = affine[9]
    matrix_affine[2][2] = 1
    mult = np.matmul(matrix_affine, matrix_coords)
    return int(round(mult[0][0])), int(round(mult[1][0]))

def is_in_rectangle(point, rectangle):
    '''check if point is in rectangle
    Args:
        point(tuple): X,Y coordinates
        rectangle(tuple): (X,Y,width,height)
    '''
    x = point[0]
    y = point[1]
    x1 = rectangle[0]
    y1 = rectangle[1]
    x2 = rectangle[2] + rectangle[0]
    y2 = rectangle[3] + rectangle[1]
    if x1 <= x <= x2 and y1 <= y <= y2:
        return True
    return False

def load_mask(image):
    '''this function loads a mask given the path
    Args:
        image(path): the path to the mask. Mask needs to be of depth 1 and height*width of dimension
    '''
    img = Image.open(image)
    mask = np.array(img)
    return mask

def load_mask_sequence(list_paths):
    masks_sequence = []
    for mask in list_paths:
        masks_sequence.append(load_mask(mask))
    return masks_sequence

class Pattern():
    '''this class stores all the infor & methods related to patterns i.e. camera sized images
    '''
    def __init__(self, dmd, channel, pattern, folder_manager_obj=None):
        '''Args:
             dmd: the DMD object
             channel(str): the name of the channel
             pattern(list of list): a camera frame sized array of ones
             folder_manager_obj(obj)(optional): the folder manager object with all the relevant experiment paths
        '''
        self.dmd_width = dmd.width
        self.dmd_height = dmd.height
        self.affines = dmd.affines
        self.channel = channel
        self.pattern = pattern
        self.folder_manager_obj = folder_manager_obj
        self.mask = np.zeros((self.dmd_height, self.dmd_width))

    def get_closest_rectangle(self, coords):
        '''returns the new XY coordinates after finding correct affine to use
        Args:
            coords(tuple): (X,Y) corrdinates
        '''
        all_min = math.inf
        all_argmin = None
        for rectangle in self.affines[self.channel]:
            if is_in_rectangle(coords, rectangle):
                return do_matmult(rectangle, (coords[0], coords[1]))
            dxtmp = rectangle[0] - coords[0]
            dx2 = min((dxtmp**2, (rectangle[2]+dxtmp)**2))
            dytmp = rectangle[1] - coords[1]
            dy2 = min((dytmp**2, (rectangle[3]+dytmp)**2))
            min_ = dx2 + dy2
            if min_ < all_min:
                all_min = min_
                all_argmin = rectangle
        return do_matmult(all_argmin, (coords[0], coords[1]))

    def do_distance_rectangle(self):
        '''alternate get_closest_rectangle variant
        '''
        logger('calculating distance to rectangles...', module=MODULE)
        list_dist_matrix = []
        for rectangle in self.affines[self.channel]:
            x_rec, y_rec, width, height = int(rectangle[0]), int(rectangle[1]), int(rectangle[2]), int(rectangle[3])
            rectangle_array = np.zeros((height, width))
            dist_matrix = np.ones((self.pattern.shape[0], self.pattern.shape[1]))
            dist_matrix[y_rec:y_rec+height, x_rec:x_rec+width] = rectangle_array
            dist_matrix = ndimage.distance_transform_edt(dist_matrix)
            list_dist_matrix.append(dist_matrix)
        self.dist_matrix_all_rec = np.dstack(list_dist_matrix)
        logger('...DONE!', module=MODULE)
        logger('calculating pattern to mask coordinates...', module=MODULE)
        list_rec = []
        for row in range(self.pattern.shape[0]):
            for col in range(self.pattern.shape[1]):
                rec_index = np.unravel_index(self.dist_matrix_all_rec[row][col].argmin(), self.dist_matrix_all_rec[row][col].shape)[0]
                if rec_index not in list_rec:
                    list_rec.append(rec_index)
                x, y = do_matmult(self.affines[self.channel][rec_index], (col, row))
                if x >= self.dmd_width:
                    x = 0
                if y >= self.dmd_height:
                    y = 0
                self.pattern[row][col][0] = x
                self.pattern[row][col][1] = y
        logger('...DONE!', module=MODULE)
        np.save(os.path.join(self.folder_manager_obj.protocols_path, 'pattern_to_mask_{}.npy'.format(self.channel)), self.pattern)

    def assign_affine_to_pattern(self, coords):
        '''assign to each pixel an affine
        '''
        xmask, ymask = self.get_closest_rectangle(coords)
        self.pattern[coords[0]][coords[1]][0] = xmask
        self.pattern[coords[0]][coords[1]][1] = ymask

    def pattern_to_mask(self):
        '''method that does the transform from pattern to mask i.e. camera sized table to DMD sized table
        '''
        start_time = time.time()
        for xcoord, _ in enumerate(self.pattern):
            for ycoord, _ in enumerate(self.pattern[xcoord]):
                self.assign_affine_to_pattern((xcoord, ycoord))
        elapsed_time = time.time() - start_time
        print('{} - {}'.format(self.channel, elapsed_time))

class Mask():
    '''this class stores all info related to a mask i.e. dmd sized images
    '''
    def __init__(self, dmd, ddmask):
        '''Args:
            dmd(instance of DMD): instance of the DMD class
            ddmask(2D numpy array): a 2D numpy array of size dmd.width*dmd.height
            allpixel(bool)(optional): a bol that charaterizes if mask is allpixels or custom pattern
        '''
        self.dmd = dmd
        self.ddmask = ddmask
        self.pixeloff = '\x00' #hex for 0
        self.pixelon = '\x01' #hex for 1
        self.mask = self.create_mask()
        logger('mask ready to be uploaded to DMD', print_=False, module=MODULE)

    def create_mask(self):
        '''changes the format of the numpy array to fit the micromanger SLM format I.E. a 1D list of '\x00' or '\x01' characters
        '''
        flatarray = self.ddmask.ravel().tolist()
        tostring = [self.pixelon if pixel == 1. else self.pixeloff for pixel in flatarray]
        mask = ''.join(tostring)
        return mask

class DMD():
    '''this class regroups all the methods that relate to the control of the DMD
    '''
    def __init__(self, mmc):
        '''Args:
            mmc(CMMCore object): created in Loader class
        '''
        self.mmc = mmc
        self.name = self.mmc.getSLMDevice()
        self.height = self.mmc.getSLMHeight(self.name)
        self.width = self.mmc.getSLMWidth(self.name)
        self.bppx = self.mmc.getSLMBytesPerPixel(self.name) #bytes per Pixel
        logger('DMD {} - ({}*{}) - {}bppx'.format(self.name, self.width, self.height, self.bppx), module=MODULE)

        self.list_channels = []
        self.affines = {} #key is channel name and items is list of rectangles & affines (x,y,width,height,m00,m10,m01,m11,m02,m12)

        self.image_height = self.mmc.getImageHeight()
        self.image_width = self.mmc.getImageWidth()
        self.allpixel = Mask(self, np.ones((self.height, self.width)))

    def get_affines(self, channelfile):
        '''this method extracts and stores the affines need for the DMD
        Args:
            channelfile(path): the complete path to the affine files
        '''
        channelname = ntpath.basename(channelfile).split('_')[-1].split('.')[0]
        self.affines[channelname] = []
        affines = []
        with open(channelfile) as file_:
            for line in file_.readlines():
                affines = re.sub(r'[a-zA-DF-Z/,/[\]]', '', line).split() #clean the line from any chars & [], symbols
                affines[0] = affines[0].split('=')
                affines = affines[0][1:] + affines[2:] #get resutling affines
                self.affines[channelname].append(affines)
        for index, _ in enumerate(self.affines[channelname]):
            self.affines[channelname][index] = list(map(float, self.affines[channelname][index])) #transform all str in floats

    def displaymask(self, exposure, mask):
        '''turns on pixels in DMD
        Args:
            exposure(float): exposure time in ms
            mask(mask): Mask object
        '''
        logger('uploaded mask | exposure: {}ms'.format(exposure), module=MODULE)
        self.mmc.setSLMExposure(self.name, exposure)
        self.mmc.setSLMImage(self.name, mask.mask)
        self.mmc.displaySLMImage(self.name)

    def displaymask_sequence(self, list_exposure, list_mask):
        '''does a sequence of DMD images
        Args:
            exposure(list of float): list of exposure time in ms
            mask(list of Mask): Mask object
        '''
        for index, mask in enumerate(list_mask):
            self.displaymask(list_exposure[index], mask)
            while self.mmc.deviceBusy(self.name):
                pass
