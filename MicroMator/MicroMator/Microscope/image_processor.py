'''
Created on Jul 8, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
from PIL import Image

def downscale(path, image, save_path=None, binning_factor=2):
    '''downscale the image if needed
    Args:
        image(str): the name of the image
        path(str): the full path to the image
        save_path(str)(optiona): if save_path then save in specified path
        binning_factor(int)(optional): the factor used to bin
    '''

    image_name = image
    image_path = path
    #open and downscale by 2
    img = Image.open(os.path.join(path, image), 'r')
    width, height = img.size
    rimg = img.resize((int(width/binning_factor), int(height/binning_factor)), resample=Image.NEAREST)
    #save with new name binned_{}
    rimage_name = 'binned_{}'.format(image_name)
    rimage = os.path.join(image_path, 'binned', rimage_name)
    if not save_path:
        rimg.save(rimage)
    else:
        rimage = os.path.join(save_path, rimage_name)
        rimg.save(rimage)
    return rimage
