# -*- coding: utf-8 -*-
'''
Created on Jan 22, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import glob
import sys
import threading
import json
import errno
import site
import shutil
import importlib
import importlib.util
import logging
import MicroMator.DataManagement.position_structure as ps
import MicroMator.DataManagement.protocol_structure as pros
from MicroMator.FileManagement import logger, write_json, makedirs_
import MicroMator.FileManagement.file_data_extractor as fde
import MicroMator.FileManagement.folder_manager as fm
import MicroMator.DataManagement.frame_structure as fs
import MicroMator.GUI as gui

MODULE = 'Core'

def load_micromator_options(microfluidics, analysis, events, options_json_file):
    '''this function will read the .json file and extract the info for the MicroMator expriment
    Args:
        microfluidics(bool): If True enables use of the corresponding module
        analysis(bool): cellstar, numpy or False
        events(bool): If True enables use of the corresponding module
        options_json_file(str): the complete path to the json file that contains the options for the experiment
    '''
    with open(options_json_file) as jsonfile:
        optionsjson = json.load(jsonfile)
        if not microfluidics:
            optionsjson['MICROFLUIDIC_PARAMETERS'] = False
        if analysis:
            module = optionsjson['ANALYSIS_SOFTWARE'][analysis]['module']
            parameters = optionsjson['ANALYSIS_SOFTWARE'][analysis]['parameters']
            optionsjson['ANALYSIS_SOFTWARE'] = {}
            optionsjson['ANALYSIS_SOFTWARE']['module'] = module
            optionsjson['ANALYSIS_SOFTWARE']['parameters'] = parameters
        else:
            optionsjson['ANALYSIS_SOFTWARE'] = False
        if not events:
            optionsjson['EVENTS_PARAMETERS'] = False
    return optionsjson

def load_custom_module(modulepath):
    '''this function will load custom modules for the user
    Args:
        modulepath(str): the path to the module that the user wishes to import
            ex: "/path/to/file.py"
    '''
    spec = importlib.util.spec_from_file_location("", modulepath)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    logger('loaded custom module from {}'.format(modulepath), module=MODULE)
    return module

def load_custom_module_v2(modulepath):
    '''this function will load custom modules for the user
    Args:
        modulepath(str): the path to the module that the user wishes to import
            ex: "/path/to/file.py"
    '''
    module = __import__(modulepath)
    logger('loaded custom module from {}'.format(modulepath), module=MODULE)
    return module

def user_folders(user):
    '''function will create user folders to store user specific MicroMator options
    Args:
        users(list): the list of users
    '''
    userdocpath = os.path.expanduser('~/Documents/MicroMator/')
    makedirs_(os.path.join(userdocpath, user))

class MicroMator():
    '''this class will Be initializing MicroMator modules
    '''
    def __init__(self, options, MANAGER, GLOBADICT, GLOBALFLUIDICS, argsgui):
        '''Args:
            options(dict): the dict read from the default_options.json
            MANAGER(multiprocessing.Manager instance): the instance of the multiprocess.Manager() object to create 'global' inter process coms
            GLOBADICT(dict): the signal dict for inter modular communications
            GLOBALFLUIDICS(dict): the dict of ONIX related signals
            argsgui(args.g): the args.g argument to see if GUI is enabled or not
        '''
        self.globaldict = GLOBADICT
        self.options = options
        #Initializes the Folder environment all the important info/files will be stored in
        self.folder_manager_obj = fm.Filer(self.options)
        logging.basicConfig(filename=self.folder_manager_obj.micromator_log_path, level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')
        # logger_ = logging.getLogger(self.folder_manager_obj.micromator_log_path)
        # def my_handler(type_, value, tb_):
        #     logger_.exception("Uncaught exception: {0}".format(str(value)))
        # sys.excepthook = my_handler
        write_json(self.options, os.path.join(self.folder_manager_obj.protocols_path, 'MicroMator_parameters.json'))
        write_json(self.options, os.path.join(os.path.expanduser('~/Documents/MicroMator/'), 'last_MicroMator_exp_parameters.json'))

        #Initializing dummy protocol values for GUI
        self.protocol = pros.Protocol(timestamp=0, acquorder=None, acqNumframes=0, acqInterval=0, channel_list=None, positions_list=None, dmd=False, track=False)

        #Initializing GUI
        self.guithread = None
        if not argsgui:
            self.guithread = threading.Thread(target=gui.rungui, args=(self, self.options, self.folder_manager_obj, self.protocol, self.globaldict, GLOBALFLUIDICS))

        if self.options['MICROFLUIDIC_PARAMETERS']:
            microfluidics_module = load_custom_module(self.options['MICROFLUIDIC_PARAMETERS']['module'])
            microfluidics_module.initializer(self.options['MICROFLUIDIC_PARAMETERS'], self.folder_manager_obj, GLOBALFLUIDICS, self.guithread)

        #Initializing GUI
        if not argsgui:
            if not self.guithread.is_alive():
                self.guithread.start()

        #create/assign user folder
        self.user_initialzer()
        self.user = self.options['FOLDER_PARAMETERS']['user']

        #Waits for user to open Micromanager then find and save positions in .pos
        if not argsgui:
            self.micromanager_gui_waiter()
        else:
            self.globaldict['INIT'].value = True

        #Create instance of Acquisition_data_extractor
        if self.options["MICROMANAGER_PARAMETERS"]['mmversion'] in ('gamma', 'beta'):
            logger("ACQ: gamma/beta", module=MODULE)
            self.acqu_data = fde.AcquisitionDataExtractormm2(self.folder_manager_obj, self.options)
        elif self.options["MICROMANAGER_PARAMETERS"]['mmversion'] == '1.4':
            logger('ACQ: 1423', module=MODULE)
            self.acqu_data = fde.AcquisitionDataExtractormm14(self.folder_manager_obj, self.options)
        else:
            logger('Incorrect or unsupported micromanager version, please make sure the micromanger path include either gamma, beta or 1422/1423', 'error', module=MODULE)

        #Initializing the Positions object
        self.positions = ps.AllPositions(self.folder_manager_obj, self.options["MICROMANAGER_PARAMETERS"]["mmversion"]).positions
        for _ in self.positions:
            self.globaldict['ANALYSIS_PER_POS'].append(MANAGER.Value('i', False))
        #Initializing the Protocol object
        self.protocol_initializer()

        #Initializing the Data(Frame) object
        self.frame_object_list = []
        self.data_initializer(MANAGER) #the list of lists that contain the analysis of the images cellstared [pos[frame]]

        if "DISCORD" not in self.options:
            self.options['DISCORD'] = False
        if self.options["DISCORD"]:
            import subprocess
            t = subprocess.Popen(["python", "C:\\Users\\lifeware\\workspace\\MicroMator\\MicroMatorBot.py"])

        #Initializing analysis object
        if self.options['ANALYSIS_SOFTWARE']:
            self.analysis_initializer()
            self.analysisthread = threading.Thread(target=self.analysis_obj.looper, args=())
            self.analysisthread.start()

        if self.options['MODEL']:
            model = load_custom_module_v2(self.options['MODEL']['module'])
            n_frames_between_update = self.options['MODEL']['parameters']['n_frames_between_update']
            measurement_period = self.options['MODEL']['parameters']['measurement_period']
            readout_key = self.options['MODEL']['parameters']['readout_key']
            stopframe = self.options['MODEL']['parameters']['stopframe']
            pop = self.options['MODEL']['parameters']['pop']
            logger('loaded model from {}'.format(self.options['MODEL']['module']), module=MODULE)
            logger('chosen parameters are {}'.format(self.options['MODEL']['parameters']), module=MODULE)
            for pos in self.positions:
                logger('starting model prediction for pos{}'.format(pos.index), module=MODULE)
                self.modelthread = threading.Thread(target=model.looper, args=(self.globaldict,
                                                                               self.protocol,
                                                                               self.folder_manager_obj,
                                                                               pos.index,
                                                                               n_frames_between_update,
                                                                               measurement_period,
                                                                               readout_key,
                                                                               stopframe, 
                                                                               len(self.positions),
                                                                               pop))
                self.modelthread.start()

    def protocol_initializer(self):
        '''this function initializes & calls for the filling of the protocol object
        '''
        self.protocol.timestamp = 0
        self.protocol.acquorder = self.acqu_data.acquorder
        self.protocol.acqNumframes = self.acqu_data.acqNumframes
        self.protocol.acqInterval = self.acqu_data.acqInterval
        self.protocol.channel_list = self.acqu_data.channel_list
        self.protocol.relativeZSlice = self.acqu_data.relativeZSlice
        self.protocol.slices = self.acqu_data.slices
        self.protocol.channelGroup = self.acqu_data.channelGroup
        self.protocol.sort_channels()
        self.protocol.positions_list = self.positions
        logger('initialized Protocol object:\n{}'.format(self.protocol), module=MODULE)
        self.globaldict['PLOADED'].value = True

    def micromanager_gui_waiter(self):
        '''waits for user to load up micromanager and find his points of interest
        '''
        while not self.globaldict['MMLOADED'].value:
            if self.globaldict['ABORT'].value:
                logger('Aborting MicroMator experiment', module=MODULE)
                sys.exit()

    def data_initializer(self, manager):
        '''this function initializes & calls for the filling of the Data object
        Args:
            manager(multiprocessing.Manager): used to create inter processing objects
        '''
        for _ in range(self.acqu_data.acqNumframes):
            self.frame_object_list.append(fs.Frame(manager, self.positions, self.protocol))
        logger('initialized Frame object', module=MODULE)

    def analysis_initializer(self):
        '''initializes the analysis_object if needed for this Micromator run
        '''
        analyzer = load_custom_module(self.options['ANALYSIS_SOFTWARE']['module'])
        self.analysis_obj = analyzer.Analysis(self)

    def user_initialzer(self):
        '''creates if none existent the user folders
        '''
        for sitepackage in site.getsitepackages():
            if 'site-packages' in sitepackage:
                break
        os.path.join(sitepackage, 'MicroMator\\ImageAnalysis\\cellstar\\DefaultParametersMicroMator.m')
        for user in os.listdir(self.options['FOLDER_PARAMETERS']['exp_folder']):
            user_folders(user)
            dst = os.path.join(os.path.expanduser('~/Documents/MicroMator/'), user, 'DefaultParameters.m')
            parameters = os.path.join(sitepackage, 'MicroMator\\ImageAnalysis\\cellstar\\DefaultParametersMicroMator.m')
            if not os.path.exists(dst):
                shutil.copy(parameters, dst)
