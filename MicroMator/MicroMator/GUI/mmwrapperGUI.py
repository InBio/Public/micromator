'''
Created on Oct 17, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import tkinter as Tk
import threading
from datetime import datetime
import MicroMator.FileManagement as fm

def count_time(globaldict):
    '''simple function will count the time
    Args:
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
    '''
    while globaldict['INIT'].value and not globaldict['END_ACQ'].value:
        fm.sleep_(0.1)
        fmt = "%Y-%m-%d %H:%M:%S"
        tdelta = datetime.strptime(globaldict['START_TIME'].value, fmt) - datetime.strptime(datetime.now().strftime(fmt), fmt)
        globaldict['EXPTIMER'].value = int(1-tdelta.total_seconds())

def humanize_time(secs):
    '''converts seconds to time
    Args:
        sec(int): seconds
    '''
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    return '%02d:%02d:%02d' % (hours, mins, secs)


class Application(Tk.Frame):
    '''this class is the GUI for the MMwrapper acqu loop
    '''
    def __init__(self, globaldict, folder_manager_obj, master=None):
        '''Args:
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
            folder_manager_obj(obj): the folder_manager instance
        '''
        Tk.Frame.__init__(self, master)
        self.timer = '00:00:00'
        self.pos = 0
        self.frame = 0
        self.nextframetimer = 0
        self.globaldict = globaldict
        self.folder_manager_obj = folder_manager_obj
        self.pack()
        self.createWidgets()
        self.update_timer()
        self.update_next_frame_in()
        self.update_frame_number()
        self.update_pos_location()
        self.update_channel()

    def save_and_abort_event(self):
        '''this method sets the threading event so the current run can stop
        '''
        fm.save_filestatus(self.globaldict, self.folder_manager_obj.protocols_path)
        self.globaldict['END_ACQ'].value = True
        self.labelframe.pack_forget()
        self.destroy()

    def abort_event(self):
        '''this method sets the threading event so the current run can stop
        '''
        self.globaldict['END_ACQ'].value = True
        self.labelframe.pack_forget()
        self.destroy()

    def pause_event(self):
        '''this method acts as the toggle button to pause the current run
        '''
        if self.pause.config('text')[-1] == 'Pause':
            self.globaldict['UNPAUSE'].value = False
            self.globaldict['PAUSE'].value = True
            self.pause.config(text='Resume')
        else:
            self.pause.config(text='Pause')
            self.globaldict['UNPAUSE'].value = True
            self.globaldict['PAUSE'].value = False

    def init_event(self):
        self.globaldict['INIT'].value = True
        timerthread = threading.Thread(target=count_time, args=(self.globaldict,))
        timerthread.start()
        self.start.grid_forget()

    def update_timer(self):
        '''updates timer label
        '''
        self.infotimer.configure(text=humanize_time(self.globaldict['EXPTIMER'].value))
        self.after(100, self.update_timer)

    def update_next_frame_in(self):
        '''updates next frame label
        '''
        self.timetoframe.configure(text=humanize_time(self.globaldict['TIME_TO_NEXT_FRAME'].value))
        self.after(100, self.update_next_frame_in)

    def update_frame_number(self):
        '''updates timer label
        '''
        self.totalframes.configure(text='{}/{}'.format(self.globaldict['CURRENT_FRAME'].value, self.globaldict['TOTAL_FRAMES'].value))
        self.after(100, self.update_frame_number)

    def update_pos_location(self):
        '''updates timer label
        '''
        self.poslocator.configure(text='{}'.format(self.globaldict['CURRENT_POS'].value))
        self.after(100, self.update_pos_location)

    def update_channel(self):
        '''updates timer label
        '''
        self.current_channel.configure(text='{}'.format(self.globaldict['CURRENT_CHANNEL'].value))
        self.after(100, self.update_channel)

    def open_explorer(self):
        '''command to open explorer
        '''
        os.system("start {}".format(os.path.join(self.folder_manager_obj.specific_experience_path)))

    def createWidgets(self):
        '''this method is where all the widgets and buttons are created
        '''
        self.labelframe = Tk.LabelFrame(self, text="MM Acquisition controls")
        self.labelframe.pack(side='left', fill="both", expand="yes")
        self.expinfo_frame = Tk.LabelFrame(self, text='MicroMator Information')
        self.expinfo_frame.pack(side='right', expand="yes")
        #Start run
        self.start = Tk.Button(master=self.labelframe, width=18, text='Start', command=lambda:self.init_event())
        self.start.grid(row=0, column=0)
        #Pause toggle button
        self.pause = Tk.Button(master=self.labelframe, width=18, text='Pause', command=self.pause_event)
        self.pause.grid(row=2, column=0)
        #Open explorer button
        self.explorer_button = Tk.Button(master=self.labelframe, width=18, text="MicroMator folder", command=self.open_explorer)
        self.explorer_button.grid(row=3, column=0, sticky=Tk.W)
        #Quit button
        self.squit = Tk.Button(master=self.labelframe, width=18, text='Save & Quit', fg='red', command=self.save_and_abort_event)
        self.squit.grid(row=4, column=0)
        #Quit button
        self.quit = Tk.Button(master=self.labelframe, width=18, text='Quit', fg='red', command=self.abort_event)
        self.quit.grid(row=5, column=0)
        #infolabel
        self.timerlabel = Tk.Label(master=self.expinfo_frame, width=10, text='Global Timer')
        self.timerlabel.grid(row=0, column=0, sticky=Tk.W)
        self.infotimer = Tk.Label(master=self.expinfo_frame, text='00:00:00', anchor='e')
        self.infotimer.grid(row=0, column=2)
        #time_to_next_frame
        self.timetoframelabel = Tk.Label(master=self.expinfo_frame, width=10, text='Next Frame at')
        self.timetoframelabel.grid(row=1, column=0, sticky=Tk.W)
        self.timetoframe = Tk.Label(master=self.expinfo_frame, text='0', anchor='e')
        self.timetoframe.grid(row=1, column=2)
        #totalframes
        self.totalframeslabel = Tk.Label(master=self.expinfo_frame, width=10, text='Frame')
        self.totalframeslabel.grid(row=2, column=0, sticky=Tk.W)
        self.totalframes = Tk.Label(master=self.expinfo_frame, text='0/{}'.format(self.globaldict['TOTAL_FRAMES'].value), anchor='e')
        self.totalframes.grid(row=2, column=2)
        #poslocator
        self.poslocatorlabel = Tk.Label(master=self.expinfo_frame, width=10, text='Pos')
        self.poslocatorlabel.grid(row=3, column=0, sticky=Tk.W)
        self.poslocator = Tk.Label(master=self.expinfo_frame, text='{}'.format(self.globaldict['CURRENT_POS'].value), anchor='e')
        self.poslocator.grid(row=3, column=2)
        #CURRENT_CHANNEL
        self.current_channellabel = Tk.Label(master=self.expinfo_frame, width=10, text='Channel')
        self.current_channellabel.grid(row=4, column=0, sticky=Tk.W)
        self.current_channel = Tk.Label(master=self.expinfo_frame, text='{}'.format(self.globaldict['CURRENT_CHANNEL'].value), anchor='e')
        self.current_channel.grid(row=4, column=2)
