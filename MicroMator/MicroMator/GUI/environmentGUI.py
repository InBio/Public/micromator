# -*- coding: utf-8 -*-
'''
Created on Jan 31, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import os
import copy
import time
import datetime
import json
import tkinter as Tk
from tkinter import filedialog
from tkinter import messagebox
#from MicroMator.FileManagement import logger

MODULE='GUI_environment'

def disable(childList):
    '''disables frame
    '''
    for child in childList.winfo_children():
        child.configure(state='disable')

def enable(childList):
    '''enables frame
    '''
    for child in childList.winfo_children():
        child.configure(state='normal')

def enable_or_disable(frame, var):
    '''checks if module needs to be disabled or not
    '''
    if var.get():
        enable(frame)
    else:
        disable(frame)

def combine_funcs(*funcs):
    '''combines functions
    '''
    def combined_func(*args, **kwargs):
        '''is the combined function
        '''
        for f in funcs:
            f(*args, **kwargs)
    return combined_func

def launch_gui(options, root):
    '''launches the GUI
    '''
    root.title('MicroMator')
    root.iconbitmap(os.path.join(os.path.normpath(os.path.dirname(os.path.realpath(__file__))), 'm.ico')) #icon location (must be .ico)
    app = Application(options, master=root)
    root.geometry('+%d+%d' % (450, 195))
    app.mainloop()
    time.sleep(1)
    return app.options

#CreateToolTip code shamelessly copied from StackOverflow
class CreateToolTip(object):
    """
    create a tooltip for a given widget
    """
    def __init__(self, widget, text='widget info'):
        self.waittime = 500     #miliseconds
        self.wraplength = 180   #pixels
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.leave)
        self.widget.bind("<ButtonPress>", self.leave)
        self.id = None
        self.tw = None

    def enter(self, event=None):
        self.schedule()

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.showtip)

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.widget.after_cancel(id)

    def showtip(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = Tk.Label(self.tw, text=self.text, justify='left',
                       background="#ffffff", relief='solid', borderwidth=1,
                       wraplength = self.wraplength)
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tw
        self.tw= None
        if tw:
            tw.destroy()

class Application(Tk.Frame):
    '''this class is the GUI for that will let the user fill the folder_parameters
    '''
    def __init__(self, options, master=None):
        '''Args:
            options(dict): the dict with all the options
        '''
        Tk.Frame.__init__(self, master)
        self.master = master
        self.default_options = options
        self.options = options
        self.usedate_var = Tk.BooleanVar()
        self.simulation_var = Tk.BooleanVar()
        self.analysis_var = Tk.BooleanVar()
        self.microfluidics_var = Tk.BooleanVar()
        self.event_var = Tk.BooleanVar()
        self.model_var = Tk.BooleanVar()
        self.useZ_var = Tk.BooleanVar()
        self.dmd_var = Tk.BooleanVar()
        self.dmd_or_direct_var = Tk.BooleanVar()
        self.dmd_mask_var = Tk.StringVar()
        self.binning_var = Tk.BooleanVar()
        self.add_complexe_protocol_var = Tk.BooleanVar()
        self.add_analysis_pos_chooser = Tk.BooleanVar()
        self.analysis_list_params = []
        self.analysis_list_params_labels = []
        self.var = []

        self.users = []
        for user in os.listdir(self.options['FOLDER_PARAMETERS']['exp_folder']):
            self.users.append(user)
        self.analysis_software = []
        if self.options['ANALYSIS_SOFTWARE']:
            self.analysis_software = list(self.options['ANALYSIS_SOFTWARE'].keys())
        self.pack()
        self.createWidgets()

    def folder_command(self):
        '''what happens when push the folder button
        '''
        folder = filedialog.askdirectory()
        if folder:
            self.folder_text.delete("1.0", Tk.END)
            self.folder_text.insert(Tk.END, folder)

    def config_command(self):
        '''what happens when push the config button
        '''
        config = filedialog.askopenfilename(filetypes=[("Micromanager config", "*.cfg")])
        if config:
            self.config_text.delete("1.0", Tk.END)
            self.config_text.insert(Tk.END, config)

    def sim_config_command(self):
        '''what happens when push the sim_config button
        '''
        config = filedialog.askopenfilename(filetypes=[("Micromanager config", "*.cfg")])
        if config:
            self.sim_config_text.delete("1.0", Tk.END)
            self.sim_config_text.insert(Tk.END, config)

    def mmloc_command(self):
        '''what happens when push the mmloc button
        '''
        config = filedialog.askdirectory()
        if config:
            self.mmloc_text.delete("1.0", Tk.END)
            self.mmloc_text.insert(Tk.END, config)

    def exit_command(self):
        '''exit command
        '''
        self.quitting()
        self.options = 'Abort'
        #logger('Aborting MicroMator experiment', module=MODULE)

    def write_options_command(self):
        '''what happens when you press write options folder
        '''
        self.options = copy.deepcopy(self.default_options)
        self.options['ANALYSIS_SOFTWARE'] = {}
        try:
            self.options['FOLDER_PARAMETERS']['user'] = self.users[self.user_w.curselection()[0]]
        except IndexError as error:
            #logger(error, module=MODULE)
            #logger('no user selected, please select a user...', module=MODULE)
            messagebox.showerror("Error", "No User selected")
            return
        self.options['FOLDER_PARAMETERS']['project_name'] = os.path.join(self.project.get(1.0, Tk.END).strip('\n'))
        self.options['FOLDER_PARAMETERS']['exp_name'] = os.path.join(self.exp_name.get(1.0, Tk.END).strip('\n'))
        if not self.options['FOLDER_PARAMETERS']['project_name'] and not self.options['FOLDER_PARAMETERS']['exp_name']:
            messagebox.showwarning("Warning", "The experiment needs a project name or an experiment name")
            return
        exp_folder = os.path.join(self.folder_text.get(1.0, Tk.END).strip('\n'))
        if not os.path.isdir(exp_folder):
            messagebox.showerror("Error", "The experiment Folder path is incorrect")
            return
        self.options['FOLDER_PARAMETERS']['exp_folder'] = exp_folder
        self.options['FOLDER_PARAMETERS']['usedate'] = self.usedate_var.get()
        full_exp_path = os.path.join(self.options['FOLDER_PARAMETERS']['exp_folder'],
                                     self.options['FOLDER_PARAMETERS']['user'],
                                     self.options['FOLDER_PARAMETERS']['project_name'],
                                     self.options['FOLDER_PARAMETERS']['exp_name'])
        if self.options['FOLDER_PARAMETERS']['usedate']:
            date = datetime.datetime.now()
            full_exp_path = os.path.join(self.options['FOLDER_PARAMETERS']['exp_folder'],
                                         self.options['FOLDER_PARAMETERS']['user'],
                                         self.options['FOLDER_PARAMETERS']['project_name'],
                                         '{}-{}-{}_{}'.format(date.year, date.day, date.month, self.options['FOLDER_PARAMETERS']['exp_name']))
        if os.path.exists(full_exp_path):
            messagebox.showwarning("Warning", "The experiment name already exists")
        mmconfig = os.path.join(self.config_text.get(1.0, Tk.END).strip('\n'))
        if not os.path.exists(mmconfig):
            messagebox.showerror("Error", "The micromanager configuration path is incorrect")
            return
        self.options['MICROMANAGER_PARAMETERS']["mmconfig"] = mmconfig
        mmloc = os.path.join(self.mmloc_text.get(1.0, Tk.END).strip('\n'))
        if not os.path.exists(mmloc):
            messagebox.showerror("Error", "The micromanager location path is incorrect")
            return
        self.options['MICROMANAGER_PARAMETERS']["micromanagerlocation"] = mmloc
        mmsimulator = os.path.join(self.sim_config_text.get(1.0, Tk.END).strip('\n'))
        if not os.path.exists(mmsimulator):
            messagebox.showerror("Error", "The micromanager simulator configuration path is incorrect")
            return
        self.options['MICROMANAGER_PARAMETERS']["mmsimulator"] = mmsimulator
        self.options['MICROMANAGER_PARAMETERS']["Simulation"] = self.simulation_var.get()
        self.options['MICROMANAGER_PARAMETERS']["useZforpos"] = self.useZ_var.get()
        if self.binning_var.get():
            self.options['MICROMANAGER_PARAMETERS']['binning'] = float(self.binning_factor.get())
        else:
            self.options['MICROMANAGER_PARAMETERS']['binning'] = False
        if self.add_complexe_protocol_var.get():
            self.options['MICROMANAGER_PARAMETERS']['complexeprotocol'] = True
        self.options['MICROMANAGER_PARAMETERS']['dmdallpixel'] = self.dmd_or_direct_var.get()
        if self.analysis_var.get():
            soft = self.analysis_software[self.analysis_w.curselection()[0]]
            self.options['ANALYSIS_SOFTWARE']['module'] = self.default_options["ANALYSIS_SOFTWARE"][soft]['module']
            self.options['ANALYSIS_SOFTWARE']['parameters'] = self.default_options["ANALYSIS_SOFTWARE"][soft]['parameters']
            checkbutton_rank = 0
            for index, param in enumerate(self.default_options["ANALYSIS_SOFTWARE"][soft]['parameters']):
                print(self.analysis_list_params[index])
                if type(self.analysis_list_params[index]) == Tk.Spinbox:
                    self.default_options["ANALYSIS_SOFTWARE"][soft]['parameters'][param] = float(self.analysis_list_params[index].get())
                elif type(self.analysis_list_params[index]) == Tk.Text:
                    self.default_options["ANALYSIS_SOFTWARE"][soft]['parameters'][param] = self.analysis_list_params[index].get(1.0, Tk.END).strip('\n')
                elif type(self.analysis_list_params[index]) == Tk.Checkbutton:
                    # self.default_options["ANALYSIS_SOFTWARE"][soft]['parameters'][param] = self.var.get()
                    self.default_options["ANALYSIS_SOFTWARE"][soft]['parameters'][param] = self.var[checkbutton_rank].get()
                    checkbutton_rank += 1
            if self.add_analysis_pos_chooser.get():
                self.options['ANALYSIS_SOFTWARE']['parameters']['analysis_pos_chooser'] = True
        else:
            self.options['ANALYSIS_SOFTWARE'] = False
        if self.microfluidics_var.get():
            microfluidicmodule = os.path.join(self.microfluidics_text.get(1.0, Tk.END).strip('\n'))
            if not os.path.exists(microfluidicmodule):
                messagebox.showerror("Error", "The microfluidics module path is incorrect")
                return
            self.options['MICROFLUIDIC_PARAMETERS']["module"] = microfluidicmodule
        else:
            self.options['MICROFLUIDIC_PARAMETERS'] = False
        if self.event_var.get():
            event = os.path.join(self.event_text.get(1.0, Tk.END).strip('\n'))
            if not os.path.exists(event):
                messagebox.showerror("Error", "The Event module path is incorrect")
                return
            self.options['EVENTS_PARAMETERS']["creator"] = event
        else:
            self.options['EVENTS_PARAMETERS'] = False
        if self.model_var.get():
            model = os.path.join(self.model_text.get(1.0, Tk.END).strip('\n'))
            if not os.path.exists(model):
                messagebox.showerror("Error", "The Model module path is incorrect")
                return
            self.options['MODEL']["module"] = model
            for index, param in enumerate(self.default_options['MODEL']['parameters']):
                if type(self.list_params[index]) == Tk.Spinbox:
                    self.options['MODEL']['parameters'][param] = float(self.list_params[index].get())
                elif type(self.list_params[index]) == Tk.Checkbutton:
                    self.options['MODEL']['parameters'][param] = self.var_list[index].get()
                else:
                    self.options['MODEL']['parameters'][param] = self.list_params[index].get(1.0, Tk.END).strip('\n')
        else:
            self.options['MODEL'] = False
        self.quit.grid(row=6, column=1)
        #logger(self.options, module=MODULE)
        print(self.options)

    def microfluidics_command(self):
        '''what happens when you press the microfluidics button
        '''
        microfluidics = filedialog.askopenfilename(filetypes=[("microfluidics module", "*.py")])
        if microfluidics:
            self.microfluidics_text.delete("1.0", Tk.END)
            self.microfluidics_text.insert(Tk.END, microfluidics)

    def event_command(self):
        '''what happens when you press the event button
        '''
        event = filedialog.askopenfilename(filetypes=[("event module", "*.py")])
        if event:
            self.event_text.delete("1.0", Tk.END)
            self.event_text.insert(Tk.END, event)

    def model_command(self):
        '''what happens when you press the model button
        '''
        model = filedialog.askopenfilename(filetypes=[("model module", "*.py")])
        if model:
            self.model_text.delete("1.0", Tk.END)
            self.model_text.insert(Tk.END, model)

    def get_folder_command_project(self):
        '''what happens when you press the project button
        '''
        folder = filedialog.askdirectory()
        if folder:
            self.project.delete("1.0", Tk.END)
            self.project.insert(Tk.END, folder.split('/')[-1])

    def get_folder_command_exp(self):
        '''what happens when you press the exp_name button
        '''
        folder = filedialog.askdirectory()
        if folder:
            self.exp_name.delete("1.0", Tk.END)
            self.exp_name.insert(Tk.END, folder.split('/')[-1])

    def get_user_command(self):
        '''what happens when you press the user button
        '''
        folder = filedialog.askdirectory()
        if folder:
            user = folder.split('/')[-1]
            self.user_w.insert(Tk.END, user)
            self.users.append(user)

    def quitting(self):
        del self.usedate_var
        del self.simulation_var
        del self.analysis_var
        del self.microfluidics_var
        del self.event_var
        del self.model_var
        del self.useZ_var
        del self.dmd_var
        del self.dmd_or_direct_var
        del self.dmd_mask_var
        del self.binning_var
        del self.add_complexe_protocol_var
        del self.add_analysis_pos_chooser
        try:
            del self.var
        except AttributeError:
            pass

    def onselect(self, evt):
        # Note here that Tkinter passes an event object to onselect()
        row = 2
        w = evt.widget
        index = int(w.curselection()[0])
        value = w.get(index)
        parameters = self.options['ANALYSIS_SOFTWARE'][value]['parameters']
        print('You selected item %d: "%s"' % (index, value))
        print(parameters)
        for i,_ in enumerate(self.analysis_list_params):
            self.analysis_list_params[i].destroy()
            self.analysis_list_params_labels[i].destroy()
        self.analysis_list_params = []
        self.analysis_list_params_labels = []
        for param in parameters:
            self.analysis_list_params_labels.append(Tk.Label(self.analysisframe, text=param))
            type_param = type(parameters[param])
            if type_param == int:
                var = Tk.IntVar()
                var.set(parameters[param])
                self.analysis_list_params.append(Tk.Spinbox(master=self.analysisframe, from_=-10000, to=10000, justify=Tk.RIGHT, width=10, wrap=True, textvariable=var))
            elif type_param == float:
                var = Tk.IntVar()
                var.set(parameters[param])
                self.analysis_list_params.append(Tk.Spinbox(master=self.analysisframe, from_=-10000, to=10000, justify=Tk.RIGHT, width=10, wrap=True, increment=0.1, textvariable=var))
            elif type_param == bool:
                # self.var = Tk.BooleanVar()
                # self.analysis_list_params.append(Tk.Checkbutton(self.analysisframe, variable=self.var))
                self.var.append(Tk.BooleanVar())
                self.analysis_list_params.append(Tk.Checkbutton(self.analysisframe, variable=self.var[-1]))
            else:
                self.analysis_list_params.append(Tk.Text(self.analysisframe, height=1, width=90, wrap=None))
                self.analysis_list_params[-1].insert(Tk.END, parameters[param])
            self.analysis_list_params_labels[-1].grid(row=row, column=0, sticky=Tk.W)
            self.analysis_list_params[-1].grid(row=row, column=1, sticky=Tk.W)
            row += 1

    def createWidgets(self):
        '''this method is where all the widgets and buttons are created
        '''
        #Variables
        self.usedate_var.set(True)
        self.simulation_var.set(False)
        self.analysis_var.set(False)
        self.microfluidics_var.set(False)
        self.event_var.set(False)
        self.model_var.set(False)
        self.useZ_var.set(False)
        self.dmd_var.set(False)
        self.dmd_or_direct_var.set(False)
        self.dmd_mask_var.set(False)
        self.binning_var.set(False)
        self.add_complexe_protocol_var.set(False)
        self.add_analysis_pos_chooser.set(False)

        #Frames
        font = ("Helvetica", "12", "bold")
        self.labelframe = Tk.LabelFrame(self, text="MicroMator user options", font=font)
        self.labelframe.grid(row=0, sticky=Tk.W)
        self.mmlabelframe = Tk.LabelFrame(self, text="Micromanager user options", font=font)
        self.mmlabelframe.grid(row=1, sticky=Tk.W)
        self.eventframe = Tk.LabelFrame(self, text="Event module options", font=font)
        self.eventframe.grid(row=7, sticky=Tk.W)
        self.modelframe = Tk.LabelFrame(self, text="Model module options", font=font)
        self.modelframe.grid(row=9, sticky=Tk.W)
        self.analysisframe = Tk.LabelFrame(self, text="Analysis module", font=font)
        self.analysisframe.grid(row=3, sticky=Tk.W)
        self.analysispotframe = Tk.LabelFrame(self, text="Analysis module options", font=font)
        self.analysispotframe.grid(row=3, column=1, columnspan=2, sticky=Tk.W)
        self.microfluidicsframe = Tk.LabelFrame(self, text="Microfluidics module options", font=font)
        self.microfluidicsframe.grid(row=5, sticky=Tk.W)
        self.startframe = Tk.LabelFrame(self, text="Start MicroMator", font=font)
        self.startframe.grid(row=10, sticky=Tk.W)
        #self.dmd_frame = Tk.LabelFrame(self.mmlabelframe, text="DMD options")
        #self.dmd_frame.grid(row=9, sticky=Tk.W)
        self.binning_frame = Tk.LabelFrame(self.mmlabelframe, text="Binning options")
        self.binning_frame.grid(row=11, column=0, sticky=Tk.W)

        #User widget & label
        self.user_w_button = Tk.Button(self.labelframe, text='User', command=self.get_user_command)
        self.user_ttp = CreateToolTip(self.user_w_button, 'Press to add a new user by creating new folder in Path')
        self.user_w = Tk.Listbox(self.labelframe, height=len(self.users)+1, selectmode=Tk.BROWSE, activestyle='dotbox', exportselection=0)
        for user in self.users:
            self.user_w.insert(Tk.END, user)
        self.user_w_button.grid(row=1, column=0)
        self.user_w.grid(row=1, column=1)

        #project widget & label
        self.project_label = Tk.Button(self.labelframe, text='Project name', command=self.get_folder_command_project)
        self.project_ttp = CreateToolTip(self.project_label, 'Select or create new project name')
        self.project = Tk.Text(self.labelframe, height=1, width=50, wrap=None)
        self.project_label.grid(row=2, column=0, sticky=Tk.W, padx=10)
        self.project.grid(row=2, column=1, columnspan=2, sticky=Tk.W)

        #Exp_name widget & label
        self.exp_name_label = Tk.Button(self.labelframe, text='Experiment name', command=self.get_folder_command_exp)
        self.exp_ttp = CreateToolTip(self.exp_name_label, 'Select or create new experiment name')
        self.exp_name = Tk.Text(self.labelframe, height=1, width=50, wrap=None)
        self.exp_name_label.grid(row=3, column=0, padx=10)
        self.exp_name.grid(row=3, column=1, columnspan=2, sticky=Tk.W)

        #folder widget & label
        self.folder = Tk.Button(self.labelframe, height=1, width=10, command=self.folder_command, text='Path')
        self.folder_ttp = CreateToolTip(self.folder, 'Choose the path for the MicroMator experiment')
        self.folder_text = Tk.Text(self.labelframe, height=2, width=90, wrap=None)
        self.folder_text.insert(Tk.END, self.default_options['FOLDER_PARAMETERS']['exp_folder'])
        self.folder.grid(row=0, column=0)
        self.folder_text.grid(row=0, column=1, columnspan=2)

        #usedate widget & label
        self.usedate = Tk.Checkbutton(self.labelframe, text='Use date', variable=self.usedate_var)
        self.usedate.grid(row=4, column=0, sticky=Tk.W)
        self.usedate_ttp = CreateToolTip(self.usedate, "If ticked, the experiment name will be : 'year_date_month_MyExperimentName'")

        #micromanager location widget & label
        self.mmloc = Tk.Button(self.mmlabelframe, height=1, width=10, command=self.mmloc_command, text='Vµmanager')
        self.mmloc_text = Tk.Text(self.mmlabelframe, height=2, width=90, wrap=None)
        self.mmloc_text.insert(Tk.END, self.default_options['MICROMANAGER_PARAMETERS']['micromanagerlocation'])
        self.mmloc.grid(row=1, column=0, sticky=Tk.W, padx=10)
        self.mmloc_text.grid(row=1, column=1, columnspan=2, sticky=Tk.W)
        self.mmloc_ttp = CreateToolTip(self.mmloc, 'Select the micromanager version ("beta", "gamma" or "1.422/1.423") need to be in given micromanger path')

        #micromanger config widget & label
        self.config = Tk.Button(self.mmlabelframe, height=1, width=10, command=self.config_command, text='Config')
        self.config_text = Tk.Text(self.mmlabelframe, height=2, width=90, wrap=None)
        self.config_text.insert(Tk.END, self.default_options['MICROMANAGER_PARAMETERS']['mmconfig'])
        self.config.grid(row=2, column=0, sticky=Tk.W, padx=10)
        self.config_text.grid(row=2, column=1, columnspan=2, sticky=Tk.W)
        self.config_ttp = CreateToolTip(self.config, 'Select the micromanager .cfg file')

        #useZ button
        self.useZ_button = Tk.Checkbutton(self.mmlabelframe, text='use Z coord', variable=self.useZ_var)
        self.useZ_button.grid(row=5, column=0, sticky=Tk.W)
        self.useZ_ttp = CreateToolTip(self.useZ_button, 'If unticked MicroMator will not use the Z when changing positions, useful for letting the AutoFocus take care of the Z')

        #micromanager sim_config widget & label
        self.sim_config = Tk.Button(self.mmlabelframe, height=1, width=10, command=self.sim_config_command, text='Sim Config')
        self.sim_config_text = Tk.Text(self.mmlabelframe, height=2, width=90, wrap=None)
        self.sim_config_text.insert(Tk.END, self.default_options['MICROMANAGER_PARAMETERS']['mmsimulator'])
        self.sim_config.grid(row=4, column=0, sticky=Tk.W, padx=10)
        self.sim_config_text.grid(row=4, column=1, columnspan=2, sticky=Tk.W)
        self.sim_config_ttp = CreateToolTip(self.sim_config, 'Select the micromator simulation .cfg file')

        #simulation widget & label
        self.simulation = Tk.Checkbutton(self.mmlabelframe, text='Simulation Acquisition', variable=self.simulation_var)
        self.simulation.grid(row=0, column=0, sticky=Tk.W)
        self.simulation_ttp = CreateToolTip(self.simulation, 'If ticked it will be a simulation MicroMator run, useful for testing')

        #use DMD or not button
        #self.dmd = Tk.Checkbutton(self.mmlabelframe, text='DMD', variable=self.dmd_var, command=lambda: enable_or_disable(self.dmd_frame, self.dmd_var))
        #self.dmd.grid(row=7, column=0, sticky=Tk.W)
        #self.dmd_ttp = CreateToolTip(self.dmd, 'If ticked it will enable use of a DMD like device')

        self.dmd_or_direct = Tk.Checkbutton(self.mmlabelframe, text='DMD allpixels instead of DIRECT', variable=self.dmd_or_direct_var)
        self.dmd_or_direct.grid(row=7, column=0, sticky=Tk.W)
        self.dmd_msk_generation_ttp = CreateToolTip(self.dmd_or_direct, 'if use all pixels map instead of Direct')

        #dmd_mask_modes = [('Static mask generation', 'One mask per pos that does not change during exp'), ('Dynamic mask generation', 'One mask per pos & per frame')]
        #for text, desc in dmd_mask_modes:
            #self.dmd_msk_generation = Tk.Radiobutton(self.dmd_frame, text=text, variable=self.dmd_mask_var, value=text)
            #self.dmd_msk_generation.pack(anchor=Tk.W)
            #self.dmd_msk_generation_ttp = CreateToolTip(self.dmd_msk_generation, desc)

        #bin images or not button
        self.binning = Tk.Checkbutton(self.mmlabelframe, text='Bin images', variable=self.binning_var, command=lambda: enable_or_disable(self.binning_frame, self.binning_var))
        self.binning.grid(row=10, column=0, sticky=Tk.W)
        self.binning_ttp = CreateToolTip(self.binning, 'If ticked it will bin the taken images')
        self.binning_factor = Tk.Spinbox(master=self.binning_frame, from_=0, to_=10, justify=Tk.RIGHT, width=10, wrap=True, increment=0.1)
        self.binning_factor_ttp = CreateToolTip(self.binning_factor, 'The ratio at which images are binned')
        self.binning_factor_label = Tk.Label(master=self.binning_frame, text='Ratio')
        self.binning_factor.grid(row=0, column=0, sticky=Tk.W)
        self.binning_factor_label.grid(row=0, column=1)

        #complexe acquisition protocol button
        self.add_complexe_protocol = Tk.Checkbutton(self.mmlabelframe, text='Complexe Protocol', variable=self.add_complexe_protocol_var)
        self.add_complexe_protocol.grid(row=12, column=0, sticky=Tk.W)

        #save options widget & label
        self.write_options = Tk.Button(self.startframe, height=1, width=20, command=self.write_options_command, text='Save Options')
        self.write_options.grid(row=6, column=0)
        self.write_options_ttp = CreateToolTip(self.write_options, 'Save and verify if options used for MicroMator experiment are correct')

        #Abort option
        self.abort = Tk.Button(self.startframe, height=1, width=20, fg='red', command=combine_funcs(self.master.destroy, self.exit_command), text='Abort')
        self.abort.grid(row=6, column=2)
        self.abort_ttp = CreateToolTip(self.abort, 'Aborts MicroMator exp launch')

        #analysis widget & label
        self.analysis_w_label = Tk.Label(self.analysisframe, text='Image Analysis Software')
        self.analysis_w = Tk.Listbox(self.analysisframe, height=len(self.analysis_software)+1, selectmode=Tk.BROWSE, activestyle='dotbox', exportselection=0)
        self.analysis_w.bind("<<ListboxSelect>>", self.onselect)
        for soft in self.analysis_software:
            self.analysis_w.insert(Tk.END, soft)
        self.analysis_w_label.grid(row=0, column=0, sticky=Tk.W, padx=10)
        self.analysis_w.grid(row=0, column=1, sticky=Tk.W)

        #analysis_chooser button
        self.analysis_pos_chooser = Tk.Checkbutton(self.analysisframe, text='Analysis pos chooser', variable=self.add_analysis_pos_chooser)
        self.analysis_pos_chooser.grid(row=1, column=0)

        #microfluidics widget & label
        self.microfluidics = Tk.Button(self.microfluidicsframe, height=1, width=10, command=self.microfluidics_command, text='Microfluidics')
        self.microfluidics_text = Tk.Text(self.microfluidicsframe, height=2, width=90, wrap=None)
        self.microfluidics_text.insert(Tk.END, self.default_options['MICROFLUIDIC_PARAMETERS']['module'])
        self.microfluidics.grid(row=0, column=0, sticky=Tk.W, padx=10)
        self.microfluidics_text.grid(row=0, column=1, sticky=Tk.E)
        self.microfluidics_ttp = CreateToolTip(self.microfluidics, 'Select the Microfluidics module path')

        #event widget & label
        self.event = Tk.Button(self.eventframe, height=1, width=10, command=self.event_command, text='Event')
        self.event_text = Tk.Text(self.eventframe, height=2, width=90, wrap=None)
        self.event_text.insert(Tk.END, self.default_options['EVENTS_PARAMETERS']['creator'])
        self.event.grid(row=0, column=0, sticky=Tk.W, padx=10)
        self.event_text.grid(row=0, column=1, sticky=Tk.E)
        self.even_ttp = CreateToolTip(self.event, 'Select the Event module path')

        #model widget & label
        self.model = Tk.Button(self.modelframe, height=1, width=10, command=self.model_command, text='Model')
        self.model_text = Tk.Text(self.modelframe, height=2, width=90, wrap=None)
        self.model_text.insert(Tk.END, self.default_options['MODEL']['module'])
        self.model.grid(row=0, column=0, sticky=Tk.W, padx=10)
        self.model_text.grid(row=0, column=1, sticky=Tk.E)
        self.list_params = []
        self.list_params_labels = []
        row = 1
        self.var_list = []
        for param in self.default_options['MODEL']['parameters']:
            self.list_params_labels.append(Tk.Label(self.modelframe, text=param))
            type_param = type(self.default_options['MODEL']['parameters'][param])
            if type_param == int:
                var = Tk.IntVar()
                var.set(self.default_options['MODEL']['parameters'][param])
                self.var_list.append(var)
                self.list_params.append(Tk.Spinbox(master=self.modelframe, from_=-1000, to=1000, justify=Tk.RIGHT, width=10, wrap=True, textvariable=var))
            elif type_param == float:
                var = Tk.IntVar()
                var.set(self.default_options['MODEL']['parameters'][param])
                self.var_list.append(var)
                self.list_params.append(Tk.Spinbox(master=self.modelframe, from_=-1000, to=1000, justify=Tk.RIGHT, width=10, wrap=True, increment=0.1, textvariable=var))
            elif type_param == bool:
                var = Tk.BooleanVar()
                var.set(self.default_options['MODEL']['parameters'][param])
                self.var_list.append(var)
                self.list_params.append(Tk.Checkbutton(master=self.modelframe, variable=var))
            else:
                self.list_params.append(Tk.Text(self.modelframe, height=1, width=20, wrap=None))
                self.var_list.append(None)
                self.list_params[-1].insert(Tk.END, self.default_options['MODEL']['parameters'][param])
            self.list_params_labels[-1].grid(row=row, column=0, sticky=Tk.W)
            self.list_params[-1].grid(row=row, column=1, sticky=Tk.W)
            row += 1

        self.model_ttp = CreateToolTip(self.model, 'Select the Model module path')

        #module widget & label
        self.analysisuse = Tk.Checkbutton(self, text='Enable Analysis Module', variable=self.analysis_var,
                                          command=lambda: enable_or_disable(self.analysisframe, self.analysis_var))
        self.analysisuse_ttp = CreateToolTip(self.analysisuse, 'If ticked MicroMator experiment will run live analysis using selected module')
        self.analysisuse.grid(row=2, column=0, sticky=Tk.W)

        self.microfluidicsuse = Tk.Checkbutton(self, text='Enable Microfluidics Module', variable=self.microfluidics_var,
                                               command=lambda: enable_or_disable(self.microfluidicsframe, self.microfluidics_var))
        self.microfluidicsuse_ttp = CreateToolTip(self.microfluidicsuse, 'If ticked MicroMator experiment will run microfluidics module using selected module')
        self.microfluidicsuse.grid(row=4, column=0, sticky=Tk.W)

        self.eventuse = Tk.Checkbutton(self, text='Enable Event Module', variable=self.event_var,
                                       command=lambda: enable_or_disable(self.eventframe, self.event_var))
        self.eventuse_ttp = CreateToolTip(self.eventuse, 'If ticked MicroMator experiment will run Events using selected module')
        self.eventuse.grid(row=6, column=0, sticky=Tk.W)

        self.modeluse = Tk.Checkbutton(self, text='Enable Model Module', variable=self.model_var,
                                       command=lambda: enable_or_disable(self.modelframe, self.model_var))
        self.modeluse_ttp = CreateToolTip(self.modeluse, 'If ticked MicroMator experiment will run models using selected module')
        self.modeluse.grid(row=8, column=0, sticky=Tk.W)

        #check disable or enable
        enable_or_disable(self.analysisframe, self.analysis_var)
        enable_or_disable(self.microfluidicsframe, self.microfluidics_var)
        enable_or_disable(self.eventframe, self.event_var)
        enable_or_disable(self.modelframe, self.model_var)
        #enable_or_disable(self.dmd_frame, self.dmd_var)
        enable_or_disable(self.binning_frame, self.binning_var)

        #quit widget & label
        self.quit = Tk.Button(self.startframe, text='Start MicroMator protocol creation', command=combine_funcs(self.master.destroy, self.quitting), fg='green')


if __name__ == '__main__':
    root = Tk.Tk()
    options_json_file = 'C:\\Users\\lifeware\\workspace\\MicroMator\\MicroMator\\default_options.json'
    with open(options_json_file) as jsonfile:
        optionsjson = json.load(jsonfile)
    launch_gui(optionsjson, root)
