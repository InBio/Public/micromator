#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Nov 5, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import time
import os
import json
import tkinter as Tk
from MicroMator.FileManagement import logger

MODULE = 'Complex Protocol GUI'

def launch_gui(options, globaldict, protocol, positions):
    '''launches the GUI
    '''
    root = Tk.Toplevel()
    root.iconbitmap(os.path.join(os.path.normpath(os.path.dirname(os.path.realpath(__file__))), 'm.ico')) #icon location (must be .ico)
    app = Application(options, globaldict, protocol, positions, master=root)
    app.mainloop()
    root.destroy()
    time.sleep(1)

def combine_funcs(*funcs):
    '''combines functions
    '''
    def combined_func(*args, **kwargs):
        '''is the combined function
        '''
        for f in funcs:
            f(*args, **kwargs)
    return combined_func

class Application(Tk.Frame):
    '''this class is the GUI for that will let the user design complexe acq_protocols
    '''
    def __init__(self, options, globaldict, protocol, positions, master=None):
        '''Args:
            options(dict): the dict with all the options
            protocol(object): the Protocol object that has channels and the exposures
            position(list): the list of Position object
        '''
        Tk.Frame.__init__(self, master)
        self.master = master
        self.options = options
        self.globaldict = globaldict
        self.protocol = protocol
        self.positions = positions
        self.channel_list = [[Tk.BooleanVar() for i, _ in enumerate(self.protocol.channel_list)] for j, _ in enumerate(self.positions)]
        for pos, _ in enumerate(self.positions):
            for channel in self.channel_list[pos]:
                channel.set(True)
        self.pack()
        self.createWidgets()

    def add_channel_to_pos(self):
        '''this method will save the protocol
        '''
        for index, position in enumerate(self.positions):
            for i, channel in enumerate(self.protocol.channel_list):
                if not self.channel_list[index][i].get() and channel not in position.channels:
                    position.channels.append(channel)
            if position.channels:
                print(position.label, position.channels)
        logger('saved protocol', module=MODULE)

    def exit_command(self):
        '''exit command
        '''
        self.add_channel_to_pos()
        self.quitting()
        self.globaldict['COMPLEXE_GUI_DONE'].value = True
        self.master.quit()

    def quitting(self):
        for i in self.channel_list:
            for j in i:
                del j
        self.channel_list = []

    def createWidgets(self):
        '''this method is where all the widgets and buttons are created
        '''
        self.labelframe = Tk.LabelFrame(self, text="MicroMator Complex Acquisition")
        self.labelframe.grid(row=0)
        for index, position in enumerate(self.positions):
            if position.label:
                self.pos_label = Tk.LabelFrame(master=self.labelframe, text='{}'.format(position.label), font="Verdana 10 bold")
                self.pos_label.grid(row=0, column=index)
            for i, channel in enumerate(self.protocol.channel_list):
                self.channel_use = Tk.Checkbutton(self.pos_label, text='{} - {}ms'.format(channel.name, channel.exposure), variable=self.channel_list[index][i])
                self.channel_use.grid(row=i+1, column=index, sticky=Tk.W)
        self.add_complexe_protocol = Tk.Button(self, height=1, width=20, command=self.add_channel_to_pos, text='Save')
        self.add_complexe_protocol.grid(row=1, column=0, sticky=Tk.W)
        self.abort = Tk.Button(self, height=1, width=20, fg='red', command=combine_funcs(self.master.destroy, self.exit_command), text='Save & Quit')
        self.abort.grid(row=2, column=0, sticky=Tk.W)

if __name__ == '__main__':
    import MicroMator.DataManagement.position_structure as ps
    import MicroMator.DataManagement.protocol_structure as pros
    import MicroMator.FileManagement.folder_manager as fm
    import MicroMator.FileManagement.file_data_extractor as fde
    root = Tk.Tk()
    options_json_file = 'D:\\lifeware\\Experiments\\StevenFletcher\\test\\test\\Protocols\\MicroMator_parameters.json'
    with open(options_json_file) as jsonfile:
        options = json.load(jsonfile)

    folder_manager_obj = fm.Filer(options)
    acqu_data = fde.AcquisitionDataExtractormm2(folder_manager_obj, options)
    positions = ps.AllPositions(folder_manager_obj, options["MICROMANAGER_PARAMETERS"]["mmversion"]).positions
    protocol = pros.Protocol(timestamp=0, acquorder=None, acqNumframes=0, acqInterval=0, channel_list=acqu_data.channel_list, positions_list=None, dmd=False, track=False)

    launch_gui(options, None, protocol, positions)
