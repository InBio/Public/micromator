# -*- coding: utf-8 -*-
'''
Created on Feb 12, 2019

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import time
import os
import tkinter as Tk
from tkinter import messagebox
import threading
import subprocess
#from MicroMator.FileManagement import logger

MODULE = 'GUI_prompt'

def launch_gui(folder_manager_obj, options, globaldict):
    '''launches the GUI
    Args:
        folder_manager_obj(str): the path to the experiment folder
        options(str): the path to micromanager
    '''
    root = Tk.Toplevel()
    root.title('MicroMator')
    root.iconbitmap(os.path.join(os.path.normpath(os.path.dirname(os.path.realpath(__file__))), 'm.ico')) #icon location (must be .ico)
    app = Application(folder_manager_obj, options, globaldict, master=root)
    root.geometry('+%d+%d' % (450, 195))
    app.mainloop()
    root.destroy()
    time.sleep(1)

def combine_funcs(*funcs):
    '''combines functions
    '''
    def combined_func(*args, **kwargs):
        '''is the combined function
        '''
        for f in funcs:
            f(*args, **kwargs)
    return combined_func

class Application(Tk.Frame):
    '''this class is the GUI for that will let the user fill the folder_parameters
    '''
    def __init__(self, folder_manager_obj, options, globaldict, master=None):
        '''Args:
        '''
        Tk.Frame.__init__(self, master)
        self.master = master
        self.folder_manager_obj = folder_manager_obj
        self.path = folder_manager_obj.protocols_path
        self.options = options
        self.micromanager_path = options['MICROMANAGER_PARAMETERS']['micromanagerlocation']
        self.globaldict = globaldict
        self.acq_ready = self.pos_ready = self.mask_ready = False
        self.pack()
        self.createWidgets()

    def micromanager_command(self):
        '''opens micromanager
        '''
        #logger('Opening Micromanager', module=MODULE)
        subprocess.Popen(os.path.join(self.micromanager_path, 'ImageJ.exe'))

    def quit_command(self):
        '''what happens when you call quit
        '''
        self.globaldict['MMLOADED'].value = True
        self.open_micromanager.pack_forget()
        self.message_acq.pack_forget()
        self.message_pos.pack_forget()
        if self.options["MICROMANAGER_PARAMETERS"]["dmdallpixel"]:
            self.message_mask.pack_forget()
        self.message.pack_forget()
        self.quit.pack_forget()
        self.master.quit()

    def check_acq_file(self):
        '''func to check if .acq file are present
        '''
        while True:
            if self.globaldict['ABORT'].value:
                return
            for file_ in os.listdir(self.path):
                if file_.endswith('.acq'):
                    self.message_acq['fg'] = 'green'
                    self.acq_ready = True
                    return

    def check_pos_file(self):
        '''func to check if .pos file are present
        '''
        while True:
            if self.globaldict['ABORT'].value:
                return
            for file_ in os.listdir(self.path):
                if file_.endswith('.pos'):
                    self.message_pos['fg'] = 'green'
                    self.pos_ready = True
                    return

    def check_mask_file(self):
        '''func to check if dmd mask file is present
        '''
        while True:
            if self.globaldict['ABORT'].value:
                return
            for file_ in os.listdir(self.folder_manager_obj.bitmaps_path):
                if file_.endswith('.tif'):
                    self.message_mask['fg'] = 'green'
                    self.mask_ready = True
                    return

    def check_quit_button(self):
        '''func to turn red to green when
        '''
        while True:
            if self.globaldict['ABORT'].value:
                return
            if self.acq_ready and self.pos_ready:
                self.closemm.pack()
                return

    def closemm_command(self):
        '''what happens when you press close command
        '''
        self.closemm.pack_forget()
        messagebox.showwarning('Warning', 'Please make sure Micromanager is closed before proceeding!')
        self.quit.pack()

    def aborting(self):
        '''set abort signal
        '''
        self.globaldict['ABORT'].value = True

    def createWidgets(self):
        '''this method is where all the widgets and buttons are created
        '''
        self.abort = Tk.Button(self, text='Abort', fg='red', command=combine_funcs(self.master.destroy, self.aborting))
        self.open_micromanager = Tk.Button(self, command=self.micromanager_command, text='Open micromanager')
        #self.open_micromanager.pack()
        self.abort.pack({"side": "top"})
        self.message = Tk.Message(self, text='Save in : {}'.format(self.path), width=500)
        self.message_acq = Tk.Message(self, text='.acq Acquisition file', width=500, fg='red')
        threading.Thread(target=self.check_acq_file).start()
        self.message_pos = Tk.Message(self, text='.pos Positions file', width=500, fg='red')
        threading.Thread(target=self.check_pos_file).start()
        self.message.pack()
        if self.options["MICROMANAGER_PARAMETERS"]["dmdallpixel"]:
            self.message_mask = Tk.Message(self, text='.tif DMD mask file', width=500, fg='red')
            #threading.Thread(target=self.check_mask_file).start()
            self.message_mask.pack()
        self.message_acq.pack()
        self.message_pos.pack()
        self.closemm = Tk.Button(self, text='Please Close MicroManager', command=self.closemm_command, fg='red')
        self.quit = Tk.Button(self, text='Load Protocol & Positions', command=self.quit_command, fg='green')
        threading.Thread(target=self.check_quit_button).start()

if __name__ == '__main__':
    launch_gui('D:\\lifeware\\Experiments\\', 'C:\\Program Files\\Micro-Manager-2.0beta_old', None)
