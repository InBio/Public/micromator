# -*- coding: utf-8 -*-
'''
Created on Oct 18, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import sys
import os
import time
import threading
import tkinter as Tk
from MicroMator.GUI import parampromptGUI as pgui, mmwrapperGUI as mmgui, imageviewerGUI as imgui
from MicroMator.GUI import complexe_acq_editorGUI as cagui
from MicroMator.GUI import analysis_pos_chooserGUI as apcgui

def check_quit(root, cellasic, globaldict, globalonix):
    '''checks if experiment has ended to quit GUI
    Args:
        root(object): the Tkinter object
        cellasic(object instance): the cellasic object instance
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        globalonix(dict): the dict with all the global signals for the GUI
    '''
    while True:
        time.sleep(0.05)
        if globaldict['ABORT'].value:
            root.quit()
            sys.exit()
            return
        if globaldict['END_ACQ'].value: #MMwrapper loop
            if cellasic:
                if globalonix['ABORT'].value or globalonix['RUNEND'].value: #cellasic loop
                    globalonix['ABORT'].value = True
                    globalonix['RUNEND'].value = True
                    time.sleep(2)
                    root.quit()
                    return
            else:
                time.sleep(2)
                root.quit()
                return

def rungui(micromator, options, folder_manager_obj, acqu_protocol, globaldict, globalonix):
    '''this function runs the gui
    Args:
        options(dict): dict of all options
        folder_manager_obj(obj): the folder_manager instance
        acqu_protocol(Protocol object): the Protocol object that stores all the info for the acquisition
        globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
          example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        globalonix(dict): the dict with all the global signals for the GUI
    '''
    wait = True
    root = Tk.Tk()
    def _delete_window():
        print('closing window...')
        globaldict['END_ACQ'].value = True
        globalonix['ABORT'].value = True
        globalonix['RUNEND'].value = True
        while imgui.CHANNEL.isSet() and imgui.FRAME.isSet() and imgui.POS.isSet():
            time.sleep(1)
    root.protocol("WM_DELETE_WINDOW", _delete_window)
    root.title('MicroMator')
    root.iconbitmap(os.path.join(os.path.normpath(os.path.dirname(os.path.realpath(__file__))), 'm.ico')) #icon location (must be .ico)
    root.withdraw()
    check_quit_thread = threading.Thread(target=check_quit, args=(root, options['MICROFLUIDIC_PARAMETERS'], globaldict, globalonix))
    check_quit_thread.start()
    if options['MICROFLUIDIC_PARAMETERS']:
        if 'libonix' in options['MICROFLUIDIC_PARAMETERS']['module']:
            import libonixGUI.libonixGUI as libgui
            import libonixGUI.stepscriptGUI as stepgui
            micromatormodule = libgui.Application(globalonix, master=root)
            micromatormodule.master.title('MicroMator') #title of window
            micromatormodule.pack()
            cellroot = Tk.Toplevel()
            root.iconbitmap(os.path.join(os.path.normpath(os.path.dirname(os.path.realpath(__file__))), 'm.ico')) #icon location (must be .ico)
            cellasicscriptmodule = stepgui.Application(options['MICROFLUIDIC_PARAMETERS']['step_script'], folder_manager_obj.protocols_path, master=cellroot)
            cellroot.config(menu=cellasicscriptmodule.menubar)
            cellasicscriptmodule.master.title('CellASIC Scripter')
            cellroot.quit()
            pgui.launch_gui(folder_manager_obj, options, globaldict)
            wait = False
            while not globaldict['MMLOADED'].value:
                micromatormodule.update_idletasks()
                micromatormodule.update()
    if wait:
        pgui.launch_gui(folder_manager_obj, options, globaldict)
    if globaldict['ABORT'].value:
        root.quit()
        sys.exit()
        return
    root.update()
    root.deiconify()
    microscopemodule = mmgui.Application(globaldict, folder_manager_obj, master=root)
    microscopemodule.master.title('MicroMator') #title of window
    microscopemodule.pack()
    if options["MICROMANAGER_PARAMETERS"]['complexeprotocol']:
        while not globaldict['PLOADED'].value:
            pass
        cagui.launch_gui(options, globaldict, micromator.protocol, micromator.positions)
    try:
        options['ANALYSIS_SOFTWARE']['parameters']['analysis_pos_chooser']
        while not globaldict['PLOADED'].value:
            pass
        apcgui.launch_gui(options, globaldict, micromator.protocol, micromator.positions)
    except:
        pass
    #imgui.Application(folder_manager_obj.acquisitions_path, acqu_protocol, options, globaldict, root)
    root.geometry('+%d+%d' % (450, 195))
    microscopemodule.mainloop()
    root.quit()
