'''
Created on Oct 17, 2018

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''

import os
import tkinter as Tk
import threading
import time
import natsort
from PIL import ImageTk, Image
from MicroMator.FileManagement import logger

POS = threading.Event()
FRAME = threading.Event()
CHANNEL = threading.Event()
MODULE = 'GUI_imgviewer'

class Application(Tk.Frame):
    '''this class is the GUI for the MMwrapper acqu loop
    '''
    def __init__(self, folder_manager_obj_acqupath, acqu_protocol, options, globaldict, master=None):
        '''Args:
            folder_manager_obj_acqupath(str): the path of the acquisition folder
            acqu_protocol(Protocol object): the Protocol object containing the info of the experiment
            options(dict): dict of all micromator options
            globaldict(dict): the dictionnary with all the global signals used to communicate betweeen different modules.
              example of one key of this dictionnary: 'INIT': GLOBALVARS.Value('i', False), #Bool that contains the Start signal for the GUI & the Acqu loop
        '''
        Tk.Frame.__init__(self, master)
        self.folder_manager_obj_acqupath = folder_manager_obj_acqupath
        self.acqu_protocol = acqu_protocol
        self.pos = 0
        self.frame = 0
        self.channel = 0
        self.channel_names = []
        self.globaldict = globaldict
        self.options = options
        self.pack()
        self.createwidgets()

    def createwidgets(self):
        '''this method is where all the widgets and buttons are created
        '''
        self.labelframe = Tk.LabelFrame(self, text="Image Viewer")
        self.labelframe.pack(side="bottom", fill="both", expand="yes")

        #label widget
        self.pathlabel = Tk.Label(self.labelframe)
        self.pathlabel.pack(side="bottom", fill="both", expand="yes")

        #image viewer window
        self.image = ImageTk.PhotoImage(Image.open(os.path.join(os.path.normpath(os.path.dirname(os.path.realpath(__file__))), 'MicroMator_background.png')))
        self.imgpanel = Tk.Label(self.labelframe, image=self.image)
        self.imgpanel.pack(side="bottom", fill="both", expand="yes")

        #pos slider
        self.posslider = Tk.Scale(self.labelframe, label='POS', from_=0, to=0, orient=Tk.HORIZONTAL, width=12.5)
        threading.Thread(target=self.getpos, args=(), daemon=True).start()
        self.posslider.pack(side="bottom", fill="both", expand="yes")

        #frame slider
        self.frameslider = Tk.Scale(self.labelframe, label='FRAME', from_=0, to=0, orient=Tk.HORIZONTAL, width=12.5)
        threading.Thread(target=self.getframe, args=(), daemon=True).start()
        self.frameslider.pack(side="bottom", fill="both", expand="yes")

        #channel slider
        self.channelslider = Tk.Scale(self.labelframe, label='CHANNEL', from_=0, to=0, orient=Tk.HORIZONTAL, width=12.5)
        threading.Thread(target=self.getchannel, args=(), daemon=True).start()
        self.channelslider.pack(side="bottom", fill="both", expand="yes")

        while not self.globaldict['PLOADED'].value:
            pass
        threading.Thread(target=self.update_gui, args=()).start()

    def getpos(self):
        '''this method will get the position user set and show correct image pos
        '''
        self.pos = self.posslider.get()
        while not self.globaldict['END_ACQ'].value:
            time.sleep(0.05)
            if self.pos != self.posslider.get():
                self.pos = self.posslider.get()
                try:
                    self.set_image(self.get_imagepath())
                except FileNotFoundError:
                    logger('image not written on disk yet...', 'warning', True, module=MODULE)
                logger('pos: {}'.format(self.pos), print_=False, module=MODULE)
        POS.set()

    def getframe(self):
        '''this method will get the frame user set and show correct image frame
        '''
        self.frame = self.frameslider.get()
        while not self.globaldict['END_ACQ'].value:
            time.sleep(0.05)
            if self.frame != self.frameslider.get():
                self.frame = self.frameslider.get()
                try:
                    self.set_image(self.get_imagepath())
                except FileNotFoundError:
                    logger('image not written on disk yet...', 'warning', False, module=MODULE)
                logger('frame: {}'.format(self.frame), print_=False, module=MODULE)
        FRAME.set()

    def getchannel(self):
        '''this method will get the channel user set and show correct image channel
        '''
        self.channel = self.channelslider.get()
        while not self.globaldict['END_ACQ'].value:
            time.sleep(0.05)
            if self.channel != self.channelslider.get():
                self.channel = self.channelslider.get()
                try:
                    self.set_image(self.get_imagepath())
                except FileNotFoundError:
                    logger('image not written on disk yet...', 'warning', False, module=MODULE)
                logger('channel: {}'.format(self.channel), print_=False, module=MODULE)
        CHANNEL.set()

    def channelname(self):
        '''switches to correct channel
        '''
        return self.channel_names[self.channel]

    def get_imagepath(self):
        '''creates the new path where to find image
        '''
        if self.options["MICROMANAGER_PARAMETERS"]["dmd"]:
            channel_name = self.channelname().replace('DIRECT', 'DMD')
        else:
            channel_name = self.channelname()
        imgname = 'img_{}_{}.tif'.format(self.frame, channel_name)
        imgpath = os.path.join(self.folder_manager_obj_acqupath, 'pos{}'.format(self.pos), imgname)
        return imgpath

    def set_image(self, image):
        '''this metod changes image when called
        Args:
            image(str): full path to image
        '''
        img = Image.open(image)
        #converting to 8bit images
        image8 = img.convert('L')
        if image8.size != (1024, 1024):
            image8 = image8.resize((1024, 1024))
        self.image = ImageTk.PhotoImage(image8)
        self.imgpanel.configure(image=self.image)
        self.pathlabel.configure(font=("Courier", 10))
        self.pathlabel.configure(text=image)

    def update_gui(self):
        '''this method will update the values of the sliders according to what is in the various folders
        '''
        while not self.globaldict['END_ACQ'].value:
            time.sleep(0.05)
            number_frames = 0
            number_pos = len(os.listdir(self.folder_manager_obj_acqupath))-1
            number_channels = len(self.acqu_protocol.brightfield)+len(self.acqu_protocol.fluochannels)-1
            for channel in self.acqu_protocol.channel_list:
                self.channel_names.append(channel.name)
            if len(os.listdir(os.path.join(self.folder_manager_obj_acqupath, 'pos{}'.format(number_pos)))) != 1:
                number_images = os.listdir(os.path.join(self.folder_manager_obj_acqupath, 'pos{}'.format(number_pos)))
                if number_images:
                    number_images = natsort.natsorted(number_images)
                    last_image = number_images[-1]
                    number_frames = [int(s) for s in last_image.split('_') if s.isdigit()][0]
            self.posslider.configure(to=number_pos)
            self.frameslider.configure(to=number_frames)
            self.channelslider.configure(to=number_channels)
