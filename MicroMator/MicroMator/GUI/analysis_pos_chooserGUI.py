'''
Created on Mar 17, 2020

@author: Steven Fletcher - INRIA - Institut Pasteur - InBio
'''
import time
import os
import json
import tkinter as Tk
from MicroMator.FileManagement import logger

MODULE = 'Complex Protocol GUI'

def launch_gui(options, globaldict, protocol, positions):
    '''launches the GUI
    '''
    root = Tk.Toplevel()
    root.iconbitmap(os.path.join(os.path.normpath(os.path.dirname(os.path.realpath(__file__))), 'm.ico')) #icon location (must be .ico)
    app = Application(options, globaldict, positions, master=root)
    app.mainloop()
    root.destroy()
    time.sleep(1)

def combine_funcs(*funcs):
    '''combines functions
    '''
    def combined_func(*args, **kwargs):
        '''is the combined function
        '''
        for f in funcs:
            f(*args, **kwargs)
    return combined_func

class Application(Tk.Frame):
    '''this class is the GUI for that will let the user choose which pos should be analyzed if not all
    '''
    def __init__(self, options, globaldict, positions, master=None):
        '''Args:
            options(dict): the dict with all the options
            position(list): the list of Position object
        '''
        Tk.Frame.__init__(self, master)
        self.master = master
        self.options = options
        self.globaldict = globaldict
        self.positions = positions
        self.pos_to_analyze = [Tk.BooleanVar() for i, _ in enumerate(self.positions)]
        for pos in self.pos_to_analyze:
            pos.set(True)
        self.pack()
        self.createWidgets()

    def add_pos_to_analyze(self):
        '''this method will save the protocol
        '''
        for index, pos in enumerate(self.pos_to_analyze):
            if pos.get():
                logger('will analyze pos{}'.format(index), module=MODULE)
            else:
                logger('will not analyze pos{}'.format(index), module=MODULE)
                self.positions[index].analyse = False

    def exit_command(self):
        '''exit command
        '''
        self.add_pos_to_analyze()
        self.quitting()
        self.globaldict['COMPLEXE_GUI_DONE'].value = True
        self.master.quit()

    def quitting(self):
        for i in self.pos_to_analyze:
            del i
        self.pos_to_analyze = []

    def createWidgets(self):
        '''this method is where all the widgets and buttons are created
        '''
        self.labelframe = Tk.LabelFrame(self, text="MicroMator Complex Acquisition")
        self.labelframe.grid(row=0)
        for index, position in enumerate(self.positions):
            if position.label:
                self.pos_use = Tk.Checkbutton(self, text='{}'.format(position.label), font="Verdana 10 bold", variable=self.pos_to_analyze[index])
                self.pos_use.grid(row=index, column=0, sticky=Tk.W)
        self.add_complexe_protocol = Tk.Button(self, height=1, width=20, command=self.add_pos_to_analyze, text='Save')
        self.add_complexe_protocol.grid(row=index+1, column=0, sticky=Tk.W)
        self.abort = Tk.Button(self, height=1, width=20, fg='red', command=combine_funcs(self.master.destroy, self.exit_command), text='Save & Quit')
        self.abort.grid(row=index+2, column=0, sticky=Tk.W)


if __name__ == '__main__':
    import MicroMator.DataManagement.position_structure as ps
    import MicroMator.DataManagement.protocol_structure as pros
    import MicroMator.FileManagement.folder_manager as fm
    import MicroMator.FileManagement.file_data_extractor as fde
    root = Tk.Tk()
    options_json_file = 'C:\\Users\\Steven\\Documents\\Work\\StevenFletcher\\test\\multicolorevent_slide\\Protocols\\MicroMator_parameters.json'
    with open(options_json_file) as jsonfile:
        options = json.load(jsonfile)
    folder_manager_obj = fm.Filer(options)
    print(folder_manager_obj.acquisitions_path)
    acqu_data = fde.AcquisitionDataExtractormm2(folder_manager_obj, options)
    positions = ps.AllPositions(folder_manager_obj, options["MICROMANAGER_PARAMETERS"]["mmversion"]).positions
    protocol = pros.Protocol(timestamp=0, acquorder=None, acqNumframes=0, acqInterval=0, channel_list=acqu_data.channel_list, positions_list=None, dmd=False, track=False)

    launch_gui(options, None, protocol, positions)
